#ifndef PBWBC_JOINTVELOCITYLIMITER_ROS_HPP
#define PBWBC_JOINTVELOCITYLIMITER_ROS_HPP
#include <pbwbc/JointVelocityLimiter.hpp>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <pbwbc/ROSTools.hpp>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>

namespace pbwbc {

// TODO:
// * implement JointVelocityLimiter interface
class PBWBC_EXPORT JointVelocityLimiterROS : public JointVelocityLimiter {
public:
    JointVelocityLimiterROS(
            std::shared_ptr<RobotState> const &,
            std::shared_ptr<Model> const &,
            std::shared_ptr<ConfigMap> const &config,
            ros::NodeHandle const & nh
            );
    virtual ~JointVelocityLimiterROS();

    void update(timestamp_t const &);

protected:
    ros::NodeHandle nh_;
    RealTimePublisher<sensor_msgs::JointState> desired_torque_publisher_;
    
    std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr_;
};

} // namespace pbwbc
#endif // PBWBC_JOINTVELOCITYLIMITER_ROS_HPP