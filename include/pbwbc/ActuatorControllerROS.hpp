#ifndef PBWBC_ACTUATORCONTROLLERROS_HPP
#define PBWBC_ACTUATORCONTROLLERROS_HPP
#include <pbwbc/ActuatorController.hpp>
#include <ros/ros.h>
#include <pbwbc/ROSTools.hpp>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>
#include <pbwbc/Float32Stamped.h>
#include <geometry_msgs/WrenchStamped.h>

namespace pbwbc {



class PBWBC_EXPORT ActuatorAdmittanceControllerROS : public ActuatorAdmittanceController {
public:
    ActuatorAdmittanceControllerROS(
            std::string const & joint_name,
            std::shared_ptr<RobotState> const & state,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config,
            std::shared_ptr<SensorState> const & sensor_state,
            std::shared_ptr<ActuatorController> const & underlying_controller,
            ros::NodeHandle const & nh
            );

    virtual ~ActuatorAdmittanceControllerROS();

    virtual void update(timestamp_t const &, double tau_d);
    
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
protected:
    ros::NodeHandle nh_;
    std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr_;
    RealTimePublisher<pbwbc::Float32Stamped> tau_actuator_d_pub_;
    RealTimePublisher<pbwbc::Float32Stamped> tau_projected_pub_;
    RealTimePublisher<geometry_msgs::WrenchStamped> wrench_projected_pub_;
    RealTimePublisher<geometry_msgs::WrenchStamped> wrench_measured_pub_;
};


} // namespace pbwbc
#endif // PBWBC_ACTUATORCONTROLLERROS_HPP
