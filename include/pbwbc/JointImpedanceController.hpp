#ifndef PBWBC_JOINTIMPEDANCECONTROLLER_HPP
#define PBWBC_JOINTIMPEDANCECONTROLLER_HPP
#include <pbwbc/Controller.hpp>
#include <pbwbc/JointTrajectoryGenerator.hpp>
#include <pbwbc/Config.hpp>

namespace pbwbc {

//! \brief A Joint impedance controller
//!
//! Features:
//! - constant stiffness or saturated stiffness with a maximum torque
//! - gravity compensation
//! - damping computation based on inertia matrix diagonal elements
//! 
//!
//! Minimal settings for the yaml file:
//! \code{.yaml}
//! JointImpedanceController:
//!   type: "JointImpedanceController"
//!   impedance_controlled_joint_names: [leg_left_1_joint, leg_left_2_joint, leg_left_3_joint]
//!   nominal_stiffness: 10.
//! \endcode
//!
//! Complete settings:
//! \code{.yaml}
//! JointImpedanceController:
//!   type: "JointImpedanceController"
//!   impedance_controlled_joint_names: [leg_left_1_joint, leg_left_2_joint, leg_left_3_joint]
//!   nominal_stiffness: 0.
//!   gravity_compensation_gain: 0.
//!   stiffness_mode: "saturated" # alternative: linear
//!   stiffness_step_max: 1.
//!   maximum_torque: 100.
//!   integral_gain: 0.
//!   damping_mode: "ratio" # alternative: value or double_diagonalization
//!   joints: # override per joint settings
//!     leg_left_1_joint: {nominal_stiffness: 300.0,   maximum_torque: 100.0, damping_ratio: 0.7}
//!     leg_left_1_joint: {nominal_stiffness: 300.0,   maximum_torque: 100.0, damping_ratio: 0.7}
//!     leg_left_1_joint: {nominal_stiffness: 100.0,   maximum_torque: 100.0, damping_ratio: 0.7}
//! \endcode
class PBWBC_EXPORT JointImpedanceController : public Controller {
public:
    JointImpedanceController(
            std::shared_ptr<RobotState> const &,
            std::shared_ptr<Model> const &,
            std::shared_ptr<ConfigMap> const &config
            );
    virtual ~JointImpedanceController();

    virtual void start(timestamp_t const &);
    virtual void stop(timestamp_t const &);

    virtual void update(timestamp_t const &);

    std::vector<std::string> motionEntities();
    void startMotion(timestamp_t const & timestamp, std::string const &, Eigen::Ref<Eigen::VectorXd> const & goal, double const duration, bool absolute);
    bool isMotionRunning(timestamp_t const &, std::string const & );
    void stopMotion(timestamp_t const &, std::string const &);
    Eigen::VectorXd const & getMotionState(std::string const & entity);

    bool hasTaskJacobian() const;
    Eigen::MatrixXd const & getTaskJacobian() const;

    enum stiffness_mode_t {
        stiffness_linear = 0,
        stiffness_saturated = 1,
    };

    enum damping_mode_t {
        damping_value = 0,
        damping_ratio = 1,
        damping_double_diagonalization = 2
    };
protected:
    std::shared_ptr<RobotState> state_;
    std::shared_ptr<Model> model_;
    std::shared_ptr<ConfigMap> config_;

    stiffness_mode_t stiffness_mode_;
    damping_mode_t damping_mode_;

    // copies of model values to allow override
    typedef Eigen::VectorXd vector_t;
    vector_t position_min_;
    vector_t position_max_;
        
    vector_t maximum_torque_;

    double gravity_compensation_gain_;
    double feedforward_gain_;
    double stiffness_step_max_;

    vector_t nominal_stiffness_desired_;
    vector_t nominal_stiffness_;
    vector_t damping_value_;
    vector_t damping_ratio_;
    vector_t integral_gain_;
    vector_t integral_windup_;

    vector_t local_stiffness_;
    vector_t integrator_state_;
    Eigen::GeneralizedSelfAdjointEigenSolver<Eigen::MatrixXd> eigensolver_;

    RobotState state_d_; //<! desired state used for control
    // this probably needs a lock

    std::vector<std::string> impedance_controlled_joint_names_;
    std::vector<int> impedance_controlled_joints_ids_;
    std::map< std::string, int> jointID_map_;


    Eigen::MatrixXd task_jacobian_;

    int q_offset_;
    int dq_offset_;

    std::shared_ptr<JointTrajectoryGenerator> trajectory_generator_;
    RobotState goal_state_;

    int update_pose_manually_;
};

} // namespace pbwbc
#endif // PBWBC_JOINTIMPEDANCECONTROLLER_HPP
