#ifndef PBWBC_MODEL_HPP
#define PBWBC_MODEL_HPP
#include <pbwbc/Base.hpp>
#include <pbwbc/State.hpp>

namespace pbwbc {


// wraps RBDL or pinocchio models
//
// can be projected into COM space
class PBWBC_EXPORT Model {
public:
    Model();
    virtual ~Model();

    typedef Eigen::Ref<Eigen::VectorXd> configuration_t;

    virtual void update(timestamp_t const &) = 0;

    virtual int y_size() = 0;
    virtual int dy_size() = 0;
    virtual int q_size() = 0;
    virtual int dq_size() = 0;
    virtual int contact_size() = 0;

    typedef Eigen::Vector3d translation_t;
    virtual translation_t bodyTranslation(int body_id) = 0;
    typedef Eigen::Quaterniond orientation_t;
    virtual orientation_t bodyOrientation(int body_id) = 0;
    virtual void getJacobian(int body_id, Eigen::Ref<Eigen::MatrixXd> jac ) = 0;
    virtual void getJacobianWorld(int body_id, Eigen::Ref<Eigen::MatrixXd> jac ) = 0;

    typedef Eigen::MatrixXd matrix_t;
    typedef Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> rowmatrix_t;
    typedef Eigen::VectorXd vector_t;
    virtual matrix_t const & MassMatrix() = 0;
    virtual matrix_t const & Coriolis() = 0;
    virtual vector_t const & gravity() = 0;
    
    virtual rowmatrix_t const & InvMassMatrix() = 0;

    virtual double mass() = 0;
    //! \brief get com position in world frame
    virtual translation_t com() = 0;
    virtual translation_t dcom() = 0;
    virtual Eigen::MatrixXd const & comJacobian() = 0;

    // return the names of actual joints (not including base)
    virtual std::vector<std::string> const & jointNames() = 0;
    virtual vector_t const & position_max() = 0;
    virtual vector_t const & position_min() = 0;
    virtual vector_t const & velocity_max() = 0;
    virtual vector_t const & torque_max() = 0;
    
    virtual void setState(std::shared_ptr<RobotState> const &) = 0;

    virtual void setBasePosition(
        translation_t const & translation,
        orientation_t const & orientation,
        RobotState & state) = 0;
    virtual void setBaseVelocity(
        translation_t const & translation,
        translation_t const & orientation,
        RobotState & state) = 0;

    virtual int getFrameId(std::string const & frame_name) = 0;
    virtual std::string const & getFrameName(int frameId) = 0;
    virtual std::vector<int> getJointFrames() = 0;
    virtual std::vector<int> getLinkFrames() = 0;

    virtual Eigen::Ref<const Eigen::VectorXd> const stripBasePosition(
            Eigen::Ref<const Eigen::VectorXd> const & unfiltered) = 0;
    virtual Eigen::Ref<const Eigen::VectorXd> const stripBaseVelocity(
            Eigen::Ref<const Eigen::VectorXd> const & unfiltered) = 0;
};
	


} // namespace pbwbc
#endif // PBWBC_MODEL_HPP
