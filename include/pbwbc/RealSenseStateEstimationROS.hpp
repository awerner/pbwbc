#ifndef PBWBC_REALSENSESTATEESTIMATIONROS_HPP
#define PBWBC_REALSENSESTATEESTIMATIONROS_HPP
#include <pbwbc/RealSenseStateEstimation.hpp>
#include <pbwbc/ROSTools.hpp>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <std_msgs/Float32.h>

namespace pbwbc {

//! \brief ROS Wrapper for RealSenseStateEstimation
//! 
//! Publishes state and camera states. Exposes some parameters via dynamic_reconfigure.
//! Publishes to base_link pose via tf using RealTimeTransformBroadcaster
class PBWBC_EXPORT RealSenseStateEstimationROS : public RealSenseStateEstimation {
public:

    RealSenseStateEstimationROS(
            std::shared_ptr<ConfigMap> const & config,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<SensorState> const & sensor_state,
            ros::NodeHandle const & nh
            );

    virtual ~RealSenseStateEstimationROS();

    virtual void start(timestamp_t const &);
    //virtual void stop(timestamp_t const &) {}

    virtual void update(timestamp_t const &);

protected:
    ros::NodeHandle nh_;
    RealTimePublisher<geometry_msgs::PoseStamped> state_pose_pub_;
    RealTimePublisher<geometry_msgs::TwistStamped> state_twist_pub_;
    std::vector<RealTimePublisher<geometry_msgs::PoseStamped>> camera_pose_pub_;
    std::vector<RealTimePublisher<geometry_msgs::TwistStamped>> camera_twist_pub_;
    std::vector<RealTimePublisher<geometry_msgs::TwistStamped>> camera_accel_pub_;
    std::vector<RealTimePublisher<std_msgs::Float32>> camera_confidence_pub_;
    std::vector<RealTimePublisher<geometry_msgs::PoseStamped>> camera_pose_error_pub_;
    std::vector<RealTimePublisher<std_msgs::Float32>> camera_active_pub_;
    std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr_;
    std::shared_ptr<RealTimeTransformBroadcaster> tf_broadcaster_;

    RealTimePublisher<geometry_msgs::PoseStamped> imu_pose_pub_;
    RealTimePublisher<geometry_msgs::TwistStamped> imu_velocity_pub_;
    RealTimePublisher<geometry_msgs::TwistStamped> imu_acceleration_pub_;
    int publisher_subsampling_;
    int publisher_subsampling_counter_;
    enum frame_enum {
        world = 0,
        sensor = 1,
        world_position_body_velocity = 2
    };
    int frame_;
};

} // namespace

#endif // PBWBC_REALSENSESTATEESTIMATIONROS_HPP
