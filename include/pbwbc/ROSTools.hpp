#ifndef LIBPBWBC_ROSTOOLS_HPP
#define LIBPBWBC_ROSTOOLS_HPP
#include <pbwbc/Base.hpp>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <boost/variant.hpp>
#include <thread>
#include <mutex>
#include <atomic>
#include <regex>
#include <condition_variable>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Float64MultiArray.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_msgs/TFMessage.h>
#include <geometry_msgs/TransformStamped.h>
#include <pbwbc/State.hpp>
#include <pbwbc/Model.hpp>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>

namespace pbwbc {



//! \brief A ring buffer implementation for one producer and two consumer threads. 
//!
//! The producer fills the buffer by using one of two methods:
//! * produce(T const & m)
//! * writeBuffer() and advanceWriteBuffer()
//!
//! The first consumer uses ready() to detect if data
//! is available and consume() to obtain one data item.
//! When the consumer fails to keep the buffer from overflowing,
//! this is reported during destruction of the RingBuffer.
//! If the buffer is full according to this consumer,
//! no data is write (TODO: is this meaningful)
//!
//! The second consumer uses ready_alternate() and
//! consume_alternate(). In contrast to the first
//! consumer, if the buffer reaches full, the read
//! iterator associated with this consumer is automatically
//! advanced.
//!
//! TODO: check if this is actually thread safe
template <typename T>
class RingBuffer {
public:
    RingBuffer(int size)
        : data_(size),
          read_iterator_(data_.begin()),
          read_alt_iterator_(data_.begin()),
          write_iterator_(data_.begin()),
          buffer_full_on_write_(0) {
            if(size<2) {
                throw std::runtime_error("Need larger buffer size than 2");
            }
          }

    RingBuffer(int size, T const & m)
        : data_(size,m),
          read_iterator_(data_.begin()),
          read_alt_iterator_(data_.begin()),
          write_iterator_(data_.begin()),
          buffer_full_on_write_(0) {
            if(size<2) {
                throw std::runtime_error("Need larger buffer size than 2");
            }
          }

    RingBuffer(RingBuffer && m)
        : data_(m.data_),
          read_iterator_(data_.begin()+std::distance(m.data_.begin(),m.read_iterator_.load())),
          read_alt_iterator_(data_.begin()+std::distance(m.data_.begin(),m.read_alt_iterator_.load())),
          write_iterator_(data_.begin()+std::distance(m.data_.begin(),m.write_iterator_.load())),
          buffer_full_on_write_(m.buffer_full_on_write_) {}

    virtual ~RingBuffer() {
        if(buffer_full_on_write_) {
            PBWBC_DEBUG("buffer_full_on_write_ = "
                    << buffer_full_on_write_);
        }
    }

    //! consume for first read thread
    void consume(T & m, ros::Time & ts) {
        iterator_t curr_read = read_iterator_.load();
        if( curr_read==write_iterator_.load() ) {
            throw std::runtime_error("buffer empty");
        }
        ts = std::get<0>(*curr_read);
        m = std::get<1>(*curr_read);
        iterator_t next_read = curr_read+1;
        if(next_read==data_.end()) {
            next_read = data_.begin();
        }
        read_iterator_.compare_exchange_weak(curr_read,next_read);
    }
    
    bool ready () {
        return read_iterator_.load()!=write_iterator_.load();
    }

    //! consume for second read thread
    void consume_alternate(T & m, ros::Time & ts) {
        iterator_t read = read_alt_iterator_.load();
        if( read==write_iterator_.load() ) {
            throw std::runtime_error("buffer empty");
        }
        ts = std::get<0>(*read);
        m = std::get<1>(*read);
        read++;
        if(read==data_.end()) {
            read = data_.begin();
        }
        read_alt_iterator_.store(read);
    }

    bool ready_alternate() {
        return read_alt_iterator_.load()!=write_iterator_.load();
    }

    void produce(T const & m, ros::Time ts = ros::Time::now()) {
        // do not write if buffer full
        iterator_t curr_write = write_iterator_.load();
        iterator_t next_write = curr_write+1;
        if(next_write==data_.end()) {
            next_write = data_.begin();
        }
        if(next_write==read_iterator_.load()) {
            buffer_full_on_write_++;
        }

        // move alternate read iterator forward
        iterator_t curr_read_alt = read_alt_iterator_.load();
        if(next_write==curr_read_alt) {
            iterator_t next_alt_read = curr_read_alt+1;
            if(next_alt_read == data_.end()) {
                next_alt_read = data_.begin();
            }
            // this overrides iterator modifications with compare_exchange_weak in consume
            read_alt_iterator_.store(next_alt_read);
        }

        *curr_write = std::make_tuple(ts,m);
        write_iterator_.compare_exchange_weak(curr_write,next_write);
    }

    std::tuple<ros::Time,T> & writeBuffer() {
        return *write_iterator_.load();
    }
    
    void advanceWriteBuffer() {
        // do not write if buffer full
        iterator_t curr_write = write_iterator_.load();
        iterator_t next_write = curr_write+1;
        if(next_write==data_.end()) {
            next_write = data_.begin();
        }
        if(next_write==read_iterator_.load()) {
            buffer_full_on_write_++;
        }
        // move alternate read iterator forward
        iterator_t curr_read_alt = read_alt_iterator_.load();
        if(next_write==curr_read_alt) {
            iterator_t next_alt_read = curr_read_alt+1;
            if(next_alt_read == data_.end()) {
                next_alt_read = data_.begin();
            }
            // this overrides iterator modifications with compare_exchange_weak in consume
            read_alt_iterator_.store(next_alt_read);
        }
        write_iterator_.compare_exchange_weak(curr_write,next_write);
    }

    void fill(T const & m) {
        std::fill(data_.begin(),data_.end(),
                std::make_tuple(ros::Time::now(),m));
    }

    void clear() {
        read_iterator_.store(data_.begin());
        read_alt_iterator_.store(data_.begin());
        write_iterator_.store(data_.begin());
    }

    int size() {
        return data_.size();
    }

    void resize(int size) {
        //1. allocate new buffer space, copying preinitialized values
        try {
            std::vector<data_tuple_t> new_data(size,data_.at(0));
            //2. reset iterators to next buffer
            //TODO: not entirely sure if the next four statements need to be executed in an atomic way
            read_iterator_.store(new_data.begin());
            read_alt_iterator_.store(new_data.begin());
            write_iterator_.store(new_data.begin());
            //3. swap buffers
            data_.swap(new_data);
        } catch ( std::bad_alloc const & ex ) {
            PBWBC_DEBUG("buffer could not be resized (bad_alloc): " << ex.what());
        }
    }

    //! \brief return statistics about the buffer
    //!
    //! Returns
    //! - size
    //! - position of write iterator
    //! - position of read iterator
    //! - position of read_alternate iterator
    std::tuple<int,int,int,int> stats() {
        return std::make_tuple(
                data_.size(),
                std::distance(data_.begin(),write_iterator_.load()),
                std::distance(data_.begin(),read_iterator_.load()),
                std::distance(data_.begin(),read_alt_iterator_.load()));
    }

protected:
    typedef std::tuple<ros::Time,T> data_tuple_t;
    std::vector<data_tuple_t> data_;
    typedef typename std::vector<data_tuple_t>::iterator iterator_t;
    std::atomic<iterator_t> read_iterator_;
    std::atomic<iterator_t> read_alt_iterator_;
    std::atomic<iterator_t> write_iterator_;
    std::size_t buffer_full_on_write_;
};

template <typename T>
class RealTimePublisher;

// TODO:
// Rewrite this so RealTimePublisher is the owner of the message buffer which
// is registered with TypeStorage on creation. Have one mutex per Publisher.
// Keep the same API otherwise.

//! \brief RealTimePublisherBackEnd is the central part of a logging and publishing
//! system. 
//!
//! A RealTimePublisher object (substitute for ros::Publisher or realtime_tools::RealTimePublisher) can be
//! obtained for a topic with advertise(). To trigger sending all messages
//! in a background thread call trigger_send(). The publisher does commonly
//! only send out a certain amount of packages. To obtain all data samples,
//! data is collected in a ring buffer for each topic. The contents of this ring buffer can be
//! written to a file by calling trigger_write_to_file().
//!
//! realtime_tool::RealTimePublisher creates one thread per topic.
//! This could be a bit too many context switches when publishing ~100 topics,
//! so this implementation has only one background thread.
//!
class PBWBC_EXPORT RealTimePublisherBackEnd {
public:
    RealTimePublisherBackEnd();
    ~RealTimePublisherBackEnd();
    
    //! get a publisher for a given topic, use this instead of ros::NodeHandle::advertise
    //!
    //! \param publish_divisor 0=don't publish, 1=full rate, 2=half rate ...
    template <typename T>
    static RealTimePublisher<T> advertise(
            ros::NodeHandle & nh,
            std::string const & topic,
            int buffer_size,
            int publish_divisor=1,
            int ros_queue_size=1) {
        // get singleton
        TypeStorage<T> & s = RealTimePublisherBackEnd::get_storage<T>();
        // aquire lock
        std::unique_lock<std::mutex> lock(s.mutex_);
        // add new publisher
        // TODO: delete this item on it's destruction,
        // making this restart safe
        s.data.push_back(
                typename TypeStorage<T>::pub_info_t(nh.advertise<T>(topic,ros_queue_size),
                           buffer_size,
                           publish_divisor));
        int idx = s.data.size() - 1;
        return RealTimePublisher<T>(idx,topic);
    }

    //! \brief call this to trigger sending all messages,
    //         sending happens in a background thread
    bool trigger_send();
    //! \brief call this to write the messages in all ring buffers to a logfile
    //!
    //! Writing to file happens in a background thread
    bool trigger_write_to_file(std::string const & filename,
            std::string const & topic_filter = std::string(".*"));
    //! \brief call this to write the messages to a rosbag file
    //!
    //!  Writing to a file happens in this thread
    bool write_to_file(std::string const & filename,
            std::string const & topic_filter = std::string(".*"));

protected:
    std::mutex mutex_;
    std::condition_variable cv_;
    std::condition_variable file_cv_;
    std::condition_variable file_finished_cv_;
    bool keep_running_;
    bool run_;
    bool file_run_;
    std::string rosbag_filename_;
    std::string rosbag_topic_filter_;
    std::thread thread_;
    std::thread file_thread_;


    //! This variable stores callbacks for all publisher types
    static std::vector<std::function<void()>> storage_send_;
    static std::vector<std::function<int(rosbag::Bag &, std::regex const &)>> storage_file_;
    
    void thread_function();
    void file_thread_function();
    
    //! \brief This class handles all publishing for one message type
    template <typename T>
    class TypeStorage {
        public:
        TypeStorage()  {
            RealTimePublisherBackEnd::storage_send_.push_back(
                    [this](){
                       std::unique_lock<std::mutex> lock(this->mutex_);
                       ros::Time ts;
                       T m;
                       for(auto & pub : data) {
                           while(pub.buffer.ready()) {
                               pub.buffer.consume(m,ts);
                               if(pub.publisher_divisor!=0 && pub.publisher_divisor_counter==0) {
                                   pub.publisher.publish(m);
                               }
                               pub.publisher_divisor_counter++;
                               if(pub.publisher_divisor_counter==pub.publisher_divisor) {
                                   pub.publisher_divisor_counter = 0;
                               }
                           }
                       }
                    });
            RealTimePublisherBackEnd::storage_file_.push_back(
                    [this](rosbag::Bag & bag, std::regex const & topic_filter) -> int {
                       std::unique_lock<std::mutex> lock(this->mutex_);
                       ros::Time ts;
                       T m;
                       int messages = 0;
                       for(auto & pub : data) {
                           std::string topic = pub.publisher.getTopic();
                           if(!std::regex_match(topic,topic_filter)) {
                               continue;
                           }
                           //int topic_messages = 0;
                           //auto stats = pub.buffer.stats();
                           while(pub.buffer.ready_alternate()) {
                               pub.buffer.consume_alternate(m,ts);
                               bag.write(topic,ts,m);
                               messages++;
                               //topic_messages++;
                           }
                           //PBWBC_DEBUG("Saving topic: " << topic << " msgs: " << topic_messages <<
                           //        " stats: "
                           //        << std::get<0>(stats) << " "
                           //        << std::get<1>(stats) << " "
                           //        << std::get<2>(stats) << " "
                           //        << std::get<3>(stats));
                                   
                       }
                       return messages;
                    });
        };
        class pub_info_t {
        public:
            pub_info_t( ros::Publisher const & _publisher, int _buffer_size, int _publisher_divisor)
                : publisher(_publisher),
                  buffer(_buffer_size),
                  publisher_divisor(_publisher_divisor),
                  publisher_divisor_counter(0) {};

            ros::Publisher publisher;
            RingBuffer<T> buffer;
            int publisher_divisor;
            int publisher_divisor_counter;
        };
        std::mutex mutex_;
        std::vector<pub_info_t> data;

        
    };
    
    // Singleton for the message type
    template <typename T>
    static TypeStorage<T> & get_storage() {
        static TypeStorage<T> storage;
        return storage;
    }

    template <typename T>
    friend class RealTimePublisher;
};


//! \brief A real-time safe substitute for ros::Publisher for use with
//! RealTimePublisherBackEnd.
template <typename T>
class RealTimePublisher {
    public:
        //! Real-time safe function to publish a message
        void publish(T const & msg) {
            if(idx_==-1) {
                throw std::runtime_error("Publishing on uninitialized publisher");
            }
            // get singleton
            RealTimePublisherBackEnd::TypeStorage<T> & s = RealTimePublisherBackEnd::get_storage<T>();
            // acquire lock
            if(!s.mutex_.try_lock())return;
            // add message to buffer
            // copy message to reuse existing variable length buffers
            // TODO: std::move
            s.data.at(idx_).buffer.produce(msg);
            s.mutex_.unlock();
        }

        // TODO: This currently locks this for all topics of the same type
        bool trylock() {
            if(idx_==-1) {
                throw std::runtime_error("Publishing on uninitialized publisher");
            }
            // get singleton
            RealTimePublisherBackEnd::TypeStorage<T> & s = RealTimePublisherBackEnd::get_storage<T>();
            // aquire lock
            return s.mutex_.try_lock();
        }

        void unlockAndPublish(ros::Time ts = ros::Time::now()) {
            if(idx_==-1) {
                throw std::runtime_error("Publishing on uninitialized publisher");
            }
            // get singleton
            RealTimePublisherBackEnd::TypeStorage<T> & s = RealTimePublisherBackEnd::get_storage<T>();
            std::get<0>(s.data.at(idx_).buffer.writeBuffer()) = ts;
            s.data.at(idx_).buffer.advanceWriteBuffer();
            s.mutex_.unlock();
        }

        //! Accessor for the current message buffer. Avoids the copy that happends when using
        //! publish()
        T & msg () {
            if(idx_==-1) {
                throw std::runtime_error("Publishing on uninitialized publisher");
            }
            // get singleton
            RealTimePublisherBackEnd::TypeStorage<T> & s = RealTimePublisherBackEnd::get_storage<T>();
            return std::get<1>(s.data.at(idx_).buffer.writeBuffer());
        }

        //! copy current write buffer to all other buffers
        //! use msg() to write to current buffer
        void fillBuffer() {
            RealTimePublisherBackEnd::TypeStorage<T> & s = RealTimePublisherBackEnd::get_storage<T>();
            T m = std::get<1>(s.data.at(idx_).buffer.writeBuffer());
            s.data.at(idx_).buffer.fill(m);
        }

        RealTimePublisher(RealTimePublisher const & m) : 
            idx_(m.idx_),
            topic_(m.topic_),
            lost_messages_(m.lost_messages_) {};
        RealTimePublisher(int idx, std::string const & topic) : 
            idx_(idx),
            topic_(topic),
            lost_messages_(0) {};
        RealTimePublisher() : idx_(-1), lost_messages_(0) {};

        ~RealTimePublisher() {
            if(lost_messages_!=0) {
                PBWBC_DEBUG("Lost " << lost_messages_ << " messages on topic "
                        << topic_);
            }
        };

        int buffer_size() {
            RealTimePublisherBackEnd::TypeStorage<T> & s = RealTimePublisherBackEnd::get_storage<T>();
            return s.data.at(idx_).buffer.size();
        }

        void set_buffer_size(int buffer_size) {
            PBWBC_DEBUG("Setting buffer size for topic " << topic_ << " to " << buffer_size);
            RealTimePublisherBackEnd::TypeStorage<T> & s = RealTimePublisherBackEnd::get_storage<T>();
            s.data.at(idx_).buffer.resize(buffer_size);
        }

        int publisher_divisor() {
            RealTimePublisherBackEnd::TypeStorage<T> & s = RealTimePublisherBackEnd::get_storage<T>();
            return s.data.at(idx_).publisher_divisor;
        }
        
        void set_publish_divisor(int publisher_divisor) {
            PBWBC_DEBUG("Setting publisher divisor for topic " << topic_ << " to " << publisher_divisor);
            RealTimePublisherBackEnd::TypeStorage<T> & s = RealTimePublisherBackEnd::get_storage<T>();
            s.data.at(idx_).publisher_divisor = publisher_divisor;
        }
    
        
        void register_ddynamic_reconfigure_options(ddynamic_reconfigure::DDynamicReconfigure & ddr) {
            // TODO: if this class is detroyed and ddynamic_reconfigure calls this, bad things happen
            ddr.registerVariable<int>(topic_+"_publisher_divisor",publisher_divisor(),
                boost::bind(&RealTimePublisher<T>::set_publish_divisor,this,
                    _1),"",1,100);
            ddr.registerVariable<int>(topic_+"_buffer_size",buffer_size(),
                boost::bind(&RealTimePublisher<T>::set_buffer_size,this,
                    _1),"",2,100000);
        }
    protected:
        int idx_;
        std::string topic_;
        std::size_t lost_messages_;
};

//! \brief Real-time wrapper for tf::TransformBroadcaster
//! 
//! Uses one thread per instance.
class RealTimeTransformBroadcaster {
public:
    RealTimeTransformBroadcaster(ros::NodeHandle const & nh,
            std::string const & origin_name,
            std::string const & base_frame_name,
            std::shared_ptr<Model> const & model);
    virtual ~RealTimeTransformBroadcaster();

    //! \brief Publish data of a single frame to tf (currently the base frame)
    void publish(
            timestamp_t const & timestamp,
            Eigen::Ref<const Eigen::VectorXd> const & translation,
            Eigen::Quaterniond const & orientation);

    //! \brief Publish data of all link frames in the model to tf
    void setBasePosition(Eigen::Ref<const Eigen::VectorXd> const & translation,
                      Eigen::Quaterniond const & orientation);
    
    void setJointPositions(RobotState const & state);

    void publish(timestamp_t const & timestamp);

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
protected:
    ros::NodeHandle nh_;
    std::string origin_name_;
    std::string base_frame_name_;
    std::shared_ptr<Model> model_;
    tf2_ros::TransformBroadcaster tf_broadcaster_;
    RealTimePublisher<tf2_msgs::TFMessage> logging_publisher_;

    bool keep_running_;
    bool run_;
    std::thread thread_;
    std::mutex mutex_;
    std::condition_variable cv_;
   
    timestamp_t timestamp_; 
    Eigen::Vector3d translation_;
    Eigen::Quaterniond orientation_;
    std::map<int,std::tuple<std::string,Eigen::Vector3d,Eigen::Quaterniond>> tf_map_;

    void thread_function();
};

void assign(std_msgs::Float32MultiArray & msg,
        Eigen::Ref<const Eigen::MatrixXd> const m);

void assign(std_msgs::Float64MultiArray & msg,
        Eigen::Ref<const Eigen::MatrixXd> const m);

void assign(Eigen::MatrixXf & m,
        std_msgs::Float32MultiArray const & msg);

void assign(Eigen::MatrixXd & m,
        std_msgs::Float64MultiArray const & msg);

void assign(Eigen::VectorXf & m,
        std_msgs::Float32MultiArray const & msg);

void assign(Eigen::VectorXd & m,
        std_msgs::Float64MultiArray const & msg);

void assign(geometry_msgs::Transform & msg, 
        const Eigen::Ref<const Eigen::VectorXd> & translation,
        Eigen::Quaterniond orientation = Eigen::Quaterniond::Identity());

void assign(geometry_msgs::Twist & msg, 
        const Eigen::Ref<const Eigen::VectorXd> translation,
        const Eigen::Ref<const Eigen::VectorXd> orientation);

void assign(
        CartesianState::position_state & state,
        geometry_msgs::Transform const & msg);

void assign(
        geometry_msgs::Wrench & msg,
        const Eigen::Ref<const Eigen::VectorXd> & wrench);

void assign(
        CartesianState::position_state & state,
        geometry_msgs::Pose const & msg);

void assign(
        CartesianState::velocity_state & state,
        geometry_msgs::Twist const & msg);

void assign(
       Eigen::Ref<Eigen::VectorXd> wrench,
       geometry_msgs::Wrench const & msg);


} // namespace

#endif // LIBPBWBC_ROSTOOLS_HPP
