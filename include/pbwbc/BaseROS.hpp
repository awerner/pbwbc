#ifndef PBWBC_BASEROS_HPP
#define PBWBC_BASEROS_HPP
#include <Base.hpp>

namespace pbwbc {

class PBWBC_EXPORT ConfigMapROS : public ConfigMap {
public:
    ConfigMapROS(ros_node_handle const & nh);
    virtual ~ConfigMapROS();

    template <typename T>
    virtual void param(std::string const & parameter_name, T & variable, T const & default_value ) {
        nh_.param(parameter_name,variable,default_value);
    }

    template <typename T>
    virtual bool getParam(std::string const & parameter_name, T & variable) {
        return nh_.param(parameter_name,variable);
    }

    virtual ConfigMap submap(std::string const & parameter_name){
        std::runtime_error("Not implemented");
    };
};

} // namespace pbwbc

#endif // PBWBC_BASEROS_HPP
