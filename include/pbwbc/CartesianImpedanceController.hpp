#ifndef PBWBC_CARTESIANIMPEDANCECONTROLLER_HPP
#define PBWBC_CARTESIANIMPEDANCECONTROLLER_HPP
#include <pbwbc/Controller.hpp>
#include <pbwbc/Model.hpp>
#include <pbwbc/Base.hpp>
#include <pbwbc/TripleBuffer.hpp>
#include <pbwbc/CartesianTrajectoryGenerator.hpp>


namespace pbwbc {

//! \brief Implementation of a Cartesian impedance controller
//!    
//! TODO: based on
//! 
//! Features:
//! - Model based damping computation computed in background thread
//! - Quaternion based
//! - Integrated minimum jerk interpolator (CartesianTrajectoryGenerator)
//! - Stiffness interpolation for time-variant stiffnesses
//! - Feedforward terms
//! - MotionOrchestrator interface
//!
//! Minimal yaml configuration 
//! \code{.yaml}
//! CartesianImpedanceController:
//!    type: "CartesianImpedanceController"
//!    frame_name: right_sole_link
//!    stiffness: [ 400., 400., 500., 20.0, 20.0, 20.0 ]
//!    damping_ratios: [ 0.7, 0.7, 0.7, 0.7, 0.7, 0.7 ]
//!    damping_mode: 2 # manual = 0, diagonal_with_ratio = 1, double_diagonalization = 2
//! \endcode
//!
//! 
//! Complete yaml configuration 
//! \code{.yaml}
//! CartesianImpedanceController:
//!    type: "CartesianImpedanceController"
//!    frame_name: right_sole_link
//!    stiffness: [ 400., 400., 500., 20.0, 20.0, 20.0 ]
//!    damping: [ 10., 10., 10., 0.1, 0.1, 0.1 ]
//!    damping_ratios: [ 0.7, 0.7, 0.7, 0.7, 0.7, 0.7 ]
//!    damping_mode: 2 # manual = 0, diagonal_with_ratio = 1, double_diagonalization = 2
//!    feedforward_gain: 1. # use 100% of feed forward terms
//!    stiffness_step_max: 1. # mean 1 N/m or 1Nm/rad stiffness change per iteration
//! \endcode
//!
//! The controller is used in MultiContactForceDistribution to control the position
//! of non-active contacts.
class PBWBC_EXPORT CartesianImpedanceController : public Controller {
public:
    CartesianImpedanceController(
            std::shared_ptr<RobotState> const &,
            std::shared_ptr<Model> const &,
            std::shared_ptr<ConfigMap> const & //std::string const & parameter_ns
            );
    CartesianImpedanceController(CartesianImpedanceController const & m);
    virtual ~CartesianImpedanceController();

    virtual void start(timestamp_t const &);
    virtual void stop(timestamp_t const &);

    void set_goal(
            Eigen::Ref<Eigen::Vector3d> const & translation,
            Eigen::Quaterniond const & orientation,
            bool hard=false);

    Eigen::Vector3d get_translation_goal();
    Eigen::Quaterniond get_orientation_goal();
    
    Eigen::Vector3d get_translation_state();
    Eigen::Quaterniond get_orientation_state();

    // hard = false will use gain interpolation
    void set_stiffness(Eigen::Ref<Eigen::MatrixXd> stiffness, bool hard=false);

    enum stiffness_mode_t {
        stiffness_linear = 0,
        stiffness_saturated = 1,
    };
    enum damping_mode_enum {
        damping_mode_manual = 0,
        damping_mode_diagonal_with_ratio = 1,
        damping_mode_double_diagonalization = 2
    };
    typedef damping_mode_enum damping_mode_t;
    void set_damping_mode(damping_mode_t damping_mode);
    
    void set_damping(Eigen::Ref<Eigen::MatrixXd> damping, bool hard=false);
    void set_damping_ratio(Eigen::Ref<Eigen::VectorXd> damping_ratios, bool hard=false);

    std::vector<std::string> motionEntities();
    void startMotion(timestamp_t const & timestamp, std::string const &, Eigen::Ref<Eigen::VectorXd> const & goal, double const duration, bool absolute);
    bool isMotionRunning(timestamp_t const &, std::string const & );
    void stopMotion(timestamp_t const &, std::string const &);
    Eigen::VectorXd const & getMotionState(std::string const & entity);

    virtual void update(timestamp_t const &);
    
    bool hasTaskForces();
    wrench_t const & getTaskForces();

    bool hasTaskJacobian();
    Eigen::MatrixXd const & getTaskJacobian() const;

    int getFrameId() {return frame_id_;}

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef Eigen::Matrix<double,6,6> damping_t;
protected:
    damping_t compute_damping_with_double_diagonalization();
    std::shared_ptr<RobotState> state_;
    std::shared_ptr<Model> model_;
    std::shared_ptr<ConfigMap> config_;
    int stiffness_mode_;
    double maximum_force_; //!< maximum force applied in satured stiffness mode
    double maximum_torque_; //!< maximum torque applied in satured stiffness mode
    int damping_mode_;
    int frame_id_;

    enum {
        damping_computed_locally = 0,
        damping_computed_by_model_thread = 1
    } damping_update_;

    CartesianState cartesian_state_; //this is the actual current pose
    CartesianState goal_state_; //desired end pose
    CartesianState state_d_; //desired pose for current iteration
    CartesianState state_initial_; //pose at start

    //TODO: the following are for testing purposes, while there is no higher entity setting the goal_state_
    Eigen::Vector3d state_translation_offset_d_; //user-defined translation offset
    Eigen::Vector3d state_orientation_offset_euler_d_; //user-defined orientation offset
    int update_pose_; // user-defined switch to trigger the update of goal_state_
    double stiffness_step_max_;

    typedef Eigen::DiagonalMatrix<double,6> diag_t;
    diag_t K_desired_;
    diag_t K_;
    TripleBuffer<diag_t> K_local_;
    diag_t D_;
    diag_t xi_;
    double feedforward_gain_;

    wrench_t wrench_;
    TripleBuffer<Eigen::MatrixXd> jacobian_;
    TripleBuffer<Eigen::Matrix<double,6,6>> projected_inertia_;
   
    int model_compute_task_id_; 
    TripleBuffer<damping_t> D_model_compute_output_;
    void compute_model_callback();

    std::shared_ptr<CartesianTrajectoryGenerator> trajectory_generator_;

    Eigen::Matrix<double, 6, 1> error_;
    Eigen::Matrix<double, 6, 1> derror_;
    Eigen::Matrix<double, 6, 1> acceleration_;
    Eigen::Matrix<double, 6, 1> W_V_T_;
    Eigen::Matrix<double, 6, 1> W_V_Td_;
    Eigen::Matrix<double, 6, 1> B_V_T_;
};


//class CartesianImpedanceComController : public CartesianImpedanceController {
//    // TODO: adopt impedance controller to control the COM translation
//};


} // namespace pbwbc
#endif // PBWBC_CARTESIANIMPEDANCECONTROLLER_HPP
