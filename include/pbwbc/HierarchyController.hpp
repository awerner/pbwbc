#ifndef PBWBC_HIERARCHYCONTROLLER_HPP
#define PBWBC_HIERARCHYCONTROLLER_HPP
#include <pbwbc/Controller.hpp>
#include <pbwbc/Model.hpp>

namespace pbwbc {

//! \brief Implements a strict hierachy based on Nullspace projectors.
//!
//! Controllers which implement the APIs:
//! - Controller::hasTaskJacobian()
//! - Controller::getTaskJacobian()
//! can be used to construct this controller.
//!
//! Based on:
//! - Dietrich, Alexander, Christian Ott, and Alin Albu-Sch�ffer. "An overview of null space projections for redundant, torque-controlled robots." IJRR 2015
//! 
//! Options:
//! - dynamically consistent nullspace projector (TODO)
//!
//! TODO:
//! - computation time is too long
class PBWBC_EXPORT HierarchyController : public Controller {
public:
    HierarchyController(
        std::shared_ptr<Model> const & model,
        std::vector<std::shared_ptr<Controller>> const & controllers);

    virtual ~HierarchyController();

    virtual void start(timestamp_t const &);
    virtual void stop(timestamp_t const &);
    virtual void update(timestamp_t const &);

protected:
    std::shared_ptr<Model> model_;
    std::vector<std::shared_ptr<Controller>> controllers_;
        
    Eigen::MatrixXd AWIAT_;
    Eigen::PartialPivLU<Eigen::MatrixXd> Mlu_;
    Eigen::PartialPivLU<Eigen::MatrixXd> lu_;
    Eigen::MatrixXd nullspace_projector_;
    Eigen::MatrixXd new_nullspace_projector_;
    Eigen::MatrixXd weighted_pseudoinverse_;
    Eigen::MatrixXd minv_;
};


} // pbwbc
#endif // PBWBC_HIERARCHYCONTROLLER_HPP


