#ifndef PBWBC_SIMULATOR_HPP
#define PBWBC_SIMULATOR_HPP
#include <pbwbc/Base.hpp>
#include <pbwbc/State.hpp>
#include <pbwbc/Model.hpp>

namespace pbwbc {



//! \brief A simple simulator with contacts.
//! 
//! The goal of this simulation is to have a minimalistic test environment.
//! Does not work currently.
//!
//! Features:
//! - Variable step solver + contact dynamics
//! - just a simple floor plane.
//!
//! A forward dynamic simulation based on pinocchio used a variable
//! step integrator from boost/odeint is implemented. This is WIP
//! for any floating base case.
//! 
//! Likely a BDF based ode solver like ode15s is needed to get complex contact
//! scenarios working.
//! See:
//! https://www.researchgate.net/post/What_are_the_available_Stiff_ODE_Solvers
//! 
class PBWBC_EXPORT Simulator {
    class SimulatorDetails;
public:
    Simulator(std::shared_ptr<Model> const & model,
            double time_step, double abs_tol=1e-3, double rel_tol=1e-4,
            int solver_type=0);
    virtual ~Simulator();

    typedef Model::translation_t translation_t;
    void addPointContact(std::string const & frame, translation_t const & offset);

    void reset();
    // compute simulate and return after dT has passed
    void update(timestamp_t const &);

    std::shared_ptr<RobotState> const & state();

    typedef RobotState::state_t vector_t;
    void setTau(vector_t const & tau);
    typedef Eigen::Matrix<double,6,1> wrench_t;
    void setBaseWrench(wrench_t const & wrench);
    wrench_t contact_wrench(int contact_id);

    double time();

    void set_contact_stiffness(translation_t const &);
    void set_contact_damping(translation_t const &);

    void enable_log(std::string const & filename);

    void set_state_filter_constant(double);
    void set_damping(double);
    int step_count();
    int tried_step_count();
protected:
    friend class SimulatorDetails;
    typedef Eigen::VectorXd sim_state_t;
    typedef Eigen::Vector2d vector2_t;
    std::shared_ptr<Model> model_;
    const double time_step_;
    std::shared_ptr<RobotState> state_;

    double t_;
    double micro_step_;
    int step_count_;
    int tried_step_count_;
    int solver_type_;
    int reset_request_;
    std::vector<int> contact_binary_state_;
    std::vector<double> contact_distance_;
    double state_filter_constant_;
    double damping_;
    double minimum_micro_step_;
    vector_t tau_;
    wrench_t base_wrench_;
    vector_t full_tau_;
    typedef std::tuple<int,translation_t> contact_info_t;
    std::vector<contact_info_t> contacts_;

    void function(sim_state_t const & x, sim_state_t & dxdt, double const t);
    void contact_model(
        wrench_t & wrench, vector2_t & dcontact_state,
        double normal_position, double normal_velocity,
        Eigen::Ref<const vector2_t> const & tangential_velocity,
        Eigen::Ref<const vector2_t> const & contact_state) const;
   
    double mu_static_;
    double mu_kinetic_; 
    typedef Eigen::Vector3d vector3_t;
    vector3_t translation_stiffness_;
    vector3_t translation_damping_;
    
    enum sim_state_entry {
        entry_y_state,
        entry_dy_state,
        entry_y_filter_state,
        entry_dy_filter_state,
        entry_contact_state
    };

    sim_state_t sim_state_;
    int sim_state_size();
    std::map<sim_state_entry,std::pair<int,int>> sim_state_map_;

    std::vector<wrench_t> contact_wrenches_;

    std::shared_ptr<SimulatorDetails> sim_;
    std::shared_ptr<std::ofstream> logfile_;
};

} // namespace

#endif // PBWBC_SIMULATOR_HPP

