#ifndef PBWBC_BASE_HPP
#define PBWBC_BASE_HPP
#include <Eigen/Dense>
#include <iostream>
#include <memory>
#include <chrono>
#include <log4cxx/logger.h>

#ifdef HAVE_LTTNG
#include <lttng/tracef.h>
#define PBWBC_TRACEF(...) tracef(__VA_ARGS__);
#else
#define PBWBC_TRACEF(...)
#endif // HAVE_LTTNG

#if BUILDING_PBWBC
#define PBWBC_EXPORT __attribute__((__visibility__("default")))
#else
#define PBWBC_EXPORT
#endif

namespace pbwbc {

class PBWBC_EXPORT logger {
public:
    static log4cxx::LoggerPtr global_logger;
    static log4cxx::LoggerPtr pbwbc_logger;
};

typedef Eigen::Matrix<double,6,1> wrench_t;

typedef std::chrono::time_point<std::chrono::system_clock,
             std::chrono::duration<double>> timestamp_t;

} //namespace pbwbc

#ifndef PBWBC_TRACE
#define PBWBC_TRACE(x) LOG4CXX_TRACE(pbwbc::logger::pbwbc_logger, x);
#endif //PBWBC_TRACE
#ifndef PBWBC_DEBUG
#define PBWBC_DEBUG(x) LOG4CXX_DEBUG(pbwbc::logger::pbwbc_logger, x);
#endif //PBWBC_DEBUG
#ifndef PBWBC_INFO
#define PBWBC_INFO(x) LOG4CXX_INFO(pbwbc::logger::pbwbc_logger, x);
#endif //PBWBC_INFO
#ifndef PBWBC_WARN
#define PBWBC_WARN(x) LOG4CXX_WARN(pbwbc::logger::pbwbc_logger, x);
#endif //PBWBC_WARN
#ifndef PBWBC_ERROR
#define PBWBC_ERROR(x) LOG4CXX_ERROR(pbwbc::logger::pbwbc_logger, x);
#endif //PBWBC_ERROR
#ifndef PBWBC_FATAL
#define PBWBC_FATAL(x) LOG4CXX_FATAL(pbwbc::logger::pbwbc_logger, x);
#endif //PBWBC_FATAL

#endif // PBWBC_BASE_HPP
