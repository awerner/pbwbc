#pragma once
#include <pbwbc/PBWBC.hpp>
#include <pbwbc/ROSTools.hpp>
#include <sensor_msgs/JointState.h>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>

namespace pbwbc {

class PBWBC_EXPORT PBWBCROS : public PBWBC {
public:
    PBWBCROS(std::shared_ptr<RobotState> const &,
          std::shared_ptr<SensorState> const &,
          std::shared_ptr<Model> const &,
          std::shared_ptr<ConfigMap> const &config,
          std::vector<std::shared_ptr<ActuatorController>> const & actuatorcontrollers,
          ros::NodeHandle & nh);

    virtual ~PBWBCROS();

    virtual void update(timestamp_t const &);
    
    virtual void initialize();
    
protected:
    ros::NodeHandle nh_;
    std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr_;
    RealTimePublisher<sensor_msgs::JointState> current_state_publisher_;
    RealTimePublisher<sensor_msgs::JointState> desired_torque_publisher_;
    RealTimePublisher<sensor_msgs::JointState> logging_joint_state_publisher_;
};

} // namespace
