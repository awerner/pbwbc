#ifndef PBWBC_MOTIONORCHESTRATOR_HPP
#define PBWBC_MOTIONORCHESTRATOR_HPP

#include <pbwbc/Controller.hpp>
#include <pbwbc/Config.hpp>
#include <pbwbc/Model.hpp>
#include <mutex>

namespace pbwbc {

//! Stores the desired motion / action
class PBWBC_EXPORT Action {
public:
    std::string name;
    std::string controllerName;
    std::string entityName;
    Eigen::VectorXd destination;
    double startTime;
    double duration;
    bool absolute;
};



/**
 * This class defines a timed sequence of actions which cause changes to desired quantites in
 * the controllers. This can e.g. be a motion of the hand or a contact change.
 *
 * A sequence of actions is either loaded from the config file or created
 * through a ROS service call. The action is described by the quantity to be changed
 * and the value. Typically this is commanded to an trajectory generator which then
 * changes the desired quantity in a smooth manner.
 *
 * Uses class Action to store each action.
 */
class PBWBC_EXPORT MotionOrchestrator {
public:
    MotionOrchestrator(
                std::shared_ptr<Model> const & model,
                std::shared_ptr<ConfigMap> const & config,
                std::vector<std::shared_ptr<Controller>> const & controllers);
    virtual ~MotionOrchestrator();

    virtual void start(timestamp_t const & timestamp);
    virtual void stop(timestamp_t const & timestamp);
    virtual void update(timestamp_t const & timestamp);

protected:

    std::shared_ptr<Model> model_;
    std::shared_ptr<ConfigMap> config_;
    std::map< std::tuple<std::string,std::string>, std::shared_ptr<Controller> > entity_map_;

    timestamp_t start_time_;
    double current_time_;

    std::mutex action_mutex_;
    std::vector<std::shared_ptr<Action>> actions_;
};


} // namespace pbwbc
#endif // PBWBC_MOTIONORCHESTRATOR_HPP
