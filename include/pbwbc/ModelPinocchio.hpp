#ifndef PBWBC_MODELPINOCCHIO_HPP
#define PBWBC_MODELPINOCCHIO_HPP
#include <pbwbc/Model.hpp>
#include <thread>
#include <mutex>
#include <condition_variable>

namespace pbwbc {

//! \brief Implements the Model using Pinocchio
//!
//! Computes computationally expensive quantities in a background thread
class PBWBC_EXPORT ModelPinocchio : public Model {
    ModelPinocchio();
    class PinocchioData;
public:
    // create instance like this:
    static std::shared_ptr<ModelPinocchio> create_from_urdf_file(std::string const &);
    static std::shared_ptr<ModelPinocchio> create_from_urdf_data(std::string const &);

    void reduceModel(std::vector<long unsigned int> const &); // lock joints in the model
    
    ~ModelPinocchio();

    typedef Eigen::Ref<Eigen::VectorXd> configuration_t;

    void update(timestamp_t const &);

    // compute model, but wait for completion of all computations
    // Use this at initialization
    void blocking_update(timestamp_t const &);
    
    int y_size();
    int dy_size();
    int q_size();
    int dq_size();
    int contact_size();

    translation_t bodyTranslation(int body_id) ;
    orientation_t bodyOrientation(int body_id) ;
    void getJacobian(int body_id, Eigen::Ref<Eigen::MatrixXd> jac ) ;
    void getJacobianWorld(int body_id, Eigen::Ref<Eigen::MatrixXd> jac ) ;

    matrix_t const & MassMatrix();
    matrix_t const & Coriolis();
    vector_t const & gravity();
    
    rowmatrix_t const & InvMassMatrix();

    virtual double mass();
    // com
    translation_t com();
    translation_t dcom();
    Eigen::MatrixXd const & comJacobian();
    
    std::vector<std::string> const & jointNames();
    vector_t const & position_max();
    vector_t const & position_min();
    vector_t const & velocity_max();
    vector_t const & torque_max();

    void setState(std::shared_ptr<RobotState> const &);
    void addContact(std::string const & frame_name);
    
    void setBasePosition(
        translation_t const & translation,
        orientation_t const & orientation,
        RobotState & state);
    void setBaseVelocity(
        translation_t const & translation,
        translation_t const & orientation,
        RobotState & state);
    Eigen::Ref<const Eigen::VectorXd> const stripBasePosition(
            Eigen::Ref<const Eigen::VectorXd> const & unfiltered);
    Eigen::Ref<const Eigen::VectorXd> const stripBaseVelocity(
            Eigen::Ref<const Eigen::VectorXd> const & unfiltered);
    
    int getFrameId(std::string const & frame_name);
    std::string const & getFrameName(int frameId);
    std::vector<int> getJointFrames();
    std::vector<int> getLinkFrames();

    std::shared_ptr<PinocchioData> const & pinocchio_data();

    int getPinocchioJointId(std::string const & joint_name);

    // add a compute task to the model compute thread
    // e.g. recompute damping terms for Cartesian Impedance Controller
    typedef std::function<void()> function_t;
    int register_compute_task(function_t const & function);
    void deregister_compute_task(int task_id);

    void setRealTimeParameters(int policy, int priority);
protected:
    std::shared_ptr<PinocchioData> d;
    std::shared_ptr<RobotState> state_;

    std::vector<int> contact_frame_ids_;
    Eigen::MatrixXd com_jacobian_;
    std::vector<std::string> joint_names_;

    int valid_buffer_;
    int compute_buffer_;
    bool compute_has_run_;
    bool keep_running_;
    std::thread compute_thread_;
    std::mutex compute_mutex_;
    std::condition_variable compute_cv_;
    std::condition_variable compute_done_cv_;
    void compute_thread();
    int compute_downsample_;
    std::vector<std::pair<int,function_t>> model_compute_tasks_;
    int model_compute_task_next_id_;

    std::map<int,RobotState> state_buffer_;
    int compute_thread_counter_;
};

} // namespace

#endif //PBWBC_MODELPINOCCHIO_HPP
