#ifndef PBWBC_JOINTVELOCITYLIMITER_HPP
#define PBWBC_JOINTVELOCITYLIMITER_HPP
#include <pbwbc/Controller.hpp>
#include <pbwbc/Config.hpp>
#include <pbwbc/State.hpp>
#include <pbwbc/Model.hpp>
#include <pbwbc/Base.hpp>

namespace pbwbc {

class PBWBC_EXPORT JointVelocityLimiter : public Controller {
public:
    JointVelocityLimiter(
            std::shared_ptr<RobotState> const &,
            std::shared_ptr<Model> const &,
            std::shared_ptr<ConfigMap> const &config
            );
    virtual ~JointVelocityLimiter();

    virtual void start(timestamp_t const &);
    virtual void stop(timestamp_t const &);

    virtual void update(timestamp_t const &);

protected:
    std::shared_ptr<RobotState> state_;
    std::shared_ptr<Model> model_;
    std::shared_ptr<ConfigMap> config_;

    typedef Eigen::VectorXd vector_t;
    
    vector_t maximum_torque_;
    vector_t velocity_max_;
    vector_t torque_scale_;
    vector_t offset_;


    std::vector<std::string> velocity_limited_joint_names_;
    std::vector<int> velocity_limited_joints_ids_;
    std::map< std::string, int> jointID_map_;

    int dq_offset_;

    double vel_min_, vel_max_, vel_, max_effort_;

};

} // namespace pbwbc
#endif // PBWBC_JOINTVELOCITYLIMITER_HPP
