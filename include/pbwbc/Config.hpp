#ifndef PBWBC_CONFIG_HPP
#define PBWBC_CONFIG_HPP
#include <pbwbc/Base.hpp>
#include <memory>
#include <vector>

namespace pbwbc {

//! \brief Abstract class for config queries
//!
//! Allows to query a database which maps string to values.
//! Where values can be float, int, bool, string or vectors of these data types.
//!
//! Implementations:
//! - StandaloneConfigMap
//! - ROSConfigMap
class PBWBC_EXPORT ConfigMap {
public:
    ConfigMap();
    virtual ~ConfigMap();

#ifndef DOXYGEN_SHOULD_SKIP_THIS  
#define param_fun(T) virtual void param(std::string const & parameter_name, T & variable, T const & default_value ) const { \
        throw std::runtime_error("Data type not implemented by this ConfigMap"); }
    param_fun(double)
    param_fun(int)
    param_fun(bool)
    param_fun(std::string)
    param_fun(std::vector<std::string>)
#undef param_fun
#endif //DOXYGEN_SHOULD_SKIP_THIS
    template<typename T>
    void param(std::string const & parameter_name, std::vector<T> & variable, std::vector<T> const & default_value) const  {
        std::string str;
        if(!this->getParam(parameter_name,str)) {
            variable = default_value;
            return;
        }
        std::istringstream sstr(str);
        std::string token;
        variable.clear();
        while(std::getline(sstr, token, ',')) {
            T el;
            std::istringstream elsstr(token);
            elsstr >> el;
            if(elsstr.fail()) break; // can fail silently here
            variable.push_back(el);
        }
        if(variable.size()!=default_value.size()) {
            std::stringstream str;
            str << "Parsing " << parameter_name << " did yield " << variable.size() << " tokens, not the required " << default_value.size();
            throw std::runtime_error(str.str());
        }
    }

    void param(std::string const & parameter_name, Eigen::Ref<Eigen::VectorXd> variable, Eigen::Ref<const Eigen::VectorXd> const & default_value) const {
        std::vector<double> nums;
        if(this->getParam(parameter_name,nums)) {
            variable.resize(nums.size());
            if(variable.size()!=default_value.size()) {
                std::stringstream str;
                str << "Parsing " << parameter_name << " did yield " << variable.size() << " tokens, not the required " << default_value.size();
                throw std::runtime_error(str.str());
            }
            std::copy(nums.begin(),nums.end(),&variable(0));
        } else {
            variable = default_value;
        }
    }

    //! convenience wrapper to return parameter value directly
    template <typename T>
    T param(std::string const & parameter_name, T const & default_value) const {
        T value;
        param(parameter_name,value,default_value);
        return value;
    }

    //template <typename T>
    //bool getParam(std::string const & parameter_name, T & variable) {
    //    std::runtime_error("Data type not implemented by this ConfigMap");
    //}

#ifndef DOXYGEN_SHOULD_SKIP_THIS  
#define param_fun(T) virtual bool getParam(std::string const & parameter_name, T & variable) const { \
        throw std::runtime_error("Data type not implemented by this ConfigMap"); }
    param_fun(double)
    param_fun(int)
    param_fun(bool)
    param_fun(std::string)
    param_fun(std::vector<std::string>)
    param_fun(std::vector<double>)
    param_fun(std::vector<int>)
#undef param_fun
#endif //DOXYGEN_SHOULD_SKIP_THIS

    template<typename T>
    bool getParam(std::string const & parameter_name, Eigen::MatrixBase<T> & variable) const {
        std::vector<double> nums;
        if(this->getParam(parameter_name,nums)) {
            if(nums.size()!=static_cast<std::size_t>(variable.size())) {
                std::stringstream str;
                str << "Parsing " << parameter_name << " did yield " << nums.size() << " tokens, not the required " << variable.size();
                throw std::runtime_error(str.str());
            }
            std::copy(nums.begin(),nums.end(),&variable(0));
            return true;
        } else {
            return false;
        }
    }

    bool getParam(std::string const & parameter_name, Eigen::VectorXd & variable) const;

    virtual bool list_items(std::string const & parameter, std::vector<std::string> & keys );

    virtual std::shared_ptr<ConfigMap> submap(std::string const & parameter_name);
};


}
#endif // PBWBC_CONFIG_HPP
