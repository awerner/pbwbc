/*
 * Author: Alexander Werner
 * Author: Olivier STASSE
 *
 *
 * This implements the ros_control interface for using all components of Talos.
 * The idea is to implement this in a really minimalistic way. In should only
 * provide nice generic interfaces for use with PBWBC.
 * 
 * This code is strongly inspired by roscontrol_sot. This had two extra features:
 * * ring buffer logging
 * * fall back controller
 * Both features are useful, however ring buffer logging should be implemented in the
 * controller to allow synchronous logging of arbitrary quantities. The fall back controller
 * is also a meaningful feature and shall be implemented here.
 *
 */


#pragma once

#include <string>
#include <map>
#include <algorithm>    // std::find

#include <controller_interface/controller.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/imu_sensor_interface.h>
#include <hardware_interface/force_torque_sensor_interface.h>
#ifdef TEMPERATURE_SENSOR_CONTROLLER
#include <pal_hardware_interfaces/actuator_temperature_interface.h>
#endif

#include <ros/ros.h>
#include <control_toolbox/pid.h>

/** URDF DOM*/
#include <urdf_parser/urdf_parser.h>

#include <pbwbc/PBWBCROS.hpp>
#include <pbwbc/Config.hpp>
#include <pbwbc/ROSConfig.hpp>
#include <pbwbc/ROSTools.hpp>
#include <pbwbc/StateEstimation.hpp>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Bool.h>

#include <pbwbc/triggerRosbag.h>
#include <pbwbc/controllerSwitch.h>

namespace pbwbc {

  enum ControlMode { POSITION, EFFORT};

  class XmlrpcHelperException : public ros::Exception
  {
  public:
    XmlrpcHelperException(const std::string& what)
      : ros::Exception(what) {}
  };


  struct EffortControlPDMotorControlData
  {
    control_toolbox::Pid pid_controller;

    double prev;
    double vel_prev;
    double des_pos;
    double integ_err;

    EffortControlPDMotorControlData();
    void read_from_xmlrpc_value(const std::string &prefix);
  };


  namespace lhi = hardware_interface;
  namespace lci = controller_interface;
  
  // wraps a ROS actuator 
  class RCActuatorController: public ActuatorController {
  public:
      RCActuatorController(lhi::JointHandle & joint) 
        : joint_(joint) {}

      virtual ~RCActuatorController() {}
      void update(timestamp_t const &, double tau_d) {
        joint_.setCommand(tau_d);
      };
  protected:
      lhi::JointHandle & joint_;
  };

  template<typename BaseController>
  class PBWBCController : public BaseController {

  protected:
    typedef BaseController BaseControllerType;
    /// Robot nb dofs.
    size_t nbDofs_;

  private:

    /// @{ \name Ros-control related fields

    /// \brief Vector of joint handles.
    std::vector<lhi::JointHandle> joints_;
    std::vector<std::string> joints_name_;
    std::vector<std::string> locked_joints_name_;

    /// \brief Vector towards the IMU.
    std::vector<lhi::ImuSensorHandle> imu_sensor_;

    /// \brief Vector of 6D force sensor.
    std::vector<lhi::ForceTorqueSensorHandle> ft_sensors_;

#ifdef TEMPERATURE_SENSOR_CONTROLLER
    /// \brief Vector of temperature sensors for the actuators.
    std::map<int,lhi::ActuatorTemperatureSensorHandle> act_temp_sensors_;
#endif

    /// \brief Interface to the joints controlled in position.
    lhi::PositionJointInterface * pos_iface_;

    /// \brief Interface to the joints controlled in force.
    lhi::EffortJointInterface * effort_iface_;

    /// \brief Interface to the sensors (IMU).
    lhi::ImuSensorInterface* imu_iface_;

    /// \brief Interface to the sensors (Force).
    lhi::ForceTorqueSensorInterface* ft_iface_;

#ifdef TEMPERATURE_SENSOR_CONTROLLER
    /// \brief Interface to the actuator temperature sensor.
    lhi::ActuatorTemperatureSensorInterface  * act_temp_iface_;
#endif
    /// @}

    const std::string type_name_;

    /// \brief Adapt the interface to Gazebo simulation
    bool simulation_mode_;

    /// \brief If the main controller is active
    bool controller_active_;

    /// \brief The robot can controlled in effort or position mode (default).
    ControlMode control_mode_;

    /// \brief Implement a PD controller for the robot when the dynamic graph
    /// is not on.
    std::map<std::string, EffortControlPDMotorControlData> effort_mode_pd_motors_;

    /// \brief Give the desired position when the dynamic graph is not on.
    std::vector<double> desired_init_pose_;

    /// \brief Map from ros-control quantities to robot device
    /// ros-control quantities are for the sensors:
    /// * motor-angles
    /// * joint-angles
    /// * velocities
    /// * torques
    /// ros-control quantities for control are:
    /// * joints
    /// * torques
    std::map<std::string,std::string> mapFromRCToPBWBC_;

    /// To be able to subsample control period.
    double accumulated_time_;

    /// Jitter for the subsampling.
    double jitter_;

    /// \brief Verbosity level for ROS messages during initRequest/initialization phase.
    /// 0: no messages or error 1: info 2: debug
    int verbosity_level_;

    /// URDF model of the robot.
    urdf::ModelInterfaceSharedPtr modelURDF_;

    std::shared_ptr<ConfigMap> config_;
    std::shared_ptr<StateEstimation> state_estimation_;
    std::shared_ptr<SensorState> sensor_state_;
    std::shared_ptr<Model> model_;
    std::shared_ptr<PBWBC> pbwbc_;

    // tau sensor simulation
    Eigen::VectorXd tau_simulation_;
    double tau_simulation_tc_;

    //Number of base DOFs defined in PINOCCHIO model state
    int q_offset_;
    int dq_offset_;
      
    // for fake joint torques in simulation: track if torques have been computed yet
    bool jointTorque_first_iteration_;

  public :
    typedef typename BaseController::ClaimedResources ClaimedResources;

    PBWBCController ();
    ~PBWBCController ();

    /// \brief Read the configuration files,
    /// claims the request to the robot and initialize the Stack-Of-Tasks.
    bool initRequest (lhi::RobotHW * robot_hw,
		      ros::NodeHandle &robot_nh,
		      ros::NodeHandle &controller_nh,
		      ClaimedResources & claimed_resources);

    /// \brief Display claimed resources
    void displayClaimedResources(ClaimedResources & claimed_resources);

    /// \brief Claims
    bool init();

    /// \brief Read the sensor values, calls the control graph, and apply the control.
    ///
    void update(const ros::Time&, const ros::Duration& );
    /// \brief Starting by filling the sensors.
    void starting(const ros::Time&);
    /// \brief Stopping the control
    void stopping(const ros::Time&);
    /// \brief Display the kind of hardware interface that this controller is using.
    virtual std::string getHardwareInterfaceType() const;
  
    bool initRequestForwarding (lhi::RobotHW * robot_hw,
            ros::NodeHandle &robot_nh,
            ros::NodeHandle &controller_nh,
            ClaimedResources & claimed_resources);
    void startingForwarding(const ros::Time & ros_time);
    void stoppingForwarding(const ros::Time & ros_time);
    void updateForwarding(const ros::Time & ros_time, const ros::Duration& period);
    std::shared_ptr<ActuatorController> create_actuatorcontroller (int idJoint);
 
    // compatibility with pal base controller 
    bool loadEtras(ros::NodeHandle& controller_nh);
    void startingExtras(const ros::Time& time);
    void updateExtras(const ros::Time& time, const ros::Duration& period);
    void stoppingExtra(const ros::Time& time);

  protected:
    /// Initialize the roscontrol interfaces
    bool initInterfaces(lhi::RobotHW * robot_hw,
			ros::NodeHandle &,
			ros::NodeHandle &,
			ClaimedResources & claimed_resources);

    /// Initialize the hardware interface using the joints.
    bool initJoints();
    /// Initialize the hardware interface accessing the IMU.
    bool initIMU();
    /// Initialize the hardware interface accessing the force sensors.
    bool initForceSensors();
    /// Initialize the hardware interface accessing the temperature sensors.
    bool initTemperatureSensors();

    /// Initialize internal structure for the logs based on nbDofs
    /// number of force sensors and size of the buffer.
    void initLogs();

    ///@{ \name Read the parameter server
    /// \brief Entry point
    bool readParams(ros::NodeHandle &robot_nh);

    /// \brief Creates the list of actuated joint names.
    bool readParamsJointNames(ros::NodeHandle &robot_nh);

    /// \brief Creates the list of locked joint names (i.e. joints that are not actuated).
    bool readParamsLockedJointNames(ros::NodeHandle &robot_nh);

    /// \Brief Set the mapping between ros-control and the robot device
    /// For instance the yaml file should have a line with map_rc_to_pbwbc_device:
    ///   map_rc_to_pbwbc_device: [ ]
    bool readParamsFromRCToPBWBC(ros::NodeHandle &robot_nh);

    /// \brief Read the control mode.
    bool readParamsControlMode(ros::NodeHandle & robot_nh);

    /// \brief Read the PID information of the robot in effort mode.
    bool readParamsEffortControlPDMotorControlData(ros::NodeHandle &robot_nh);

    /// \brief Read the desired initial pose of the robot in position mode.
    bool readParamsPositionControlData(ros::NodeHandle &robot_nh);

    /// \brief Read the control period.
    bool readParamsdt(ros::NodeHandle & robot_nh);

    /// \brief Read verbosity level to display messages mostly during initialization
    void readParamsVerbosityLevel(ros::NodeHandle &robot_nh);
    ///@}

    /// \brief Get the information from the low level and calls fillSensorsIn.
    void fillJoints();

    /// In the map sensorsIn_ creates the key "name_IMUNb"
    /// and associate to this key the vector data.
    void setSensorsImu(std::string &name,
		       int IMUNb,
		       std::vector<double> &data);

    /// @{ \name Fill the sensors
    /// Read the imus and set the interface to the SoT.
    void fillImu();
    /// Read the force sensors
    void fillForceSensors();
    /// Read the temperature sensors
    void fillTempSensors();
    /// Entry point for reading all the sensors .
    void fillSensors();
    ///@}

    ///@{ Control the robot while waiting for the SoT
    /// Default control in effort.
    void localStandbyEffortControlMode(timestamp_t const &, const ros::Duration& period);
    /// Default control in position.
    void localStandbyPositionControlMode(timestamp_t const &);

    ///@}
    /// Extract control values to send to the simulator.
    void readControl(std::map<std::string,double> &controlValues);

    double jointPosition(int idx);
    double jointVelocity(int idx);
    double jointTorque(int idx);

    /// Map of sensor readings
    std::map <std::string,double> sensorsIn_;

    /// Map of control values
    std::map<std::string,double> controlValues_;

    /// Control period
    double dt_;

    /// \brief Command send to motors
    /// Depending on control_mode it can be either
    /// position control or torque control.
    std::vector<double> command_;

    /// Read URDF model from /robot_description parameter.
    bool readUrdf(ros::NodeHandle &robot_nh);
    
    pbwbc::RealTimePublisherBackEnd pb_; // start the background publisher thread

    ros::Subscriber joystick_subscriber_;

    void joystick_subscriber_callback(const sensor_msgs::JoyConstPtr & );
    
    // button in /joy messages which triggers soft stop
    int softstop_button_idx_;

    // request variable for soft stop
    bool softstop_request_;

    // request start of controller from soft stop
    bool start_request_;

    // command to be executed on soft stop trigger
    std::string softstop_command_;
    std::thread softstop_command_thread_;
    
    // actuator wrappers
    std::vector<std::shared_ptr<pbwbc::ActuatorController>> actuatorcontrollers_raw_;
    std::vector<std::shared_ptr<pbwbc::ActuatorController>> actuatorcontrollers_;

    // joint mapping to actuator interface
    std::vector<int> joint_mapping_;
    void create_joint_mapping ();

    // create hardware_stateestimation
    std::shared_ptr<StateEstimation> create_hardware_stateestimation(
            std::shared_ptr<Model> const &,
            ros::NodeHandle const &);

    ros::ServiceServer trigger_rosbag_service_;  
    bool callback_trigger_rosbag (
          pbwbc::triggerRosbagRequest & request,
          pbwbc::triggerRosbagResponse & response);
    
    ros::ServiceServer controller_switch_service_;  
    bool callback_controller_switch (
          pbwbc::controllerSwitchRequest & request,
          pbwbc::controllerSwitchResponse & response);

    RealTimePublisher<std_msgs::Bool> controller_active_publisher_;

  };
}

