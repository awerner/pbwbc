#ifndef PBWBC_REALSENSESTATEESTIMATION_HPP
#define PBWBC_REALSENSESTATEESTIMATION_HPP
#include <pbwbc/StateEstimation.hpp>
#include <pbwbc/Config.hpp>
#include <pbwbc/Model.hpp>
#include <pbwbc/Algebra.hpp>
#include <mutex>
#include <thread>
#include <fstream>
#include <Eigen/Dense>

namespace pbwbc {

class rs2_details;
class rs2_camera_details;
class rs2_frame;

//! \brief State estimation based on Intel RealSense T265 tracking cameras
//!
//! Uses an arbitrary number of tracking cameras and the measurements from
//! the internal imu to obtain the base state of the robot.
class PBWBC_EXPORT RealSenseStateEstimation : public StateEstimation {
public:
    RealSenseStateEstimation(
            std::shared_ptr<ConfigMap> const & config,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<SensorState> const & sensor_state
            );

    virtual ~RealSenseStateEstimation();

    virtual void start(timestamp_t const &);
    virtual void stop(timestamp_t const &);

    virtual void update(timestamp_t const &);

    std::shared_ptr<RobotState> const & state();

    void enable_log(std::string const & filename);

    CartesianState const & getState() const;
    CartesianState & getStartState();

protected:
    std::shared_ptr<Model> model_;
    std::shared_ptr<RobotState> robotstate_;
    std::shared_ptr<SensorState> sensor_state_;
    int imu_idx_;
    int base_link_idx_;

    timestamp_t last_update_;
    timestamp_t last_correction_;

    std::shared_ptr<std::ofstream> logfile_;
            

    double position_translation_gain_;
    double velocity_translation_gain_;
    double orientation_gain_;
    double world_frame_gain_;
    double W_T_H_gain_;

    double translation_rejection_threshold_;
    double orientation_rejection_threshold_;

    CartesianState state_;

    CartesianState start_state_;
    bool reset_state_;

    class CameraBuffer {
    public:
        std::string serial_no;
        std::string name;
        bool found;
        bool enabled;
        bool initialized;

        CartesianState state;
        int tracker_confidence;
        int mapper_confidence;
        int tracker_max_confidence;
        int mapping_enabled;

        Eigen::Vector3d sensor_acceleration;

        Eigen::Vector3d B_T_C_p;
        Eigen::Quaterniond B_T_C_q;

        Eigen::Vector3d W_T_H_p;
        Eigen::Quaterniond W_T_H_q;

        double position_translation_gain;
        double position_velocity_gain;

        std::size_t counter_until_valid;
       
        std::shared_ptr<rs2_camera_details> d; 

        double timestamp;
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    };

    std::mutex lock_;
    std::shared_ptr<rs2_details> rs2_;
    std::vector<CameraBuffer,Eigen::aligned_allocator<CameraBuffer>> camera_buffers_;

    void camera_callback(CameraBuffer & buffer,rs2_frame const & frame);
        
    typedef Eigen::Matrix<double,6,1> Vector6d;
    Eigen::Isometry3d TfromVQ(Eigen::Vector3d const & v, Eigen::Quaterniond const & q);

};

} // namespace

#endif // PBWBC_REALSENSESTATEESTIMATION_HPP
