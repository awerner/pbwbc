#pragma once

// This class provides a ROS JointTrajectoryController interface
// 
// This approach takes the original code from 
// https://github.com/ros-controls/ros_controllers/blob/melodic-devel/joint_trajectory_controller/include/joint_trajectory_controller/joint_trajectory_controller.h
// and removes the dependency on the hardware_interface.
// Rather than commanding to a hardware interface, instances can be queried for the
// desired state. This functionality can then be used to control either the
// JointImpedanceController or the CartesianImpedanceController. 
//


// C++ standard
#include <cassert>
#include <stdexcept>
#include <string>
#include <memory>

// Boost
#include <boost/shared_ptr.hpp>
#include <boost/dynamic_bitset.hpp>

// ROS
#include <ros/node_handle.h>

// URDF
#include <urdf/model.h>

// ROS messages
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <control_msgs/JointTrajectoryControllerState.h>
#include <control_msgs/QueryTrajectoryState.h>
#include <trajectory_msgs/JointTrajectory.h>

// actionlib
#include <actionlib/server/action_server.h>

// realtime_tools
#include <realtime_tools/realtime_box.h>
#include <realtime_tools/realtime_buffer.h>
#include <realtime_tools/realtime_publisher.h>


// Project
#include <trajectory_interface/trajectory_interface.h>

#include <joint_trajectory_controller/joint_trajectory_segment.h>
#include <joint_trajectory_controller/init_joint_trajectory.h>
#include <joint_trajectory_controller/hardware_interface_adapter.h>

namespace pbwbc {

class PBWBC_EXPORT JointTrajectoryControllerROS {
    public:

        JointTrajectoryControllerROS(
                ros::NodeHandle& controller_nh);

        void start(timestamp_t const &);
        void stop(timestamp_t const &);
        void update(timestamp_t const &);

        std::vector<int> getControlledJoints() const;

        void getDesiredState(
                Eigen::Ref<Eigen::VectorXd> position,
                Eigen::Ref<Eigen::VectorXd> velocity,
                Eigen::Ref<Eigen::VectorXd> acceleration) const;

    protected:
        void init();
        typedef trajectory_interface::QuinticSplineSegment<double> SegmentImpl;

        struct TimeData
        {
            TimeData() : time(0.0), period(0.0), uptime(0.0) {}

            ros::Time     time;   ///< Time of last update cycle
            ros::Duration period; ///< Period of last update cycle
            ros::Time     uptime; ///< Controller uptime. Set to zero at every restart.
        };

        typedef actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction>                  ActionServer;
        typedef std::shared_ptr<ActionServer>                                                       ActionServerPtr;
        typedef ActionServer::GoalHandle                                                            GoalHandle;
        typedef realtime_tools::RealtimeServerGoalHandle<control_msgs::FollowJointTrajectoryAction> RealtimeGoalHandle;
        typedef boost::shared_ptr<RealtimeGoalHandle>                                               RealtimeGoalHandlePtr;
        typedef trajectory_msgs::JointTrajectory::ConstPtr                                          JointTrajectoryConstPtr;
        typedef realtime_tools::RealtimePublisher<control_msgs::JointTrajectoryControllerState>     StatePublisher;
        typedef std::unique_ptr<StatePublisher>                                                     StatePublisherPtr;

        typedef JointTrajectorySegment<SegmentImpl> Segment;
        typedef std::vector<Segment> TrajectoryPerJoint;
        typedef std::vector<TrajectoryPerJoint> Trajectory;
        typedef std::shared_ptr<Trajectory> TrajectoryPtr;
        typedef std::shared_ptr<TrajectoryPerJoint> TrajectoryPerJointPtr;
        typedef realtime_tools::RealtimeBox<TrajectoryPtr> TrajectoryBox;
        typedef typename Segment::Scalar Scalar;

        bool                      verbose_;            ///< Hard coded verbose flag to help in debugging
        std::string               name_;               ///< Controller name.
        std::vector<int>          joints_;             ///< Joint ids (not including base of controlled joints
        std::vector<bool>         angle_wraparound_;   ///< Whether controlled joints wrap around or not.
        std::vector<std::string>  joint_names_;        ///< Controlled joint names.
        SegmentTolerances<Scalar> default_tolerances_; ///< Default trajectory segment tolerances.

        RealtimeGoalHandlePtr     rt_active_goal_;     ///< Currently active action goal, if any.

        /**
         * Thread-safe container with a smart pointer to trajectory currently being followed.
         * Can be either a hold trajectory or a trajectory received from a ROS message.
         *
         * We store the hold trajectory in a separate class member because the \p starting(time) method must be realtime-safe.
         * The (single segment) hold trajectory is preallocated at initialization time and its size is kept unchanged.
         */
        TrajectoryBox curr_trajectory_box_;
        TrajectoryPtr hold_trajectory_ptr_; ///< Last hold trajectory values.

        typename Segment::State current_state_;         ///< Preallocated workspace variable.
        typename Segment::State desired_state_;         ///< Preallocated workspace variable.
        typename Segment::State state_error_;           ///< Preallocated workspace variable.
        typename Segment::State desired_joint_state_;   ///< Preallocated workspace variable.
        typename Segment::State state_joint_error_;     ///< Preallocated workspace variable.

        realtime_tools::RealtimeBuffer<TimeData> time_data_;

        ros::Duration state_publisher_period_;
        ros::Duration action_monitor_period_;

        typename Segment::Time stop_trajectory_duration_;  ///< Duration for stop ramp. If zero, the controller stops at the actual position.
        boost::dynamic_bitset<> successful_joint_traj_;
        bool allow_partial_joints_goal_;

        // ROS API
        ros::NodeHandle    controller_nh_;
        ros::Subscriber    trajectory_command_sub_;
        ActionServerPtr    action_server_;
        ros::ServiceServer query_state_service_;
        StatePublisherPtr  state_publisher_;

        ros::Timer         goal_handle_timer_;
        ros::Time          last_state_publish_time_;

        virtual bool updateTrajectoryCommand(const JointTrajectoryConstPtr& msg, RealtimeGoalHandlePtr gh, std::string* error_string = 0);
        virtual void trajectoryCommandCB(const JointTrajectoryConstPtr& msg);
        virtual void goalCB(GoalHandle gh);
        virtual void cancelCB(GoalHandle gh);
        virtual void preemptActiveGoal();
        virtual bool queryStateService(control_msgs::QueryTrajectoryState::Request&  req,
                control_msgs::QueryTrajectoryState::Response& resp);

        /**
         * \brief Publish current controller state at a throttled frequency.
         * \note This method is realtime-safe and is meant to be called from \ref update, as it shares data with it without
         * any locking.
         */
        void publishState(const ros::Time& time);

        /**
         * \brief Hold the current position.
         *
         * Substitutes the current trajectory with a single-segment one going from the current position and velocity to
         * zero velocity.
         * \see parameter stop_trajectory_duration
         * \note This method is realtime-safe.
         */
        void setHoldPosition(const ros::Time& time, RealtimeGoalHandlePtr gh=RealtimeGoalHandlePtr());

};

} // namespace

