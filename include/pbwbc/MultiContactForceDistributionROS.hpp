#ifndef PBWBC_MULTICONTACTBALANCERROS_HPP
#define PBWBC_MULTICONTACTBALANCERROS_HPP
#include <pbwbc/MultiContactForceDistribution.hpp>
#include <ros/ros.h>
#include <pbwbc/ROSTools.hpp>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <pbwbc/Int32Stamped.h>
#include <pbwbc/Float32Stamped.h>
#include <pbwbc/Float32MultiArrayStamped.h>
#include <pbwbc/StateStamped.h>
#include <sensor_msgs/JointState.h>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>

namespace pbwbc {


class PBWBC_EXPORT Contact6DSurfaceROS : public Contact6DSurface {
public:
    Contact6DSurfaceROS(Contact6DSurface const &, ros::NodeHandle const & nh );

    void update(timestamp_t const &);

protected:
    ros::NodeHandle nh_;
    
    RealTimePublisher<geometry_msgs::TransformStamped> state_publisher_;
    RealTimePublisher<geometry_msgs::WrenchStamped> desired_wrench_publisher_;
    RealTimePublisher<geometry_msgs::WrenchStamped> measured_wrench_publisher_;

    RealTimePublisher<pbwbc::Int32Stamped> contact_state_publisher_;
    RealTimePublisher<pbwbc::Float32Stamped> contact_state_timer_publisher_;
    RealTimePublisher<pbwbc::Float32MultiArrayStamped> contact_weights_publisher_;
    RealTimePublisher<pbwbc::Float32MultiArrayStamped> CIT_publisher_;
    RealTimePublisher<pbwbc::Float32MultiArrayStamped> ci0_publisher_;

    std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr_;
};

class PBWBC_EXPORT MultiContactForceDistributionROS : public MultiContactForceDistribution {
    public:
        MultiContactForceDistributionROS(
            std::shared_ptr<RobotState> const &,
            std::shared_ptr<SensorState> const &,
            std::shared_ptr<Model> const &,
            std::shared_ptr<ConfigMap> const & config,
            ros::NodeHandle const & nh
            );

        virtual ~MultiContactForceDistributionROS();

        virtual void update(timestamp_t const &);

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    protected:
        ros::NodeHandle nh_;
        RealTimePublisher<pbwbc::StateStamped> com_desired_state_publisher_;
        RealTimePublisher<pbwbc::StateStamped> com_current_state_publisher_;
        RealTimePublisher<geometry_msgs::WrenchStamped> com_desired_wrench_publisher_;
        RealTimePublisher<geometry_msgs::WrenchStamped> com_realized_wrench_publisher_;
        RealTimePublisher<sensor_msgs::JointState> desired_torque_publisher_;
        RealTimePublisher<pbwbc::Float32MultiArrayStamped> G_publisher_;
        RealTimePublisher<pbwbc::Float32MultiArrayStamped> g0_publisher_;
        RealTimePublisher<pbwbc::Float32MultiArrayStamped> JT_publisher_;
        
        std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr_;
};


} // namespace pbwbc
#endif // PBWBC_MULTICONTACTBALANCERROS_HPP
