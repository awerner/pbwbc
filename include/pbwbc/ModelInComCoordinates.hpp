#ifndef PBWBC_MODELINCOMCOORDINATES_HPP
#define PBWBC_MODELINCOMCOORDINATES_HPP
#include <pbwbc/Model.hpp>

namespace pbwbc {

//! \brief Converts the coordinates of a Model to use the COM instead of the base translation
//!
//! Based on:
//! - Garofalo, Gianluca, et al. "On the inertially decoupled structure of the floating base robot dynamics." IFAC Papers 2015
//! - Henze et al. "Passivity-based whole-body balancing for torque-controlled humanoid robots in multi-contact scenarios" IJRR 2016
class PBWBC_EXPORT ModelInComCoordinates : public Model {
public:
    ModelInComCoordinates(
            std::shared_ptr<Model> const &,
            std::shared_ptr<RobotState> const &
            );
    ~ModelInComCoordinates(){};

    void update(timestamp_t const &); 

    int y_size();
    int dy_size();
    int q_size();
    int dq_size();
    int contact_size();

    translation_t com();
    translation_t dcom();
    translation_t bodyTranslation(int body_id);
    orientation_t bodyOrientation(int body_id);
    
    double mass();

    matrix_t const & MassMatrix();

    matrix_t const & Coriolis();

    vector_t const & gravity();
    
    rowmatrix_t const & InvMassMatrix();

    Eigen::MatrixXd const & comJacobian();

    void getJacobian(int frame_id, Eigen::Ref<Eigen::MatrixXd> jac );
    void getJacobianWorld(int frame_id, Eigen::Ref<Eigen::MatrixXd> jac );

    int getFrameId(std::string const & frame_name);
    std::string const & getFrameName(int frameId);
    std::vector<int> getJointFrames();
    std::vector<int> getLinkFrames();

    std::vector<std::string> const & jointNames();
    vector_t const & position_max();
    vector_t const & position_min();
    vector_t const & velocity_max();
    vector_t const & torque_max();

    void setState(std::shared_ptr<RobotState> const & state);

    void setBasePosition(
        translation_t const & translation,
        orientation_t const & orientation,
        RobotState & state);

    void setBaseVelocity(
        translation_t const & translation,
        translation_t const & orientation,
        RobotState & state);
    Eigen::Ref<const Eigen::VectorXd> const stripBasePosition(
            Eigen::Ref<const Eigen::VectorXd> const & unfiltered);
    Eigen::Ref<const Eigen::VectorXd> const stripBaseVelocity(
            Eigen::Ref<const Eigen::VectorXd> const & unfiltered);

protected:
    std::shared_ptr<Model> model_;
    std::shared_ptr<ModelInComCoordinates> modelInComCoordinates_;
    std::shared_ptr<RobotState> state_;
    Eigen::MatrixXd Q_;
    Eigen::PartialPivLU<Eigen::MatrixXd> lu_;

    // temporaries
    Eigen::MatrixXd transformation_;
    Eigen::MatrixXd jacobian_;
    Eigen::MatrixXd com_jacobian_;
    Eigen::MatrixXd massmatrix_;
};


} // namespace

#endif 
