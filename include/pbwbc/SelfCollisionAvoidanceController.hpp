#pragma once
#include <pbwbc/Base.hpp>
#include <pbwbc/Config.hpp>
#include <pbwbc/Controller.hpp>
#include <pbwbc/TripleBuffer.hpp>
#include <pbwbc/State.hpp>
#include <pbwbc/Model.hpp>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <thread>

namespace pbwbc {

//! \brief A potential field based approach to avoid self collisions
//!
//! Implements a potential field based self collision avoidance. Based
//! on the current state of the robot the positions of collision objects
//! attached to specific links of the robot 
//! are computed. These objects can be geometric primitives such as capsules.
//!  Once the distance of the objects is known and is lower
//! than a configurable threshold a repulsive force is computed. This force
//! is than projected into the joint space using the relevant Jacobians.
//! In addition to the position dependant force a velocity dependant damping
//! force is computed to ensure stable behavior.
//!
//! Based on:
//! - Dietrich, Alexander, et al. "Extensions to reactive self-collision avoidance for torque and position controlled humanoids." ICRA 2011
//!
//! This implementation uses pinocchio which wraps the flexible collision
//! library (fcl) and thus allows meshes as well as geometric primitives.
//! The collision meshes defined in the URDF of the robot can be used.
//! However collision computation time for meshes is typically too long.
//! To minimize computation time in the real-time thread, the collisions are
//! computed in a background thread.
class PBWBC_EXPORT SelfCollisionAvoidanceController : public Controller {
public:
    SelfCollisionAvoidanceController(
        std::shared_ptr<RobotState> const &state,
        std::shared_ptr<Model> const &model,
        std::shared_ptr<ConfigMap> const & config,
        std::string const & urdf_content);

    virtual ~SelfCollisionAvoidanceController();

    virtual void start(timestamp_t const &);
    virtual void stop(timestamp_t const &);
    virtual void update(timestamp_t const &);

protected:
    std::shared_ptr<RobotState> state_;
    std::shared_ptr<Model> model_;
    std::shared_ptr<ConfigMap> config_;

    // config settings
    double max_force_; //<! maximum force applied by the potatial field
    double damping_; //<! damping ratio
    //! Vector of start distances, one for each collision pair.
    //! if set to negative values, the collision pair is ignored.
    std::vector<double> start_distance_;
    
    double dynamic_gain_; //<! gain to scale output for startup and other things
    long update_too_late_; //<! counter for the background thread overruns

    class impl; //<! class to store pinocchio geometry model and data, hiding it from the API
    std::shared_ptr<impl> impl_; //<! store pinocchio geometry model and data
    
    typedef Eigen::Vector3d vector3_t;
    typedef Eigen::Matrix<double,2,2> matrix_2x2_t;

    // compute thread related
    void collision_compute_thread();
    bool request_compute_;
    bool keep_running_;
    int compute_downsample_;
    int counter_;
    std::mutex mutex_;
    std::condition_variable cv_;
    std::thread compute_thread_;
    // thread i/o and temporaries
    TripleBuffer<RobotState> compute_thread_state_;
    TripleBuffer<Eigen::VectorXd> collision_tau_;
    std::atomic<std::chrono::system_clock::time_point> last_update_;
    Eigen::MatrixXd jacobian_A_;
    Eigen::MatrixXd jacobian_B_;
    Eigen::MatrixXd frame_jacobian_;
    Eigen::MatrixXd jacobian_temp_;
    Eigen::PartialPivLU<Eigen::MatrixXd> mass_lu_;
    Eigen::MatrixXd mass_inv_;
    bool mass_inv_initialized_;
        
    // model compute task related
    std::mutex model_compute_mutex_;
    int model_compute_task_id_;
    void model_compute_task();

    // helper functions
    //! compute Jacobians at the point of collision
    void jacobian_at_collision( Eigen::MatrixXd & jacobian_i,
        const vector3_t& point_i, const vector3_t& direction_i,
        const std::size_t body_idx);
    //! compute damping based on double diagonalization
    void double_diag_damping(matrix_2x2_t & D,
        double distance, std::size_t pair_idx);
    //! compute potential function
    double potential_d0(double distance, std::size_t pair_idx ) const;
    //! compute potential function first derivative
    double potential_d1(double distance, std::size_t pair_idx ) const;
    //! compute potential function second derivative
    double potential_d2(double distance, std::size_t pair_idx ) const;

};


} // namespace

