#ifndef PBWBC_CARTESIANTRAJECTORYGENERATOR_HPP
#define PBWBC_CARTESIANTRAJECTORYGENERATOR_HPP

#include <pbwbc/Controller.hpp>
#include <pbwbc/Config.hpp>
#include <pbwbc/Model.hpp>
#include <mutex>

namespace pbwbc {

//! \brief Generator for minimal jerk trajectories to a goal
//!
//! Minimal jerk in translation directions, rotation is TODO
class PBWBC_EXPORT CartesianTrajectoryGenerator {
    public:
        CartesianTrajectoryGenerator(
                    std::shared_ptr<Model> const & model,
                    std::shared_ptr<ConfigMap> const & config);
        virtual ~CartesianTrajectoryGenerator();

        virtual void start(timestamp_t const & timestamp);
        virtual void stop(timestamp_t const & timestamp);
        virtual void update(timestamp_t const & timestamp);
        
        virtual void setInitialState(CartesianState & state);

        virtual void start6DMotion(timestamp_t const & timestamp, const CartesianState start_point, const CartesianState final_point, double duration);   
        virtual void startTranslation(timestamp_t const & timestamp, const Eigen::Ref<const Eigen::VectorXd> & start_point, const Eigen::Ref<const Eigen::VectorXd> & final_point , double duration);
        virtual void startOrientation(timestamp_t const & timestamp, const Eigen::Quaterniond & start_point, const Eigen::Quaterniond & final_point , double duration);
        virtual void stopMotion(timestamp_t const & timestamp);

        CartesianState const & getState() const;
        
        virtual bool isRunning(timestamp_t const &) const;
	    
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    protected:

        std::shared_ptr<Model> model_;
        std::shared_ptr<ConfigMap> config_;

        timestamp_t start_time_;
        double trajectory_time_ratio_;
        double trajectory_time_ratio2_;
        double trajectory_time_ratio3_;
        double trajectory_time_ratio4_;
        double trajectory_time_ratio5_;
        double trajectory_duration_;

        std::mutex state_lock_;
        bool run_;
        CartesianState start_state_;
        CartesianState final_state_;
        CartesianState state_;

};


} // namespace pbwbc
#endif // PBWBC_CARTESIANTRAJECTORYGENERATOR_HPP
