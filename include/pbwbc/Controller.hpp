#ifndef PBWBC_CONTROLLER_HPP
#define PBWBC_CONTROLLER_HPP
#include <pbwbc/Base.hpp>

namespace pbwbc {

//! \brief Controller abstract base class.
//!
//! Concept: 
//! A controller which computes a torque. With function
//! tau_d = f(y,dy)
//!
//! The major API is:
//! - start()
//! - update()
//! - stop()
//! 
//!
//! Asynchronyous requests for changing the internal state
//! of a controller can be made through the "motion" API:
//! - motionEntities()
//! - getMotionState()
//! - startMotion()
//! - isMotionRunning()
//! - stopMotion()
//!
//! For using a controller in a strict Hierarchy (implemented in
//! HierarchyController) the controller needs to implement:
//! - hasTaskForces()
//! - getTaskForces()
//! - hasTaskJacobian()
//! - getTaskJacobian()
class PBWBC_EXPORT Controller {
public:
    //! Constructor
    //! \param name Name of the controller
    //! \param dy_size Size of the torque vector which the controller computes
    Controller(std::string const & name, int dy_size);

    virtual ~Controller();

    //! \brief start() must be called once directly before starting the real-time 
    //! loop which continously calls update().
    //!
    //! Code in start() must be real-time safe
    virtual void start(timestamp_t const &);

    //! \brief stop() must be called once directly after stopping the real-time 
    //! loop which continously calls update()
    //!
    //! Code in start() must be real-time safe
    virtual void stop(timestamp_t const &);

    //! \brief update() is called to compute the torque values
    //! 
    //! Code in update() must be real-time safe. The code should not expect to
    //! be called at fixed time intervals.
    virtual void update(timestamp_t const &) = 0;
   
    //! \brief get the name of the controller 
    std::string const & name();

    //! \brief get the compute torque values
    //!
    //! Only valid after calling update()
    Eigen::VectorXd const & get_tau() const;

    //! \brief test if getTaskForces is implemented by this controller
    virtual bool hasTaskForces() const;
    //! \brief return vector of task forces
    //!
    //! Results are valid after calling update()
    virtual Eigen::VectorXd const & getTaskForces() const;

    //! \brief test if getTaskJacobian is implemented by this controller
    virtual bool hasTaskJacobian() const;
    //! \brief return task jacobian
    //!
    //! Results are valid after calling update()
    virtual Eigen::MatrixXd const & getTaskJacobian() const;

    //! report a list a controllable entities for the motion API
    virtual std::vector<std::string> motionEntities();
    //! get current desired state of the \p entity
    virtual Eigen::VectorXd const & getMotionState(std::string const & entity);
    //! \brief start a motion for an \p entity
    virtual void startMotion(timestamp_t const &, std::string const &, Eigen::Ref<Eigen::VectorXd> const &, double const duration, bool absolute);
    //! \brief query if \p entity is in motion
    virtual bool isMotionRunning(timestamp_t const &, std::string const & entity);
    //! \brief stop motion \p entity
    virtual void stopMotion(timestamp_t const &, std::string const & entity);

protected:
    std::string name_; //!< name of the controller
    Eigen::VectorXd tau_d_; //!< buffer for the output torque returns by get_tau()
};


//! \brief Exception which signals when a controller detects a problem
//! which will result in an unstable system
class PBWBC_EXPORT ControllerException : public std::runtime_error {
public:
    ControllerException(const std::string & what);
};


} // namespace pbwbc

#endif // PBWBC_CONTROLLER_HPP
