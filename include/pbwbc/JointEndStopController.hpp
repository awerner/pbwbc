#ifndef PBWBC_JOINTENDSTOPCONTROLLER_HPP
#define PBWBC_JOINTENDSTOPCONTROLLER_HPP
#include <pbwbc/Controller.hpp>
#include <pbwbc/Model.hpp>
#include <pbwbc/State.hpp>
#include <pbwbc/Config.hpp>

namespace pbwbc {

//! \brief Controller which realizes repulsive potential field at joint limits
//!
//! This controller realizes a torque which keeps the joint within specified joint limits.
//! The torque is computed using a potential function. Additionally a damping torque
//! is computed to make the system stable. The damping is computed using the diagonal
//! elements of the inertia matrix. The joint limits are extracted from model.
//! When the joint is moved to the joint limit, the maximum torque defined in the model
//! for this joints is applied.
//!
//! For each joint the joint limits can be adopted.
//!
//! Minimal settings for the yaml file:
//! \code{.yaml}
//!      JointEndStopController:                                                                      # controller name
//!        type: "JointEndStopController"                                                             # controller type
//!        impedance_controlled_joint_names: [arm_left_4_joint, arm_right_4_joint, arm_right_5_joint] # list of joint for which the torque is computed
//! \endcode
//!
//! Complete settings:
//! \code{.yaml}
//!      JointEndStopController:
//!        type: "JointEndStopController"
//!        impedance_controlled_joint_names: [arm_left_4_joint, arm_right_4_joint, arm_right_5_joint]
//!        endstop_offset: 0. # TODO
//!        endstop_distance: 0.1 # offset from the joint limit at which the potential field starts
//!        damping_ratio: 1. # default value, can be override per joint
//!        joints:
//!            arm_right_4_joint: { position_min: -1.0, position_max: -0.1, damping_ratio: 0.2 } # override default settings for each joint
//!            arm_left_4_joint: { position_min: -1.0, position_max: -0.1 }
//! \endcode
class PBWBC_EXPORT JointEndStopController : public Controller {
public:
    JointEndStopController(
            std::shared_ptr<RobotState> const & state,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config
            );

    virtual ~JointEndStopController();

    virtual void start(timestamp_t const &);
    virtual void stop(timestamp_t const &);
    virtual void update(timestamp_t const &);
    
    bool hasTaskJacobian() const;
    Eigen::MatrixXd const & getTaskJacobian() const;

protected:
    std::shared_ptr<RobotState> state_; //!< current state in configuration space
    std::shared_ptr<Model> model_; //!< model implementation
    std::shared_ptr<ConfigMap> config_; //!< configuration access
    
    // copies of model values to allow override
    typedef Eigen::VectorXd vector_t;
    //! Minimal position of the joint at which the maximum_torque_ specified for this joint is applied
    vector_t position_min_;
    //! Maximal position of the joint at which the maximum_torque_ specified for this joint is applied
    vector_t position_max_;
    //! Maximal torque applied for this joint
    vector_t maximum_torque_;

    double endstop_offset_;
    //! offset from the joint limits position_min_ and position_max_ at which the repulsive potential starts
    double endstop_distance_;
    
    //! damping ratios
    vector_t damping_ratio_;
    
    //! list of joints for which the torque is computed
    std::vector<int> impedance_controlled_joints_ids_;

    //! buffer for the task jacobian
    Eigen::MatrixXd task_jacobian_;

    //! counter for smooth activation of potential fields at startup
    int counter_;

    //! \brief Potential function first derivative
    /**
     * \f[
     *  \frac{\tau_\text{max}}{s^2} (s - d)^2
     * \f]
     */
    double potential_d1(double d, int idx);
    //! \brief Potential function second derivative
    /**
     * \f[
     *  2 \frac{\tau_\text{max}}{s^2} (s - d)
     * \f]
     */
    double potential_d2(double d, int idx);
};


} // namespace pbwbc
#endif // PBWBC_JOINTENDSTOPCONTROLLER_HPP
