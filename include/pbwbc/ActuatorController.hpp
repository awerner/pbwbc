#ifndef PBWBC_ACTUATORCONTROLLER_HPP
#define PBWBC_ACTUATORCONTROLLER_HPP
#include <pbwbc/Algebra.hpp>
#include <pbwbc/Base.hpp>
#include <pbwbc/Config.hpp>
#include <pbwbc/State.hpp>
#include <pbwbc/Model.hpp>

namespace pbwbc {

// ABC for any type of actuator controller
// realize a desired torque for a single joint
class PBWBC_EXPORT ActuatorController {
public:
    ActuatorController();

    virtual ~ActuatorController();

    virtual void start(timestamp_t const &);
    virtual void stop(timestamp_t const &);

    virtual void update(timestamp_t const &, double tau_d) = 0;
protected:
};


// realizes a torque interface using an admittance controller 
// with a FTS and using another joint
// * Manages Torque offset
// * Uses FTS class which provides a filter
class PBWBC_EXPORT ActuatorAdmittanceController : public ActuatorController {
public:
    ActuatorAdmittanceController(
            std::string const & joint_name,
            std::shared_ptr<RobotState> const & state,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config,
            std::shared_ptr<SensorState> const & sensor_state,
            std::shared_ptr<ActuatorController> const & underlying_controller
            );
    virtual ~ActuatorAdmittanceController();

    void start(timestamp_t const &);
    void stop(timestamp_t const &);

    void update(timestamp_t const &, double tau_d);

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
protected:
    std::string joint_name_;
    std::shared_ptr<RobotState> state_;
    std::shared_ptr<Model> model_;
    std::shared_ptr<ConfigMap> config_;
    std::shared_ptr<SensorState> sensor_state_;
    std::shared_ptr<ActuatorController> underlying_controller_;

    timestamp_t last_update_;
    double K_desired_;
    double K_;
    double I_;
    double integral_store_;
    double integral_windup_;
    double ft_filter_gain_;
    double feedforward_gain_;
    int joint_idx_;
    bool dq_throttle_enabled_;
    double dq_max_;
    double dq_throttle_;
    double command_filter_gain_;

    int fts_id_;
    int fts_frame_id_;
    int joint_frame_id_;
    int joint_axis_;
    int joint_direction_;

    Eigen::Transform<double,3,Eigen::Isometry> world_H_fts_;
    Eigen::Transform<double,3,Eigen::Isometry> world_H_joint_;
    wrench_t measured_wrench_;
    wrench_t filtered_wrench_;
    wrench_t projected_wrench_;
    double tau_projected_;
    double tau_actuator_d_;
    double command_filter_state_;
};


// Does nothing controller for joints which are not controlled
class PBWBC_EXPORT JointNullController : public ActuatorController {
    JointNullController();

    virtual ~JointNullController();

    void update(timestamp_t const &, double tau_d);
};


class PBWBC_EXPORT FakeActuator : public ActuatorController {
public:
    FakeActuator(double & variable) :
    variable_(variable) {}

    void update(timestamp_t const &, double tau_d) {
        variable_ = tau_d;
    }

protected:
    double & variable_;
};


// Uses the motor current interface to realize a torque
// TODO: needs a way to access joint torque sensor
//class JointMotorTorqueController : public ActuatorController {
//    void start(timestamp_t const void start());
//    void stop(timestamp_t const void update(timestamp_t const void stop()));
//
//    void update(double tau_d);
//}

} // namespace pbwbc

#endif // PBWBC_ACTUATORCONTROLLER_HPP
