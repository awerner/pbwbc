#ifndef PBWBC_ROSCONFIG_HPP
#define PBWBC_ROSCONFIG_HPP
#include <pbwbc/Config.hpp>
#include <ros/ros.h>

namespace pbwbc {

  class PBWBC_EXPORT ROSConfigMap : public ConfigMap {
      public:
          ROSConfigMap(ros::NodeHandle & nh) : controller_nh(nh) {}
          virtual ~ROSConfigMap() {}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define param(T) \
          void param(std::string const & parameter_name, T & variable, T const & default_value ) const;
          param(int)
          param(bool)
          param(double)
          param(std::string)
          param(std::vector<std::string>)
#undef param

#define getParam(T) \
          bool getParam(std::string const & parameter_name, T & variable) const;
          getParam(int)
          getParam(bool)
          getParam(double)
          getParam(std::string)
          getParam(std::vector<std::string>)
          getParam(std::vector<double>)
          getParam(std::vector<int>)
#undef getParam
#endif // DOXYGEN_SHOULD_SKIP_THIS

          std::shared_ptr<ConfigMap> submap(std::string const & prefix);
          bool list_items(std::string const & parameter, std::vector<std::string> & keys );
      protected:
          ros::NodeHandle controller_nh;
          std::string prefix;
  };

}

#endif
