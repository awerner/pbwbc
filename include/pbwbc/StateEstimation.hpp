#ifndef PBWBC_STATEESTIMATION_HPP
#define PBWBC_STATEESTIMATION_HPP
#include <pbwbc/State.hpp>
#include <memory>

namespace pbwbc {

//! \brief Abstract base class for state estimation
//!
//! The state estimation fills only the base state part of the state
class PBWBC_EXPORT StateEstimation {
public:
    StateEstimation(){};
    virtual ~StateEstimation(){};

    virtual void start(timestamp_t const &) {};
    virtual void stop(timestamp_t const &) {};
    virtual void update(timestamp_t const &) = 0;

    //! obtain the state
    virtual std::shared_ptr<RobotState> const & state() = 0;
};

} // namespace

#endif // PBWBC_STATEESTIMATION_HPP
