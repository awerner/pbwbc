#pragma once
#include <pbwbc/MotionOrchestrator.hpp>
#include <pbwbc/ROSTools.hpp>
#include <pbwbc/AddAction.h>

namespace pbwbc {


/**
 * extends MotionOrchestrator by adding a ROS service to
 * add actions on the fly
 *
 * To add an action
 * ```bash
 * rosservice call /pbwbc_controller/addAction "name: {data: ''}
 * controller: {data: 'MultiContactForceDistribution'}
 * entity: {data: 'com'}
 * destination: [0,0,0,0,0,0]
 * absolute: false
 * start_time: 0.0
 * duration: 0.0"
 * ```
 *
 */
class PBWBC_EXPORT MotionOrchestratorROS : public MotionOrchestrator {
public:
    MotionOrchestratorROS(
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config,
            std::vector<std::shared_ptr<Controller>> const & controllers,
            ros::NodeHandle const &);
    virtual ~MotionOrchestratorROS();

protected:
    ros::NodeHandle nh_;
   
    ros::ServiceServer addAction_service_; 
    bool callback_AddAction (
          pbwbc::AddActionRequest & request,
          pbwbc::AddActionResponse & response);

};

}


