#ifndef PBWBC_STANDALONECONFIG_HPP
#define PBWBC_STANDALONECONFIG_HPP
#include <pbwbc/Config.hpp>
#include <memory>
#include <map>
#include <boost/variant.hpp>

namespace pbwbc {

class PBWBC_EXPORT StandaloneConfigMap : public pbwbc::ConfigMap {
public:
    virtual ~StandaloneConfigMap() {}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define param(T) \
    void param(std::string const & parameter_name, T & variable, T const & default_value ) const { \
        if(map_.find(parameter_name)!=map_.end()) { \
            variant_t const & v = map_.at(parameter_name); \
            variable = boost::get<T>(v); \
        } else { \
            variable = default_value; \
        } \
    }
param(int)
param(double)
param(std::string)
#undef param

#define getParam(T) \
    bool getParam(std::string const & parameter_name, T & variable) const { \
        if(map_.find(parameter_name)!=map_.end()) { \
            variant_t const & v = map_.at(parameter_name); \
            variable = boost::get<T>(v); \
            return true; \
        } else { \
            return false; \
        } \
    }
getParam(int)
getParam(double)
getParam(std::string)
#undef getParam
    template <typename T>
    void add(std::string const & parameter_name, T const & value) {
        map_[parameter_name] = variant_t(value);
    }

# define getParam(T) \
    bool getParam(std::string const & parameter_name, std::vector<T> & variable) const  {\
        std::string str;\
        if(!this->getParam(parameter_name,str)) {\
            return false;\
        }\
        std::istringstream sstr(str);\
        std::string token;\
        variable.clear();\
        while(std::getline(sstr, token, ',')) {\
            T el;\
            std::istringstream elsstr(token);\
            elsstr >> el;\
            if(elsstr.fail()) break; \
            variable.push_back(el);\
        }\
        return true;\
    }
    
    getParam(int)
    getParam(double)
    getParam(std::string)
#undef getParam
#endif // DOXYGEN_SHOULD_SKIP_THIS
    
protected:
    typedef boost::variant<int,double,std::string> variant_t;
    std::map<std::string,variant_t> map_;
};
}

#endif // PBWBC_STANDALONECONFIG_HPP
