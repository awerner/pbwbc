#ifndef PBWBC_ROSSTATEESTIMATION_HPP
#define PBWBC_ROSSTATEESTIMATION_HPP
#include <pbwbc/StateEstimation.hpp>
#include <ros/ros.h>
#include <pbwbc/State.hpp>
#include <pbwbc/Config.hpp>
#include <mutex>
#include <pbwbc/Model.hpp>
#include <pbwbc/ROSTools.hpp>
#include <nav_msgs/Odometry.h>

namespace pbwbc {

//! \brief Subscribes to /floating_base_pose_simulation to fake a base state estimation
//!
//! Publishes to base_link pose via tf using RealTimeTransformBroadcaster
class PBWBC_EXPORT ROSStateEstimation : public StateEstimation {
public:
    ROSStateEstimation(
            std::shared_ptr<ConfigMap> const & config,
            std::shared_ptr<Model> const & model,
            ros::NodeHandle const & nh
            );

    virtual ~ROSStateEstimation();

    virtual void start(timestamp_t const &);
    virtual void stop(timestamp_t const &);

    virtual void update(timestamp_t const &);

    std::shared_ptr<RobotState> const & state();
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
protected:
    ros::Subscriber subscriber_;
    void callback(const nav_msgs::Odometry::ConstPtr & msg);
    ros::NodeHandle nh_;
    std::shared_ptr<Model> model_;
    std::shared_ptr<RobotState> state_;
    bool initialized_;
    std::mutex mutex_;
    CartesianState base_state_;
    std::shared_ptr<RealTimeTransformBroadcaster> tf_broadcaster_;
};


} //namespace

#endif
