#ifndef PBWBC_PBWBC_HPP
#define PBWBC_PBWBC_HPP
#include <pbwbc/Base.hpp>
#include <pbwbc/ActuatorController.hpp>
#include <pbwbc/Config.hpp>
#include <pbwbc/Controller.hpp>
#include <pbwbc/MotionOrchestrator.hpp>

namespace pbwbc {

//! \brief Contains multiple Controller objects (without hierarchy) and ActuatorController objects for all joints.
//!
//! purpose: contain all controllers do things like
//! - compute controllers (start,update,stop)
//! - accumulating the torques from each controller
//! - send torque to actuators -> actuatorController
class PBWBC_EXPORT PBWBC {
public:
    PBWBC(std::shared_ptr<RobotState> const &,
          std::shared_ptr<SensorState> const &,
          std::shared_ptr<Model> const &,
          std::shared_ptr<ConfigMap> const &config,
          std::vector<std::shared_ptr<ActuatorController>> const & actuatorcontrollers);

    virtual ~PBWBC();

    void addController(ConfigMap const & config); // create from config file entry

    void addController(std::shared_ptr<Controller> const & controller); // check and append

    void addMotionOrchestrator();

    std::vector<std::shared_ptr<Controller>> const & getControllers() const;

    virtual void start(timestamp_t const &);

    virtual void stop(timestamp_t const &);

    virtual void update(timestamp_t const &);

    Eigen::VectorXd const & getTau();

    virtual void initialize();
protected:
    std::shared_ptr<RobotState> state_;
    std::shared_ptr<SensorState> sensor_state_;
    std::shared_ptr<Model> model_;
    std::shared_ptr<ConfigMap> config_;
    Eigen::VectorXd tau_offsets_;

    std::vector<std::shared_ptr<Controller>> controllers_; // -> compute tau

    std::vector<std::shared_ptr<ActuatorController>> actuatorcontrollers_; // -> realize tau

    std::shared_ptr<MotionOrchestrator> motionOrchestrator_; //-> provide reference trajectories
    

    Eigen::VectorXd tau_d_;
};


} // namespace 

#endif // PBWBC_PBWBC_HPP
