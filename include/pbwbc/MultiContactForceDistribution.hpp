#ifndef PBWBC_MULTICONTACTBALANCER_HPP
#define PBWBC_MULTICONTACTBALANCER_HPP
#include <pbwbc/CartesianImpedanceController.hpp>
#include <pbwbc/Controller.hpp>
#include <pbwbc/Config.hpp>
#include <pbwbc/Model.hpp>
#include <pbwbc/Base.hpp>
#include <pbwbc/State.hpp>
#include <chrono>
#include <pbwbc/ModelInComCoordinates.hpp>

namespace pbwbc {



//! Describes an abstract Contact for use in the MultiContactForceDistribution
//!
class PBWBC_EXPORT Contact {
    public:
        Contact(std::string const & name);
        virtual ~Contact();

        virtual Eigen::Vector3d translation() const = 0;
        virtual Eigen::Quaterniond orientation() const = 0;
        virtual void Jacobian(Eigen::Ref<Eigen::MatrixXd> ) const = 0;
        // Constraints defines as CIT * wrench + ci0 >= 0
        virtual void contactConstraints(Eigen::Ref<Eigen::MatrixXd>, Eigen::Ref<Eigen::VectorXd> ) = 0;
        virtual int constraintsNum() const = 0;

        enum contact_state_t {
            contact_state_off = 0,
            contact_state_full = 1,
            contact_state_custom = 100 // custom contact states start here
        };
        typedef contact_state_t contact_state_t;

        virtual void setState(int state) = 0;
        virtual int getStateRequest() = 0;
        virtual int getState() = 0;
        virtual void updateStateMachine(timestamp_t const & timestamp) = 0;
        virtual void incrementStateTimer(timestamp_t const & timestamp) = 0;
        virtual wrench_t const & measuredWrench() = 0;
        virtual void setDesiredWrench(wrench_t const & wrench) = 0;
        virtual wrench_t const & taskForce() = 0;
        typedef Eigen::Matrix<double,6,1> weights_t;
        virtual weights_t const & weights() = 0;
        virtual double const & regularizationWeight() = 0;
        std::string const & name() const;
        virtual int getFrameId() const = 0;

        virtual void start(timestamp_t const &);
        virtual void update(timestamp_t const &) = 0;
        virtual void stop(timestamp_t const &);

        std::shared_ptr<CartesianImpedanceController> const & cartesianController();

        virtual void validateContact(std::shared_ptr<RobotState> const & RobotState, 
                const Eigen::Ref<const Eigen::MatrixXd> & contact_jacobian) = 0;

        void startMotion(timestamp_t const &, std::string const &, Eigen::Ref<Eigen::VectorXd> const &, double const duration, bool absolute);
        bool isMotionRunning(timestamp_t const &, std::string const & entity);
        void stopMotion(timestamp_t const &, std::string const & entity);
        Eigen::VectorXd const & getMotionState(std::string const & entity);

    protected:
        std::shared_ptr<CartesianImpedanceController> cartcontroller_;
        std::string name_;
        
};



//! \brief Implements a surface contact.
//!
//! This class
//! - implements a 6D surface contact, with contact constraints
//! - integrates a Cartesian impedance controller, to be used when the contact is not in effect
//! - implements a contact state machine to allow switching between in contact and free floating
//! The name is obviously bad, but we will work with that for now
class PBWBC_EXPORT Contact6DSurface : public Contact {
    public:
        Contact6DSurface(std::string const & name,
                std::shared_ptr<Model> const & model,
                std::shared_ptr<ConfigMap> const & config,
                std::shared_ptr<SensorState> const & sensor_state,
                std::shared_ptr<CartesianImpedanceController> const &);

        void start(timestamp_t const &);
        void update(timestamp_t const &);

        Eigen::Vector3d translation() const;
        Eigen::Quaterniond orientation() const;
        void Jacobian(Eigen::Ref<Eigen::MatrixXd> ) const;

        // linear contact force inequalities:
        // CIT * W + ci0 >= 0
        void contactConstraints(Eigen::Ref<Eigen::MatrixXd> contact_constraints_CIT, 
                                 Eigen::Ref<Eigen::VectorXd> contact_constraints_ci0);
        int constraintsNum() const;
        
        enum contact_state_custom_t {
            contact_state_enabling_normal = contact_state_custom + 0,
            contact_state_disabling_normal = contact_state_custom + 1,
            contact_state_normal = contact_state_custom + 2,
            contact_state_enabling_full = contact_state_custom + 3,
            contact_state_disabling_full = contact_state_custom + 4,
        };
        void setState(int state);
        int getStateRequest();
        void updateStateMachine(timestamp_t const & timestamp);
        int getState();
        void incrementStateTimer(timestamp_t const & timestamp);
        int getFrameId() const;

        wrench_t const & measuredWrench();
        void setDesiredWrench(wrench_t const & wrench);
        wrench_t const & taskForce();
        weights_t const & weights();
        double const & regularizationWeight();
        
        void validateContact(std::shared_ptr<RobotState> const & RobotState, 
                const Eigen::Ref<const Eigen::MatrixXd> & contact_jacobian);

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    protected:
        std::shared_ptr<Model> model_;
        std::shared_ptr<ConfigMap> config_;
        std::shared_ptr<SensorState> sensor_state_;
        
        static const int constraints_num_ = 11;
        int contact_state_;
        int init_contact_state_;
        double contact_request_;
        double contact_state_timer_;
        timestamp_t contact_state_start_time_;
        bool contact_auto_switch_;
        double contact_auto_enabling_normal_force_min_;
        double contact_auto_disabling_normal_force_max_;
        double contact_auto_enabling_normal_force_;
        double contact_auto_disabling_full_force_max_;
        double contact_auto_enabling_full_force_min_;
        double contact_max_position_weight_;
        double contact_state_enabling_normal_time_;
        double contact_state_disabling_normal_time_;
        double contact_state_enabling_full_time_;
        double contact_state_disabling_full_time_;
        double contact_fts_settling_time_;
       
        double regularization_weight_;
        double regularization_max_;

        weights_t weights_;
        weights_t weights_max_;

        int contact_velocity_error_counter_;
        int contact_force_error_counter_;
        double contact_velocity_max_;
        double contact_force_min_;
        int contact_velocity_max_errors_;
        int contact_force_max_errors_;
        double contact_mu_;
        double zmp_x_positive_;
        double zmp_x_negative_;
        double zmp_y_positive_;
        double zmp_y_negative_;
        double constraint_normal_force_min_;
        double tau_z_max_;

        int frame_id_;
        int ft_sensor_id_;

        wrench_t measured_wrench_;
        wrench_t desired_wrench_;

        Eigen::MatrixXd CIT_;
        Eigen::VectorXd ci0_;
};

// TODO
// class Contact6DMultiMode
// implemented edge and corner contact cases


//! \brief A controller which stabilized the COM and the base link orientation using available
//!         contacts.
//! 
//! 
//! A cartesian impedance controller stabilized the COM and the base link orientation.
//! The forces this controller requires are realized using available contacts while
//! adhering to the contact constraints. When contacts are not active, their
//! state is stabilized by an CartesianImpedanceController.
//! To find all end effector forces, a quadratic program is solved which considers the contact
//! constraints.
//!
//! Based on:
//! - Henze et al. "Passivity-based whole-body balancing for torque-controlled humanoid robots in multi-contact scenarios" IJRR 2016
//!
//! Some people describe this as "kind of a CartesianTaskHierachyController with only soft priorities".
class PBWBC_EXPORT MultiContactForceDistribution : public Controller {
public:
    MultiContactForceDistribution(
            std::shared_ptr<RobotState> const &,
            std::shared_ptr<SensorState> const &,
            std::shared_ptr<Model> const &,
            std::shared_ptr<ConfigMap> const &);

    virtual ~MultiContactForceDistribution();

    std::vector<std::shared_ptr<Contact>> const & getContacts() const;

    virtual void start(timestamp_t const &);
    virtual void stop(timestamp_t const &);
    virtual void update(timestamp_t const &);
    
    bool hasTaskJacobian()const;
    Eigen::MatrixXd const & getTaskJacobian() const;

    void setDesiredComState(CartesianState::position_state const & pos,
        CartesianState::velocity_state const & vel,
        CartesianState::velocity_state const & acc);

    CartesianState const & getCurrentComState() const;
    CartesianState const & getDesiredComState() const;

    std::vector<std::string> motionEntities();
    void startMotion(timestamp_t const &, std::string const &, Eigen::Ref<Eigen::VectorXd> const &, double const duration, bool absolute);
    bool isMotionRunning(timestamp_t const &, std::string const & entity);
    void stopMotion(timestamp_t const &, std::string const & entity);
    Eigen::VectorXd const & getMotionState(std::string const & entity);

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    enum damping_mode_t {
        damping_value = 0,
        damping_ratio = 1,
        damping_double_diagonalization = 2
    };
protected:
    //! \brief a container for qpOASES implementation details which should not be part of the API
    class qpOASES_impl;
    //! \brief a container for eiquadprog implementation details which should not be part of the API
    class eiquadprog_impl;
    void initialize();
    void balancerController(timestamp_t const & timestamp, Eigen::Matrix<double,6,1> & balancer_wrench );
    void updateBalancerObserverResidual();
    void updateBalancerObserverState();
    int constraintsNum() const;

    void solveQP();
    
    typedef Model::translation_t translation_t;
    typedef Model::orientation_t orientation_t;

    std::shared_ptr<RobotState> state_;
    std::shared_ptr<Model> model_;
    std::shared_ptr<ModelInComCoordinates> modelInComCoordinates_;
    std::shared_ptr<ConfigMap> config_;
    
    std::shared_ptr<CartesianTrajectoryGenerator> trajectory_generator_;
    std::vector<std::shared_ptr<Contact>> contacts_;

    int base_frame_id_;

    Eigen::Matrix<double,6,1> K_balancer_;
    Eigen::Matrix<double,6,1> K_balancer_des_;
    double K_balancer_step_;
    Eigen::Matrix<double,6,1> D_balancer_;
    Eigen::Matrix<double,6,1> damping_ratio_balancer_;
    damping_mode_t damping_mode_;
    Eigen::Matrix<double,6,1> I_balancer_;
    Eigen::Matrix<double,6,1> balancer_ierror_;
    Eigen::Matrix<double,6,1> balancer_ierror_windup_;
    Eigen::Matrix<double,3,1> balancer_observer_state_;
    Eigen::Matrix<double,6,1> balancer_com_weight_;
    double balancer_regularization_;
    Eigen::Vector3d balancer_observer_residual_;
    double balancer_observer_gain_;
    Eigen::Matrix<double,6,1> balancer_observer_feedback_gain_;
    double balancer_ierror_velocity_threshold_;
    std::string solver_;

    Eigen::Matrix<double,6,1> gravity_;
    double balancer_output_enable_des_;
    double balancer_output_enable_;

    //Constraints variables
    Eigen::MatrixXd contact_jacobian_;
    Eigen::MatrixXd base_jacobian_;
    mutable Eigen::MatrixXd task_jacobian_;
    Eigen::MatrixXd JT_;
    Eigen::MatrixXd G_;
    Eigen::MatrixXd G_temp_;
    Eigen::VectorXd g0_;
    Eigen::MatrixXd CE_;
    Eigen::VectorXd ce0_;
    Eigen::MatrixXd CIT_;
    Eigen::VectorXd ci0_;
    Eigen::VectorXd W_C_desired_;
    Eigen::Matrix<double,6,1> W_x_realized_;

    //qpOASES variables
    std::shared_ptr<qpOASES_impl> qpOASES_impl_;
    bool qpOASES_solver_initialized_;
    int nWSR_;
    Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> qpOASES_A_;
    Eigen::VectorXd qpOASES_lbA_;
    Eigen::VectorXd qpOASES_ubA_;
    Eigen::VectorXd qpOASES_lb_;
    Eigen::VectorXd qpOASES_ub_;

    std::shared_ptr<eiquadprog_impl> quadprog_impl_;

    wrench_t balancer_wrench_;
    CartesianState cartesian_state_;
    CartesianState state_d_;

    CartesianState state_initial_; //pose at start
    CartesianState goal_state_; //desired end pose
    Eigen::Vector3d state_translation_offset_d_; //user-defined translation offset
    Eigen::Vector3d state_orientation_offset_euler_d_; //user-defined orientation offset
    int update_pose_; // user-defined switch to trigger the update of goal_state_

    
    timestamp_t last_execution_timestamp_;
    double dt_;

    std::string simulation_mode_;
    
};


} // namespace pbwbc
#endif // PBWBC_MULTICONTACTBALANCER_HPP
