#ifndef PBWBC_CARTESIANIMPEDANCECONTROLLERROS_HPP
#define PBWBC_CARTESIANIMPEDANCECONTROLLERROS_HPP
#include <pbwbc/CartesianImpedanceController.hpp>
#include <ros/ros.h>
#include <pbwbc/ROSTools.hpp>
#include <geometry_msgs/WrenchStamped.h>
#include <pbwbc/StateStamped.h>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>

namespace pbwbc {

//! \brief ROS Wrapper for the CartesianImpedanceController
//!
//! Exposes via dynamic_reconfigure:
//! - stiffness
//! - damping
//! - goal position
//! - stiffness step max
class PBWBC_EXPORT CartesianImpedanceControllerROS : public CartesianImpedanceController {
public:
    CartesianImpedanceControllerROS(
            std::shared_ptr<RobotState> const & state,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config,
            ros::NodeHandle const & nh
            );
    
    CartesianImpedanceControllerROS(
            CartesianImpedanceController const &,
            ros::NodeHandle const & nh
            );

    virtual ~CartesianImpedanceControllerROS();


    virtual void update(timestamp_t const &);

    
    // listen to interpolator soft stop
    // publish
    // subscribe goal
    // have interpolator interface
    // have JTC interface
    // have a trajectoryplayer interface for joint and cart space
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
protected:
    void init();
    ros::NodeHandle nh_;
    RealTimePublisher<pbwbc::StateStamped> desired_state_publisher_;
    RealTimePublisher<pbwbc::StateStamped> current_state_publisher_;
    RealTimePublisher<geometry_msgs::WrenchStamped> wrench_publisher_;

    std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr_;

};



} // namespace pbwbc
#endif // PBWBC_CARTESIANIMPEDANCECONTROLLERROS_HPP
