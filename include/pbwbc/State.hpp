#ifndef PBWBC_STATE_HPP
#define PBWBC_STATE_HPP
#include <pbwbc/Base.hpp>
#include <map>

namespace pbwbc {

//! \brief State in configuration space
class PBWBC_EXPORT RobotState {
public:
    RobotState(int y_size, int dy_size);
    ~RobotState();

    typedef Eigen::VectorXd state_t;
    state_t y;
    state_t dy;
    state_t ddy;
};

//! \brief Cartesian state of a rigid body
class PBWBC_EXPORT CartesianState {
public:
    class position_state {
    public:
        Eigen::Vector3d translation;
        Eigen::Quaternion<double,Eigen::DontAlign> orientation;
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    } position;
    class velocity_state {
    public:
        Eigen::Vector3d translation;
        Eigen::Vector3d orientation;
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    } velocity,acceleration;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

//! \brief Imu measurements
class PBWBC_EXPORT ImuState {
public:
    Eigen::Vector3d translation;
    Eigen::Quaterniond orientation;
    Eigen::Vector3d linearVelocity;
    Eigen::Vector3d angularVelocity;
    Eigen::Vector3d linearAcceleration;
    Eigen::Vector3d angularAcceleration;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

//! \brief Collection of sensor measurements
//!
//! Tuples store:
//! - frame_id of the sensor
//! - name of the sensor
//! - current measurement
class PBWBC_EXPORT SensorState {
public:
    std::vector<std::tuple<int,std::string,wrench_t>> ftsWrenches;
    std::vector<std::tuple<int,std::string,ImuState>> imuStates;
    Eigen::VectorXd jointTorques;
    Eigen::VectorXd jointTemperatures;
};

} // namespace pbwbc
#endif // PBWBC_STATE_HPP
