#pragma once
#include <pbwbc/SelfCollisionAvoidanceController.hpp>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <pbwbc/ROSTools.hpp>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>

namespace pbwbc {

//! \brief ROS Wrapper for SelfCollisionAvoidanceControllerROS
//!
//! Exposes via dynamic_reconfigure:
//! - start distance (sd_COLLISION_PAIR_NAME)
//! - maximum force
//! - damping
class PBWBC_EXPORT SelfCollisionAvoidanceControllerROS : public SelfCollisionAvoidanceController {
public:
    SelfCollisionAvoidanceControllerROS(
            std::shared_ptr<RobotState> const &,
            std::shared_ptr<Model> const &,
            std::shared_ptr<ConfigMap> const &config,
            std::string const & urdf_contents,
            ros::NodeHandle const & nh
            );
    virtual ~SelfCollisionAvoidanceControllerROS();

    void update(timestamp_t const &);

protected:
    ros::NodeHandle nh_;
    RealTimePublisher<sensor_msgs::JointState> desired_torque_publisher_;
    sensor_msgs::JointState msg;
    
    std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr_;
};

} // namespace pbwbc

