#pragma once
#include <pbwbc/JointEndStopController.hpp>
#include <sensor_msgs/JointState.h>
#include <pbwbc/ROSTools.hpp>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>

namespace pbwbc {

// TODO:
// * implement JointTrajectoryController interface
class PBWBC_EXPORT JointEndStopControllerROS : public JointEndStopController {
public:
    JointEndStopControllerROS(
            std::shared_ptr<RobotState> const &,
            std::shared_ptr<Model> const &,
            std::shared_ptr<ConfigMap> const &config,
            ros::NodeHandle const & nh
            );
    virtual ~JointEndStopControllerROS();

    void update(timestamp_t const &);

protected:
    ros::NodeHandle nh_;
    RealTimePublisher<sensor_msgs::JointState> desired_torque_publisher_;
    
    std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr_;
};


} // namespace

