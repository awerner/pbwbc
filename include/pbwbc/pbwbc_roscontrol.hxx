#pragma once
// from roscontrol-pbwbc-controller

#include <fstream>
#include <iomanip>
#include <dlfcn.h>
#include <sstream>
#include <thread>
#include <chrono>

#include <pluginlib/class_list_macros.h>

#include <ros/console.h>
#include <pbwbc/pbwbc_roscontrol.hpp>
#include <pbwbc/ModelPinocchio.hpp>

#include <pbwbc/JointImpedanceControllerROS.hpp>
#include <pbwbc/CartesianImpedanceControllerROS.hpp>
#include <pbwbc/MultiContactForceDistributionROS.hpp>
#include <pbwbc/HierarchyController.hpp>
#include <pbwbc/JointEndStopControllerROS.hpp>
#include <pbwbc/JointVelocityLimiterROS.hpp>
#include <pbwbc/ActuatorControllerROS.hpp>


#define HAVE_HPP_FCL
#ifdef HAVE_HPP_FCL
#include <pbwbc/SelfCollisionAvoidanceControllerROS.hpp>
#endif
#include <pbwbc/ROSStateEstimation.hpp>
#include <pbwbc/RealSenseStateEstimationROS.hpp>
#include <fenv.h>
#include <sys/resource.h>

//#define CATCH_EXCEPTIONS

/// lhi: nickname for local_hardware_interface
/// Depends if we are on the real robot or not.

namespace lhi = ::hardware_interface;
using namespace lhi;

namespace pbwbc  
{
  typedef std::map<std::string,std::string>::iterator it_map_rt_to_pbwbc;

  EffortControlPDMotorControlData::EffortControlPDMotorControlData() {
    prev = 0.0; vel_prev = 0.0; des_pos=0.0;
    integ_err=0.0; 
  }
  
  void EffortControlPDMotorControlData::read_from_xmlrpc_value
  (const std::string &prefix) {
    pid_controller.initParam(prefix);
  }
  
  template<typename T> 
  PBWBCController<T>::
  PBWBCController():
    type_name_("PBWBCController"),
    simulation_mode_(false),
    controller_active_(false),
    control_mode_(EFFORT),
    accumulated_time_(0.0),
    jitter_(0.0),
    verbosity_level_(0)
    {}
  
  template<typename T> 
  PBWBCController<T>::~PBWBCController() {
    if(softstop_command_thread_.joinable()) {
        softstop_command_thread_.join();
    }
  }
  
  template<typename T> 
  void PBWBCController<T>::
  displayClaimedResources(ClaimedResources & claimed_resources)
  {
    typename ClaimedResources::iterator it_claim;
    ROS_INFO_STREAM("Size of claimed resources: "<< claimed_resources.size());
    for (it_claim = claimed_resources.begin(); 
	    it_claim != claimed_resources.end(); 
	    ++it_claim)
    {
	    hardware_interface::InterfaceResources & aclaim = *it_claim;
	    ROS_INFO_STREAM("Claimed by PBWBCController: " << aclaim.hardware_interface);
	
	    for(std::set<std::string>::iterator
	      it_set_res=aclaim.resources.begin();
	    it_set_res!=aclaim.resources.end();
	    it_set_res++)
	    {
	      ROS_INFO_STREAM(" Resources belonging to the interface:" <<
			    *it_set_res);
	    }
	    
    }
  }

  template<typename T> 
  void PBWBCController<T>::initLogs()
  {
    ROS_INFO_STREAM("Initialize log data structure");
  }
  
  template<typename T>
  bool PBWBCController<T>::initRequestForwarding (lhi::RobotHW * robot_hw,
          ros::NodeHandle &robot_nh,
          ros::NodeHandle &controller_nh,
          ClaimedResources & claimed_resources) {
      return true;
  }
  
  template<typename T>
  std::shared_ptr<ActuatorController> PBWBCController<T>::create_actuatorcontroller (int idJoint) {
        return std::shared_ptr<pbwbc::ActuatorController>(
                        new pbwbc::RCActuatorController(joints_[idJoint]) );
  }
  
  
  template<typename T>
  bool PBWBCController<T>::initRequest (lhi::RobotHW * robot_hw,
          ros::NodeHandle &robot_nh,
          ros::NodeHandle &controller_nh,
          ClaimedResources & claimed_resources) 
  {
    // call PAL base controller init request if necessary initRequest if 
    if(!initRequestForwarding(robot_hw,robot_nh,controller_nh,claimed_resources))return false;

    ROS_DEBUG_STREAM("controller started in namespace \"" << ros::this_node::getNamespace() << "\"");
    if(getenv("PBWBC_DEBUGGER_ATTACH")) 
    {
      double wait = 0;
      while(wait==0) 
      {
        ROS_ERROR("Waiting for debugger attach");
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
      }
    }

    /// Read the parameter server
    if (!readParams(robot_nh))
      return false;
    
    //ROS_DEBUG_STREAM("controller NodeHandle is in namespace \"" << controller_nh.getNamespace() << "\"");
    std::shared_ptr<pbwbc::ConfigMap> config(new pbwbc::ROSConfigMap(robot_nh));
    config = config->submap("pbwbc_controller");
    config_ = config;
    
    create_joint_mapping();

    /// Create ros control interfaces to hardware
    /// Recalls: init() is called by initInterfaces()
    if (!initInterfaces(robot_hw,robot_nh,controller_nh,claimed_resources)) {
      return false;
    }

    config->param("softstop_button_idx",softstop_button_idx_,-1);
    if(softstop_button_idx_!=-1) {
        joystick_subscriber_ = controller_nh.subscribe("/joy",1,
                &PBWBCController::joystick_subscriber_callback,this);
        ROS_DEBUG_STREAM("softstop_button_idx: " << softstop_button_idx_);
    }

    // activate coredumps
    long max_coredump_size = 1000000000l;
    struct rlimit limit;
    getrlimit(RLIMIT_CORE,&limit);
    limit.rlim_cur = max_coredump_size;
    setrlimit(RLIMIT_CORE,&limit);
    
    std::string robot_description_str;
    robot_nh.getParam("/robot_description",robot_description_str);
    model_ = pbwbc::ModelPinocchio::create_from_urdf_data(robot_description_str);

    //Look up the indices of locked joints (from locked_joints_name_) in model_
    std::vector<unsigned long int> model_locked_joints_ids;
    for(auto const & joint_name : locked_joints_name_) {  
      int index = std::dynamic_pointer_cast<pbwbc::ModelPinocchio>(model_)->getPinocchioJointId(joint_name);
      ROS_DEBUG_STREAM("Locked joint " << joint_name << " found at index " << index);
      model_locked_joints_ids.push_back(index);
    }

    std::dynamic_pointer_cast<pbwbc::ModelPinocchio>(model_)->reduceModel(model_locked_joints_ids);

    config->getParam("softstop_command", softstop_command_);
    if(softstop_command_.size()) {
        ROS_DEBUG_STREAM("Softstop command \"" << softstop_command_ << "\"");
    }

    //ROS_DEBUG_STREAM("model q size " << model_->q_size());
    //ROS_DEBUG_STREAM("model dq size " << model_->dq_size());
    //for(auto const & joint_name : model_->jointNames()) {  
    //  int index = std::dynamic_pointer_cast<pbwbc::ModelPinocchio>(model_)->getPinocchioJointId(joint_name);
    //  ROS_DEBUG_STREAM("Found joint " << joint_name << " in model at index " << index);
    //}
    
    auto sconfig = config->submap("sensors");
    sensor_state_.reset(new SensorState);
    for(auto const & imu_name : imu_iface_->getNames()) {
        ROS_DEBUG_STREAM("Looking for parameters for imu " << imu_name);
        auto myconfig = sconfig->submap("imus")->submap(imu_name);
        std::string parent_frame_name;
        int frame_id;
        if(!myconfig->getParam("parent_frame_name",parent_frame_name)) {
            ROS_ERROR_STREAM("Missing parent_frame_name");
            frame_id = -1;
        } else {
            frame_id = model_->getFrameId(parent_frame_name);
            ROS_DEBUG_STREAM("Adding imu sensor with parent frame id " << frame_id);
        }
        sensor_state_->imuStates.push_back(
                std::make_tuple(frame_id,imu_name,ImuState()));
    }
    const std::vector<std::string>& ft_iface_names = ft_iface_->getNames();
    for(auto const & ft_name : ft_iface_->getNames()) {
        ROS_DEBUG_STREAM("Looking for parameters for ft " << ft_name);
        auto myconfig = sconfig->submap("fts")->submap(ft_name);
        std::string parent_frame_name;
        int frame_id;
        if(!myconfig->getParam("parent_frame_name",parent_frame_name)) {
            ROS_ERROR_STREAM("Missing parent_frame_name");
            frame_id = -1;
        } else {
            frame_id = model_->getFrameId(parent_frame_name);
            ROS_DEBUG_STREAM("Adding force torque sensor with parent frame id " << frame_id);
        }
        sensor_state_->ftsWrenches.push_back(
                std::make_tuple(frame_id,ft_name,wrench_t::Zero()));
    }
    sensor_state_->jointTorques.resize(model_->q_size());
    sensor_state_->jointTemperatures.resize(model_->q_size());

    tau_simulation_.resize(model_->q_size());
    config->param("tau_simulation_tc_", tau_simulation_tc_, 0.1);

    std::string state_estimation_name;
    auto state_estimation_config = config->submap("StateEstimation");
    if(!state_estimation_config->getParam("type", state_estimation_name)) {
        throw std::runtime_error("State estimaton not specified");
    }
    if(state_estimation_name=="ROSStateEstimation") {
        state_estimation_.reset(new ROSStateEstimation(
                   state_estimation_config,model_,ros::NodeHandle(controller_nh,"StateEstimation")));
    } else if (state_estimation_name=="RealSenseStateEstimation") {
        state_estimation_.reset(new RealSenseStateEstimationROS(
                   state_estimation_config,model_,sensor_state_,ros::NodeHandle(controller_nh,"StateEstimation")));
    } else if ( state_estimation_name == "HardwareStateEstimation" ) {
        state_estimation_ = create_hardware_stateestimation(model_,ros::NodeHandle(controller_nh,"StateEstimation"));
    } else {
        throw std::runtime_error("State Estimation type not available");
    }
    // create shared robot state
    q_offset_ = model_->y_size() - model_->q_size();
    dq_offset_ = model_->dy_size() - model_->dq_size();

    // make model use shared robot state
    model_->setState(state_estimation_->state());
    std::string model_compute_thread_scheduler;
    int model_compute_thread_priority;
    if(config->getParam("model_compute_thread_scheduler", model_compute_thread_scheduler)
         && config->getParam("model_compute_thread_priority", model_compute_thread_priority)) {
        std::map<std::string,int> scheduler_map = { {"SCHED_FIFO", SCHED_FIFO}, {"SCHED_OTHER", SCHED_OTHER } };
        auto pmodel = std::dynamic_pointer_cast<pbwbc::ModelPinocchio>(model_);
        if(pmodel) {
            pmodel->setRealTimeParameters(scheduler_map.at(model_compute_thread_scheduler),
                                           model_compute_thread_priority);
        }
    }

    // initialize robot state to sane values (TODO: base state)
    state_estimation_->state()->y.setZero();
    model_->setBasePosition(Eigen::Vector3d::Zero(),Eigen::Quaterniond::Identity(),
            *state_estimation_->state());
    state_estimation_->state()->dy.setZero();

    // wait for model computation thread to compute all quantities
    if(auto pmodel = std::dynamic_pointer_cast<pbwbc::ModelPinocchio>(model_)) {
        pmodel->blocking_update(std::chrono::system_clock::now());
    }

    //Get the settings of actuators associated to each joint
    std::vector<std::string> joint_names_in_actuatorControllers_list;
    config->list_items("ActuatorControllers",joint_names_in_actuatorControllers_list);

    /// Fill desired position during the phase where the robot is waiting.
    for(unsigned int idJoint=0;idJoint<joints_name_.size();idJoint++) 
    {
      std::string joint_name = joints_name_.at(idJoint);
      std::map<std::string,EffortControlPDMotorControlData>::iterator
          search_ecpd = effort_mode_pd_motors_.find(joint_name);

      if (search_ecpd!=effort_mode_pd_motors_.end()) 
      {
        EffortControlPDMotorControlData & ecpdcdata = search_ecpd->second;
        ecpdcdata.des_pos = jointPosition(idJoint);
        if(verbosity_level_ < 5000) 
        {
          ROS_DEBUG_STREAM("Desired position: " << joint_name << " " << ecpdcdata.des_pos);
        }
        // create actuator wrapper for this joint
        std::shared_ptr<pbwbc::ActuatorController> ac = create_actuatorcontroller(idJoint);
        // save this ActuatorController interface for the standby controller
        actuatorcontrollers_raw_.push_back(ac);

        for (auto joint_name_in_actuatorControllers_list : joint_names_in_actuatorControllers_list) {
          if (joint_name_in_actuatorControllers_list == joint_name) {
            auto actuatorcontroller_config = 
              config->submap("ActuatorControllers")->submap(joint_name_in_actuatorControllers_list);
            
            std::string actuatorcontroller_type;
            if(!actuatorcontroller_config->getParam("type", actuatorcontroller_type)) {
              throw std::runtime_error("ActuatorController: parameter 'type' not found");
            }
            if (actuatorcontroller_type == "ActuatorAdmittanceController") {
              PBWBC_DEBUG("Creating ActuatorAdmittanceController for joint " << joint_name_in_actuatorControllers_list);
              std::string controller_name = "ActuatorAdmittanceController/"+joint_name;
              ac = std::shared_ptr<ActuatorController>(
                new ActuatorAdmittanceControllerROS(joint_name, state_estimation_->state(),model_,
                        actuatorcontroller_config,
                        sensor_state_, 
                        ac, 
                        ros::NodeHandle(controller_nh,controller_name)));
            } else {
              PBWBC_DEBUG("type = " << actuatorcontroller_type);
              throw std::runtime_error("ActuatorController type not yet implemented");
            }
          }
        }
        actuatorcontrollers_.push_back(ac);
      } 
      else 
      {
        ROS_ERROR_STREAM("No effort controller gains defined for joint " << joint_name);
      }
    }


    


    pbwbc_.reset(new pbwbc::PBWBCROS(
                state_estimation_->state(),
                sensor_state_,
                model_,
                config,
                actuatorcontrollers_,
                controller_nh));

    // config based creation of controllers is not supported yet,
    // manually adding controllers
    std::function<std::shared_ptr<Controller>(std::string const & controller_name,std::shared_ptr<ConfigMap> const &)> 
        controller_factory = [&](
            std::string const & controller_name,
            std::shared_ptr<ConfigMap> const & controller_config)
                -> std::shared_ptr<Controller> {
        std::string controller_type;
        bool have_type = controller_config->getParam("type",controller_type);
        if(!have_type) {
            throw std::runtime_error("Controller has no type");
        }
        // TODO: replace this with a function
        // std::shared_ptr<Controller> controller_factory_ros(state,sensorstate,model,config,nh);
        PBWBC_DEBUG("Controller type: " << controller_type);
        if(controller_type=="JointImpedanceController") {
            return std::shared_ptr<pbwbc::Controller>(
                        new pbwbc::JointImpedanceControllerROS(
                          state_estimation_->state(),
                          model_,
                          controller_config,
                          ros::NodeHandle(controller_nh,controller_name)));
        } else if(controller_type=="JointEndStopController") {
            return std::shared_ptr<pbwbc::Controller>(
                        new pbwbc::JointEndStopControllerROS(
                          state_estimation_->state(),
                          model_,
                          controller_config,
                          ros::NodeHandle(controller_nh,controller_name)));
        } else if(controller_type=="JointVelocityLimiter") {
            return std::shared_ptr<pbwbc::Controller>(
                        new pbwbc::JointVelocityLimiterROS(
                          state_estimation_->state(),
                          model_,
                          controller_config,
                          ros::NodeHandle(controller_nh,controller_name)));
        } else if(controller_type=="CartesianImpedanceController") {
            return std::shared_ptr<pbwbc::Controller>(
                        new pbwbc::CartesianImpedanceControllerROS(
                          state_estimation_->state(),
                          model_,
                          controller_config,
                          ros::NodeHandle(controller_nh,controller_name)));
        } else if(controller_type=="MultiContactForceDistribution") {
            return std::shared_ptr<pbwbc::Controller>(
                        new pbwbc::MultiContactForceDistributionROS(
                          state_estimation_->state(),
                          sensor_state_,
                          model_,
                          controller_config,
                          ros::NodeHandle(controller_nh,controller_name)));
        } else if(controller_type=="SelfCollisionAvoidanceController") {
#ifdef      HAVE_HPP_FCL
            return std::shared_ptr<pbwbc::Controller>(
                        new pbwbc::SelfCollisionAvoidanceControllerROS(
                          state_estimation_->state(),
                          model_,
                          controller_config,
                          robot_description_str,
                          ros::NodeHandle(controller_nh,controller_name)));
#else       
            throw std::runtime_error("SelfCollisionAvoidanceController not enabled");
#endif      // HAVE_HPP_FCL
        } else if(controller_type=="HierarchyController") {
            std::vector<std::shared_ptr<Controller>> controllers_hierarchy;
            std::vector<std::string> controller_names_hierarchy;
            if(!controller_config->getParam("hierarchy",controller_names_hierarchy)) {
                throw std::runtime_error("hierarchy not defined");
            }
            PBWBC_DEBUG("--- Adding Controllers in HierarchyController (start) ---");
            for(auto const & controller_name_hierarchy : controller_names_hierarchy) {
                PBWBC_DEBUG("Controller name in hierarchy: " << controller_name_hierarchy);
                auto controller_config_hierarchy = controller_config->submap("Controllers")->submap(controller_name_hierarchy);
                controllers_hierarchy.push_back(
                        controller_factory(controller_name_hierarchy,controller_config_hierarchy));
            }
            PBWBC_DEBUG("--- Adding Controllers in HierarchyController (end) ---");
            return std::shared_ptr<pbwbc::Controller>(
                        new pbwbc::HierarchyController(
                          model_,
                          controllers_hierarchy));
        } else {
            throw std::runtime_error("Controller type not known");
        }
    
    };
    //
    std::vector<std::string> controller_names;
    config->list_items("Controllers",controller_names);
    for(auto const & controller_name : controller_names) {
        PBWBC_DEBUG("Controller name: " << controller_name);
        auto controller_config = config->submap("Controllers")->submap(controller_name);
        pbwbc_->addController(controller_factory(controller_name,controller_config));
    }

    //Trajectory player for each of the controllers
    //TODO: this may not be the cleanest way to do this. Fix it
    pbwbc_->initialize();
    
    trigger_rosbag_service_ = controller_nh.advertiseService("triggerRosbagDump", 
            &PBWBCController::callback_trigger_rosbag, this);
    controller_switch_service_ = controller_nh.advertiseService("controllerSwitch", 
            &PBWBCController::callback_controller_switch, this);
    controller_active_publisher_ = RealTimePublisherBackEnd::advertise<std_msgs::Bool>(
            controller_nh,"controller_active",1000,10);

    ROS_DEBUG_STREAM("Initialization finished");


    return true;
  }
  
  template<typename T>
  bool PBWBCController<T>::
  initInterfaces(lhi::RobotHW * robot_hw,
		 ros::NodeHandle &,
		 ros::NodeHandle &,
		 ClaimedResources & claimed_resources)
  {
    std::string lns;
    lns="hardware_interface";

    // Check if construction finished cleanly
    if (this->state_!=PBWBCController::CONSTRUCTED)
    {
      ROS_ERROR("Cannot initialize this controller because it failed to be constructed");
    }

    // Get a pointer to the joint position control interface
    pos_iface_ = robot_hw->get<PositionJointInterface>();
    if (!pos_iface_)
    {
	    ROS_WARN("This controller did not find  a hardware interface of type '%s'."
		  " Make sure this is registered in the %s::RobotHW class if it is required.",
		  getHardwareInterfaceType().c_str(), lns.c_str());
    }

    // Get a pointer to the joint effort control interface
    effort_iface_ = robot_hw->get<EffortJointInterface>();
    if (! effort_iface_)
    {
   	  ROS_WARN("This controller did not find a hardware interface of type '%s'."
		  " Make sure this is registered in the %s::RobotHW class if it is required.",
		  getHardwareInterfaceType().c_str(),lns.c_str());
    }

    // Get a pointer to the force-torque sensor interface
    ft_iface_ = robot_hw->get<ForceTorqueSensorInterface>();
    if (! ft_iface_ )
    {
	    ROS_WARN("This controller did not find a hardware interface of type '%s '. " 
		  " Make sure this is registered inthe %s::RobotHW class if it is required.",
		    internal :: demangledTypeName<ForceTorqueSensorInterface>().c_str(),lns.c_str());
    }
    
    // Get a pointer to the IMU sensor interface
    imu_iface_ = robot_hw->get<ImuSensorInterface>();
    if (! imu_iface_)
    {
	    ROS_WARN("This controller did not find a hardware interface of type '%s'."
		  " Make sure this is registered in the %s::RobotHW class if it is required.",
		    internal :: demangledTypeName<ImuSensorInterface>().c_str(),lns.c_str());
    }

    // Temperature sensor not available in simulation mode
    if (!simulation_mode_) {
      #ifdef TEMPERATURE_SENSOR_CONTROLLER
	      // Get a pointer to the actuator temperature sensor interface
	      act_temp_iface_ = robot_hw->get<ActuatorTemperatureSensorInterface>();
	      if (!act_temp_iface_)
	      {
	        ROS_WARN("This controller did not find a hardware interface of type '%s'."
		      " Make sure this is registered in the %s::RobotHW class if it is required.",
		        internal :: demangledTypeName<ActuatorTemperatureSensorInterface>().c_str(),lns.c_str());
	      }
      #endif
    }
	
    // Return which resources are claimed by this controller
    pos_iface_->clearClaims();
    effort_iface_->clearClaims();
    
    if (! init())
    {
	    ROS_ERROR("Failed to initialize pwbccontroller" );
	    PBWBC_FATAL("FAILED LOADING PBWBC CONTROLLER");
	    return false ;
    }
    if (verbosity_level_<5000)
      ROS_INFO_STREAM("Initialization of interfaces for pbwbc-controller Ok !");

    hardware_interface::InterfaceResources iface_res;
    iface_res.hardware_interface = hardware_interface::internal::demangledTypeName<PositionJointInterface>();
    iface_res.resources = pos_iface_->getClaims();
    claimed_resources.push_back(iface_res);
    
    /// Display claimed ressources
    if (verbosity_level_<5000)
      displayClaimedResources(claimed_resources);
    pos_iface_->clearClaims();

    iface_res.hardware_interface = hardware_interface::internal::demangledTypeName<EffortJointInterface>();
    iface_res.resources = effort_iface_->getClaims();
    claimed_resources.push_back(iface_res);
    if (verbosity_level_<5000)
      displayClaimedResources(claimed_resources);
    
    effort_iface_->clearClaims();
    if (verbosity_level_<5000)
      ROS_INFO_STREAM("Initialization of pbwbc-controller Ok !");
    // success
    this->state_ = PBWBCController::INITIALIZED;

    return true;
  }
    
  template<typename T>
  bool PBWBCController<T>::init() {
    if (!initJoints()) 
      return false;
    if (!initIMU())
      return false;
    if (!initForceSensors())
      return false;
    if (!initTemperatureSensors())
      return false;

    return true;
  }

  template<typename T>
  void PBWBCController<T>::
  readParamsVerbosityLevel(ros::NodeHandle &robot_nh) {
      if (robot_nh.hasParam("/pbwbc_controller/verbosity_level")) {
          robot_nh.getParam("/pbwbc_controller/verbosity_level",verbosity_level_);
          ROS_INFO_STREAM("Verbosity_level " << verbosity_level_);
      }
      logger::pbwbc_logger->setLevel(log4cxx::Level::toLevel(verbosity_level_));
      if (robot_nh.hasParam("/pbwbc_controller/log/size")) {
          int llength;
          robot_nh.getParam("/pbwbc_controller/log/size",llength);
      }

      if(verbosity_level_ < 5000) {
          if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
              ros::console::notifyLoggerLevelsChanged();
          }
      }
  }
  

  template<typename T>
  bool PBWBCController<T>::
  readParamsPositionControlData(ros::NodeHandle &)
  {
    return false;
  }
 
  // read paramters for standby effort controller 
  template<typename T>
  bool PBWBCController<T>::readParamsEffortControlPDMotorControlData(ros::NodeHandle &robot_nh) {
      if (robot_nh.hasParam("/pbwbc_controller/effort_control_pd_motor_init/gains")) {
          XmlRpc::XmlRpcValue xml_rpc_ecpd_init;
          robot_nh.getParamCached("/pbwbc_controller/effort_control_pd_motor_init/gains",
                  xml_rpc_ecpd_init);

          /// Display gain during transition control.
          if (verbosity_level_ < 5000)
              ROS_INFO("/pbwbc_controller/effort_control_pd_motor_init/gains: %d %d %d\n",
                      xml_rpc_ecpd_init.getType(),XmlRpc::XmlRpcValue::TypeArray,XmlRpc::XmlRpcValue::TypeStruct);

          effort_mode_pd_motors_.clear();

          for (size_t i=0;i<joints_name_.size();i++) {
              if (xml_rpc_ecpd_init.hasMember(joints_name_[i])) {
                  std::string prefix= "/pbwbc_controller/effort_control_pd_motor_init/gains/" + joints_name_[i];
                  effort_mode_pd_motors_[joints_name_[i]].read_from_xmlrpc_value(prefix);
              } else {
                  /// TODO: EFFORT or POSITION control actuator by actuator to make sure
                  /// that is the actuator is effort control, the violation of this part is
                  /// trigerring an error.
                  ROS_INFO("joint %s not in /pbwbc_controller/effort_control_pd_motor_init/gains\n",
                          joints_name_[i].c_str());
              }
          }
          return true;
      }

      ROS_ERROR("No parameter /pbwbc_controller/effort_controler_pd_motor_init");
      return false;
  }

  // read parameters to defined which quantities are mapped to sot?
  template<typename T>
  bool PBWBCController<T>::readParamsFromRCToPBWBC(ros::NodeHandle &robot_nh) {
          if (robot_nh.hasParam("/pbwbc_controller/map_rc_to_pbwbc")) {
              if (robot_nh.getParam("/pbwbc_controller/map_rc_to_pbwbc",
                          mapFromRCToPBWBC_)) {
                  /// TODO: Check if the mapping is complete wrt to the interface and the mapping.
                  if (verbosity_level_ < 5000) {
                      ROS_INFO_STREAM("Loading map rc to pbwbc: ");
                      for (it_map_rt_to_pbwbc it = mapFromRCToPBWBC_.begin(); 
                              it != mapFromRCToPBWBC_.end(); ++it) {
                          ROS_INFO_STREAM( it->first << ", " << it->second);
                      }
                  }
              } else {
                  ROS_ERROR_STREAM("Could not read param /pbwbc_controller/map_rc_to_pbwbc");
                  return false;
              }
          } else {
              ROS_ERROR_STREAM("Param /pbwbc_controller/map_rc_to_pbwbc does not exist!");
              return false;
          }
          return true;
      }


  // read parameter which defines the controlled joints
  template<typename T>
  bool PBWBCController<T>::readParamsJointNames(ros::NodeHandle &robot_nh) {
    /// Check if the /pbwbc_controller/joint_names parameter exists.
    if (robot_nh.hasParam("/pbwbc_controller/joint_names")) 
      {
    	/// Read the joint_names list from this parameter
    	robot_nh.getParam("/pbwbc_controller/joint_names",
    			  joints_name_);
    	for(std::vector<std::string>::size_type i=0;i<joints_name_.size();i++)
    	  {
    	    if (verbosity_level_ < 5000)
    	      ROS_INFO_STREAM("joints_name_[" << i << "]=" << joints_name_[i]);

    	    if (modelURDF_.use_count())
    	      {
    		urdf::JointConstSharedPtr aJCSP = modelURDF_->getJoint(joints_name_[i]);
    		if (aJCSP.use_count()!=0)
    		  {
    		    if (verbosity_level_ < 5000)
    		      ROS_INFO_STREAM( joints_name_[i] + " found in the robot model" );
    		  }
    		else
    		  {
    		    ROS_ERROR(" %s not found in the robot model",joints_name_[i].c_str());
    		    return false;
    		  }
    	      }
    	    else
    	      {
    		ROS_ERROR("No robot model loaded in /robot_description");
    		return false;
	      }
	  }
      }
    else
      return false;

    /// Deduce from this the degree of freedom number.
    nbDofs_ = joints_name_.size();
	
    return true;
  }


   // read parameter which defines the non controlled (locked) joints
  template<typename T>
  bool PBWBCController<T>::readParamsLockedJointNames(ros::NodeHandle &robot_nh) {
    /// Check if the /pbwbc_controller/locked_joint_names parameter exists.
    if (robot_nh.hasParam("/pbwbc_controller/locked_joint_names")) 
      {
      /// Read the locked_joint_names list from this parameter
      robot_nh.getParam("/pbwbc_controller/locked_joint_names",
            locked_joints_name_);
      for(std::vector<std::string>::size_type i=0;i<locked_joints_name_.size();i++)
        {
          if (verbosity_level_ < 5000)
            ROS_INFO_STREAM("locked_joints_name_[" << i << "]=" << locked_joints_name_[i]);

          if (modelURDF_.use_count())
            {
        urdf::JointConstSharedPtr aJCSP = modelURDF_->getJoint(locked_joints_name_[i]);
        if (aJCSP.use_count()!=0)
          {
            if (verbosity_level_ < 5000)
              ROS_INFO_STREAM( locked_joints_name_[i] + " found in the robot model" );
          }
        else
          {
            ROS_ERROR(" %s not found in the robot model",locked_joints_name_[i].c_str());
            return false;
          }
            }
          else
            {
        ROS_ERROR("No robot model loaded in /robot_description");
        return false;
        }
    }
      }
    else
      return false;
  
    return true;
  }


  template<typename T>
  bool PBWBCController<T>::
  readParamsControlMode(ros::NodeHandle &robot_nh)
  {
    // Read param for the list of joint names.
      if (robot_nh.hasParam("/pbwbc_controller/control_mode")) {
          std::string scontrol_mode,seffort("EFFORT"),sposition("POSITION");

          /// Read the joint_names list
          robot_nh.getParam("/pbwbc_controller/control_mode",scontrol_mode);
          if (verbosity_level_ < 5000)
              ROS_INFO_STREAM("control mode read from param :|" << scontrol_mode<<"|");

          if (scontrol_mode==seffort)
              control_mode_ = EFFORT;
          else if (scontrol_mode==sposition)
              control_mode_ = POSITION;
          else 
          {
              ROS_INFO_STREAM("Error in specifying control mode-> falls back to default position. Wrong control is:" << scontrol_mode);
              std::string::size_type n;
              n = scontrol_mode.find("EFFORT");
              ROS_INFO_STREAM("n: " << n << " size: " << scontrol_mode.size() << " "<< sposition.size() << " " << seffort.size());
              control_mode_ = POSITION;
          }
      } else {
          ROS_INFO_STREAM("Default control mode : EFFORT");
      }

      /// Always return true;
      return true;
  }

  template<typename T>
  bool PBWBCController<T>::
  readParamsdt(ros::NodeHandle &robot_nh)
  {
    /// Reading the jitter is optional but it is a very good idea
    if (robot_nh.hasParam("/pbwbc_controller/jitter"))
    {
	    robot_nh.getParam("/pbwbc_controller/jitter",jitter_);
	    if (verbosity_level_ < 5000)
	      ROS_INFO_STREAM("jitter: " << jitter_);
    }

    /// Read /pbwbc_controller/dt to know what is the control period
    if (robot_nh.hasParam("/pbwbc_controller/dt"))
    {
	    robot_nh.getParam("/pbwbc_controller/dt",dt_);
	    if (verbosity_level_ < 5000)
	      ROS_INFO_STREAM("dt: " << dt_);
	    return true;
    }

    ROS_ERROR("You need to define a control period in param /pbwbc_controller/dt");
    return false;
  }

  template<typename T>
  bool PBWBCController<T>::
  readUrdf(ros::NodeHandle &robot_nh)
  {
    /// Reading the parameter /robot_description which contains the robot
    /// description
    if (!robot_nh.hasParam("/robot_description"))
      {
	ROS_ERROR("ROS application does not have robot_description");
	return false;
      }
    std::string robot_description_str;
    
    robot_nh.getParam("/robot_description",robot_description_str);

    modelURDF_ = urdf::parseURDF(robot_description_str);
    if (verbosity_level_ < 5000)
      ROS_INFO("Loaded /robot_description %ld",modelURDF_.use_count());
    return true;
  }
  
  template<typename T>
  bool PBWBCController<T>::
  readParams(ros::NodeHandle &robot_nh)
  {

    /// Read the level of verbosity for the controller (0: quiet, 1: info, 2: debug).
    /// Default to quiet
    readParamsVerbosityLevel(robot_nh);

    /// Read /pbwbc_controller/simulation_mode to know if we are in simulation mode
    // Defines if we are in simulation node.
    if (robot_nh.hasParam("/pbwbc_controller/simulation_mode")) 
      simulation_mode_ = true;
    
    /// Read URDF file.
    readUrdf(robot_nh);
    
    /// Calls readParamsJointNames
    // Reads the list of joints to be controlled.
    if (!readParamsJointNames(robot_nh))
      return false;

    /// Calls readParamsLockedJointNames
    // Reads the list of joints that are locked (not controlled).
    if (!readParamsLockedJointNames(robot_nh))
      return false;

    /// Calls readParamsControlMode.
    // Defines if the control mode is position or effort
    readParamsControlMode(robot_nh);

    /// Calls readParamsFromRCToPBWBC
    // Mapping from ros-control to pbwbc
    readParamsFromRCToPBWBC(robot_nh);

    /// Get control period
    if (!readParamsdt(robot_nh))
      return false;
    
    if (control_mode_==EFFORT)
      readParamsEffortControlPDMotorControlData(robot_nh);
    else if (control_mode_==POSITION)
      readParamsPositionControlData(robot_nh);
    return true;
  }

    
  template<typename T>
  bool PBWBCController<T>::
  initJoints()
  {
    // Init Joint Names.
    joints_.resize(joints_name_.size());
    desired_init_pose_.resize (joints_.size());
    
    for (unsigned int i=0;i<nbDofs_;i++)
    {
	    bool notok=true;
	    ControlMode lcontrol_mode = control_mode_;
	    bool failure=false;
	
	    while (notok)
	    {
	      try 
	      {
		      if (lcontrol_mode==POSITION)
		      {
		        joints_[i] = pos_iface_->getHandle(joints_name_[i]);
		        if (verbosity_level_ < 5000)
		          ROS_INFO_STREAM("Found joint " << joints_name_[i] << " in position "
				      << i << " " << joints_[i].getName());
		      }
		      else if (lcontrol_mode==EFFORT)
		      {
		        joints_[i] = effort_iface_->getHandle(joints_name_[i]);
		        if (verbosity_level_ < 5000)
		          ROS_INFO_STREAM("Found joint " << joints_name_[i] << " in effort "
				      << i << " " << joints_[i].getName());
		      }
		      // throws on failure
		      notok=false;
	      }
	      catch (...)
	      {
		      failure=true;
		      ROS_ERROR_STREAM("Could not find joint " 
				  << joints_name_[i]);
		      if (lcontrol_mode==POSITION)
		        ROS_ERROR_STREAM(" in POSITION");
		      else
		        ROS_ERROR_STREAM(" in EFFORT");
		  
		      if (lcontrol_mode==POSITION)
		        return false ;	
		      else if (lcontrol_mode==EFFORT)
		        lcontrol_mode = POSITION;
	      }
	      if (!failure)
	        desired_init_pose_[i] = joints_[i].getPosition();
	    }
    }    
    return true ;
  }
  
  template<typename T>
  bool PBWBCController<T>::
  initIMU()
  {
    // get all imu sensor names
    const std :: vector<std :: string >& imu_iface_names = imu_iface_->getNames();
    if (verbosity_level_ < 5000)
    {
	    for (unsigned i=0; i <imu_iface_names.size(); i++)
	     ROS_INFO("Got sensor %s", imu_iface_names[i].c_str());
    }
    for (unsigned i=0; i <imu_iface_names.size(); i++)
    {
      // sensor handle on imu
      imu_sensor_.push_back(imu_iface_->getHandle(imu_iface_names[i]));

    }
 
    return true ;
  }
  
  template<typename T>
  bool PBWBCController<T>::
  initForceSensors()
  {
    // get force torque sensors names package.
    const std::vector<std::string>& ft_iface_names = ft_iface_->getNames();
    if (verbosity_level_ < 5000)
    {
	    for (unsigned i=0; i <ft_iface_names.size(); i++)
	    ROS_INFO("Got sensor %s", ft_iface_names[i].c_str());
    }
    for (unsigned i=0; i <ft_iface_names.size(); i++)
    {
      // sensor handle on torque forces
      ft_sensors_.push_back(ft_iface_->getHandle(ft_iface_names[i]));
    }
    return true;
  }

  template<typename T>
  bool PBWBCController<T>::initTemperatureSensors() {
      if (!simulation_mode_) {
#ifdef TEMPERATURE_SENSOR_CONTROLLER
          // get temperature sensors names
          const std::vector<std::string>& act_temp_iface_names = act_temp_iface_->getNames();

	    if (verbosity_level_ < 5000)
              ROS_INFO("Actuator temperature sensors: %ld",act_temp_iface_names.size() ); 
              for (std::size_t i=0; i <act_temp_iface_names.size(); i++) {
                  ROS_INFO("Got sensor %s", act_temp_iface_names[i].c_str());
              }
          }

          for (std::size_t i=0; i < act_temp_iface_names.size(); i++) {
            auto const & joint_name = act_temp_iface_names.at(i);
            auto const & joint_names = joints_name_;
            auto itr = std::find(joint_names.begin(),joint_names.end(),joint_name);
            if(itr==joint_names.end()) {
                std::stringstream str;
                str << "Could not find joint " << joint_name << " in joint_names list";
                throw std::runtime_error(str.str());
            }
            act_temp_sensors_.insert(std::make_pair(std::distance(joint_names.begin(),itr),
                        act_temp_iface_->getHandle(act_temp_iface_names.at(i))));
          }
#endif	
      }
      return true;
  }
  
  template<typename T>
  void PBWBCController<T>::fillJoints() {
      for(unsigned int idJoint=0;idJoint<joints_name_.size();idJoint++) {
          state_estimation_->state()->y(q_offset_+idJoint) = jointPosition(idJoint);
          state_estimation_->state()->dy(dq_offset_+idJoint) = jointVelocity(idJoint);
          sensor_state_->jointTorques(idJoint) = jointTorque(idJoint);
      }
      if(!jointTorque_first_iteration_) {
        tau_simulation_ += tau_simulation_tc_ * ( pbwbc_->getTau() - tau_simulation_ );
      }
  }

  template<typename T>
  void PBWBCController<T>::setSensorsImu(std::string &name,
					   int IMUnb,
					   std::vector<double> & data)
  {
    std::ostringstream labelOss;
    labelOss << name << IMUnb;
    std::string label_s = labelOss.str();
  }

  template<typename T>
  void PBWBCController<T>::fillImu() {
      for(unsigned int idIMU=0;idIMU<imu_sensor_.size();idIMU++) {
          /// Fill orientations, gyrometer and acceleration from IMU.
          if (imu_sensor_[idIMU].getOrientation()) {
              for(int idx=0;idx<4;idx++) {
                  std::get<2>(sensor_state_->imuStates.at(idIMU)).orientation.coeffs()(idx)
                    = imu_sensor_[idIMU].getOrientation()[idx];
              }
          }
          if (imu_sensor_[idIMU].getAngularVelocity()) {
              for(int idx=0;idx<3;idx++) {
                  std::get<2>(sensor_state_->imuStates.at(idIMU)).angularVelocity(idx)
                    = imu_sensor_[idIMU].getAngularVelocity()[idx];
              }
          }
          if (imu_sensor_[idIMU].getLinearAcceleration()) {
              for(int idx=0;idx<3;idx++) {
                  std::get<2>(sensor_state_->imuStates.at(idIMU)).linearAcceleration[idx]
                    = imu_sensor_[idIMU].getLinearAcceleration()[idx];
              }
          }
          std::get<2>(sensor_state_->imuStates.at(idIMU)).translation.setZero();
          std::get<2>(sensor_state_->imuStates.at(idIMU)).linearVelocity.setZero();
      }
  }
  
  template<typename T>
  void PBWBCController<T>::fillForceSensors() {
      for(unsigned int idFS=0;idFS<ft_sensors_.size();idFS++) {
          for(unsigned int idForce=0;idForce<3;idForce++) {
              std::get<2>(sensor_state_->ftsWrenches.at(idFS))(idForce)
                  = ft_sensors_[idFS].getForce()[idForce];
          }
          for(unsigned int idTorque=0;idTorque<3;idTorque++) {
              std::get<2>(sensor_state_->ftsWrenches.at(idFS))(3+idTorque)
                  = ft_sensors_[idFS].getTorque()[idTorque];
          }
      }
  }

  template<typename T>
  void PBWBCController<T>::
  fillTempSensors() {
      if (!simulation_mode_) {
#ifdef TEMPERATURE_SENSOR_CONTROLLER
          for(auto const & sensor : act_temp_sensors_) {
              sensor_state_->jointTemperatures(sensor.first) = sensor.second.getValue();
          }
#endif
      } else {
          sensor_state_->jointTemperatures.setZero();
      }
  }

  template<typename T>
  void PBWBCController<T>::fillSensors()
  {
    fillJoints();
    fillImu();
    fillForceSensors();
    fillTempSensors();
  }
  

  template<typename T>
  void PBWBCController<T>::
  localStandbyEffortControlMode(timestamp_t const & timestamp, const ros::Duration& period) {
      // ROS_INFO("Compute command for effort mode: %d %d",joints_.size(),effort_mode_pd_motors_.size());
      for(unsigned int idJoint=0;idJoint<joints_name_.size();idJoint++) {
          std::string joint_name = joints_name_[idJoint];
          std::map<std::string,EffortControlPDMotorControlData>::iterator
              search_ecpd = effort_mode_pd_motors_.find(joint_name);

          if (search_ecpd!=effort_mode_pd_motors_.end()) {
              EffortControlPDMotorControlData & ecpdcdata =
                  search_ecpd->second;
              double vel_err = 0 - jointVelocity(idJoint);
              double err = ecpdcdata.des_pos - jointPosition(idJoint);

              ecpdcdata.integ_err +=err;

              double local_command = ecpdcdata.pid_controller.computeCommand(err,vel_err,period);
              // Apply command
              //control_toolbox::Pid::Gains gains = ecpdcdata.pid_controller.getGains();
              // TODO: use ActuatorControllers to switch between PAL/ros_control
              actuatorcontrollers_raw_.at(idJoint)->update(
                      timestamp,local_command);

              // Update previous value.
              //ecpdcdata.prev = DataOneIter_.motor_angle[idJoint];
          }
      }
  }
  
  template<typename T>
  void PBWBCController<T>::
  localStandbyPositionControlMode(timestamp_t const & timestamp)
  {
    //static bool first_time=true;
    throw std::runtime_error("not implemented");
    // TODO: check if desired_init_pose is always set correctly
    
    /// Iterate over all the joints
    for(unsigned int idJoint=0;idJoint<joints_name_.size();idJoint++)
      {
        /// Find the joint
        std::string joint_name = joints_name_[idJoint];
        actuatorcontrollers_raw_.at(idJoint)->update(timestamp,desired_init_pose_[idJoint]);

        //assert(joint.getName() == joint_name);
        //if (first_time)
        //  if (verbosity_level_>1) {
        //    ROS_INFO("Control joint %s (id %d) to %f\n",joint_name.c_str(),idJoint,
        //      joint.getPosition());
        //  }
      }
    //first_time=false;
  }
  
  template<typename T>
  void PBWBCController<T>::updateForwarding(const ros::Time & ros_time, const ros::Duration& period) {}
  
  template<typename T>
  void PBWBCController<T>::update(const ros::Time & ros_time, const ros::Duration& period) {
#       ifdef EIGEN_RUNTIME_NO_MALLOC
      bool was_malloc_allowed = Eigen::internal::is_malloc_allowed();
      Eigen::internal::set_is_malloc_allowed(false);
#       endif

      timestamp_t timestamp(timestamp_t::duration(ros_time.toSec()));
      //timestamp_t my_sys_time = std::chrono::system_clock::now();

      // TODO: enable FPE trap
      // This is a process wide setting and gazebo triggers
      // this. Cann not be used in simulation.
      //feenableexcept(FE_DIVBYZERO|FE_INVALID|FE_OVERFLOW);
      if(start_request_ && !controller_active_) {
          ROS_DEBUG_STREAM("PBWBCController on-line start");
          fillSensors();
          state_estimation_->start(timestamp);
          state_estimation_->update(timestamp);
          for(unsigned int idJoint=0;idJoint<joints_name_.size();idJoint++) {
              state_estimation_->state()->y(q_offset_+idJoint) = jointPosition(idJoint);
              state_estimation_->state()->dy(dq_offset_+idJoint) = jointVelocity(idJoint);
          }
          model_->update(timestamp); //necessary here so start() calls have access to the correct model
          pbwbc_->start(timestamp);
          controller_active_ = true;
          start_request_ = false;
      }
      if(controller_active_ && softstop_request_) {
          controller_active_ = false;
          // resetting desired position
          for(unsigned int idJoint=0;idJoint<joints_name_.size();idJoint++) {
              std::string joint_name = joints_name_[idJoint];
              std::map<std::string,EffortControlPDMotorControlData>::iterator
                  search_ecpd = effort_mode_pd_motors_.find(joint_name);

              if (search_ecpd!=effort_mode_pd_motors_.end()) {
                  EffortControlPDMotorControlData & ecpdcdata =
                      search_ecpd->second;
                  // reset desired position
                  ecpdcdata.des_pos = jointPosition(idJoint);
              }
          }
          if(softstop_command_.size()) {
             // TODO: create thread with non-rt prio
             softstop_command_thread_ = std::thread([this](){
                try {
                     int status = std::system(softstop_command_.c_str());
                     if(status!=0) {
                         std::stringstream str;
                         str << "Failed: ret=" << status << " errno=" << errno;
                         throw std::runtime_error(str.str());
                     }
                } catch ( std::exception const & e ) {
                     ROS_ERROR_STREAM("Executing soft stop command failed:" << e.what());
                }
                });
          }
          pb_.trigger_write_to_file("/tmp/softstop.bag");

          ROS_DEBUG_STREAM("PBWBCController::stopping");
          pbwbc_->stop(timestamp);
          state_estimation_->stop(timestamp);

          softstop_request_ = false;
      }
      // TODO: come back from softstop, call stopping and starting to reset controller
      if (controller_active_) {
          auto start = std::chrono::system_clock::now();
          bool executed = false;
          try 
          {
              double periodInSec = period.toSec();
              if (periodInSec+accumulated_time_>dt_-jitter_) {
                  executed = true;
                  
                  fillSensors();
                  state_estimation_->update(timestamp);
                  model_->update(timestamp);
                  pbwbc_->update(timestamp);
                  jointTorque_first_iteration_ = false;
                  // trigger ros publisher thread
                  accumulated_time_ = 0.0;
              } else {
                  accumulated_time_ += periodInSec;
              }
          } catch ( ControllerException const & ex ) {
                ROS_ERROR_STREAM("Got ControllerException " << ex.what());
                // switch to standby controller in next iteration
                softstop_request_ = true;
                // resetting desired position of standby controller to current position
                for(unsigned int idJoint=0;idJoint<joints_name_.size();idJoint++) {
                    std::string joint_name = joints_name_[idJoint];
                    std::map<std::string,EffortControlPDMotorControlData>::iterator
                        search_ecpd = effort_mode_pd_motors_.find(joint_name);

                    if (search_ecpd!=effort_mode_pd_motors_.end()) {
                        EffortControlPDMotorControlData & ecpdcdata =
                            search_ecpd->second;
                        ecpdcdata.des_pos = jointPosition(idJoint);
                    }
                }
                // execute standby controller to generate commands for this iteration
                localStandbyEffortControlMode(timestamp,period);
          }
#ifdef    CATCH_EXCEPTIONS
          catch (std::exception const &exc) {
              std::cerr << "Failure happened during one_iteration evaluation: std_exception: " 
                  << exc.what() << std::endl;
              std::cerr << "Use gdb on this line together with gdb to investigate the problem: " <<std::endl;
              std::cerr << __FILE__ << " " << __LINE__  << std::endl;
              throw exc;
          } catch (...) {
              std::cerr << "Failure happened during one_iteration evaluation: unknown exception" << std::endl;
              std::cerr << "Use gdb on this line together with gdb to investigate the problem: " <<std::endl;
              std::cerr << __FILE__ << " " << __LINE__  << std::endl;
          }
#endif    // CATCH_EXCEPTIONS
          auto end = std::chrono::system_clock::now();
          std::chrono::duration<double> elapsed_seconds = end-start;
          thread_local double max_runtime = 0., max_duration = 0., accumulated_runtime = 0.;
          thread_local int reduction = 0;
          if(executed) { 
            if(elapsed_seconds.count()>max_runtime)max_runtime = elapsed_seconds.count();
            accumulated_runtime += elapsed_seconds.count();
            if(period.toSec()>max_duration)max_duration = period.toSec();

            if(reduction++==10000) {
                ROS_DEBUG_STREAM("maximum compute time (wall): " << max_runtime);
                ROS_DEBUG_STREAM("average compute time (wall): " << accumulated_runtime/10000.);
                ROS_DEBUG_STREAM("maximum cycle (ros): " << max_duration);
                reduction = 0;
                max_runtime = 0.;
                accumulated_runtime = 0.;
                max_duration = 0.;
            }
          }
      } else {
          // But in effort mode it means that we are sending 0
          // Therefore implements a default PD controller on the system.
          if (control_mode_==EFFORT) {
              localStandbyEffortControlMode(timestamp,period);
          } else if (control_mode_==POSITION) {
              localStandbyPositionControlMode(timestamp);
          }
      }
      std_msgs::Bool controller_active_msg;
      controller_active_msg.data = controller_active_;
      controller_active_publisher_.publish(controller_active_msg);
      pb_.trigger_send();
      //feenableexcept(0);
#       ifdef EIGEN_RUNTIME_NO_MALLOC
      Eigen::internal::set_is_malloc_allowed(was_malloc_allowed);
#       endif
      updateForwarding(ros_time,period);
  }

  template<typename T>
  void PBWBCController<T>::startingForwarding(const ros::Time & ros_time) {}
  
  template<typename T>
  void PBWBCController<T>::stoppingForwarding(const ros::Time & ros_time) {}
  
  template<typename T>
  void PBWBCController<T>::starting(const ros::Time & ros_time) {
    startingForwarding(ros_time);
    timestamp_t timestamp(timestamp_t::duration(ros_time.toSec()));
    // rename the real-time thread so it can be identified easier
    pthread_setname_np(pthread_self(),"roscontrol");
    
    // straight away turn on pbwbc (do not turn on with standby controller)
    // TODO: make this dependant on a parameter
    controller_active_ = true;
    softstop_request_ = false;
    start_request_ = false;
    
    tau_simulation_.setZero();
    jointTorque_first_iteration_ = true;
    
    if(controller_active_) {
        ROS_DEBUG_STREAM("PBWBCController::starting");
        fillSensors();
        model_->update(timestamp); //necessary here so start() of state estimation has correct model
        state_estimation_->start(timestamp);
        state_estimation_->update(timestamp);
        for(unsigned int idJoint=0;idJoint<joints_name_.size();idJoint++) {
          state_estimation_->state()->y(q_offset_+idJoint) = jointPosition(idJoint);
          state_estimation_->state()->dy(dq_offset_+idJoint) = jointVelocity(idJoint);
        }
        model_->update(timestamp); //necessary here so start() calls have access to the correct model
        pbwbc_->start(timestamp);
    }
  }
    
  template<typename T>
  void PBWBCController<T>::stopping(const ros::Time & ros_time) {
    stoppingForwarding(ros_time);

    if(controller_active_) {
        ROS_DEBUG_STREAM("PBWBCController::stopping");
        timestamp_t timestamp(timestamp_t::duration(ros_time.toSec()));
        pbwbc_->stop(timestamp);
        state_estimation_->stop(timestamp);
        pb_.trigger_write_to_file("/tmp/softstop.bag");
    }
  }
  
  template<typename T>
  std::string PBWBCController<T>::getHardwareInterfaceType() const {
      //return type_name_;
      if (control_mode_==POSITION) {
          return lhi::internal::demangledTypeName<lhi::PositionJointInterface>();
      } else if (control_mode_==EFFORT) {
          return lhi::internal::demangledTypeName<lhi::EffortJointInterface>();
      }
      std::string voidstring("");
      return voidstring;
  }
    
  template<typename T>
  void PBWBCController<T>::joystick_subscriber_callback(const sensor_msgs::JoyConstPtr & m) {
        if(softstop_button_idx_!=-1 && controller_active_ && m->buttons.at(softstop_button_idx_)) {
            softstop_request_ = true;
            ROS_ERROR_STREAM("Received softstop request from /joy topic");
        }
  }

  template<typename T>
  bool PBWBCController<T>::loadEtras(ros::NodeHandle& controller_nh) {
    return true;
  }
  
  template<typename T>
  void PBWBCController<T>::startingExtras(const ros::Time& time) {}
  
  template<typename T>
  void PBWBCController<T>::updateExtras(const ros::Time& time, const ros::Duration& period) {}
  
  template<typename T>
  void PBWBCController<T>::stoppingExtra(const ros::Time& time) {}
  
  template<typename T>
  double PBWBCController<T>::jointPosition(int idx) {
    return joints_[idx].getPosition();
  }
  
  template<typename T>
  double PBWBCController<T>::jointVelocity(int idx) {
    return joints_[idx].getVelocity();
  }
  
  template<typename T>
  double PBWBCController<T>::jointTorque(int idx) {
    // fake joint torques sensor for simulation
    if(jointTorque_first_iteration_) {
        return 0;
    } else {
        return tau_simulation_(idx);
    }
  }

  template<typename T>
  void PBWBCController<T>::create_joint_mapping () {}
    
  template<typename T>
  std::shared_ptr<StateEstimation> PBWBCController<T>::create_hardware_stateestimation(
          std::shared_ptr<Model> const&,
          ros::NodeHandle const &) {
    throw std::runtime_error("Not available");
  }
          
  
  template<typename T>
  bool PBWBCController<T>::callback_trigger_rosbag (
          pbwbc::triggerRosbagRequest & request,
          pbwbc::triggerRosbagResponse & response) {
        PBWBC_DEBUG("Got rosbag request with topic_filter = \"" << request.topic_filter.data << "\"");
        pb_.write_to_file(request.filename.data,
                request.topic_filter.data);
        if(request.download_data.data) {
            std::ifstream rosbag_file(request.filename.data);
            if(!rosbag_file.good()) {
                PBWBC_DEBUG("Failed to open rosbag file");
                return false;
            }
            rosbag_file.seekg(0,std::ios::end);
            response.rosbag_data.data.resize(rosbag_file.tellg());
            rosbag_file.seekg(0,std::ios::beg);
            response.rosbag_data.layout.dim.resize(1);
            response.rosbag_data.layout.dim[0].label = "data";
            response.rosbag_data.layout.dim[0].size = response.rosbag_data.data.size();
            response.rosbag_data.layout.dim[0].stride = 1;
            rosbag_file.read(reinterpret_cast<char*>(&response.rosbag_data.data[0]),
                    response.rosbag_data.data.size());
        }
        return true;
  }
  
  template<typename T>
  bool PBWBCController<T>::callback_controller_switch (
          pbwbc::controllerSwitchRequest & request,
          pbwbc::controllerSwitchResponse & response) {
        if(request.activate_controller==1) {
            if(!controller_active_) {
                ROS_INFO_STREAM("Starting controller");
                start_request_ = true;
                // finish previously running softstop command thread
                if(softstop_command_thread_.joinable()) {
                    softstop_command_thread_.join();
                }
                response.status = 0;
                return true;
            }
        } else {
            if(controller_active_) {
                ROS_INFO_STREAM("Stopping controller");
                softstop_request_ = true;
                response.status = 0;
                return true;
            }
        }
        response.status = 1;
        return true;
  }

}
