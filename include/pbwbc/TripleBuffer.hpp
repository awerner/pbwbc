#pragma once
#include <atomic>
#include <algorithm>
#include <array>
#include <pbwbc/Base.hpp>

namespace pbwbc {

/** 
 * \brief Triple buffer implementation for lock free communication.
 *
 * A TripleBuffer implementation for a producer/consumer
 * arrangement. See https://en.wikipedia.org/wiki/Multiple_buffering#Triple_buffering
 * for the concept. Designed to allow two threads to exchange data without
 * any locking. Potentially adds delay to the communication.
 *
 * The producing thread uses the API:
 * - getWriteBuffer()
 * - commitWriteBuffer()
 *
 * The consuming thread uses the API:
 * - ready()
 * - getReadBuffer()
 * - freeReadBuffer()
 *
 * TODO:
 * - avoid using older data then necessay
 * - allocator for eigen objects neccessary?
 * - instrument this the valgrind/helgrind shows no false positives
 *
 * Code inspired by    
 * https://codereview.stackexchange.com/questions/163810/lock-free-zero-copy-triple-buffer
 */
template<typename T>
class TripleBuffer {
    std::atomic<T *> present_buffer;
    std::atomic<T *> ready_buffer;
    std::atomic<T *> inprogress_buffer;
    std::atomic<bool> ready_flag;

    std::array<T,3> buffers;

public:
    // if buffer contains fixed size Eigen variables this
    // might be necessary
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    /** Test if this buffer has been written to once before the
     *  consumer will get valid data when reading. */
    bool ready() const {
        return ready_flag.load();
    }

    /** Get a reference for the current write buffer */
    T & getWriteBuffer() {
        return *inprogress_buffer.load();
    }

    /** After writing the data with getWriteBuffer(), this needs to be called
     * to make the data accessible to the consumer */
    void commitWriteBuffer() {
        if(!ready_flag.load()) {
            // copy this buffer to other to finish initialization
            T const & init_data = *inprogress_buffer.load();
            *ready_buffer.load() = init_data;
            *present_buffer.load() = init_data;
            ready_flag.store(true);
        }
        T *p = ready_buffer.exchange(inprogress_buffer);
        inprogress_buffer = p;
    }

    /** Get a const reference to the current read buffer */
    T const & getReadBuffer() const {
        return *present_buffer.load();
    }

    /** After reading the data with getReadBuffer(), this needs to be called to
     * return this buffer to the system */
    void freeReadBuffer() {
        // do not return buffer if no newer data is available
        T *p = ready_buffer.exchange(present_buffer);
        present_buffer = p;
    }

    /** assign \p init to all buffer slots */
    void assignAll( T const & init ) {
        std::fill(buffers.begin(),buffers.end(),init);
    }

    /** Reset the state of the buffer so that ready() does return true until the next write */
    void reset() {
        ready_flag.store(false);
    }

    //TripleBuffer() = delete;
    //TripleBuffer(triple_buffer& other) = delete;
    //TripleBuffer(triple_buffer&& other) = delete;
    //TripleBuffer& operator=(triple_buffer& other) = delete;
    TripleBuffer()
        : present_buffer(&buffers[0]),
          ready_buffer(&buffers[1]),
          inprogress_buffer(&buffers[2]),
          ready_flag(false) {}

    /** Construct a TripleBuffer and initialize the buffer contents with \p init */
    TripleBuffer(T const & init)
        : present_buffer(&buffers[0]),
          ready_buffer(&buffers[1]),
          inprogress_buffer(&buffers[2]),
          ready_flag(false),
          buffers{init,init,init} {}

    /** Copy construct a TripleBuffer */
    TripleBuffer(TripleBuffer const & m) :
          ready_flag(false),
          buffers(m.buffers) {
            present_buffer.store(&buffers[0]);
            ready_buffer.store(&buffers[1]);
            inprogress_buffer.store(&buffers[2]);
        }

};

}


