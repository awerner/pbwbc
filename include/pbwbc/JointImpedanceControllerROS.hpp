#ifndef PBWBC_JOINTIMPEDANCECONTROLLER_ROS_HPP
#define PBWBC_JOINTIMPEDANCECONTROLLER_ROS_HPP
#include <pbwbc/JointImpedanceController.hpp>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <pbwbc/ROSTools.hpp>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>

namespace pbwbc {

// TODO:
// * implement JointTrajectoryController interface
class PBWBC_EXPORT JointImpedanceControllerROS : public JointImpedanceController {
public:
    JointImpedanceControllerROS(
            std::shared_ptr<RobotState> const &,
            std::shared_ptr<Model> const &,
            std::shared_ptr<ConfigMap> const &config,
            ros::NodeHandle const & nh
            );
    virtual ~JointImpedanceControllerROS();

    void update(timestamp_t const &);

protected:
    ros::NodeHandle nh_;
    RealTimePublisher<sensor_msgs::JointState> desired_state_publisher_;
    RealTimePublisher<sensor_msgs::JointState> current_state_publisher_;
    RealTimePublisher<sensor_msgs::JointState> desired_torque_publisher_;
    
    std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr_;
};

} // namespace pbwbc
#endif // PBWBC_JOINTIMPEDANCECONTROLLER_ROS_HPP
