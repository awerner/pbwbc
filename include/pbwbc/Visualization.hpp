#ifndef PBWBC_VISUALIZATION_HPP
#define PBWBC_VISUALIZATION_HPP
#include <pbwbc/Base.hpp>
#include <pbwbc/Model.hpp>

namespace pbwbc {

// Generic wrapper for a visualization. Can 
// * show the robot in a configuration,
// * show the robot going through a trajectory
class PBWBC_EXPORT Visualization {
public:
    Visualization(std::shared_ptr<Model> const &);
    virtual ~Visualization();

    typedef Model::configuration_t configuration_t;

    void show_configuration(configuration_t const & y);

    class configuration_with_time {
        public:
            double t;
            configuration_t y;
    };
    void show_trajectory(std::vector<configuration_with_time> const &);

protected:

};

} // namespace

#endif // PBWBC_VISUALIZATION_HPP
