#ifndef PBWBC_JOINTTRAJECTORYGENERATOR_HPP
#define PBWBC_JOINTTRAJECTORYGENERATOR_HPP

#include <pbwbc/Controller.hpp>
#include <pbwbc/Config.hpp>
#include <pbwbc/Model.hpp>
#include <pbwbc/State.hpp>


namespace pbwbc {

class JointTrajectoryGenerator {
    public:
        JointTrajectoryGenerator(
                    std::shared_ptr<Model> const & model,
                    std::shared_ptr<ConfigMap> const & config);
        virtual ~JointTrajectoryGenerator();

        virtual void start(timestamp_t const & timestamp);
        virtual void stop(timestamp_t const & timestamp);
        virtual void update(timestamp_t const & timestamp);
        
        virtual void setInitialState(RobotState & state);

        virtual void startMotion(timestamp_t const & timestamp, const RobotState start_point, const RobotState final_point, double duration);   
        virtual void stopMotion(timestamp_t const & timestamp);

        RobotState const & getState() const;
        
        virtual bool isRunning(timestamp_t const &) const;

    protected:

        std::shared_ptr<Model> model_;
        std::shared_ptr<ConfigMap> config_;

        timestamp_t start_time_;
        double trajectory_time_ratio_;
        double trajectory_time_ratio2_;
        double trajectory_time_ratio3_;
        double trajectory_time_ratio4_;
        double trajectory_time_ratio5_;
        double trajectory_duration_;

        RobotState start_state_;
        RobotState final_state_;
        RobotState state_;

};


} // namespace pbwbc
#endif // PBWBC_JOINTTRAJECTORYGENERATOR_HPP