#ifndef PBWBC_ALGEBRA_HPP
#define PBWBC_ALGEBRA_HPP
#include <pbwbc/Base.hpp>

namespace pbwbc {

template<typename Derived>
inline Eigen::Matrix<typename Derived::Scalar,3,3>
skew(Eigen::MatrixBase<Derived> const & x){
    typedef typename Derived::Scalar Scalar;
	Eigen::Matrix<Scalar,3,3> result;
	result << Scalar(0.),-x(2),x(1),
			  x(2),Scalar(0.),-x(0),
			 -x(1),x(0),Scalar(0.);
	return(result);
}

template<typename T>
inline Eigen::Matrix<T,6,6>
adjoint(Eigen::Transform<T,3,Eigen::Isometry> const &trafo){
	Eigen::Matrix<T,6,6> result;
	result.template block<3,3>(0,0) = trafo.linear();
	result.template block<3,3>(0,3) = skew(trafo.translation()) * trafo.linear();
	result.template block<3,3>(3,0).setZero();
	result.template block<3,3>(3,3) = trafo.linear();
	return(result);
}

template<typename T>
Eigen::Matrix<T,6,6> adjoint(
        Eigen::Ref<Eigen::Matrix<T,3,1>> const & r,
        Eigen::Quaternion<T> const & q) {
    Eigen::Transform<T,3,Eigen::Isometry> trafo;
    trafo.setIdentity();
    trafo.linear() = q.toRotationMatrix();
    trafo.translation() = r;
    return adjoint(trafo);
}

template<typename T>
inline Eigen::Matrix<T,6,6>
inverseAdjoint(Eigen::Transform<T,3,Eigen::Isometry> const & g){
	Eigen::Matrix<T,6,6> result;
	result.template block<3,3>(0,0) =  g.linear().transpose();
	result.template block<3,3>(0,3) =
			-g.linear().transpose() * Skew(g.translation());
	result.template block<3,3>(3,0).setZero();
	result.template block<3,3>(3,3) =  g.linear().transpose();
	return result;
}

// https://github.com/ethz-asl/maplab/blob/88d8587958c6902e7304a5ef7d88e867b30924b3/common/maplab-common/include/maplab-common/quaternion-math-inl.h
template <typename T>
inline Eigen::Quaternion<typename T::Scalar> ExpMap(
    Eigen::MatrixBase<T> & theta) {
  const typename T::Scalar theta_squared_norm = theta.squaredNorm();

  if (theta_squared_norm < 1e-6) {
    Eigen::Quaternion<typename T::Scalar> q(
        1, theta(0) * 0.5, theta(1) * 0.5, theta(2) * 0.5);
    q.normalize();
    return q;
  }

  const typename T::Scalar theta_norm = sqrt(theta_squared_norm);
  const Eigen::Matrix<typename T::Scalar, 3, 1> q_imag =
      sin(theta_norm * 0.5) * theta / theta_norm;
  Eigen::Quaternion<typename T::Scalar> q(
      cos(theta_norm * 0.5), q_imag(0), q_imag(1), q_imag(2));
  return q;
}

template<typename T>
inline Eigen::Quaternion<typename T::Scalar>
integrate_quaternion(Eigen::MatrixBase<T> const & omega, Eigen::Quaternion<typename T::Scalar> const & q_0, typename T::Scalar delta_t) {
    Eigen::Vector3d theta = omega * delta_t;
    Eigen::Quaterniond e = ExpMap(theta);
    return q_0 * e;
}

}

#endif // PBWBC_ALGEBRA_HPP
