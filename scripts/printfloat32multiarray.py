#!/usr/bin/env python



import pbwbc.pbwbc as pbwbc
import rosbag
import sys
import numpy as np
np.set_printoptions(edgeitems=30, linewidth=100000, precision=5)
if len(sys.argv)!=3:
	print("Usage printfloat32multiarray.py ROSBAG_FILENAME TOPIC_NAME")
rosbag.Bag(sys.argv[1])
mybag = rosbag.Bag('/tmp/softstop.bag')
for msg in mybag:
  if msg.topic==sys.argv[2]:
    print(pbwbc.float32multiarray2numpy(msg.message))
    print("---------------\n")
