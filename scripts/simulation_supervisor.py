#!/usr/bin/env python

try:
	status = 0
	launch = None
	print "Starting roscore"
	import subprocess
	import os
	roscore_process = subprocess.Popen("roscore",shell=True,preexec_fn=os.setsid)
	print "roscore started"
	# wait for roscore to start
	import rospy
	print "Registering supervisor node"
	rospy.init_node("simulation_supervisor")
	print "Registered supervisor node"
	
	print "Starting gazebo"
        pbwbc_package_path = subprocess.check_output("rospack find pbwbc",shell=True).strip()
	import roslaunch
	uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
	roslaunch.configure_logging(uuid)
	#cli_args = ['/home/awerner/pbwbc_ws/src/pbwbc/launch/pbwbc_gazebo.launch','gui:=false']
	#roslaunch_args = cli_args[1:]
	#roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(cli_args)[0], roslaunch_args)]
	launch = roslaunch.parent.ROSLaunchParent(uuid, [pbwbc_package_path+"/launch/pbwbc_gazebo.launch"])
	launch.start()
	#import subprocess
	# start gazebo through roslaunch
	#process = subprocess.Popen("roslaunch pbwbc pbwbc_gazebo.launch gui:=false",shell=True)

	
	# wait for gazebo to start
	from rosgraph_msgs.msg import Clock
	print "Waiting for gazebo"
	msg = rospy.wait_for_message("/clock",Clock,timeout=20.)
	print "Gazebo starting sending clock"
	# wait for controller to start
	from sensor_msgs.msg import JointState
	print "Waiting for controller"
	msg = rospy.wait_for_message("/pbwbc_controller/current_state", JointState,timeout=20.)
	print "Controller starting sending current_state"
	start_msg = rospy.wait_for_message("/clock",Clock,timeout=20.)

	print "Waiting for N secs of successful simulation"
	import pbwbc
	con = pbwbc.PBWBCController("/pbwbc_controller")
	import time
	time.sleep(1) # wait for for controller status message to arrive
	while True:
		msg = rospy.wait_for_message("/clock",Clock,timeout=1.)
		if ( msg.clock.secs - start_msg.clock.secs ) > 10.:
			print "Controller still active, considering this a success. Terminating the simulation"
			status = 0
			break
		#print "active: ", con.active()
		if not con.active():
			status = -2
			raise Exception("Controller not active any more")

except Exception as e:
	print "Got exception ",e
	status = -1
finally:
	# if done, terminate all processes
	if launch is not None:
		launch.shutdown()
	import signal
	pgrp = os.getpgid(roscore_process.pid)
	os.killpg(pgrp,signal.SIGINT)
	#roscore_process.send_signal(signal.SIGHUP)
	print "Waiting for roscore to terminate"
	roscore_process.wait()
	print "roscore has terminated"
	# report success
	print("Status {}".format(status))
	quit(status)
