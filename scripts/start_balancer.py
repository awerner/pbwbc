#!/usr/bin/env python

import rospy
rospy.init_node("test_client")

from pbwbc import *

mcfd = MultiContactForceDistribution()
joint = JointImpedanceController()
action = ActionInterface('pbwbc_controller')
pbwbc = PBWBCController('/pbwbc_controller')
import time
time.sleep(0.5)

if ['/gazebo/link_states','gazebo_msgs/LinkStates'] in rospy.get_published_topics():
    print("Simulation detected")
    simulation = True
else:
    simulation = False

if simulation:
    button_idx = None
    keyboard = True
else:
    button_idx = 1
    keyboard = None
waitforevent = WaitForEvent(button=button_idx,keyboard=keyboard,speak=(not simulation))


if not simulation:
    tts = TextToSpeech()
if not pbwbc.active():
    if not simulation:
        tts.say("Controller not active")
    raise Exception("Controller not active")

# TODO: raw_input or gamepad command
waitforevent.wait('Confirm to move to start position')
joint_names = ['leg_left_1_joint', 'leg_left_2_joint',
  'leg_left_3_joint', 'leg_left_4_joint', 'leg_left_5_joint', 'leg_left_6_joint', 'leg_right_1_joint',
  'leg_right_2_joint', 'leg_right_3_joint', 'leg_right_4_joint', 'leg_right_5_joint', 'leg_right_6_joint',
  'torso_1_joint', 'torso_2_joint', 'arm_left_1_joint', 'arm_left_2_joint', 'arm_left_3_joint',
  'arm_left_4_joint', 'arm_left_5_joint', 'arm_left_6_joint', 'arm_left_7_joint', 'gripper_left_joint',
  'arm_right_1_joint', 'arm_right_2_joint', 'arm_right_3_joint', 'arm_right_4_joint', 'arm_right_5_joint',
  'arm_right_6_joint', 'arm_right_7_joint', 'gripper_right_joint', 'head_1_joint', 'head_2_joint']
joint_positions = [0.3007355267588476, 0.10164456220124762, -0.3013610438523049, 0.6061605894321112, -0.30188959459518294, -0.10064112320417884, -0.3007043483073977, -0.10167199902667878, -0.3012389155866311, 0.6061755692367949, -0.30203064694478066, 0.10066152169064857, 2.775368901808406e-06, 0.0006471313379492472, -0.00017657721872232912, 0.1489666432892891, -0.0004436096253197519, -0.4995621013225513, 2.1402354342114904e-05, -0.0005492325680043209, 0.00040526636324589305, -1.814964656876583e-05, 0.0001742439245058236, -0.1489709292291037, 0.00043601094360568027, -0.499568514027648, -3.280819571571669e-05, 0.0005544466150082528, 0.0003844926234206625, -1.7190035572767215e-05, 0.00032706238993540637, 1.3316074642943931e-06]
exec_time = 2.
for info in zip(joint_names,joint_positions):
    action.moveJoint(info[0], [info[1]], 0., exec_time, absolute=True)
import time
print("Waiting for motion to finish")
time.sleep(exec_time)

if not simulation:
    tts.say("Place me on the ground now, waiting for contact normal force to be greater than 100 Newton ")
while True:
    contacts_detected = 0
    for contact in mcfd.contacts:
        if contact.name.find("Foot")!=-1:
            if contact.measured_wrench().wrench.force.z > 100.:
                contacts_detected = contacts_detected + 1
    if contacts_detected==2:
        break

if not pbwbc.active():
    if not simulation:
        tts.say("Controller not active")
    raise Exception("Controller not active")


waitforevent.wait('Confirm to enable balancer forces')
mcfd.set({
    'balancer_output_enable': 1.,
    })

waitforevent.wait('Hit enter to increase com stiffness to normal')
mcfd.reset() # reset all desired positions (base and contacts)

mcfd.set({
    'balancer_K_0': 1000.,
    'balancer_K_1': 1000.,
    'balancer_K_2': 3000.,
    
    'balancer_K_3': 500.,
    'balancer_K_4': 500.,
    #'balancer_K_5': 150., # TODO: fix rotation drift bug
    })

waitforevent.wait('Confirm to move COM to support center')
base_quaternion = [
        mcfd.current_state().transform.rotation.x,
        mcfd.current_state().transform.rotation.y,
        mcfd.current_state().transform.rotation.z,
        mcfd.current_state().transform.rotation.w]
support_center_x = 0.
support_center_y = 0.
support_center_z = 0.
for contact in mcfd.contacts:
    if contact.name.find("Foot")!=-1:
        #print(contact.cartimp.state())
        support_center_x = support_center_x + contact.cartimp.state().transform.translation.x
        support_center_y = support_center_y + contact.cartimp.state().transform.translation.y
        support_center_z = support_center_z + contact.cartimp.state().transform.translation.z
support_center_x = support_center_x / 2.
support_center_y = support_center_y / 2.
support_center_z = support_center_z / 2.

action.moveCOM([support_center_x,support_center_x,
                mcfd.current_state().transform.translation.z-support_center_z]+base_quaternion,
        0., 2., absolute=True)

if not pbwbc.active():
    if not simulation:
        tts.say("Controller not active")
    raise Exception("Controller not active")
# TODO: wait and move to center of support polygon

# reenable Cartesian stiffness on hand contacts
waitforevent.wait("hit enter to enable cartesian impedances for hands")
for contact in mcfd.contacts:
    if contact.name.find("Hand")!=-1:
        contact.cartimp.reset()
        contact.cartimp.set({
            'stiffness_step_max': 0.2,
            'stiffness_0': 100.,
            'stiffness_1': 100.,
            'stiffness_2': 100.,
            'stiffness_3': 10.,
            'stiffness_4': 10.,
            'stiffness_5': 10.,
            })

if not pbwbc.active():
    if not simulation:
        tts.say("Controller not active")
    raise Exception("Controller not active")
waitforevent.wait('Hit enter to ramp down joint impedance')
# lower joint stiffness 
joint.set({
    'stiffness_leg_left_1_joint': 0.,
    'stiffness_leg_left_2_joint': 0.,
    'stiffness_leg_left_3_joint': 0.,
    'stiffness_leg_left_4_joint': 0.,
    'stiffness_leg_left_5_joint': 0.,
    'stiffness_leg_left_6_joint': 0.,
    'stiffness_leg_right_1_joint': 0.,
    'stiffness_leg_right_2_joint': 0.,
    'stiffness_leg_right_3_joint': 0.,
    'stiffness_leg_right_4_joint': 0.,
    'stiffness_leg_right_5_joint': 0.,
    'stiffness_leg_right_6_joint': 0.,
    'stiffness_torso_1_joint': 10.,
    'stiffness_torso_2_joint': 100.,
    'stiffness_arm_left_1_joint': 0.,
    'stiffness_arm_left_2_joint': 0.,
    'stiffness_arm_left_3_joint': 5.,
    'stiffness_arm_left_4_joint': 0.,
    'stiffness_arm_left_5_joint': 0.,
    'stiffness_arm_left_6_joint': 0.,
    'stiffness_arm_left_7_joint': 0.,
    'stiffness_gripper_left_joint': 10.,
    'stiffness_arm_right_1_joint': 0.,
    'stiffness_arm_right_2_joint': 0.,
    'stiffness_arm_right_3_joint': 5.,
    'stiffness_arm_right_4_joint': 0.,
    'stiffness_arm_right_5_joint': 0.,
    'stiffness_arm_right_6_joint': 0.,
    'stiffness_arm_right_7_joint': 0.,
    'stiffness_gripper_right_joint': 10.,
    'stiffness_head_1_joint': 20.,
    'stiffness_head_2_joint': 20.,
    })

if not pbwbc.active():
    if not simulation:
        tts.say("Controller not active")
    raise Exception("Controller not active")
    

if not simulation:
    tts.say("Ready for that")

