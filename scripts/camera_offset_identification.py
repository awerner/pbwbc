#!/usr/bin/python

# load data from bag file
import rosbag
from geometry_msgs.msg import PoseStamped

import pinocchio as pin
import numpy
def transform_from_parameters(x):
    rx = pin.AngleAxis(x[3],numpy.matrix([1.,0.,0.], numpy.double).T)
    ry = pin.AngleAxis(x[4],numpy.matrix([0.,1.,0.], numpy.double).T)
    rz = pin.AngleAxis(x[5],numpy.matrix([0.,0.,1.], numpy.double).T)

    T = numpy.zeros(shape=(4,4))
    T[0:3,3] = x[0:3].T
    T[0:3,0:3] = rx.toRotationMatrix() * ry.toRotationMatrix() * rz.toRotationMatrix()
    T[3,3] = 1.
    return numpy.matrix(T)

def transform_from_msg(pose):
    T = numpy.zeros(shape=(4,4))
    T[0,3] = pose.position.x
    T[1,3] = pose.position.y
    T[2,3] = pose.position.z
    T[0:3,0:3] = pin.Quaternion(
            pose.orientation.w,
            pose.orientation.x,
            pose.orientation.y,
            pose.orientation.z).matrix()
    T[3,3] = 1.
    return numpy.matrix(T)

import pickle
import sys
filename = sys.argv[1]
import os

# (1) load data from rosbag file and preprocess (align, convert) and save it to a pickle file

if not os.path.exists(filename+".pickle") or os.path.getmtime(filename) > os.path.getmtime(filename+".pickle"):
    print "Updating "+filename+".pickle"
    print "Loading ",filename 
    bag = rosbag.Bag(filename)

    #model_filename = sys.argv[2]
    #print "Loading model",model_filename
    #model = pin.Model()
    #pin.buildModelFromUrdf(model_filename,model)

    # collect information from reference state estimation
    ref_state = {'t': [], 'pose': []}
    for topic, msg, t, in bag.read_messages(topics=['/sine_sweep_controller/estimator_odom']):
        ref_state['t'].append(t)
        ref_state['pose'].append(msg.pose.pose)
        # TODO: run kinematics to obtain torso_2 pose

    # update start/stop times
    t_start = ref_state['t'][0].to_sec()
    t_stop = ref_state['t'][-1].to_sec()

    #joint_state = {'t': [], 'state': []}
    #for topic, msg, t, in bag.read_messages(topics=['/joint_state']):
    #    joint_state['t'].append(t)
    #    joint_state['state'].append(msg.SOMETAG)
    #t_start = max(t_start,joint_state['t'][0].to_sec())
    #t_stop = min(t_stop,joint_state['t'][-1].to_sec())


    camera_names = [ "front", "rear" ]
    # construct dictionary which maps from camera_name -> {time list,pose list}
    camera_data = dict((name,{'t': [], 'pose': []}) for name in camera_names)

    for name in camera_names:
        for topic, msg, t, in bag.read_messages(topics=['/sine_sweep_controller/{0}/pose'.format(name)]):
            camera_data[name]['t'].append(t)
            camera_data[name]['pose'].append(msg.pose)
        # update start/stop time
        t_start = max(t_start,camera_data[name]['t'][0].to_sec())
        t_stop = min(t_stop,camera_data[name]['t'][-1].to_sec())

    print "t_start",t_start
    print "t_stop",t_stop

    bag.close()
    # create equidistant synchronized vectors of reference and camera poses
    for name,data in camera_data.iteritems():
        data['pose_set'] = []
        data['ref_pose_set'] = []
        ref_timevector = numpy.array(ref_state['t'])
        #joint_t = numpy.array(joint_state['t'])
        for t,pose in zip(data['t'],data['pose']):
            # remove out of time range samples
            if t.to_sec() < t_start or t_stop < t.to_sec(): continue
            data['pose_set'].append(transform_from_msg(pose))
            # find corresponding sample time in reference vector
            idx = numpy.argmin(numpy.abs(ref_timevector - t))

            data['ref_pose_set'].append(transform_from_msg(ref_state['pose'][idx]))
            #base_info = ref_state['pose'][idx]
            #idx = numpy.argmin(numpy.abs(joint_t - t))
            #y = base_info.xyz, base_info.quaternion_coeff, joint_state['state'][idx]
        print "Camera",name," has ",len(data['pose_set']),"samples"
        decimation = len(data['pose_set'])/1500
        for var in ['pose_set','ref_pose_set']:
            data[var] = data[var][0:-1:decimation]
        print "Camera",name," has ",len(data['pose_set']),"samples after decimation"
        del(data['t'])
        del(data['pose'])
    pickle.dump(camera_data,open(filename+".pickle",'wb'),protocol=2)
else:
    camera_data = pickle.load(open(filename+".pickle",'rb'))




# (2) Identify parameters: W_T_H and B_T_C per camera

# set up error function
def error_function(x,data):
    W_T_H = transform_from_parameters(x[0:6])  # optimized
    from numpy.linalg import inv
    C_T_B = inv(transform_from_parameters(x[6:12]))

    camera_errors = numpy.zeros(shape=(4*len(data['pose_set']),1))
    idx = 0
    for pose,ref_pose in zip(data['pose_set'],data['ref_pose_set']):
        W_T_B_s = ref_pose# reference data
        H_T_C = pose

        # compute W_T_B
        W_T_B = W_T_H * H_T_C * C_T_B

        translation_error = W_T_B[0:3,3] - W_T_B_s[0:3,3]
        orientation_error = 1e-3 * pin.Quaternion(W_T_B[0:3,0:3]).angularDistance(
                pin.Quaternion(W_T_B_s[0:3,0:3]))
        # compute translation and angular error
        camera_errors[idx:idx+3] = translation_error
        camera_errors[idx+3] = orientation_error
        idx = idx + 4
    return 1e3 * numpy.squeeze(camera_errors)


# minimize
from scipy.optimize import least_squares
x0 = (0.,0.,0., 0.,0.,0.) * 2
from numpy import pi
bounds = (((-5.,5.),) * 3 + ((-2*pi,2*pi),) * 3) * 2
lb = [x[0] for x in bounds]
ub = [x[1] for x in bounds]
for idx,name in enumerate(camera_data):
    print "#############################################"
    print "########################## Camera",name
    print "#############################################"
    local_results = []
    def worker(x0):
        res = least_squares(error_function, x0, bounds=(lb,ub), verbose=1,
                args=(camera_data[name],),max_nfev=1000)
        #print "###########"
        if not res.success:
            print "Failed run:",res.message
            print res.x
            print res.cost
            print "Status",res.status
        #if res.success:
        #    print "success"
        res.x0 = x0
        return res
    import multiprocessing
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    jobs = []
    pool = multiprocessing.Pool(8)
    
    guesses = [numpy.random.uniform(low=lb,high=ub) for i in range(4)]
    futures = [pool.apply_async(worker,(x0,)) for x0 in guesses]
    local_results = [res.get() for res in futures]
    print "Cost list", [x.cost for x in local_results]
    print "Best solution"
    for res in sorted(local_results,key=lambda x: x.cost)[0:1]:
        print "###########"
        print res.message
        print "cost:",res.cost
        print "grad:",res.grad
        print "params:",res.x
        x = res.x # TODO: take best and reoptimize
        W_T_H = transform_from_parameters(x[0:6])  # optimized
        print "W_T_H\n",W_T_H
        B_T_C = transform_from_parameters(x[6:12])
        print "B_T_C\n",B_T_C
        print "translation:",B_T_C[0:3,3].T
        print "Quaternion:",pin.Quaternion(B_T_C[0:3,0:3])
    

# Not usable approach:
#
#
## output parameters
#def classical():
#    # guesses could be generated with taking just the position into account and using SVD to align
#    # http://nghiaho.com/uploads/code/rigid_transform_3D.py_
#    def rigid_transform_3D(A, B):
#        assert len(A) == len(B)
#        N = A.shape[0]; # total points
#        centroid_A = numpy.mean(A, axis=0)
#        centroid_B = numpy.mean(B, axis=0)
#        # centre the points
#        AA = A - numpy.tile(centroid_A, (N, 1))
#        BB = B - numpy.tile(centroid_B, (N, 1))
#        # dot is matrix multiplication for array
#        H = numpy.transpose(AA) * BB
#        U, S, Vt = numpy.linalg.svd(H)
#        R = Vt.T * U.T
#        # special reflection case
#        if numpy.linalg.det(R) < 0:
#           print "Reflection detected"
#           Vt[2,:] *= -1
#           R = Vt.T * U.T
#        t = -R*centroid_A.T + centroid_B.T
#        #print t
#        return R, t
#    for idx,name in enumerate(camera_data):
#        print "#############################################"
#        print "########################## Camera",name
#        print "#############################################"
#        data = camera_data[name]
#        N = len(data['pose_set'])
#        data['pose_vector'] = numpy.zeros(shape=(N,3))
#        data['ref_pose_vector'] = numpy.zeros(shape=(N,3))
#        idx = 0
#        for pose,ref_pose in zip(data['pose_set'],data['ref_pose_set']):
#            data['pose_vector'][idx,:] = pose[0:3,3].T
#            data['ref_pose_vector'][idx,:] = ref_pose[0:3,3].T
#            idx = idx + 1
#        W_T_H_R,W_T_H_p = rigid_transform_3D(
#                numpy.mat(data['pose_vector']),
#                numpy.mat(data['ref_pose_vector']))
#
#        # project pose_set into W frame, then
#        W_T_H = numpy.mat(numpy.zeros(shape=(4,4)))
#        W_T_H[0:3,0:3] = W_T_H_R
#        W_T_H[0:3,3] = W_T_H_p
#        W_T_H[3,3] = 1.
#        print "W_T_H"
#        print "q: ",pin.Quaternion(W_T_H_R), "p",W_T_H_p
#        data['W_T_C_set'] = [W_T_H * x for x in data['pose_set']]
#        # TODO: compute avg offset and avg rotation
#        diff = numpy.array([x[0:3,3].T - y[0:3,3].T for x,y in zip(data["W_T_C_set"],data["ref_pose_set"]) ])
#        print numpy.squeeze(diff)
#        print numpy.mean(diff,axis=0)
#




