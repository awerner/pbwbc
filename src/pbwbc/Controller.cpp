#include <pbwbc/Controller.hpp>

namespace pbwbc {

Controller::Controller(std::string const & name, int dy_size):
    name_(name),
    tau_d_(Eigen::VectorXd::Zero(dy_size)) {
    //PBWBC_DEBUG("Created controller " << name_ << " with size " << dy_size);
}

Controller::~Controller() {}

void Controller::start(timestamp_t const &) {}

void Controller::stop(timestamp_t const &) {}

Eigen::VectorXd const & Controller::get_tau() const {
    return tau_d_;
}
    
bool Controller::hasTaskForces() const {
    return false;
}

Eigen::VectorXd const & Controller::getTaskForces() const {
    throw std::runtime_error("Task forces not available");
}

bool Controller::hasTaskJacobian() const {
    return false;
}

Eigen::MatrixXd const & Controller::getTaskJacobian() const {
    throw std::runtime_error("Task Jacobian not available");
}

std::string const & Controller::name() {
	return name_;	
}
 
std::vector<std::string> Controller::motionEntities() {
    return {};
}
    
bool Controller::isMotionRunning(timestamp_t const &, std::string const &) {
    throw std::runtime_error("isMotionRunning not implemented for this controller");
}

void Controller::stopMotion(timestamp_t const &, std::string const & ) {
    throw std::runtime_error("stopMotion not implemented for this controller");
}

void Controller::startMotion(timestamp_t const &, std::string const &, Eigen::Ref<Eigen::VectorXd> const &, double const, bool ){ 
    throw std::runtime_error("startMotion not implemented for this controller");
}
    
Eigen::VectorXd const & Controller::getMotionState(std::string const & entity) {
    throw std::runtime_error("getMotionState not implemented for this controller");
}

ControllerException::ControllerException(const std::string & what)
    : std::runtime_error(what) {}
    

} // namespace pbwbc
