

# A small wrapper for Gazebo to test the controllers
class Gazebo(object):
    def __init__(self):
        import rospy
        from gazebo_msgs.srv import ApplyBodyWrench
        self._apply_body_wrench = rospy.ServiceProxy('/gazebo/apply_body_wrench', ApplyBodyWrench)
        from gazebo_msgs.srv import GetLinkState
        self._get_link_state = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState)

    def apply_body_wrench(self,body_name, force, duration):
        from gazebo_msgs.srv import ApplyBodyWrenchRequest
        msg = ApplyBodyWrenchRequest()
        msg.body_name = body_name
        msg.wrench.force.x = force[0]
        msg.wrench.force.y = force[1]
        msg.wrench.force.z = force[2]
        msg.duration.secs = duration
        return self._apply_body_wrench(msg)

    def get_link_state(self,body_name):
        from gazebo_msgs.srv import GetLinkStateRequest
        msg = GetLinkStateRequest()
        msg.link_name = body_name
        return self._get_link_state(msg)


if __name__ == "__main__":
    g = Gazebo()
    print g.get_link_state("base_link").link_state.pose.position
    g.apply_body_wrench("base_link",[0.,50.,0.],1.)
    import time
    time.sleep(0.3)
    print g.get_link_state("base_link").link_state.pose.position

