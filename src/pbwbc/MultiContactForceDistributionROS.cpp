#include <pbwbc/MultiContactForceDistributionROS.hpp>
#include <pbwbc/CartesianImpedanceControllerROS.hpp>

namespace pbwbc {


Contact6DSurfaceROS::Contact6DSurfaceROS(Contact6DSurface const & x,
        ros::NodeHandle const & nh)
    : Contact6DSurface(x),
       nh_(nh) {

        // upgrade CartesianImpedanceController
        if(cartcontroller_) {
            cartcontroller_.reset(new CartesianImpedanceControllerROS(*cartcontroller_,
                        ros::NodeHandle(nh_,"CartesianImpedanceController")));
        }

        state_publisher_ = RealTimePublisherBackEnd::advertise<geometry_msgs::TransformStamped>(nh_,"transform",1000,10);

        measured_wrench_publisher_ = RealTimePublisherBackEnd::advertise<geometry_msgs::WrenchStamped>(nh_,"measured_wrench",1000,10);
        desired_wrench_publisher_ = RealTimePublisherBackEnd::advertise<geometry_msgs::WrenchStamped>(nh_,"desired_wrench",1000,10);

        contact_state_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::Int32Stamped>(nh_,"contact_state",1000,10);
        contact_state_timer_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::Float32Stamped>(nh_,"contact_state_timer",1000,10);
        contact_weights_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::Float32MultiArrayStamped>(nh_, "contact_weights",1000,10) ;

        CIT_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::Float32MultiArrayStamped>(nh_, "CIT",1000,1000);
        assign(CIT_publisher_.msg().data, CIT_);
        CIT_publisher_.fillBuffer();
        ci0_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::Float32MultiArrayStamped>(nh_, "ci0",1000,1000);
        assign(ci0_publisher_.msg().data, ci0_);
        ci0_publisher_.fillBuffer();

        
        // Parameters
        ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure(nh_));
        
        // Uncomment as needed:
        ddr_->RegisterVariable(&contact_velocity_max_, "contact_velocity_max", 0., 1.);
        ddr_->RegisterVariable(&contact_velocity_max_errors_, "contact_velocity_max_errors", 0, 200);
        ddr_->RegisterVariable(&contact_force_min_, "contact_force_min", 0., 200.);
        ddr_->RegisterVariable(&contact_force_max_errors_, "contact_force_max_errors", 0, 200);
        ddr_->RegisterVariable(&contact_mu_, "contact_mu", 0., 1.);
        ddr_->RegisterVariable(&zmp_x_positive_, "zmp_x_positive", 0., 0.2);
        ddr_->RegisterVariable(&zmp_x_negative_, "zmp_x_negative", -0.2, 0.);
        ddr_->RegisterVariable(&zmp_y_positive_, "zmp_y_positive", 0., 0.2);
        ddr_->RegisterVariable(&zmp_y_negative_, "zmp_y_negative", -0.2, 0.);
        ddr_->RegisterVariable(&tau_z_max_, "tau_z_max", 0., 100.);
        ddr_->RegisterVariable(&contact_request_, "contact_request", 0., 1.);
        ddr_->RegisterVariable(&contact_state_enabling_normal_time_, "contact_state_enabling_normal_time", 0., 5.);
        ddr_->RegisterVariable(&contact_state_disabling_normal_time_, "contact_state_disabling_normal_time", 0., 5.);
        ddr_->RegisterVariable(&contact_state_enabling_full_time_, "contact_state_enabling_full_time", 0., 5.);
        ddr_->RegisterVariable(&contact_state_disabling_full_time_, "contact_state_disabling_full_time", 0., 5.);
        ddr_->RegisterVariable(&constraint_normal_force_min_, "constraint_normal_force_min", 0., 200.);
        for(int idx = 0; idx < weights_max_.size() ; idx++) {
            std::stringstream str_weight; str_weight << "weight_" << idx;
            ddr_->RegisterVariable(&weights_max_(idx), str_weight.str(), 0., 100.);
        }
        ddr_->RegisterVariable(&regularization_max_, "regularization", 0., 0.1);

        ddr_->PublishServicesTopics();
}

void Contact6DSurfaceROS::update(timestamp_t const & timestamp) {
    ros::Time now(timestamp.time_since_epoch().count());
    Contact6DSurface::update(timestamp);
    // publish states
    if(state_publisher_.trylock()) {
        state_publisher_.msg().header.stamp = now;
        assign(state_publisher_.msg().transform,translation(),orientation());
        state_publisher_.unlockAndPublish();
    }
    if(measured_wrench_publisher_.trylock()) {
        measured_wrench_publisher_.msg().header.stamp = now;
        assign(measured_wrench_publisher_.msg().wrench,measured_wrench_);
        measured_wrench_publisher_.unlockAndPublish();
    }
    if(desired_wrench_publisher_.trylock()) {
        desired_wrench_publisher_.msg().header.stamp = now;
        assign(desired_wrench_publisher_.msg().wrench,-desired_wrench_);
        desired_wrench_publisher_.unlockAndPublish();
    }

    if(contact_state_publisher_.trylock()) {
        contact_state_publisher_.msg().header.stamp = now;
        contact_state_publisher_.msg().data.data = contact_state_;
        contact_state_publisher_.unlockAndPublish();
    }
    if(contact_state_timer_publisher_.trylock()) {
        contact_state_timer_publisher_.msg().header.stamp = now;
        contact_state_timer_publisher_.msg().data.data = contact_state_timer_;
        contact_state_timer_publisher_.unlockAndPublish();
    }
    if(contact_weights_publisher_.trylock()) {
        contact_weights_publisher_.msg().header.stamp = now;
        assign(contact_weights_publisher_.msg().data,weights_);
        contact_weights_publisher_.unlockAndPublish();
    }
    if(CIT_publisher_.trylock()) {
        CIT_publisher_.msg().header.stamp = now;
        assign(CIT_publisher_.msg().data, CIT_);
        CIT_publisher_.unlockAndPublish();
    }
    if(ci0_publisher_.trylock()) {
        ci0_publisher_.msg().header.stamp = now;
        assign(ci0_publisher_.msg().data, ci0_);
        ci0_publisher_.unlockAndPublish();
    }
}

MultiContactForceDistributionROS::MultiContactForceDistributionROS(
        std::shared_ptr<RobotState> const & state,
        std::shared_ptr<SensorState> const & sensor_state,
        std::shared_ptr<Model> const & model,
        std::shared_ptr<ConfigMap> const & config,
        ros::NodeHandle const & nh) :
    MultiContactForceDistribution(state,sensor_state,model,config),
    nh_(nh) {
        com_desired_state_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::StateStamped>(nh_,"com_desired_state",1000,10);
        com_current_state_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::StateStamped>(nh_,"com_current_state",1000,10);
        com_desired_wrench_publisher_ = RealTimePublisherBackEnd::advertise<geometry_msgs::WrenchStamped>(nh_,"com_desired_wrench",1000,10);
        com_realized_wrench_publisher_ = RealTimePublisherBackEnd::advertise<geometry_msgs::WrenchStamped>(nh_,"com_realized_wrench",1000,10);
        desired_torque_publisher_ = RealTimePublisherBackEnd::advertise<sensor_msgs::JointState>(nh_,"desired_torque",1000,10);
        desired_torque_publisher_.msg().name = model_->jointNames();
        desired_torque_publisher_.msg().effort.resize(tau_d_.size());
        desired_torque_publisher_.fillBuffer(); // resizes all RingBuffer entries to required size
        G_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::Float32MultiArrayStamped>(nh_, "qp_G",1000,1000);
        assign(G_publisher_.msg().data, G_);
        G_publisher_.fillBuffer();
        g0_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::Float32MultiArrayStamped>(nh_, "qp_g0",1000,1000);
        assign(g0_publisher_.msg().data, g0_);
        g0_publisher_.fillBuffer();
        JT_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::Float32MultiArrayStamped>(nh_, "JT",1000,1000);
        assign(JT_publisher_.msg().data, JT_);
        JT_publisher_.fillBuffer();
        
        //Parameters
        ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure(nh_));

        //Uncomment as needed:
        //ddr_->RegisterVariable(&balancer_ierror_velocity_threshold_, "balancer_ierror_velocity_threshold", 0., 1000.);
        
        //for(int idx = 0; idx < balancer_ierror_windup_.size(); idx++) {
        //    std::stringstream str_balancer_ierror_windup; str_balancer_ierror_windup << "balancer_ierror_windup_" << idx;
        //    ddr_->RegisterVariable(&balancer_ierror_windup_(idx), str_balancer_ierror_windup.str(), 0., 100.);
        //}
        
        //ddr_->RegisterVariable(&balancer_observer_gain_, "balancer_observer_gain", 0., 10.);

        //for(int idx = 0; idx < balancer_observer_feedback_gain_.size(); idx++) {
        //    std::stringstream str_balancer_observer_feedback_gain; str_balancer_observer_feedback_gain << "balancer_observer_feedback_gain_" << idx;
        //    ddr_->RegisterVariable(&balancer_observer_feedback_gain_(idx), str_balancer_observer_feedback_gain.str(), 0., 10.);
        //}

        for(int idx = 0; idx < balancer_com_weight_.size(); idx++) {
            std::stringstream str_balancer_com_weight; str_balancer_com_weight << "balancer_com_weight_" << idx;
            ddr_->RegisterVariable(&balancer_com_weight_(idx), str_balancer_com_weight.str(), 0., 100.);
        }

        ddr_->RegisterVariable(&balancer_regularization_, "balancer_regularization", 0., 0.1);

        for(int idx=0;idx<6;idx++) {
            std::stringstream str_K; str_K << "balancer_K_" << idx;
            ddr_->RegisterVariable(&K_balancer_des_(idx), str_K.str(), 0., 10000.);
            
            if(damping_mode_==damping_ratio) {
                std::stringstream str_damping_ratio; str_damping_ratio << "damping_ratio_" << idx;
                ddr_->RegisterVariable(&damping_ratio_balancer_(idx), str_damping_ratio.str(), 0., 1.);
            } else if(damping_mode_==damping_value) {
                std::stringstream str_D; str_D << "balancer_D_" << idx;
                ddr_->RegisterVariable(&D_balancer_(idx), str_D.str(), 0., 1000.);
            }
            
            //std::stringstream str_I; str_I << "balancer_I_" << idx;
            //ddr_->RegisterVariable(&I_balancer_(idx), str_I.str(), 0., 1000.);
        }
        ddr_->RegisterVariable(&K_balancer_step_, "K_balancer_step", 0., 1000.);
        ddr_->RegisterVariable(&balancer_output_enable_des_, "balancer_output_enable", 0., 1.);


        for(int idx = 0; idx < 3; idx++) {
            std::stringstream str_position; str_position << "pose_" << idx;
            ddr_->RegisterVariable(&state_translation_offset_d_(idx), str_position.str(), -3., 3.);
        }
        for(int idx = 0; idx < 3; idx++) {
            std::stringstream str_orientation; str_orientation << "pose_" << idx+3;
            ddr_->RegisterVariable(&state_orientation_offset_euler_d_(idx), str_orientation.str(), -3.14, 3.14);
        }
        ddr_->RegisterVariable(&update_pose_, "update_pose", 0., 1.);

        ddr_->PublishServicesTopics();

        // Upgrade Contacts to ROS
        for(auto & contact : contacts_ ) {
            if(typeid(*contact)==typeid(Contact6DSurface)) {
                contact.reset(new Contact6DSurfaceROS(*std::dynamic_pointer_cast<Contact6DSurface>(contact),
                            ros::NodeHandle(nh_,std::string("Contacts/")+contact->name())));
            }
        }

    }

MultiContactForceDistributionROS::~MultiContactForceDistributionROS(){}


void MultiContactForceDistributionROS::update(timestamp_t const & timestamp) {
    ros::Time now(timestamp.time_since_epoch().count());
    MultiContactForceDistribution::update(timestamp);
    // publish states
    if(com_desired_state_publisher_.trylock()) {
        com_desired_state_publisher_.msg().header.stamp = now;
        assign(com_desired_state_publisher_.msg().transform,state_d_.position.translation,state_d_.position.orientation);
        assign(com_desired_state_publisher_.msg().twist,state_d_.velocity.translation,state_d_.velocity.orientation);
        com_desired_state_publisher_.unlockAndPublish();
    }
    if(com_current_state_publisher_.trylock()) {
        com_current_state_publisher_.msg().header.stamp = now;
        assign(com_current_state_publisher_.msg().transform,cartesian_state_.position.translation,cartesian_state_.position.orientation);
        assign(com_current_state_publisher_.msg().twist,cartesian_state_.velocity.translation,cartesian_state_.velocity.orientation);
        com_current_state_publisher_.unlockAndPublish();
    }
    if(com_desired_wrench_publisher_.trylock()) {
        com_desired_wrench_publisher_.msg().header.stamp = now;
        assign(com_desired_wrench_publisher_.msg().wrench,balancer_wrench_);
        com_desired_wrench_publisher_.unlockAndPublish();
    }
    if(com_realized_wrench_publisher_.trylock()) {
        com_realized_wrench_publisher_.msg().header.stamp = now;
        assign(com_realized_wrench_publisher_.msg().wrench,W_x_realized_);
        com_realized_wrench_publisher_.unlockAndPublish();
    }
    if(desired_torque_publisher_.trylock()) {
      desired_torque_publisher_.msg().header.stamp = now;
      std::copy_n(&tau_d_(0), tau_d_.size(), &desired_torque_publisher_.msg().effort[0]);
      desired_torque_publisher_.unlockAndPublish();
    }

    if(G_publisher_.trylock()) {
        G_publisher_.msg().header.stamp = now;
        assign(G_publisher_.msg().data, G_);
        G_publisher_.unlockAndPublish();
    }
    if(g0_publisher_.trylock()) {
        g0_publisher_.msg().header.stamp = now;
        assign(g0_publisher_.msg().data, g0_);
        g0_publisher_.unlockAndPublish();
    }
    if(JT_publisher_.trylock()) {
        JT_publisher_.msg().header.stamp = now;
        assign(JT_publisher_.msg().data, JT_);
        JT_publisher_.unlockAndPublish();
    }
}

} // namespace
