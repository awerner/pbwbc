#include <pbwbc/State.hpp>

namespace pbwbc {

// State or Desired State
RobotState::RobotState(int y_size, int ydot_size) :
    y(y_size), dy(ydot_size), ddy(ydot_size) {}

RobotState::~RobotState() {}

} // ns
