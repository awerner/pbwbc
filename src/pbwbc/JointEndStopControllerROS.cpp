#include <pbwbc/JointEndStopControllerROS.hpp>

namespace pbwbc {

JointEndStopControllerROS::JointEndStopControllerROS(
            std::shared_ptr<RobotState> const & state,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config,
            ros::NodeHandle const & nh
            ) :
    JointEndStopController(state,model,config),
    nh_(nh)
    {
        desired_torque_publisher_ = RealTimePublisherBackEnd::advertise<sensor_msgs::JointState>(nh_,"desired_torque",1000,10);
        desired_torque_publisher_.msg().name = model_->jointNames();
        desired_torque_publisher_.msg().effort.resize(model_->jointNames().size());
        desired_torque_publisher_.fillBuffer();
            

        ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure(nh_));
        ddr_->RegisterVariable(&endstop_distance_,"endstop_distance",0.,1.);
        ddr_->RegisterVariable(&endstop_offset_,"enstop_offset",0.,1.);
        for (int idx : impedance_controlled_joints_ids_) {
            auto const & joint_name = model_->jointNames().at(idx);
            std::stringstream str_min; str_min << "position_min_" << joint_name;
            std::stringstream str_max; str_max << "position_max_" << joint_name;
            std::stringstream str_damping_ratio; str_damping_ratio << "xi_" << joint_name;
            ddr_->RegisterVariable(&position_min_(idx), str_min.str(), 
                    position_min_(idx)-0.1, position_min_(idx)+1.0);
            ddr_->RegisterVariable(&position_max_(idx), str_max.str(), 
                    position_max_(idx)-1.0, position_max_(idx)+0.1);
            ddr_->RegisterVariable(&damping_ratio_(idx), str_damping_ratio.str(), 0., 1.);
        }
        ddr_->PublishServicesTopics();
    }

JointEndStopControllerROS::~JointEndStopControllerROS() {}

    
void JointEndStopControllerROS::update(timestamp_t const & timestamp) {
  ros::Time now(timestamp.time_since_epoch().count());

  JointEndStopController::update(timestamp);

  if(desired_torque_publisher_.trylock()) {
      desired_torque_publisher_.msg().header.stamp = now;
      for(int idx = 0; idx<model_->q_size(); idx++) {
          desired_torque_publisher_.msg().effort[idx] = tau_d_(idx);
      }
      desired_torque_publisher_.unlockAndPublish();
  }

}

} // namespace
