#include <pbwbc/CartesianImpedanceController.hpp>
#include <pbwbc/ModelPinocchio.hpp>

namespace pbwbc {


CartesianImpedanceController::CartesianImpedanceController(
        std::shared_ptr<RobotState> const &state,
        std::shared_ptr<Model> const &model,
        std::shared_ptr<ConfigMap> const & config
        ) :
Controller([&](){
    std::string name;
    config->param("name",name,"CartesianImpedanceController");
    return name;}(),model->dq_size()),
state_(state),
model_(model),
config_(config),
wrench_(6),
jacobian_(Eigen::MatrixXd(6,model_->dy_size())){

        std::string frame_name;
        if(!config->getParam("frame_name",frame_name)) {
            throw std::runtime_error("CartesianImpedanceController: frame_name not found");
        }
        frame_id_ = model_->getFrameId(frame_name);

        //Set up the cartesian controller attached to the (misnamed) "contact"
        if (!config_->getParam("stiffness", K_desired_.diagonal())) {
            throw std::runtime_error("CartesianImpedanceController: parameter 'stiffness' not found");
        }
        K_ = K_desired_;
        K_local_.assignAll(K_);
        std::string stiffness_mode;
        config_->param("stiffness_mode",stiffness_mode,"linear");
        if(stiffness_mode=="linear"){
            stiffness_mode_ = stiffness_linear;
        }else if(stiffness_mode=="saturated") {
            stiffness_mode_ = stiffness_saturated;
            if(!config_->getParam("maximum_force",maximum_force_) ||
                    !config_->getParam("maximum_torque",maximum_torque_)) {
                throw std::runtime_error("Missing maximum_force or maximum_torque");
            }
        } else {
            throw std::runtime_error("Stiffness mode not supported");
        }
        bool damping_found = config_->getParam("damping", D_.diagonal());
        bool damping_ratios_found = config_->getParam("damping_ratios", xi_.diagonal());
        if(!damping_found || !damping_ratios_found) {
            throw std::runtime_error("CartesianImpedanceController: Need either damping gains or damping ratios");
        }
        config_->param("feedforward_gain",feedforward_gain_,0.);
        config_->param("stiffness_step_max",stiffness_step_max_,1.);
        
        if (!config_->getParam("damping_mode",     damping_mode_)) {
            throw std::runtime_error("CartesianImpedanceController: parameter 'damping_mode' not found");
        }


        if(auto pmodel = std::dynamic_pointer_cast<ModelPinocchio>(model_)) {
            damping_update_ = damping_computed_by_model_thread;
            model_compute_task_id_ = pmodel->register_compute_task([this](){
                    this->compute_model_callback();
                    });
        } else {
            model_compute_task_id_ = -1;
            damping_update_ = damping_computed_locally;
        }


        auto trajectory_generator_config = config_->submap("TrajectoryGenerator");
        trajectory_generator_.reset( new CartesianTrajectoryGenerator(
                                                            model_,
                                                            trajectory_generator_config) );
        update_pose_ = 0;
        
        state_translation_offset_d_.setZero();
        state_orientation_offset_euler_d_.setZero();
}
        

CartesianImpedanceController::~CartesianImpedanceController() {
    // delete the model task, it is bound to this instance
    if(model_compute_task_id_!=-1) {
        auto pmodel = std::dynamic_pointer_cast<ModelPinocchio>(model_);
        pmodel->deregister_compute_task(model_compute_task_id_);
    }
}

void CartesianImpedanceController::compute_model_callback() {
    D_model_compute_output_.getWriteBuffer() = this->compute_damping_with_double_diagonalization();
    D_model_compute_output_.commitWriteBuffer();
}

CartesianImpedanceController::CartesianImpedanceController(CartesianImpedanceController const & m) :
    Controller([&](){
        std::string name;
        m.config_->param("name",name,"CartesianImpedanceController");
        return name;}(),m.model_->dq_size()),
    state_(m.state_),
    model_(m.model_),
    config_(m.config_),
    stiffness_mode_(m.stiffness_mode_),
    maximum_force_(m.maximum_force_),
    maximum_torque_(m.maximum_torque_),
    damping_mode_(m.damping_mode_),
    frame_id_(m.frame_id_),
    damping_update_(m.damping_update_),
    cartesian_state_(m.cartesian_state_),
    goal_state_(m.goal_state_),
    state_d_(m.state_d_),
    state_initial_(m.state_initial_), 
    state_translation_offset_d_(m.state_translation_offset_d_),
    state_orientation_offset_euler_d_(m.state_orientation_offset_euler_d_),
    update_pose_(m.update_pose_),
    stiffness_step_max_(m.stiffness_step_max_),
    K_desired_(m.K_desired_),
    K_(m.K_),
    K_local_(m.K_local_),
    D_(m.D_),
    xi_(m.xi_),
    feedforward_gain_(m.feedforward_gain_),
    wrench_(m.wrench_),
    jacobian_(m.jacobian_),
    projected_inertia_(m.projected_inertia_),
    model_compute_task_id_(-1),
    D_model_compute_output_(m.D_model_compute_output_),
    trajectory_generator_(m.trajectory_generator_) {
        if(auto pmodel = std::dynamic_pointer_cast<ModelPinocchio>(model_)) {
            damping_update_ = damping_computed_by_model_thread;
            model_compute_task_id_ = pmodel->register_compute_task([this](){
                    this->compute_model_callback();
                    });
        }
    }

void CartesianImpedanceController::start(timestamp_t const & timestamp) {
    // update desired state from forward kinematics
    state_d_.position.translation = model_->bodyTranslation(frame_id_);
    state_d_.position.orientation = model_->bodyOrientation(frame_id_);
    state_d_.velocity.translation.setZero();
    state_d_.velocity.orientation.setZero();
    state_d_.acceleration.translation.setZero();
    state_d_.acceleration.orientation.setZero();

    state_initial_ = state_d_;
    goal_state_ = state_d_;
    state_translation_offset_d_.setZero();
    state_orientation_offset_euler_d_.setZero();

    trajectory_generator_->start(timestamp);
    trajectory_generator_->setInitialState(state_initial_);
        
}

void CartesianImpedanceController::stop(timestamp_t const & timestamp) {
    trajectory_generator_->stop(timestamp);
}

void CartesianImpedanceController::set_goal(
        Eigen::Ref<Eigen::Vector3d> const & translation,
        Eigen::Quaterniond const & orientation,
        bool hard) {
    // TODO: implement interpolator trigger
    state_d_.position.translation = translation;
    state_d_.position.orientation = orientation;
}

Eigen::Vector3d CartesianImpedanceController::get_translation_goal() {
    return state_d_.position.translation;
}

Eigen::Quaterniond CartesianImpedanceController::get_orientation_goal() {
    return state_d_.position.orientation;
}

Eigen::Vector3d CartesianImpedanceController::get_translation_state() {
    return cartesian_state_.position.translation;
}

Eigen::Quaterniond CartesianImpedanceController::get_orientation_state() {
    return cartesian_state_.position.orientation;
}

void CartesianImpedanceController::set_stiffness(Eigen::Ref<Eigen::MatrixXd> stiffness, bool hard) {
    if(stiffness.size()!=6) {
        throw std::runtime_error("stiffness does not have the correct size");
    }
    K_desired_.diagonal() = stiffness;
}

void CartesianImpedanceController::set_damping_mode(damping_mode_t damping_mode) {
    damping_mode_ = damping_mode;
}

void CartesianImpedanceController::set_damping(Eigen::Ref<Eigen::MatrixXd> damping, bool hard) {
    // TODO: implement damping interpolation
    if(damping.size()!=6) {
        throw std::runtime_error("damping does not have the correct size");
    }
    D_.diagonal() = damping;
}

void CartesianImpedanceController::set_damping_ratio(Eigen::Ref<Eigen::VectorXd> damping_ratios, bool hard) {
    if(damping_ratios.size()!=6) {
        throw std::runtime_error("damping_ratios does not have the correct size");
    }
    xi_.diagonal() = damping_ratios;
}

void CartesianImpedanceController::startMotion(timestamp_t const & timestamp, std::string const &,
        Eigen::Ref<Eigen::VectorXd> const & state_offset_d, double const duration, bool absolute) {

    if (state_offset_d.size() != 7){
        throw std::runtime_error("startMotion goal only 7 size goals");
    }
    if(absolute) {
        goal_state_.position.translation = state_offset_d.segment(0,3);
        goal_state_.position.orientation.coeffs() = state_offset_d.segment(3,4);
    } else {
        goal_state_.position.translation = state_initial_.position.translation + state_offset_d.segment(0,3);
        goal_state_.position.orientation = state_initial_.position.orientation *
            Eigen::Quaterniond(state_offset_d(6),state_offset_d(3),state_offset_d(4),state_offset_d(5));
    }
    trajectory_generator_->start6DMotion(timestamp, state_d_, goal_state_, duration); 
}
    
std::vector<std::string> CartesianImpedanceController::motionEntities() {
    return {this->name()};
}

bool CartesianImpedanceController::isMotionRunning(timestamp_t const & timestamp, std::string const &) {
    return trajectory_generator_->isRunning(timestamp);
}

void CartesianImpedanceController::stopMotion(timestamp_t const & timestamp, std::string const &) {
    trajectory_generator_->stopMotion(timestamp);
}
    
Eigen::VectorXd const & CartesianImpedanceController::getMotionState(std::string const & entity) {
    static Eigen::VectorXd buffer(7); // TODO: malloc
    buffer.segment(0,3) = goal_state_.position.translation;
    buffer.segment(3,4) = goal_state_.position.orientation.coeffs();
    return buffer;
}

void CartesianImpedanceController::update(timestamp_t const & timestamp) {
    PBWBC_TRACEF("CIC::update enter");
    
    // update cartesian_state_
    cartesian_state_.position.translation = model_->bodyTranslation(frame_id_);
    cartesian_state_.position.orientation = model_->bodyOrientation(frame_id_);

    // simple interface to control the desired position
    if (update_pose_ != 0){

        goal_state_.position.translation = state_initial_.position.translation + state_translation_offset_d_;
        goal_state_.position.orientation = state_initial_.position.orientation 
                                           * Eigen::AngleAxisd(state_orientation_offset_euler_d_(0), Eigen::Vector3d::UnitX())
                                           * Eigen::AngleAxisd(state_orientation_offset_euler_d_(1), Eigen::Vector3d::UnitY())
                                           * Eigen::AngleAxisd(state_orientation_offset_euler_d_(2), Eigen::Vector3d::UnitZ()); 

        trajectory_generator_->start6DMotion(timestamp, state_d_, goal_state_, 1.); 

        update_pose_ = 0;
    }
    trajectory_generator_->update(timestamp);
    state_d_ = trajectory_generator_->getState();

    // update jacobian
    auto & jacobian = jacobian_.getWriteBuffer();
    jacobian.setZero();
    model_->getJacobian(frame_id_,jacobian);
    // TODO: add offset for jacobian

    // translation error
    error_.head(3) << cartesian_state_.position.translation - state_d_.position.translation;

    // orientation error
    Eigen::Quaterniond orientation = cartesian_state_.position.orientation;
    if (state_d_.position.orientation.coeffs().dot(orientation.coeffs()) < 0.0) {
        orientation.coeffs() << -orientation.coeffs();
    }
    // "difference" quaternion
    Eigen::Quaterniond error_quaternion(orientation.inverse() * state_d_.position.orientation);
    error_.tail(3) << error_quaternion.x(), error_quaternion.y(), error_quaternion.z();
    // Transform to base frame
    error_.tail(3) = - orientation.toRotationMatrix() * error_.tail(3);

    // velocity
    Eigen::Matrix3d R_B = model_->bodyOrientation(frame_id_).toRotationMatrix();
    B_V_T_.noalias() = jacobian * state_->dy;
    W_V_T_.head(3) = R_B * B_V_T_.head(3);
    cartesian_state_.velocity.translation = W_V_T_.head(3);
    W_V_T_.tail(3) = R_B * B_V_T_.tail(3);
    cartesian_state_.velocity.orientation = W_V_T_.tail(3);
    W_V_Td_.head(3) = state_d_.velocity.translation;
    W_V_Td_.tail(3) = state_d_.velocity.orientation;
    derror_ = W_V_T_ - W_V_Td_;
    
    K_.diagonal().array() += (K_desired_.diagonal() - K_.diagonal()).array().max(-stiffness_step_max_).min(stiffness_step_max_);
    
    error_.head(3)  = R_B.transpose() * error_.head(3);
    error_.tail(3)  = R_B.transpose() * error_.tail(3);
    
    if(stiffness_mode_==stiffness_linear) {
        wrench_ = - ( K_ * error_ );
        K_local_.getWriteBuffer() = K_;
        K_local_.commitWriteBuffer();
    } else if (stiffness_mode_==stiffness_saturated) {
        Eigen::Matrix<double,6,1> p1;
        p1.head(3) = error_.head(3).array() * K_.diagonal().head(3).array() / maximum_force_;
        p1.tail(3) = error_.tail(3).array() * K_.diagonal().tail(3).array() / maximum_torque_;
        auto & local_stiffness = K_local_.getWriteBuffer().diagonal();
        for(int idx=0; idx<6; idx++) {
            local_stiffness(idx) = 
                std::pow( 1. / std::max(0.1, cosh(p1(idx))),2.) * K_.diagonal()(idx);
            local_stiffness(idx) = std::max(1e-4,local_stiffness(idx));
        }
        K_local_.commitWriteBuffer();
        wrench_.head(3).array() = - tanh(p1.head(3).array()) * maximum_force_;
        wrench_.tail(3).array() = - tanh(p1.tail(3).array()) * maximum_torque_;
    } else {
        throw std::runtime_error("Stiffness mode not implemented");
    }

    Eigen::Matrix<double,6,6> D;
    if(damping_mode_==damping_mode_manual) {
        D = D_;
    } else if(damping_mode_==damping_mode_double_diagonalization) {
        if(damping_update_==damping_computed_by_model_thread) {
            if(D_model_compute_output_.ready()) {
                D = D_model_compute_output_.getReadBuffer();
                D_model_compute_output_.freeReadBuffer();
            } else {
                D.setZero(); // results not ready yet
            }
        } else {
            D = compute_damping_with_double_diagonalization();
        }
    } else {
        throw std::runtime_error("Damping mode not implemented");
    }
    
    derror_.head(3) = R_B.transpose() * derror_.head(3);
    derror_.tail(3) = R_B.transpose() * derror_.tail(3);
    wrench_ -= D * derror_;

    acceleration_.head(3) = state_d_.acceleration.translation;
    acceleration_.tail(3) = state_d_.acceleration.orientation;


    if(feedforward_gain_!=0.) {
        if(damping_update_==damping_computed_by_model_thread) {
            if(projected_inertia_.ready()) {
                wrench_.noalias() += feedforward_gain_ * projected_inertia_.getReadBuffer() * acceleration_;
                projected_inertia_.freeReadBuffer();
            }
        } else {
            Eigen::Matrix<double,6,6> projected_inertia = 
                (jacobian * model_->MassMatrix().inverse() * jacobian.transpose()).inverse();
            wrench_.noalias() += feedforward_gain_ * projected_inertia * acceleration_;
        }
    }

    tau_d_.noalias() = jacobian.transpose().bottomRows(model_->dq_size()) * wrench_;
    jacobian_.commitWriteBuffer();

    PBWBC_TRACEF("CIC::update leave");
}

bool CartesianImpedanceController::hasTaskForces() { 
    return true; 
}

wrench_t const & CartesianImpedanceController::getTaskForces() {
    return wrench_;
}

bool CartesianImpedanceController::hasTaskJacobian() { 
    return true;
}

Eigen::MatrixXd const & CartesianImpedanceController::getTaskJacobian() const {
    if(!jacobian_.ready()) {
        throw std::runtime_error("Jacobian no initialized");
    }
    return jacobian_.getReadBuffer(); // no totally safe here
}

CartesianImpedanceController::damping_t CartesianImpedanceController::compute_damping_with_double_diagonalization() {
        if(!jacobian_.ready()) {
            return damping_t::Zero();
        }
        // TODO: need buffering for input variables: K_, xi_
        // TODO: fix memory allocations
        // TODO: move first mass matrix inverse to model, use pinocchio implementation there
        auto const & jacobian = jacobian_.getReadBuffer();
        auto & projected_inertia = projected_inertia_.getWriteBuffer();
        projected_inertia = 
            (jacobian * model_->InvMassMatrix() * jacobian.transpose()).inverse();
        jacobian_.freeReadBuffer();
        typedef Eigen::Matrix<double,6,6> task_inertia_t;
        auto const & K = K_local_.getReadBuffer();
        Eigen::GeneralizedSelfAdjointEigenSolver<task_inertia_t>
                eigensolver(K,projected_inertia,Eigen::ComputeEigenvectors);
        K_local_.freeReadBuffer();
        task_inertia_t MV = projected_inertia * eigensolver.eigenvectors();
        projected_inertia_.commitWriteBuffer();
        task_inertia_t z;
        z.setZero();
        z.diagonal() = 2 * eigensolver.eigenvalues().array().sqrt() * xi_.diagonal().array();
        return MV * z * MV.transpose();
}


} // namespace pbwbc
