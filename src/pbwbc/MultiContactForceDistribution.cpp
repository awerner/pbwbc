//Random documentation
//The MultiContactForceDistribution defines a QP optimization problem of the following form:
//min 0.5 * x G x + g0 x
    //s.t.
    //    CE^T x + ce0 = 0
    //    CI^T x + ci0 >= 0
    //
    // The matrix and vectors dimensions are as follows:
    //     G: n * n
    //    g0: nf
    //
    //    CE: n * p
    //   ce0: p
    //
    //    CI: n * m
    //   ci0: m
    //
    //     x: n

// The following cost function is set up for balancing:
        // e = [ J_u,l J_u,r ] * W_c_des - W_x
        //            JUT <--------------------- JT.topRows(6)
        // e_FREE = 
        // min e
        // e.T * e = (JUT * W_c_des - W_x).T      * (JUT * W_c_des - W_x)
        //         = (W_c_des.T * JUT.T - W_x.T ) * ...
        //         = W_c_des.T * JUT.T * JUT * W_c_des - W_c_des.T * JUT.T * W_x - W_x.T * JUT * W_c_des + W_x.T * W_x
        //         = ................................. - 2 * W_x.T * JUT * W_c_des                       + ..........
        //
        //
        //  Quadprog interface: min 0.5 * x G x + g0 x

// Cartesian Impedance is used for the realization of the cost function
// W_c_des is the wrench computed as Cartesian impedance on the CoM position and base link orientation
// W_x is a stacked vector of end-effector wrenches (the variable that is optimized in this problem)


// TODO: idea
        // dynamically disable foot force constraint with balancer_tcp_weight_(2) (normal force)
        //if(balancer_tcp_weight_left_(5)>1000.) { // TODO: interpolation (30ms)
        //    CI.transpose().block<ci_num,6>(0,6).setZero();
        //    ci0.segment(0,ci_num).setZero();
        //}
        //if(balancer_tcp_weight_right_(5)>1000.) {
        //    CI.transpose().block<ci_num,6>(ci_num,6).setZero();
        //    ci0.segment(ci_num,ci_num).setZero();
        //}


#include <pbwbc/MultiContactForceDistribution.hpp>
#include <pbwbc/eiquadprog.hpp>
#ifdef HAVE_QPOASES
#include "qpOASES.hpp"
#endif // HAVE_QPOASES

namespace pbwbc {
    

/////////////
/* CONTACT */
/////////////
Contact::Contact(std::string const & name) :name_(name){}
Contact::~Contact() {}

std::string const & Contact::name() const{
	return name_;
}

std::shared_ptr<CartesianImpedanceController> const & Contact::cartesianController() {
    return cartcontroller_;
}


void Contact::start(timestamp_t const & timestamp) {
    cartcontroller_->start(timestamp);
}


void Contact::stop(timestamp_t const & timestamp) {
    cartcontroller_->stop(timestamp);
}

void Contact::startMotion(timestamp_t const & timestamp, std::string const & entity, Eigen::Ref<Eigen::VectorXd> const & state_offset_d, double const duration, bool absolute) {
    cartcontroller_->startMotion(timestamp, entity, state_offset_d, duration,absolute);
}
        
bool Contact::isMotionRunning(timestamp_t const & ts, std::string const & entity) {
    return cartcontroller_->isMotionRunning(ts,entity);
}

void Contact::stopMotion(timestamp_t const & ts, std::string const & entity) {
    cartcontroller_->stopMotion(ts,entity);
}
        
Eigen::VectorXd const & Contact::getMotionState(std::string const & entity) {
    return cartcontroller_->getMotionState(entity);
}


//////////////////////
/* CONTACT6DSUFRACE */
//////////////////////
Contact6DSurface::Contact6DSurface(std::string const & name,
        std::shared_ptr<Model> const & model,
        std::shared_ptr<ConfigMap> const & config,
        std::shared_ptr<SensorState> const & sensor_state,
        std::shared_ptr<CartesianImpedanceController> const & cartcontroller 
        ): 
	Contact(name),
	model_(model),
	config_(config),
    sensor_state_(sensor_state),
    contact_state_(contact_state_full),
    contact_state_timer_(0.),
    CIT_(constraints_num_,6),
    ci0_(constraints_num_) {
        //load parameters from yaml file
        std::string frame_name;
        if(!config_->getParam("frame_name",frame_name)) {
            throw std::runtime_error("Contact6DSurface: parameter 'frame_name' not found");
        }
        frame_id_ = model_->getFrameId(frame_name);

        if (!config_->getParam("init_contact_state", init_contact_state_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'init_contact_state' not found");
        }

        if (!config_->getParam("contact_auto_switch", contact_auto_switch_)) {
            PBWBC_DEBUG("Contact6DSurface: parameter 'contact_auto_switch' not found, setting to false");
            contact_auto_switch_ = false;
        }
        if (!config_->getParam("contact_auto_enabling_normal_force_min", contact_auto_enabling_normal_force_min_)
                && contact_auto_switch_) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_auto_enabling_normal_force_min' not found, setting to 0");
        }
        if (!config_->getParam("contact_auto_disabling_normal_force_max", contact_auto_disabling_normal_force_max_)
                && contact_auto_switch_) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_auto_disabling_normal_force_max' not found");
        }
        if (!config_->getParam("contact_auto_disabling_full_force_max", contact_auto_disabling_full_force_max_)
                && contact_auto_switch_) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_auto_disabling_full_force_max' not found");
        }
        if (!config_->getParam("contact_auto_enabling_full_force_min", contact_auto_enabling_full_force_min_)
                && contact_auto_switch_) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_auto_enabling_full_force_min' not found");
        }

        if (!config_->getParam("contact_state_enabling_normal_time", contact_state_enabling_normal_time_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_state_enabling_normal_time' not found");
        }
        if (!config_->getParam("contact_state_disabling_normal_time", contact_state_disabling_normal_time_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_state_disabling_normal_time' not found");
        }
        if (!config_->getParam("contact_state_enabling_full_time", contact_state_enabling_full_time_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_state_enabling_full_time' not found");
        }
        if (!config_->getParam("contact_state_disabling_full_time", contact_state_disabling_full_time_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_state_disabling_full_time' not found");
        }
        if (!config_->getParam("contact_fts_settling_time", contact_fts_settling_time_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_fts_settling_time' not found");
        }
        if (!config_->getParam("contact_max_position_weight", contact_max_position_weight_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_max_position_weight' not found");
        }
        if (!config_->getParam("contact_velocity_max", contact_velocity_max_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_velocity_max' not found");
        }
        if (!config_->getParam("contact_force_min", contact_force_min_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_force_min' not found");
        }
        if (!config_->getParam("contact_velocity_max_errors", contact_velocity_max_errors_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_velocity_max_errors' not found");
        }
        if (!config_->getParam("contact_force_max_errors", contact_force_max_errors_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_force_max_errors' not found");
        }
        if (!config_->getParam("contact_mu",       contact_mu_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'contact_mu' not found");
        } else {
            if (contact_mu_ < 0) {
                    throw std::runtime_error("Contact6DSurface: parameter 'contact_mu_' should be > 0");
            }
        }
        if (!config_->getParam("zmp_x_positive",   zmp_x_positive_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'zmp_x_positive' not found");
        } else {
            if (zmp_x_positive_ < 0) {
                    throw std::runtime_error("Contact6DSurface: parameter 'zmp_x_positive' should be > 0");
            }
        }
        if (!config_->getParam("zmp_x_negative",   zmp_x_negative_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'zmp_x_negative' not found");
        }  else {
            if (zmp_x_negative_ > 0) {
                    throw std::runtime_error("Contact6DSurface: parameter 'zmp_x_negative' should be < 0");
            }
        }
        if (!config_->getParam("zmp_y_positive",   zmp_y_positive_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'zmp_y_positive' not found");
        } else {
            if (zmp_y_positive_ < 0) {
                    throw std::runtime_error("Contact6DSurface: parameter 'zmp_y_positive' should be > 0");
            }
        }
        if (!config_->getParam("zmp_y_negative",   zmp_y_negative_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'zmp_y_negative' not found");
        }  else {
            if (zmp_y_negative_ > 0) {
                    throw std::runtime_error("Contact6DSurface: parameter 'zmp_y_negative' should be < 0");
            }
        }
        if (!config_->getParam("constraint_normal_force_min", constraint_normal_force_min_)) {
            throw std::runtime_error("Contact6DSufrace: parameter 'constraint_normal_force_min' not found");
        } else {
            if (constraint_normal_force_min_ < 0) {
                    PBWBC_DEBUG("Contact6DSurface: parameter 'constraint_normal_force_min' should be > 0");
            }
        }
        if (!config_->getParam("tau_z_max",        tau_z_max_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'tau_z_max' not found");
        }
        if (!config_->getParam("weight",           weights_max_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'weight' not found");
        } else {
            for(int idx = 0; idx < weights_max_.size(); idx++) {
                if (weights_max_(idx) < 0) {
                    throw std::runtime_error("MultiContactForceDistribution: parameter 'weight': gains should be > 0");
                }
            }
        }
        if (!config_->getParam("regularization",   regularization_max_)) {
            throw std::runtime_error("Contact6DSurface: parameter 'regularization' not found");
        } else {
            if (regularization_max_ < 0) {
                    throw std::runtime_error("Contact6DSurface: parameter 'regularization' should be > 0");
            }
        }
       
        std::string ft_sensor_name;
        if(config_->getParam("ft_sensor",ft_sensor_name)) {
            auto itr = std::find_if(sensor_state_->ftsWrenches.begin(),
                sensor_state_->ftsWrenches.end(),
                [&](std::tuple<int,std::string,wrench_t> const &p){
                    return std::get<1>(p)==ft_sensor_name;});
            if(itr==sensor_state_->ftsWrenches.end()) {
                throw std::runtime_error("Did not find force torque sensor");
            }
            ft_sensor_id_ = std::distance(sensor_state_->ftsWrenches.begin(),itr);
        } else {
            ft_sensor_id_ = -1;
        }

        cartcontroller_ = cartcontroller;
	}


void Contact6DSurface::start(timestamp_t const & timestamp) {
    Contact::start(timestamp);
    contact_state_ = static_cast<contact_state_t>(init_contact_state_);
    //Initialise contact_request in function of init_contact_state
    contact_request_ = contact_state_;
    contact_state_start_time_ = timestamp;
    contact_velocity_error_counter_ = 0;
    contact_force_error_counter_ = 0;
}


void Contact6DSurface::update(timestamp_t const & timestamp) {
    if(ft_sensor_id_!=-1) {
        measured_wrench_ = std::get<2>(sensor_state_->ftsWrenches.at(ft_sensor_id_));
    } else {
        measured_wrench_.setZero();
    }
    cartcontroller_->update(timestamp);
    updateStateMachine(timestamp);
}

        
const int Contact6DSurface::constraints_num_;

Eigen::Vector3d Contact6DSurface::translation() const {
	return model_->bodyTranslation(frame_id_);
}

Eigen::Quaterniond Contact6DSurface::orientation() const {
	return model_->bodyOrientation(frame_id_);
}

void Contact6DSurface::Jacobian(Eigen::Ref<Eigen::MatrixXd> jac) const {
	//This returns jac * lu_.inverse().transpose(), if using ModelInComCoordinates
	model_->getJacobian(frame_id_, jac);
}

double log_ipol(double w, double v0, double v1) {
    return pow(10.,log10(v0) * (1-w) + log10(v1) * w);
}

void Contact6DSurface::contactConstraints(Eigen::Ref<Eigen::MatrixXd> contact_constraints_CIT, 
                                           Eigen::Ref<Eigen::VectorXd> contact_constraints_ci0) {
    //Make sure the matrix and vector that are passed have the right size!
    //Eigen::Matrix<double,constraints_num_,6>
    //Eigen::Matrix<double,constraints_num_,1>
    if (   (contact_constraints_CIT.rows() != constraints_num_) 
        || (contact_constraints_CIT.cols() != 6) 
        || (contact_constraints_ci0.size() != constraints_num_) ) {
        throw std::runtime_error("Contact6DSurface:contact_constraints check the size of the matrices that are passed");
    }

    contact_constraints_CIT.setZero();  
    contact_constraints_ci0.setZero();

    // Compute CIT and ci0 according to current contact_state_
    double a  = 0.;     //activate constraints (all except normal force)
    double an = 0.;     //activate normal force constraint
    double w  = 0.;     // ramp activation of constraints (all except normal force)
    double wn = 0.;     //ramp normal force constraint

    if (contact_state_ == contact_state_off ) {
        //disable all constraints: CIT, ci0 remain 0
    } else if (contact_state_ == contact_state_full) {
        //All constraints are fully activated
        a  = 1.;
        an = 1.;
        w  = 1.;
        wn = 1.;
    } else if (contact_state_ == contact_state_normal ) {
        // disable all but normal force constraint
        // keep normal force constraint
        a  = 0.;
        an = 1.;
        w  = 0.;
        wn = 1.;
    } else if (contact_state_ == contact_state_enabling_full) {
        // gradually enable constraints (other than idx 8)
        a  = 1.;
        an = 1.;
        w  = log_ipol((contact_state_timer_ / contact_state_enabling_full_time_), 1.e-3, 1.) ;
        wn = 1.;
    } else if (contact_state_ == contact_state_disabling_full) {
        // gradually disable constraints (other than idx 8)
        a  = 1; // TODO: needs interpolation
        an = 1.;
        w  = log_ipol((contact_state_timer_ / contact_state_disabling_full_time_), 1., 1.e-3) ;
        wn = 1.;
    } else if (contact_state_ == contact_state_enabling_normal ) {
        // disable all constraints but normal force constraint
        // ramp up normal force constraint
        a  = 0.;
        an = 1.;
        w  = 0.;
        wn  = log_ipol((contact_state_timer_ / contact_state_enabling_normal_time_), 1.e-3, 1.) ;
    } else if (contact_state_ == contact_state_disabling_normal ) {
        // disable all but normal force constraint
        // ramp down normal force constraint
        a  = 0.;
        an = 1.;
        w  = 0.;
        wn  = log_ipol((contact_state_timer_ / contact_state_disabling_normal_time_), 1., 1.e-3) ;
    }

    //enable all constraints accordingly
    contact_constraints_CIT.middleRows<1>(0)  <<  1.* w,  0.,     contact_mu_ * a,      0.,     0.,     0.;     //<! fcone_x_0+
    contact_constraints_CIT.middleRows<1>(1)  << -1.* w,  0.,     contact_mu_ * a,      0.,     0.,     0.;     //<! fcone_x_0-
    contact_constraints_CIT.middleRows<1>(2)  <<  0.,     1.* w,  contact_mu_ * a,      0.,     0.,     0.;     //<! fcone_y_0+
    contact_constraints_CIT.middleRows<1>(3)  <<  0.,    -1.* w,  contact_mu_ * a,      0.,     0.,     0.;     //<! fcone_y_0-
    contact_constraints_CIT.middleRows<1>(4)  <<  0.,     0.,    -zmp_x_negative_ * w,  0,      1.* a,  0.;     //<! zmp_x_0-
    contact_constraints_CIT.middleRows<1>(5)  <<  0.,     0.,     zmp_x_positive_ * w,  0.,    -1.* a,  0.;     //<! zmp_x_0+
    contact_constraints_CIT.middleRows<1>(6)  <<  0.,     0.,    -zmp_y_negative_ * w,  1.* a,  0.,     0.;     //<! zmp_y_0-
    contact_constraints_CIT.middleRows<1>(7)  <<  0.,     0.,     zmp_y_positive_ * w, -1.* a,  0.,     0.;     //<! zmp_y_0+
    contact_constraints_CIT.middleRows<1>(8)  <<  0.,     0.,     1. * an,              0.,     0.,     0.;     //<! f_z_min
    contact_constraints_CIT.middleRows<1>(9)  <<  0.,     0.,     0.,                   0.,     0.,  1.* w;     //<! tau_z-
    contact_constraints_CIT.middleRows<1>(10) <<  0.,     0.,     0.,                   0.,     0., -1.* w;     //<! tau_z+

    contact_constraints_ci0(8)  = -constraint_normal_force_min_ * wn; //<! f_z_min
    // TODO: switch next two constraints back to friction cone
    contact_constraints_ci0(9)  =  tau_z_max_ * a;         //<! tau_z-
    contact_constraints_ci0(10) =  tau_z_max_ * a;         //<! tau_z+
    
    // assign into buffer for ROS wrapper
    CIT_ = contact_constraints_CIT;
    ci0_ = contact_constraints_ci0;
}

int Contact6DSurface::constraintsNum() const {
    return constraints_num_;
}

void Contact6DSurface::setState(int contact_state){
	contact_request_ = contact_state;
}

int Contact6DSurface::getStateRequest(){
	return contact_request_;
}

int Contact6DSurface::getState() {
	return contact_state_;
}

        
int Contact6DSurface::getFrameId() const {
    return frame_id_;
}

void Contact6DSurface::incrementStateTimer(timestamp_t const & timestamp) {
    contact_state_timer_ = (std::chrono::duration_cast<std::chrono::milliseconds>(timestamp - contact_state_start_time_).count() * 1e-3); 
}

void Contact6DSurface::updateStateMachine(timestamp_t const & timestamp) {

    //With this state machine, contact state can be switched according to:
    //(a) contact_request_, or
    //(b) measured normal contact forces

    // state transitions
    if (contact_state_ == contact_state_off) { 

        if ((contact_request_> 0.) 
            || (contact_auto_switch_ && (measured_wrench_(2) > contact_auto_enabling_normal_force_min_) && (contact_state_timer_ > contact_fts_settling_time_)) ) {
            contact_state_timer_ = 0.;
            contact_state_start_time_ = timestamp;
            contact_state_ = contact_state_enabling_normal;
            PBWBC_DEBUG("contact_state_enabling_normal start for " << name());

            //Change contact request to remain in the new state until asked otherwise
            if (contact_request_ < 0.5) {
                contact_request_ = 0.5;
                PBWBC_DEBUG("Contact request set to " << contact_request_);
            }
        }

    } else if (contact_state_ == contact_state_normal) {

        if (( contact_request_ > 0.5 ) 
                   || (contact_auto_switch_ && (measured_wrench_(2) > contact_auto_enabling_full_force_min_) && (contact_state_timer_ > contact_fts_settling_time_)) ) {
            contact_state_timer_ = 0.;
            contact_state_start_time_ = timestamp;
            contact_state_ = contact_state_enabling_full;
            PBWBC_DEBUG("contact_state_enabling_full start for " << name());

            //Change contact request to remain in the new state until asked otherwise
            if (contact_request_ < 1.) {
                contact_request_ = 1.;
                PBWBC_DEBUG("Contact request set to " << contact_request_);
            }

        } else if ((contact_request_ < 0.5 )
                || (contact_auto_switch_ && (measured_wrench_(2) < contact_auto_disabling_normal_force_max_) && (contact_state_timer_ > contact_fts_settling_time_)) ) {
            contact_state_timer_ = 0;
            contact_state_start_time_ = timestamp;
            contact_state_ = contact_state_disabling_normal;
            PBWBC_DEBUG("contact_state_disabling_normal start for " << name());

            //Change contact request to remain in the new state until asked otherwise
            if (contact_request_ > 0.) {
                contact_request_ = 0.;
                PBWBC_DEBUG("Contact request set to " << contact_request_);
            }
        }

    } else if (contact_state_ == contact_state_full) { 

        if (( contact_request_ < 1. )
                || (contact_auto_switch_ && (measured_wrench_(2) < contact_auto_disabling_full_force_max_) && (contact_state_timer_ > contact_fts_settling_time_))) {

            contact_state_timer_ = 0;
            contact_state_start_time_ = timestamp;
            contact_state_ = contact_state_disabling_full;
            PBWBC_DEBUG("contact_state_disabling_full start for " << name());

            //Change contact request to remain in the new state until asked otherwise
            if (contact_request_ > 0.5) {
                contact_request_ = 0.5;
                PBWBC_DEBUG("Contact request set to " << contact_request_);
            }
        }

    } else if ((contact_state_ == contact_state_disabling_normal) && (contact_state_timer_ > contact_state_disabling_normal_time_)) {
        contact_state_timer_ = 0.;
        contact_state_start_time_ = timestamp;
        contact_state_ = contact_state_off;
        PBWBC_DEBUG("contact_state_off start for " << name());

    } else if ((contact_state_ == contact_state_enabling_normal) && (contact_state_timer_ > contact_state_enabling_normal_time_)) {
        contact_state_timer_ = 0.;
        contact_state_start_time_ = timestamp;
        contact_state_ = contact_state_normal;
        PBWBC_DEBUG("contact_state_normal start for " << name());

    } else if ((contact_state_ == contact_state_enabling_full) && (contact_state_timer_ > contact_state_enabling_full_time_)) {
        contact_state_timer_ = 0;
        contact_state_start_time_ = timestamp;
        contact_state_ = contact_state_full;
        PBWBC_DEBUG("contact_state_full start for " << name());

    } else if( (contact_state_ == contact_state_disabling_full) && (contact_state_timer_ > contact_state_disabling_full_time_) ) {
        contact_state_timer_ = 0;
        contact_state_start_time_ = timestamp;
        contact_state_ = contact_state_normal;
        PBWBC_DEBUG("contact_state_normal start for " << name());

    } else {
        //PBWBC_DEBUG("contact_state_ remains = " << contact_state_ << " for " << name());
    }
}


Contact6DSurface::weights_t const & Contact6DSurface::weights(){


    //Weights are task weights for the CartesianController 
    weights_= 0 * weights_max_;  

    if ( contact_state_ == contact_state_off ) {
        weights_= weights_max_;

    } else if ( contact_state_ == contact_state_enabling_normal ) {
        // ramp down normal force weight
        weights_= weights_max_;  
        weights_(2) = log_ipol((contact_state_timer_ / contact_state_enabling_normal_time_), weights_max_(2), 1e-3);


    } else if ( contact_state_ == contact_state_disabling_normal ) {
        // ramp up weight for normal force
        weights_= weights_max_;
        weights_(2) = log_ipol((contact_state_timer_ / contact_state_disabling_normal_time_), 1e-3, weights_max_(2));

    } else if ( contact_state_ == contact_state_normal ) {
        weights_= weights_max_;  
        weights_(2) = 0;

    } else if ( contact_state_ == contact_state_enabling_full ) {
        // ramp down weight for other forces than normal force
        for (int idx = 0; idx < weights_.size(); idx++) {
            weights_(idx) = log_ipol((contact_state_timer_ / contact_state_enabling_full_time_), weights_max_(idx), 1e-3);
        }
        weights_(2) = 0;

    } else if ( contact_state_ == contact_state_disabling_full ) {
        // ramp up weight for other forces than normal force
        for (int idx = 0; idx < weights_.size(); idx++) {
            weights_(idx) = log_ipol((contact_state_timer_ / contact_state_disabling_full_time_), 1e-3, weights_max_(idx));
        }
        weights_(2) = 0;

    } else if ( contact_state_ == contact_state_full ) {
        // set weights to 0
    }

    return weights_;
}


double const & Contact6DSurface::regularizationWeight(){
    
    regularization_weight_ = 0;

    if ( contact_state_ == contact_state_off ) {
        regularization_weight_ = 0;

    } else if ( contact_state_ == contact_state_enabling_normal ) {
        // ramp down weight for normal force
        regularization_weight_ = log_ipol((contact_state_timer_ / contact_state_enabling_normal_time_), 1e-17, regularization_max_);

    } else if ( contact_state_ == contact_state_disabling_normal ) {
        // ramp up weight for normal force
        regularization_weight_ = log_ipol((contact_state_timer_ / contact_state_disabling_normal_time_), regularization_max_, 1e-17);

    } else if ( contact_state_ == contact_state_normal ) {
        regularization_weight_ = regularization_max_;

    } else if ( contact_state_ == contact_state_enabling_full ) {
        regularization_weight_ = regularization_max_;

    } else if ( contact_state_ == contact_state_disabling_full ) {
        regularization_weight_ = regularization_max_;

    } else if ( contact_state_ == contact_state_full ) {
        regularization_weight_ = regularization_max_;
    }

    return regularization_weight_;
}

wrench_t const & Contact6DSurface::measuredWrench(){
    return measured_wrench_;
}

void Contact6DSurface::setDesiredWrench(wrench_t const & wrench){
    desired_wrench_ = wrench;
}

wrench_t const & Contact6DSurface::taskForce() {
    // TODO: ramp between default contact force and cart imp force
    return cartcontroller_->getTaskForces();
}


void Contact6DSurface::validateContact(std::shared_ptr<RobotState> const & state, const Eigen::Ref<const Eigen::MatrixXd> & contact_jacobian){
    // catch slipping contact
    // TODO: using the wrong state here. This need com velocity as base
    // fix inconsistency between using jacobian in com coordinates and using normal robotstate

    Eigen::Matrix<double,3,1> W_V_F_contact;
    W_V_F_contact.noalias() = contact_jacobian.topRows(3) * state->dy;  
    double W_V_F_contact_norm = W_V_F_contact.norm();

    if( (contact_state_ == Contact::contact_state_full) && (W_V_F_contact_norm > contact_velocity_max_)) {
        contact_velocity_error_counter_++;
    } else {
        contact_velocity_error_counter_ = 0;
    }
    if(contact_velocity_error_counter_ > contact_velocity_max_errors_) {
        PBWBC_DEBUG("contact_velocity_max_errors exceeded");
        throw ControllerException("contact_velocity_max_errors exceeded");
    }

    if(contact_state_ == Contact::contact_state_full 
            && (contact_force_min_!=0.)
            && (measured_wrench_(2) < contact_force_min_)) {
        contact_force_error_counter_++;
    } else {
        contact_force_error_counter_ = 0;
    }
    if(contact_force_error_counter_ > contact_force_max_errors_) {
        PBWBC_DEBUG("contact_force_max_errors exceeded");
        throw ControllerException("contact_force_max_errors exceeded");
    }
}


//////////////////
/* qpOASES_impl */
//////////////////
#ifdef HAVE_QPOASES
class MultiContactForceDistribution::qpOASES_impl {
    public:
    std::shared_ptr<qpOASES::SQProblem> solver;
};
#endif // HAVE_QPOASES

class MultiContactForceDistribution::eiquadprog_impl {
    public:
    std::shared_ptr<Eigen::QuadProg<double>> solver;
};



///////////////////////////////////
/* MultiContactForceDistribution */
///////////////////////////////////
MultiContactForceDistribution::MultiContactForceDistribution(
            std::shared_ptr<RobotState> const & state,
            std::shared_ptr<SensorState> const & sensor_state,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config
            ) :
	Controller([&](){
	            std::string name;
	            config->param("name",name,"MultiContactForceDistribution");
	            return name;}(),model->dq_size()),
    state_(state),
    model_(model),
    modelInComCoordinates_(new ModelInComCoordinates(model,state)),
    config_(config)
	{
        //Read params
        std::string base_frame_name;
        if(!config_->getParam("base_frame_name", base_frame_name)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter 'base_frame_name' not found");
        }
        base_frame_id_ = model_->getFrameId(base_frame_name);

        if (!config_->getParam("K_balancer", K_balancer_)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter 'K_balancer' not found");
        } else {
            for(int idx = 0; idx < K_balancer_.size(); idx++) {
                if (K_balancer_(idx) < 0) {
                    throw std::runtime_error("MultiContactForceDistribution: parameter 'K_balancer': gains should be > 0");
                }
            }
            K_balancer_des_ = K_balancer_;
        }
        bool D_gain_set;
        if (!config_->getParam("D_balancer", D_balancer_)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter 'D_balancer' not found");
        } else {
            for(int idx = 0; idx < D_balancer_.size(); idx++) {
                if (D_balancer_(idx) < 0) {
                    throw std::runtime_error("MultiContactForceDistribution: parameter 'D_balancer': gains should be > 0");
                }
            }
            D_gain_set = true;
        }
        if (!config_->getParam("I_balancer", I_balancer_)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter 'I_balancer' not found");
        } else {
            for(int idx = 0; idx < I_balancer_.size(); idx++) {
                if (I_balancer_(idx) < 0) {
                    throw std::runtime_error("MultiContactForceDistribution: parameter 'I_balancer': gains should be > 0");
                }
            }
        }

        bool damping_ratio_set;
        if (!config_->getParam("damping_ratio_balancer", damping_ratio_balancer_)) {
            damping_ratio_balancer_.setConstant(0.);
            damping_ratio_set = false;
        } else {
            for(int idx = 0; idx < damping_ratio_balancer_.size(); idx++) {
                if (damping_ratio_balancer_(idx) < 0 || damping_ratio_balancer_(idx) > 1.) {
                    throw std::runtime_error("MultiContactForceDistribution: parameter 'damping_ratio_balancer': gains should be 0. < x < 1.");
                }
            }
            damping_ratio_set = true;
        }
        std::string damping_mode;
        config_->param("damping_mode",damping_mode,"value");
        if(damping_mode=="value"){
            damping_mode_ = damping_value;
            if(!D_gain_set) {
                throw std::runtime_error("damping_mode=value but no D_balancer parameters found");
            }
        }else if(damping_mode=="ratio") {
            damping_mode_ = damping_ratio;
            if(!damping_ratio_set) {
                throw std::runtime_error("damping_mode=ratio but no damping_ratio_balancer parameters found");
            }
        } else {
            throw std::runtime_error("Damping mode not supported");
        }
        config_->param("K_balancer_step", K_balancer_step_, 5.);

        if (!config_->getParam("balancer_ierror_velocity_threshold", balancer_ierror_velocity_threshold_)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter 'balancer_ierror_velocity_threshold' not found");
        }
        if (!config_->getParam("balancer_ierror_windup", balancer_ierror_windup_)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter 'balancer_ierror_windup' not found");
        }
        if (!config_->getParam("balancer_observer_gain", balancer_observer_gain_)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter 'balancer_observer_gain' not found");
        }
        if (!config_->getParam("balancer_observer_feedback_gain", balancer_observer_feedback_gain_)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter 'balancer_observer_feedback_gain' not found");
        }
        if (!config_->getParam("balancer_com_weight", balancer_com_weight_)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter 'balancer_com_weight' not found");
        } else {
            for(int idx = 0; idx < balancer_com_weight_.size(); idx++) {
                if (balancer_com_weight_(idx) < 0) {
                    throw std::runtime_error("MultiContactForceDistribution: parameter 'balancer_com_weight': weights should be > 0");
                }
            }
        }
        if (!config_->getParam("balancer_regularization", balancer_regularization_)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter '' not found");
            if (balancer_regularization_ < 0) {
                throw std::runtime_error("MultiContactForceDistribution: parameter 'balancer_regularization' should be > 0");
            }
        }
        config_->param("balancer_output_enable", balancer_output_enable_des_, 1.);
        balancer_output_enable_ = balancer_output_enable_des_;
        if (!config_->getParam("solver", solver_)) {
            throw std::runtime_error("MultiContactForceDistribution: parameter 'solver' not found");
        }

        
        std::vector<std::string> contact_names;
        config_->list_items("Contacts",contact_names);
        for(auto const & contact_name : contact_names) {
            PBWBC_DEBUG("Adding contact " << contact_name);
            auto contact_config = config_->submap("Contacts")->submap(contact_name);
            contacts_.push_back(std::shared_ptr<pbwbc::Contact>(    
                new Contact6DSurface(contact_name, 
                                     modelInComCoordinates_,
                                     contact_config,
                                     sensor_state,
                                     std::shared_ptr<pbwbc::CartesianImpedanceController>(
                                        new CartesianImpedanceController(state_, model_, 
                                            contact_config->submap("CartesianImpedanceController")))
                )));
        }

        //Trajectory generator for the CoM, base, hip 
        auto trajectory_generator_config = config_->submap("TrajectoryGenerator");
        trajectory_generator_.reset( new CartesianTrajectoryGenerator(
                                                            model_,
                                                            trajectory_generator_config) );

        //The base link is used with the CoM task, so it is a special case. Not to be used as a contact thing
        //base_orientation_controller_.reset(new CartesianImpedanceController(state_, model_, config_->submap("BaseCartesianImpedanceController")));

        balancer_ierror_.setZero();
        balancer_observer_state_.setZero();
        
        gravity_.setZero();
        gravity_(2) = 9.81;

        initialize();
	}

void MultiContactForceDistribution::initialize() {
    //Allow to initialize variables depending on contact size and QP definition

    //Resize constraint variables
    contact_jacobian_.resize(6, model_->dy_size());
    base_jacobian_.resize(6, model_->dy_size());
    task_jacobian_.resize(6 + 6*contacts_.size(), model_->dy_size());
    JT_.resize(model_->dy_size(), 6*contacts_.size());
    G_.resize(6*contacts_.size(),6*contacts_.size());
    G_temp_.resize(6,6*contacts_.size());
    g0_.resize(6*contacts_.size());
    CE_.resize(6*contacts_.size(),0);
    ce0_.resize(0);
    CIT_.resize(constraintsNum(), 6*contacts_.size());
    ci0_.resize(constraintsNum());
    W_C_desired_.resize(6*contacts_.size());

    //Resize qpOASES variables
#   ifdef HAVE_QPOASES
        // instantiate qpOASES solver, this requires that the contact size is known
        qpOASES_impl_.reset(new qpOASES_impl);
        qpOASES_impl_->solver.reset(new qpOASES::SQProblem(6*contacts_.size(), constraintsNum()));
        qpOASES_impl_->solver->setPrintLevel(qpOASES::PL_NONE);
        qpOASES_A_.resize(CIT_.rows(), CIT_.cols());
        qpOASES_lbA_.resize(ci0_.size());
        qpOASES_ubA_.resize(ci0_.size());
        qpOASES_lb_.resize(6*contacts_.size());
        qpOASES_ub_.resize(6*contacts_.size());
#   endif // HAVE_QPOASES

    //QuadProg
    quadprog_impl_.reset(new eiquadprog_impl);
    quadprog_impl_->solver.reset(new Eigen::QuadProg<double>(6*contacts_.size(), 0, constraintsNum()));
}

MultiContactForceDistribution::~MultiContactForceDistribution() {}

void MultiContactForceDistribution::start(timestamp_t const & timestamp) {
    //com_d_ = *state_;
    //com_d_.dy.setZero();
    //com_d_.ddy.setZero();
    //PBWBC_DEBUG("In MultiContactForceDistribution::start ");
    modelInComCoordinates_->update(timestamp); //This will recompute the projection matrix in ModelInComCoordinates
    PBWBC_DEBUG("Base MassMatrix diagonal: " << modelInComCoordinates_->MassMatrix().diagonal().head(6).transpose());

    last_execution_timestamp_ = timestamp;

    for(auto & contact : contacts_) {
        contact->start(timestamp);
    }

#   ifdef HAVE_QPOASES
    qpOASES_solver_initialized_ = false;
    qpOASES_impl_->solver->reset();
    // TODO:
    //status = qpOASES_impl_->solver->init( &G(0,0), &g0(0), &A(0,0), &lb(0), &ub(0), &lbA(0), &ubA(0), nWSR, 0 );
#   endif // HAVE_QPOASES

    state_d_.position.translation = model_->com();
    state_d_.position.orientation = model_->bodyOrientation(base_frame_id_);
    state_d_.velocity.translation.setZero();
    state_d_.velocity.orientation.setZero();
    state_d_.acceleration.translation.setZero();
    state_d_.acceleration.orientation.setZero();

    state_initial_ = state_d_;
    goal_state_ = state_d_;
    state_translation_offset_d_.setZero();
    state_orientation_offset_euler_d_.setZero();
    update_pose_= 0;

    trajectory_generator_->start(timestamp);
    trajectory_generator_->setInitialState(state_initial_);
}

void MultiContactForceDistribution::stop(timestamp_t const & timestamp) {
    for(auto & contact : contacts_) {
        contact->stop(timestamp);
    }
}


int MultiContactForceDistribution::constraintsNum() const {
    int constraints_num = 0;
    for(auto const & contact : contacts_) {
        constraints_num += contact->constraintsNum();
    }
    return constraints_num;
}
    
std::vector<std::shared_ptr<Contact>> const & MultiContactForceDistribution::getContacts() const {
    return contacts_;
}

void MultiContactForceDistribution::balancerController(timestamp_t const & timestamp, Eigen::Matrix<double,6,1> & balancer_wrench ){

    // Cartesian Impedance on COM position and base orientation
    // TODO: gains should be in body fixed frame, not in world frame?
    Eigen::Matrix<double,6,1> balancer_error;
    Eigen::Matrix<double,6,1> balancer_derror;

    base_jacobian_.setZero();
    modelInComCoordinates_->getJacobian(base_frame_id_, base_jacobian_);

    // update cartesian_state_ so ROS wrapper can access the information
    cartesian_state_.position.translation = modelInComCoordinates_->com();
    cartesian_state_.position.orientation = modelInComCoordinates_->bodyOrientation(base_frame_id_);
    cartesian_state_.velocity.translation = modelInComCoordinates_->dcom();
    cartesian_state_.velocity.orientation.noalias() = base_jacobian_.bottomRows(3) * state_->dy;
    cartesian_state_.velocity.orientation = model_->bodyOrientation(base_frame_id_).toRotationMatrix()
            * cartesian_state_.velocity.orientation;

    // update goal_state_
    //TODO: THIS BELOW IS JUST FOR DEBUG, A HIGHER ENTITY SHOULD BE CALLING update_desired_state(goal_state_) INSTEAD
    if (update_pose_ != 0){

        goal_state_.position.translation = state_initial_.position.translation + state_translation_offset_d_;
        goal_state_.position.orientation = state_initial_.position.orientation 
                                           * Eigen::AngleAxisd(state_orientation_offset_euler_d_(0), Eigen::Vector3d::UnitX())
                                           * Eigen::AngleAxisd(state_orientation_offset_euler_d_(1), Eigen::Vector3d::UnitY())
                                           * Eigen::AngleAxisd(state_orientation_offset_euler_d_(2), Eigen::Vector3d::UnitZ()); 

        trajectory_generator_->start6DMotion(timestamp, cartesian_state_, goal_state_, 1.); 

        update_pose_ = 0;
    }
    trajectory_generator_->update(timestamp);
    state_d_ = trajectory_generator_->getState();
   

    // error
    balancer_error.head(3) << cartesian_state_.position.translation - state_d_.position.translation;

    Eigen::Quaterniond error_quaternion(modelInComCoordinates_->bodyOrientation(base_frame_id_).inverse() * state_d_.position.orientation);
    balancer_error.tail(3) << -error_quaternion.x(), -error_quaternion.y(), -error_quaternion.z(); // TODO: check in what frame this is

    balancer_derror.head(3) = cartesian_state_.velocity.translation;
    balancer_derror.tail(3) = cartesian_state_.velocity.orientation;

    balancer_ierror_ += balancer_error * dt_;

    for(int idx=0;idx<balancer_ierror_.size();idx++) {
        balancer_ierror_(idx) = std::min(balancer_ierror_windup_(idx),balancer_ierror_(idx));
        balancer_ierror_(idx) = std::max(-balancer_ierror_windup_(idx),balancer_ierror_(idx));
        if(I_balancer_(idx)==0.) balancer_ierror_(idx) = 0.;
    }

    // compute wrench, gains are applied in base frame
    auto R_B = modelInComCoordinates_->bodyOrientation(base_frame_id_);
    Eigen::Matrix<double,6,1> balancer_ierror = balancer_ierror_; 
    for(Eigen::Matrix<double,6,1> * e : {&balancer_error,&balancer_derror,&balancer_ierror}) {
        e->head(3) = R_B.toRotationMatrix().transpose() * e->head(3);
        e->tail(3) = R_B.toRotationMatrix().transpose() * e->tail(3);
    }
    
    // ramp stiffness values
    K_balancer_.array() += (K_balancer_des_ - K_balancer_).array().max(-K_balancer_step_).min(K_balancer_step_);
    // compute damping values
    Eigen::Matrix<double,6,1> D_balancer;
    if(damping_mode_==damping_ratio) {
        D_balancer = sqrt(K_balancer_.array() * modelInComCoordinates_->MassMatrix().diagonal().head(6).array()) 
					* 2. * damping_ratio_balancer_.array();
    } else if(damping_mode_==damping_value) {
        D_balancer = D_balancer_;
    } else {
        throw std::runtime_error("Damping mode not implemented");
    }

    balancer_wrench = 
        - (K_balancer_.asDiagonal() * balancer_error) 
        - (D_balancer.asDiagonal() * balancer_derror)
        - (I_balancer_.asDiagonal() * balancer_ierror);
    balancer_wrench.head(3) = R_B * balancer_wrench.head(3);
    balancer_wrench.tail(3) = R_B * balancer_wrench.tail(3);
    
    balancer_wrench.noalias() += gravity_ * modelInComCoordinates_->mass();

    balancer_wrench.head(3) -= balancer_observer_feedback_gain_.head(3).asDiagonal() * balancer_observer_residual_;

}

void MultiContactForceDistribution::updateBalancerObserverResidual() {
    balancer_observer_residual_ = balancer_observer_gain_ * ( modelInComCoordinates_->mass() * modelInComCoordinates_->dcom() - balancer_observer_state_ );
}

void MultiContactForceDistribution::updateBalancerObserverState() {
    // minimalistic impulse based external force observer on COM coordinates
    // see: Robot Collisions: A Survey on Detection,Isolation, and Identification (Sami Haddadin)

    Eigen::Matrix<double,3,1> temp = balancer_observer_residual_;
    temp = - gravity_.head(3) * modelInComCoordinates_->mass();
    temp.noalias() += JT_.topRows(3) * W_C_desired_;

    balancer_observer_state_.noalias() += dt_ * temp;
}


void MultiContactForceDistribution::solveQP() {
    
    if (solver_ == "EigenQuadProg") {
        // quadprog has bad api (non-constness)
        try {

            quadprog_impl_->solver->solve_quadprog(G_,g0_,CE_,ce0_,CIT_.transpose(),ci0_,W_C_desired_);
        } catch ( std::runtime_error const & e) {
            PBWBC_DEBUG("quadprog threw: " << e.what());
            throw e;
        }
    } else if (solver_ == "qpOASES") {
#       ifdef HAVE_QPOASES
        using namespace qpOASES;
        nWSR_ = 100; //maximum number of working set recalculations
        returnValue status;
        qpOASES_A_ = CIT_;
        qpOASES_lbA_ = - ci0_;
        qpOASES_ubA_.setConstant(1e10);
        qpOASES_lb_.setConstant(-1e10); // TODO: set bounds to ensure sane solutions
        qpOASES_ub_.setConstant(1e10);

        if ( !qpOASES_solver_initialized_ ) {
            // initialise and solve first QP
            status = qpOASES_impl_->solver->init( &G_(0,0), &g0_(0), &qpOASES_A_(0,0), &qpOASES_lb_(0), &qpOASES_ub_(0), &qpOASES_lbA_(0), &qpOASES_ubA_(0), nWSR_, 0 );
            PBWBC_TRACEF("MCFD::solveQP status=%i", status);
            qpOASES_impl_->solver->getPrimalSolution( &W_C_desired_(0) );

            //PBWBC_DEBUG("QP init status: " << getSimpleStatus( status ) << status);
            //PBWBC_DEBUG("qpOASES: first result\n" << W_C_desired_.transpose());
            qpOASES_solver_initialized_ = true;
        } else {
            // solve neighbouring QP using hotstart technique 
            status = qpOASES_impl_->solver->hotstart( &G_(0,0), &g0_(0), &qpOASES_A_(0,0), &qpOASES_lb_(0), &qpOASES_ub_(0), &qpOASES_lbA_(0), &qpOASES_ubA_(0), nWSR_, 0 );
            PBWBC_TRACEF("MCFD::solveQP nWSR=%i", nWSR_);
            if ( ( status != SUCCESSFUL_RETURN ) && ( status != RET_MAX_NWSR_REACHED ) ) {
                PBWBC_TRACEF("MCFD::solveQP status=%i", status);
                PBWBC_DEBUG("qpoases: failed, resetting ");
                // if an error occurs, reset problem data structures and initialise again 
                qpOASES_impl_->solver->reset();
                status = qpOASES_impl_->solver->init( &G_(0,0), &g0_(0), &qpOASES_A_(0,0), &qpOASES_lb_(0), &qpOASES_ub_(0), &qpOASES_lbA_(0), &qpOASES_ubA_(0), nWSR_, 0 );
                qpOASES_impl_->solver->getPrimalSolution( &W_C_desired_(0) );
            } else {
                // otherwise obtain optimal solution
                qpOASES_impl_->solver->getPrimalSolution( &W_C_desired_(0) );
            }
        }
        switch ( status ) {
            case SUCCESSFUL_RETURN:
                break;
            case RET_MAX_NWSR_REACHED:
                PBWBC_DEBUG("qpOASES: RET_MAX_NWSR_REACHED");
                break;
            default:
                PBWBC_DEBUG("qpOASES: failed");
                break;
        }
#       else
        throw std::runtime_error("qpOASES not available");
#       endif // HAVE_QPOASES  
    } else {
        throw std::runtime_error("Solver not implemented"); 
    }
}


void MultiContactForceDistribution::startMotion(timestamp_t const & timestamp,
        std::string const & entity,
        Eigen::Ref<Eigen::VectorXd> const & state_offset_d,
        double const duration,
        bool absolute) {
    
    if (entity == "com") {
        PBWBC_DEBUG("Starting com motion");
        if(absolute) {
            if (state_offset_d.size() != 7) {
                throw std::runtime_error("startMotion goal only takes 7 size goals");
            }
            goal_state_.position.translation = state_offset_d.segment(0,3);
            goal_state_.position.orientation.coeffs() = state_offset_d.segment(3,4);
        } else {
            if (state_offset_d.size() != 7) {
                throw std::runtime_error("startMotion goal only takes 7 size goals");
            }
            goal_state_.position.translation = state_initial_.position.translation + state_offset_d.segment(0,3);
            goal_state_.position.orientation = state_initial_.position.orientation *
                                 Eigen::Quaterniond(state_offset_d(6),
                                                    state_offset_d(3),
                                                    state_offset_d(4),
                                                    state_offset_d(5));
        }
        trajectory_generator_->start6DMotion(timestamp, state_d_, goal_state_, duration); 
        return;
    } else if (entity.find("contact:")!=std::string::npos) {
        if (state_offset_d.size() != 1) {
            throw std::runtime_error("startMotion goal needs 1 value");
        }
        PBWBC_DEBUG("Starting contact transition");
        std::string contact_name = entity.substr(entity.find(":")+1);
        PBWBC_DEBUG("Got contac tname" << contact_name);
        for(auto const & contact : contacts_) {
            if(contact->name()==contact_name) {
                contact->setState(state_offset_d(0));
                return;
            }
        }
        throw std::runtime_error("Could not find contact");
    } else {
        //figure out which contact CartesianController this is going to be
        for (auto & contact : contacts_) {
            if (contact->name() == entity) {
                PBWBC_DEBUG("Starting motion on " << contact->name());
                contact->startMotion(timestamp, entity, state_offset_d, duration, absolute);
                return;
            }
        }
    }
}
        
std::vector<std::string> MultiContactForceDistribution::motionEntities() {
    std::vector<std::string> entities {"com"}; 
    for( auto const & contact : contacts_ ) {
        entities.push_back(std::string("contact:") + contact->name());
        entities.push_back(contact->name());
    }
    return entities;
}

bool MultiContactForceDistribution::isMotionRunning(timestamp_t const & ts, std::string const & entity) {
    if (entity == "com") {
        return trajectory_generator_->isRunning(ts);
    } else if (entity.find("contact:")!=std::string::npos) {
        std::string contact_name = entity.substr(entity.find(":")+1);
        for(auto const & contact : contacts_) {
            if(contact->name()==contact_name) {
                return contact->getState()!=contact->getStateRequest();
            }
        }
        throw std::runtime_error("Could not find contact");

    } else {
        //figure out which contact CartesianController this is going to be
        for (auto & contact : contacts_) {
            if (contact->name() == entity) {
                return contact->isMotionRunning(ts,entity);
            }
        }
    }
    throw std::runtime_error("isMotionRunning: entity unknown");
}

void MultiContactForceDistribution::stopMotion(timestamp_t const & ts, std::string const & entity) {
    if (entity == "com") {
        trajectory_generator_->stopMotion(ts);
        return;
    } else if (entity.find("contact:")!=std::string::npos) {
        throw std::runtime_error("Not implemented: stopping contact transitions");
        return;
    } else {
        //figure out which contact CartesianController this is going to be
        for (auto & contact : contacts_) {
            if (contact->name() == entity) {
                contact->stopMotion(ts,entity);
                return;
            }
        }
    }
    throw std::runtime_error("isMotionRunning: entity unknown");
}
    
Eigen::VectorXd const & MultiContactForceDistribution::getMotionState(std::string const & entity) {
    if (entity == "com") {
        static Eigen::VectorXd buffer(7);
        buffer.segment(0,3) = trajectory_generator_->getState().position.translation;
        buffer.segment(3,4) = trajectory_generator_->getState().position.orientation.coeffs();
        return buffer;
    } else if (entity.find("contact:")!=std::string::npos) {
        static Eigen::VectorXd buffer(1);
        std::string contact_name = entity.substr(entity.find(":")+1);
        for(auto const & contact : contacts_) {
            if(contact->name()==contact_name) {
                buffer(0) = contact->getState();
                return buffer;
            }
        }
        throw std::runtime_error("Could not find contact");
    } else {
        //figure out which contact CartesianController this is going to be
        for (auto & contact : contacts_) {
            if (contact->name() == entity) {
                return contact->getMotionState(entity);
            }
        }
    }
    throw std::runtime_error("getMotionState: entity unknown");
}


void MultiContactForceDistribution::update(timestamp_t const & timestamp) {
    PBWBC_TRACEF("MCFD::update enter");
    tau_d_.setZero();

    // update dt_ for integral gains
    dt_ = std::chrono::duration<double>(timestamp - last_execution_timestamp_).count();
    last_execution_timestamp_ = timestamp;

    modelInComCoordinates_->update(timestamp); //This will recompute the projection matrix in ModelInComCoordinates
    
    PBWBC_TRACEF("MCFD::update %i", __LINE__);
    for(auto & contact : contacts_) {
        contact->incrementStateTimer(timestamp);
        contact->update(timestamp); // This also calls CartesianImpedanceController::update for each contact
    }

    updateBalancerObserverResidual();

    // Cartesian Impedance on COM position and base orientation
    balancerController(timestamp, balancer_wrench_);
    PBWBC_TRACEF("MCFD::update %i", __LINE__);


    // For each contact, compute its contact + cartesian controller components
    /* Stack Jacobians of balancing contacts
      JT =
      /--------------------\
      |  J_L,u^T | J_R,u^T |
      |  J_L,l^T | J_R,l^T |
      \--------------------/ */

    JT_.setZero();
    CE_.setZero();
    ce0_.setZero();
    CIT_.setZero();
    ci0_.setZero();
    G_.setZero();
    g0_.setZero();
    W_C_desired_.setZero();

    int contact_constraints_offset = 0;
    
    for(std::size_t contact_idx = 0; contact_idx < contacts_.size(); contact_idx++ ) {
        auto const & contact = contacts_[contact_idx];

        contact_jacobian_.setZero();
        contact->Jacobian(contact_jacobian_);

        // collect all adjoint matrices for contact points to build com wrench cost function
        JT_.middleCols(6*contact_idx, 6) = contact_jacobian_.transpose();

        // catch slipping contact
        contact->validateContact(state_, contact_jacobian_);

        // construct contact constraint matrix and bias vector
        contact->contactConstraints( CIT_.block( contact_constraints_offset,
                    6*contact_idx,
                    contact->constraintsNum(),
                    6),
                ci0_.segment( contact_constraints_offset, 
                    contact->constraintsNum() ) );

        // update index for next contact in the list
        contact_constraints_offset += contact->constraintsNum();

        // fill in the QP Hessian matrix and bias vector, with the end effector Cartesian impedance controller values
        G_.block(6*contact_idx, 6*contact_idx, 6, 6) = 2. * contact->weights().asDiagonal();

        g0_.segment(6*contact_idx,6) = -2. * (  -1. * contact->weights().asDiagonal() * contact->taskForce() 
            /*    + contact->regularizationWeight() * gravity_ * modelInComCoordinates_->mass() / 2. */ );
            // TODO: get better reference force
    }
    
    G_temp_.noalias() = balancer_com_weight_.asDiagonal() * JT_.topRows(6);
    G_.noalias() += 2. * JT_.topRows(6).transpose() * G_temp_;
    G_.diagonal().array() += balancer_regularization_;
    g0_.noalias() += -2. * balancer_wrench_.transpose() * G_temp_;

    PBWBC_TRACEF("MCFD::update %i", __LINE__);
    //Solve Quadratic Program to get desired contact wrenches
    solveQP(); //assigns value to W_C_desired
    PBWBC_TRACEF("MCFD::update %i", __LINE__);

    W_x_realized_ = JT_.topRows(6) * W_C_desired_;

    // Get joint torques
    tau_d_.noalias() = - JT_.bottomRows(modelInComCoordinates_->dq_size()) * W_C_desired_;  //TODO: check that
    for(std::size_t idx=0;idx<contacts_.size();idx++) {
        contacts_.at(idx)->setDesiredWrench(W_C_desired_.segment(6*idx,6));
    }

    updateBalancerObserverState();

    balancer_output_enable_ += std::min(std::max(balancer_output_enable_des_ - balancer_output_enable_,-1e-3),1e-3);
    if(balancer_output_enable_!=1.) {
        tau_d_ *= balancer_output_enable_;
    }
    PBWBC_TRACEF("MCFD::update %i", __LINE__);
}
    
bool MultiContactForceDistribution::hasTaskJacobian() const {
    return true;
}

Eigen::MatrixXd const & MultiContactForceDistribution::getTaskJacobian() const {
    // TODO: build task_jacobian in update, this here is called multiple times

    // use non-com coordinate jacobians to be compatible with other controllers
    task_jacobian_.setZero();
    task_jacobian_.topRows(3) = model_->comJacobian();
    task_jacobian_.block(3,3,3,3).setIdentity();
    Eigen::MatrixXd J(6,model_->dy_size());
    for(std::size_t contact_idx=0;contact_idx<contacts_.size();contact_idx++) {
        J.setZero();
        model_->getJacobian(contacts_.at(contact_idx)->getFrameId(),J);
        task_jacobian_.middleRows(6+6*contact_idx,6) = J;
    }
    return task_jacobian_;
}

} // namespace
