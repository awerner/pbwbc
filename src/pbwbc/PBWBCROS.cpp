#include <pbwbc/PBWBCROS.hpp>
#include <pbwbc/MotionOrchestratorROS.hpp>

namespace pbwbc {


PBWBCROS::PBWBCROS(std::shared_ptr<RobotState> const & state,
      std::shared_ptr<SensorState> const & sensor_state,
      std::shared_ptr<Model> const & model,
      std::shared_ptr<ConfigMap> const & config,
      std::vector<std::shared_ptr<ActuatorController>> const & actuatorcontrollers,
      ros::NodeHandle & nh) :
    PBWBC(state,sensor_state,model,config,actuatorcontrollers),
    nh_(nh) {
        
        // Parameters
        ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure(nh_));

        for(int idx=0; idx<model_->q_size();idx++) {
            auto const & joint_name = model_->jointNames().at(idx);
            std::stringstream str_to; str_to << "torque_offset_" << joint_name;
            ddr_->RegisterVariable(&tau_offsets_(idx), str_to.str(), -20., 20.);
        }

        current_state_publisher_ = RealTimePublisherBackEnd::advertise<sensor_msgs::JointState>(nh_,"current_state",
                config->param("current_state_buffer_size",1000),
                config->param("current_state_publisher_divisor",10));

        current_state_publisher_.msg().name = {
            "base_x",
            "base_y",
            "base_z",
            "base_q0",
            "base_q1",
            "base_q2",
            "base_qw"
        };
        current_state_publisher_.msg().name.insert(
            current_state_publisher_.msg().name.end(),
            model_->jointNames().begin(),model_->jointNames().end());
        current_state_publisher_.msg().position.resize(model_->y_size());
        current_state_publisher_.msg().velocity.resize(model_->y_size());
        current_state_publisher_.msg().effort.resize(model_->y_size());
        current_state_publisher_.fillBuffer();
        current_state_publisher_.register_ddynamic_reconfigure_options(*ddr_);
        assert(static_cast<int>(current_state_publisher_.msg().position.size())==model_->y_size());

        desired_torque_publisher_ = RealTimePublisherBackEnd::advertise<sensor_msgs::JointState>(nh_,"desired_torque",
                config->param("desired_torque_buffer_size",1000),
                config->param("desired_torque_publisher_divisor",10));
        desired_torque_publisher_.msg().name = model_->jointNames();
        desired_torque_publisher_.msg().effort.resize(model_->q_size());
        desired_torque_publisher_.fillBuffer();
        desired_torque_publisher_.register_ddynamic_reconfigure_options(*ddr_);
        
        logging_joint_state_publisher_ = RealTimePublisherBackEnd::advertise<sensor_msgs::JointState>(nh_,"/joint_states",1000,0);
        logging_joint_state_publisher_.msg().name = model_->jointNames();
        logging_joint_state_publisher_.msg().position.resize(model_->q_size());
        logging_joint_state_publisher_.msg().velocity.resize(model_->q_size());
        logging_joint_state_publisher_.msg().effort.resize(model_->q_size());
        logging_joint_state_publisher_.fillBuffer();
        
        ddr_->PublishServicesTopics();
}

PBWBCROS::~PBWBCROS() {};

void PBWBCROS::update(timestamp_t const & timestamp) {
    ros::Time now(timestamp.time_since_epoch().count());
    if(current_state_publisher_.trylock()) {
        // publish state
        current_state_publisher_.msg().header.stamp = now;
        for(int idx = 0; idx<6; idx++) {
            current_state_publisher_.msg().position.at(idx) = state_->y(idx);
            current_state_publisher_.msg().velocity.at(idx) = state_->dy(idx);
            current_state_publisher_.msg().effort.at(idx) = 0.;
        }

        current_state_publisher_.msg().position.at(6) = state_->y(6);
        current_state_publisher_.msg().velocity.at(6) = 0.;
        current_state_publisher_.msg().effort.at(6) = 0.;

        for(int idx = 0; idx<model_->q_size(); idx++) {
            current_state_publisher_.msg().position.at(7+idx) = state_->y.tail(model_->q_size())(idx);
            current_state_publisher_.msg().velocity.at(7+idx) = state_->dy.tail(model_->dq_size())(idx);
            current_state_publisher_.msg().effort.at(7+idx) = sensor_state_->jointTorques(idx);
        }
        current_state_publisher_.unlockAndPublish();
    }

    PBWBC::update(timestamp);
    // publish command
    if(desired_torque_publisher_.trylock()) {
        desired_torque_publisher_.msg().header.stamp = now;
        for(int idx = 0; idx<model_->q_size(); idx++) {
            desired_torque_publisher_.msg().effort[idx] = tau_d_(idx);
        }
        desired_torque_publisher_.unlockAndPublish();
    }
    if(logging_joint_state_publisher_.trylock()) {
        logging_joint_state_publisher_.msg().header.stamp = now;
        for(int idx = 0; idx<model_->q_size(); idx++) {
            logging_joint_state_publisher_.msg().position.at(idx) = 
                model_->stripBasePosition(state_->y)(idx);
            logging_joint_state_publisher_.msg().velocity.at(idx)= 
                model_->stripBaseVelocity(state_->dy)(idx);
            logging_joint_state_publisher_.msg().effort.at(idx) =
                tau_d_(idx);
        }
        logging_joint_state_publisher_.unlockAndPublish();
    }
}
    

void PBWBCROS::initialize() {
    motionOrchestrator_.reset(new MotionOrchestratorROS(model_,config_,controllers_,nh_));
}
    

} // namespace
