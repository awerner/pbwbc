#ifndef PBWBC_PINOCCHIODATA_HPP
#define PBWBC_PINOCCHIODATA_HPP
#include "pinocchio/multibody/model.hpp"
#include "pinocchio/multibody/data.hpp"
#include <pbwbc/ModelPinocchio.hpp>

namespace pbwbc {

class ModelPinocchio::PinocchioData {
public:
    std::shared_ptr<pinocchio::Model> model_;
    std::shared_ptr<pinocchio::Data> data_;

    std::map<int,std::shared_ptr<pinocchio::Data>> buffer_data_;
};
}

#endif // PBWBC_PINOCCHIODATA_HPP
