#include <pbwbc/PBWBC.hpp>
namespace pbwbc {
    PBWBC::PBWBC(std::shared_ptr<RobotState> const &state,
          std::shared_ptr<SensorState> const & sensor_state,
          std::shared_ptr<Model> const &model,
          std::shared_ptr<ConfigMap> const &config,
          std::vector<std::shared_ptr<ActuatorController>> const & actuatorcontrollers):
        state_(state),
        sensor_state_(sensor_state),
        model_(model),
        config_(config),
        tau_offsets_(Eigen::VectorXd::Zero(actuatorcontrollers.size())),
        actuatorcontrollers_(actuatorcontrollers),
        tau_d_(actuatorcontrollers.size()) {
        if(actuatorcontrollers_.size()!=static_cast<std::size_t>(model_->q_size())) {
            throw std::runtime_error("All joints need to be controlled");
        }
         
        std::vector<std::string> torque_offset_joints;
        config_->list_items("torque_offsets",torque_offset_joints);
        auto tconfig = config_->submap("torque_offsets");
        for(auto const & name : torque_offset_joints) {
            auto res = std::find(model_->jointNames().begin(),
                    model_->jointNames().end(),name);
            if(res==model_->jointNames().end()) {
                throw std::runtime_error("Joint name not found");
            }
            int idx = std::distance(model_->jointNames().begin(),res);
            if(tconfig->getParam(name,tau_offsets_(idx))) {
                PBWBC_DEBUG("torque_offset for joint " << name << 
                        " = " << tau_offsets_(idx));
            }
        }
    }
    
    PBWBC::~PBWBC() {};

    void PBWBC::addController(ConfigMap const & config) {
        throw std::runtime_error("Not implemented");
    }

    void PBWBC::addController(std::shared_ptr<Controller> const & controller) {
        if(this->tau_d_.size()!=controller->get_tau().size()) {
            std::stringstream str;
            str <<  "Controller output size not compatible. Have " << 
                this->tau_d_.size() << " actuators and controller provides "
                << controller->get_tau().size();
            throw std::runtime_error(str.str());
        }
        controllers_.push_back(controller);
    }

    std::vector<std::shared_ptr<Controller>> const & PBWBC::getControllers() const {
        return controllers_;
    }

    void PBWBC::initialize() {
        motionOrchestrator_.reset(new MotionOrchestrator(model_,config_,controllers_));
    }

    void PBWBC::start(timestamp_t const & timestamp) {
        motionOrchestrator_->start(timestamp);
        for(auto const & controller : controllers_) {
            controller->start(timestamp);
        }
        // run actuator controllers
        for(std::size_t idx=0;idx<actuatorcontrollers_.size();idx++) {
            actuatorcontrollers_[idx]->start(timestamp);
        }
    }

    void PBWBC::stop(timestamp_t const & timestamp) {
        motionOrchestrator_->stop(timestamp);
        for(auto const & controller : controllers_) {
            controller->stop(timestamp);
        }
        // run actuator controllers
        for(std::size_t idx=0;idx<actuatorcontrollers_.size();idx++) {
            actuatorcontrollers_[idx]->stop(timestamp);
        }
    }

    void PBWBC::update(timestamp_t const & timestamp) {
        PBWBC_TRACEF("PBWBC::update enter");
        // TODO: some statistics how long each computation takes

        // TODO: commands:
        // for correct command generation
        // there need to be a mechanism to get the current global command
        // even if local commands had been active
        // also locking needs to be there

        // update motion players
        motionOrchestrator_->update(timestamp);

        // compute controllers and generate torque
        tau_d_.setZero();
        for(auto const & controller : controllers_) {
            PBWBC_TRACEF("PBWBC::update: %s", controller->name().c_str());
            // update desired state
            controller->update(timestamp);  // controllers are likely
                                   // JointEndStop
                                   // Hierarchy
                                   //    MultiContactForceDistribution
                                   //          CartesianImpedanceComController: Com
                                   //          CartesianImpedanceController: Hip
                                   //          CartesianImpedanceController: left leg
                                   //          CartesianImpedanceController: right leg
                                   //          CartesianImpedanceController: left wrist
                                   //          CartesianImpedanceController: right wrist
                                   //          CartesianImpedanceCLController: object_impedance // both arms one object
                                   //    JointImpedance <-- deals with obvious nullspace (head, gripper, etc)
                                   // 
            //PBWBC_DEBUG("torque form controller: " << controller->name() << ": " << controller->get_tau().transpose());
            tau_d_ += controller->get_tau();
        }
        tau_d_ += tau_offsets_;
        PBWBC_TRACEF("PBWBC::update: controller finished");
        // forward torque to joint level controllers
        for(std::size_t idx=0;idx<actuatorcontrollers_.size();idx++) {
            actuatorcontrollers_[idx]->update(timestamp,tau_d_(idx));
        }
        PBWBC_TRACEF("PBWBC::update: leave");
    }

    Eigen::VectorXd const & PBWBC::getTau() {
        return tau_d_;
    }

} // namespace
