#include <pbwbc/ModelPinocchio.hpp>
#include "pinocchio/multibody/model.hpp"
#include "pinocchio/parsers/urdf.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/center-of-mass.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/algorithm/rnea.hpp"
#include "pinocchio/algorithm/crba.hpp"
#include "pinocchio/algorithm/aba.hpp"
#include "pinocchio/algorithm/model.hpp"
#include <pbwbc/PinocchioData.hpp>
// For pinocchio Data member variables see
// https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/structpinocchio_1_1DataTpl.html

namespace pbwbc {


ModelPinocchio::ModelPinocchio() :
    d(new PinocchioData) {
    d->model_ = std::shared_ptr<pinocchio::Model>(new pinocchio::Model);

    // create background computation thread
    valid_buffer_ = -1;
    compute_downsample_ = 5;
    compute_has_run_ = true;
    keep_running_ = true;
    model_compute_task_next_id_ = 0;
    compute_thread_ = std::thread([this](){
          this->compute_thread();
          });
    pthread_setname_np(compute_thread_.native_handle(),"model_compute");

    // decreate priority of compute_thread by one
    // this constructor is called from a non-RT thread on the robot,
    // the priority needs to be manually reset with setRealTimeParameters
    struct sched_param param;
    int policy;
    errno = pthread_getschedparam(pthread_self(),&policy,&param);
    if(errno) {
		perror("ERROR: pthread_getschedparam");
		throw std::runtime_error("errno!=0");
	}
    param.sched_priority = std::max(static_cast<int>(param.sched_priority) - 1,0);
    PBWBC_DEBUG("Starting ModelPinocchio compute_thread with priority: " 
            << param.sched_priority);
    errno = pthread_setschedparam(compute_thread_.native_handle(),policy,&param);
    if(errno) {
		perror("ERROR: pthread_setschedparam");
		throw std::runtime_error("errno!=0");
	}
    compute_thread_counter_ = 0;
}

void ModelPinocchio::setRealTimeParameters(int policy, int priority) {
    struct sched_param param;
    param.sched_priority =  priority;
    PBWBC_DEBUG("Setting ModelPinocchio compute_thread priority: " 
            << param.sched_priority);
    errno = pthread_setschedparam(compute_thread_.native_handle(),policy,&param);
    if(errno) {
		perror("ERROR: pthread_setschedparam");
		throw std::runtime_error("errno!=0");
	}
}

std::shared_ptr<ModelPinocchio>
ModelPinocchio::create_from_urdf_file(std::string const & filename) {
    std::shared_ptr<ModelPinocchio> d(new ModelPinocchio);
    pinocchio::urdf::buildModel(filename,
            pinocchio::JointModelFreeFlyer(),*d->pinocchio_data()->model_,false);
    d->pinocchio_data()->data_ = std::shared_ptr<pinocchio::Data>(
            new pinocchio::Data(*d->pinocchio_data()->model_));
    for(int idx : {0,1}) {
        d->pinocchio_data()->buffer_data_[idx] = std::shared_ptr<pinocchio::Data>(
                new pinocchio::Data(*d->pinocchio_data()->model_));
    }
    d->joint_names_ = d->pinocchio_data()->model_->names;
    d->joint_names_.erase(d->joint_names_.begin(),d->joint_names_.begin()+2);
    d->com_jacobian_.resize(3,d->dy_size());
    return d;
}

std::shared_ptr<ModelPinocchio>
ModelPinocchio::create_from_urdf_data(std::string const & filename) {
    std::shared_ptr<ModelPinocchio> d(new ModelPinocchio);
    pinocchio::urdf::buildModelFromXML(filename,
            pinocchio::JointModelFreeFlyer(),*d->pinocchio_data()->model_,false);
    d->pinocchio_data()->data_ = std::shared_ptr<pinocchio::Data>(
            new pinocchio::Data(*d->pinocchio_data()->model_));
    for(int idx : {0,1}) {
        d->pinocchio_data()->buffer_data_[idx] = std::shared_ptr<pinocchio::Data>(
                new pinocchio::Data(*d->pinocchio_data()->model_));
    }
    d->joint_names_ = d->pinocchio_data()->model_->names;
    d->joint_names_.erase(d->joint_names_.begin(),d->joint_names_.begin()+2);
    d->com_jacobian_.resize(3,d->dy_size());
    return d;
}

void
ModelPinocchio::reduceModel(std::vector<long unsigned int> const & list_of_joints_to_lock) {
    Eigen::VectorXd reference_configuration;
    if(d->model_->referenceConfigurations.empty()) {
        PBWBC_DEBUG("Model has no reference configuration, using zero configuration");
        reference_configuration = Eigen::VectorXd::Zero(d->model_->nq);
    } else {
        reference_configuration = d->model_->referenceConfigurations.begin()->second;
    }
    d->model_.reset(new pinocchio::Model(
        pinocchio::buildReducedModel(*d->model_,list_of_joints_to_lock,
            reference_configuration)));
    d->data_ = std::shared_ptr<pinocchio::Data>(
            new pinocchio::Data(*d->model_));
    for(int idx : {0,1}) {
        d->buffer_data_[idx] = std::shared_ptr<pinocchio::Data>(
                new pinocchio::Data(*d->model_));
    }
    joint_names_ = d->model_->names;
    joint_names_.erase(joint_names_.begin(),joint_names_.begin()+2);
    com_jacobian_.resize(3, dy_size());
}

ModelPinocchio::~ModelPinocchio() {
  {
      std::unique_lock<std::mutex> lock(compute_mutex_);
      keep_running_ = false;
      compute_has_run_ = false;
  }

  for(int idx=0;idx<100;idx++) {
      std::unique_lock<std::mutex> lock(compute_mutex_);
      if(compute_has_run_)break;
      compute_cv_.notify_one();
      std::this_thread::sleep_for(std::chrono::microseconds(10)); 
  }
  compute_thread_.join();
}

void ModelPinocchio::setState(std::shared_ptr<RobotState> const & state) {
    state_ = state;
    for(int idx : {0,1}) {
        state_buffer_.insert(std::make_pair(idx,RobotState(*state_)));
    }
}

void ModelPinocchio::addContact(std::string const & name) {
    int contact_frame_id = d->model_->getFrameId(name);
    PBWBC_DEBUG(contact_frame_id);
    contact_frame_ids_.push_back(contact_frame_id);
}

void ModelPinocchio::blocking_update(timestamp_t const & timestamp) {
    {
        std::unique_lock<std::mutex> lock(compute_mutex_);
        valid_buffer_ = -1;
    }
    compute_thread_counter_ = 0;
    update(timestamp);
    while(true) {
      {
        std::unique_lock<std::mutex> lock(compute_mutex_);
        if(valid_buffer_!=-1) {
            break;
        }
      }
      std::this_thread::sleep_for(std::chrono::microseconds(10)); 
    }
}

void ModelPinocchio::update(timestamp_t const &) {
    pinocchio::forwardKinematics(*d->model_,*d->data_,state_->y,state_->dy,state_->ddy);

    pinocchio::computeJointJacobians(*d->model_,*d->data_,state_->y);
    pinocchio::framesForwardKinematics(*d->model_,*d->data_,state_->y);

    pinocchio::jacobianCenterOfMass(*d->model_,*d->data_,false);

    if(compute_thread_counter_==0) {
        PBWBC_TRACEF("MOD trigger");
        std::unique_lock<std::mutex>(compute_mutex_);
        if(!compute_has_run_) {
            PBWBC_TRACEF("MOD overrun");
            PBWBC_DEBUG("compute_thread has not run in the time expected");
            compute_cv_.notify_one();
            return; // do not overwrite input arguments
        }
        compute_buffer_ = (valid_buffer_==0)? 1 : 0;
        state_buffer_.at(compute_buffer_).y = state_->y;
        state_buffer_.at(compute_buffer_).dy = state_->dy;
        compute_has_run_ = false;
        compute_cv_.notify_one();
    }
    compute_thread_counter_++;
    if(compute_thread_counter_>=compute_downsample_)compute_thread_counter_=0;
}

void ModelPinocchio::compute_thread() {
    //PBWBC_DEBUG("update_model_thread ready: " << compute_thread_.get_id());
    while(true) {
        std::unique_lock<std::mutex> lock(compute_mutex_);         
        compute_cv_.wait(lock);
        if(!keep_running_) {
            compute_has_run_ = true;
            //PBWBC_DEBUG("compute_thread received termination request");
            break;
        }
        if(compute_has_run_) {
            //PBWBC_DEBUG("spurious wakeup");
            continue;
        }
        PBWBC_TRACEF("MOD start");
        pinocchio::forwardKinematics(*d->model_,*d->buffer_data_.at(compute_buffer_),
            state_buffer_.at(compute_buffer_).y,
            state_buffer_.at(compute_buffer_).dy,
            state_buffer_.at(compute_buffer_).ddy);

        // TODO: evaluate moving jacobians to compute thread
        // and only do forward kinematics in RT loop
        pinocchio::computeJointJacobians(*d->model_,
            *d->buffer_data_.at(compute_buffer_),
            state_buffer_.at(compute_buffer_).y);
        pinocchio::framesForwardKinematics(*d->model_,
            *d->buffer_data_.at(compute_buffer_),
            state_buffer_.at(compute_buffer_).y);

        // compute quantities, use current temp buffer to store results
        pinocchio::computeGeneralizedGravity(*d->model_,
                *d->buffer_data_.at(compute_buffer_),state_buffer_.at(compute_buffer_).y);
        pinocchio::crba(*d->model_,
                *d->buffer_data_.at(compute_buffer_),state_buffer_.at(compute_buffer_).y);
        d->buffer_data_.at(compute_buffer_)->M.triangularView<Eigen::StrictlyLower>() = 
            d->buffer_data_.at(compute_buffer_)->M.transpose().triangularView<Eigen::StrictlyLower>();

        // TODO: check if cholesky decomposition is more efficient of
        // MassMatrix and inverse MassMatrix are computed
        pinocchio::computeMinverse(*d->model_,*d->buffer_data_.at(compute_buffer_),
                state_buffer_.at(compute_buffer_).y);
        d->buffer_data_.at(compute_buffer_)->Minv.triangularView<Eigen::StrictlyLower>() = 
            d->buffer_data_.at(compute_buffer_)->Minv.transpose().triangularView<Eigen::StrictlyLower>();

        for(auto const & task : model_compute_tasks_) {
            // TODO: if a task accesses a Jacobian, there is no double buffering
            // => add double/triple buffering for d->data_ in fast cycle
            // or force these queries to use d->buffer_data_ for Jacobians
            task.second();
        }
        // update variables to signal completion of the computation and
        // point queries to correct buffer
        compute_has_run_ = true;
        valid_buffer_ = compute_buffer_;
        compute_done_cv_.notify_all();
        PBWBC_TRACEF("MOD done");
    }
}

double ModelPinocchio::mass() {
    // TODO: cache this
    return pinocchio::computeTotalMass(*d->model_,*d->data_);
}

void ModelPinocchio::setBasePosition(
        translation_t const & translation,
        orientation_t const & orientation,
        RobotState & state) {
    auto const & rj = d->model_->joints.at(1);
    state.y.segment(rj.idx_q(),rj.nq()).segment(0,3) = translation;
    state.y.segment(rj.idx_q(),rj.nq()).segment(3,4) = orientation.coeffs();
}

void ModelPinocchio::setBaseVelocity(
        translation_t const & translation,
        translation_t const & orientation,
        RobotState & state) {
    auto const & rj = d->model_->joints.at(1);
    state.dy.segment(rj.idx_q(),rj.nv()).segment(0,3) = translation;
    state.dy.segment(rj.idx_q(),rj.nv()).segment(3,3) = orientation;
}

int ModelPinocchio::y_size() {
    return d->model_->nq;
}

int ModelPinocchio::dy_size() {
    return d->model_->nv;
}

int ModelPinocchio::q_size() {
    return d->model_->nq - d->model_->joints.at(1).nq();
}

int ModelPinocchio::dq_size() {
    return d->model_->nv - d->model_->joints.at(1).nv();
}

int ModelPinocchio::contact_size() {
    return contact_frame_ids_.size();
}

ModelPinocchio::translation_t ModelPinocchio::bodyTranslation(int frame_id) {
    return d->data_->oMf.at(frame_id).translation();
}
ModelPinocchio::orientation_t ModelPinocchio::bodyOrientation(int frame_id) {
    return ModelPinocchio::orientation_t(d->data_->oMf.at(frame_id).rotation());
}

void ModelPinocchio::getJacobian(int body_id, Eigen::Ref<Eigen::MatrixXd> jac ) {
    pinocchio::getFrameJacobian(*d->model_,
            *d->data_,
            body_id,
            pinocchio::LOCAL,
            jac);
}

void ModelPinocchio::getJacobianWorld(int body_id, Eigen::Ref<Eigen::MatrixXd> jac ) {
    pinocchio::getFrameJacobian(*d->model_,
            *d->data_,
            body_id,
            pinocchio::WORLD,
            jac);
}

ModelPinocchio::matrix_t const & ModelPinocchio::MassMatrix() {
    int v = valid_buffer_;
    if(!(v==0 || v==1)) {
        PBWBC_DEBUG("v = " << v);
    }
    return d->buffer_data_.at(v)->M;
}

ModelPinocchio::rowmatrix_t const & ModelPinocchio::InvMassMatrix() {
    int v = valid_buffer_;
    if(!(v==0 || v==1)) {
        PBWBC_DEBUG("v = " << v);
    }
    return d->buffer_data_.at(v)->Minv;
}

ModelPinocchio::matrix_t const & ModelPinocchio::Coriolis() {
    throw std::runtime_error("Check if this is calculated");
    return d->data_->C;
}
ModelPinocchio::vector_t const & ModelPinocchio::gravity() {
    int v = valid_buffer_;
    if(!(v==0 || v==1)) {
        PBWBC_DEBUG("v = " << v);
    }
    return d->buffer_data_.at(v)->g;
}

ModelPinocchio::translation_t ModelPinocchio::com() {
    pinocchio::centerOfMass(
            *d->model_,
            *d->data_,
            state_->y,
            state_->dy);
    return d->data_->com[0];
}


ModelPinocchio::translation_t ModelPinocchio::dcom() {
    pinocchio::centerOfMass(
            *d->model_,
            *d->data_,
            state_->y,
            state_->dy);
    return d->data_->vcom[0];
}

Eigen::MatrixXd const & ModelPinocchio::comJacobian() {
    com_jacobian_ = pinocchio::jacobianCenterOfMass(
            *d->model_,
            *d->data_,
            state_->y);
    return com_jacobian_;
}

std::vector<std::string> const & ModelPinocchio::jointNames() {
    return joint_names_;
}

Model::vector_t const & ModelPinocchio::position_max() {
    return d->model_->upperPositionLimit;
}
Model::vector_t const & ModelPinocchio::position_min() {
    return d->model_->lowerPositionLimit;
}
Model::vector_t const & ModelPinocchio::velocity_max() {
    return d->model_->velocityLimit;
}
Model::vector_t const & ModelPinocchio::torque_max() {
    return d->model_->effortLimit;
}
    
int ModelPinocchio::getFrameId(std::string const & frame_name) {
    std::size_t frameId = d->model_->getFrameId(frame_name);
    if(frameId == d->model_->frames.size()) {
        std::stringstream str;
        str << "Frame not found: \"" << frame_name << "\"";
        throw std::runtime_error(str.str());
    }
    return frameId;
}

std::string const & ModelPinocchio::getFrameName(int frameId) {
    return d->model_->frames.at(frameId).name;
}

std::vector<int> ModelPinocchio::getJointFrames() {
   std::vector<int> ids;
   for(std::size_t id = 0; id < d->model_->frames.size(); id++ ) {
      auto const & frame = d->model_->frames.at(id);
      if(frame.type==pinocchio::FrameType::JOINT) {
        ids.push_back(id);
      }
   } 
   return ids;
}

std::vector<int> ModelPinocchio::getLinkFrames() {
   std::vector<int> ids;
   for(std::size_t id = 0; id < d->model_->frames.size(); id++ ) {
      auto const & frame = d->model_->frames.at(id);
      if(frame.type==pinocchio::FrameType::BODY) {
        ids.push_back(id);
      }
   } 
   return ids;
}
    
std::shared_ptr<ModelPinocchio::PinocchioData> const & ModelPinocchio::pinocchio_data() {
    return d;
}

int ModelPinocchio::getPinocchioJointId(std::string const & joint_name) {
    return d->model_->getJointId(joint_name);
}

int ModelPinocchio::register_compute_task(function_t const & function) {
    std::unique_lock<std::mutex> lock(compute_mutex_);
    model_compute_tasks_.push_back(std::pair<int,function_t>(
                model_compute_task_next_id_,function));
    return model_compute_task_next_id_++;
}

void ModelPinocchio::deregister_compute_task(int task_id) {
    std::unique_lock<std::mutex> lock(compute_mutex_);
    auto itr = std::find_if(model_compute_tasks_.begin(),
            model_compute_tasks_.end(),
            [task_id](std::pair<int,function_t> const & info) {
                return info.first==task_id;
            });
    if(itr!=model_compute_tasks_.end()) {
        model_compute_tasks_.erase(itr);
    }
}

Eigen::Ref<const Eigen::VectorXd> const ModelPinocchio::stripBasePosition(Eigen::Ref<const Eigen::VectorXd> const & unfiltered) {
        int q_size = d->model_->nq - d->model_->joints.at(1).nq();
        return unfiltered.tail(q_size);
}

Eigen::Ref<const Eigen::VectorXd> const ModelPinocchio::stripBaseVelocity(Eigen::Ref<const Eigen::VectorXd> const & unfiltered) {
        int dq_size = d->model_->nv - d->model_->joints.at(1).nv();
        return unfiltered.tail(dq_size);
}

}


