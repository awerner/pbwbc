#include <pbwbc/ROSConfig.hpp>
#include <xmlrpcpp/XmlRpc.h>


namespace pbwbc {
#define param(T) \
          void ROSConfigMap::param(std::string const & parameter_name, T & variable, T const & default_value ) const { \
              /*ROS_DEBUG_STREAM("getting parameter: " << prefix + parameter_name);*/ \
              if(parameter_name.at(0)=='/') { \
                controller_nh.param(parameter_name,variable,default_value); \
              } else { \
                controller_nh.param(prefix + parameter_name,variable,default_value); \
              } \
          }
          param(int)
          param(bool)
          param(double)
          param(std::string)
          param(std::vector<std::string>)
#undef param

#define getParam(T) \
          bool ROSConfigMap::getParam(std::string const & parameter_name, T & variable) const { \
              /*ROS_DEBUG_STREAM("getting parameter: " << prefix + parameter_name);*/ \
              if(parameter_name.at(0)=='/') { \
                return controller_nh.getParam(parameter_name,variable); \
              } else { \
                return controller_nh.getParam(prefix + parameter_name,variable); \
            } \
          }
          getParam(int)
          getParam(bool)
          getParam(double)
          getParam(std::string)
          getParam(std::vector<std::string>)
          getParam(std::vector<double>)
          getParam(std::vector<int>)
#undef getParam



    std::shared_ptr<ConfigMap> ROSConfigMap::submap(std::string const & prefix) {
        std::shared_ptr<ROSConfigMap> sub( new ROSConfigMap(*this));
        sub->prefix = sub->prefix + prefix + "/";
        return sub;
    }

    bool ROSConfigMap::list_items(std::string const & parameter, std::vector<std::string> & keys ) {
        //ROS_DEBUG_STREAM("ROSConfigMap::list_items in namespace " << controller_nh.getNamespace() <<
        //    " with prefix " << prefix);
        try { 
            XmlRpc::XmlRpcValue entry;
            if(!controller_nh.getParam(prefix + parameter,entry)) return false;
            if(entry.getType() != XmlRpc::XmlRpcValue::TypeStruct) {
                throw std::runtime_error("Expected TypeStruct");
            }
            XmlRpc::XmlRpcValue& _entry = const_cast<XmlRpc::XmlRpcValue&>(entry);
            for (XmlRpc::XmlRpcValue::iterator itr = _entry.begin(); itr != _entry.end() ; itr++)  {
                //std::cout << itr->first << std::endl;
                keys.push_back(itr->first);
                //std::cout << itr->toXml() << std::endl;
            }
        } catch ( XmlRpc::XmlRpcException const & ex ) {
            std::cout << "XmlRpc::XmlRpcException: code(" << ex.getCode() << ") " << ex.getMessage() << std::endl;
            return false;
        }
        return true;
    }

} // namespace

