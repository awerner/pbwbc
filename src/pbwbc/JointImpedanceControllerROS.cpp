#include <pbwbc/JointImpedanceControllerROS.hpp>

namespace pbwbc {


JointImpedanceControllerROS::JointImpedanceControllerROS(
            std::shared_ptr<RobotState> const & state,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config,
            ros::NodeHandle const & nh
            ) :
    JointImpedanceController(state,model,config),
    nh_(nh)
    {

        ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure(nh_));
        for (std::string joint_name : impedance_controlled_joint_names_) {
            if (jointID_map_.count(joint_name)) { 
                int idx = jointID_map_.at(joint_name);

                std::stringstream str_setpoint; str_setpoint << "setpoint_" << joint_name;
                ddr_->RegisterVariable(&goal_state_.y.tail(model_->dq_size())(idx), str_setpoint.str(), position_min_(idx), position_max_(idx));
            }
        }

        for (std::string joint_name : impedance_controlled_joint_names_) {
            if (jointID_map_.count(joint_name)) { 
                int idx = jointID_map_.at(joint_name);
                std::stringstream str_stiffness; str_stiffness << "stiffness_" << joint_name;
                //std::stringstream str_damping; str_damping << "damping_" << joint_name;
                std::stringstream str_damping_ratio; str_damping_ratio << "damping_ratio_" << joint_name;
                std::stringstream str_max_effort;  str_max_effort << "max_effort_" << joint_name;

                ddr_->RegisterVariable(&nominal_stiffness_desired_(idx), str_stiffness.str(), 0., 5000.);
                //ddr_->RegisterVariable(&damping_value_(idx), str_damping.str(), 0., 200.);
                ddr_->RegisterVariable(&damping_ratio_(idx), str_damping_ratio.str(), 0., 1.);
                ddr_->RegisterVariable(&maximum_torque_(idx), str_max_effort.str(), 0., 
                        model_->stripBaseVelocity(model_->torque_max())(idx));
            }
        }
        ddr_->RegisterVariable(&update_pose_manually_, "update_pose_manually", 0., 1.);
        ddr_->RegisterVariable(&stiffness_step_max_, "stiffness_step_max", 0., 20.);
        ddr_->RegisterVariable(&feedforward_gain_, "feedforward_gain", 0., 1.);

        
        desired_state_publisher_ = RealTimePublisherBackEnd::advertise<sensor_msgs::JointState>(nh_,"desired_state",
                config_->param("desired_state_buffer_size",1000),
                config_->param("desired_state_publisher_divisor",10));
        desired_state_publisher_.msg().name = model_->jointNames();
        desired_state_publisher_.msg().position.resize(model_->q_size());
        desired_state_publisher_.msg().velocity.resize(model_->q_size());
        desired_state_publisher_.fillBuffer();
        desired_state_publisher_.register_ddynamic_reconfigure_options(*ddr_);
        current_state_publisher_ = RealTimePublisherBackEnd::advertise<sensor_msgs::JointState>(nh_,"current_state",
                config_->param("current_state_buffer_size",1000),
                config_->param("current_state_publisher_divisor",10));
        current_state_publisher_.msg().name = model_->jointNames();
        current_state_publisher_.msg().position.resize(model_->q_size());
        current_state_publisher_.msg().velocity.resize(model_->q_size());
        current_state_publisher_.fillBuffer();
        current_state_publisher_.register_ddynamic_reconfigure_options(*ddr_);
        desired_torque_publisher_ = RealTimePublisherBackEnd::advertise<sensor_msgs::JointState>(nh_,"desired_torque",
                config_->param("desired_torque_buffer_size",1000),
                config_->param("desired_torque_publisher_divisor",10));
        desired_torque_publisher_.msg().name = model_->jointNames();
        desired_torque_publisher_.msg().effort.resize(model_->q_size());
        desired_torque_publisher_.fillBuffer();
        desired_torque_publisher_.register_ddynamic_reconfigure_options(*ddr_);
        
        ddr_->PublishServicesTopics();
    }

JointImpedanceControllerROS::~JointImpedanceControllerROS() {}

    
void JointImpedanceControllerROS::update(timestamp_t const & timestamp) {
    ros::Time now(timestamp.time_since_epoch().count());
    // publish state
    if(current_state_publisher_.trylock()) {
        current_state_publisher_.msg().header.stamp = now;
        std::copy_n(&state_->y.tail(model_->q_size())(0),
                model_->q_size(),
                &current_state_publisher_.msg().position.at(0));
        std::copy_n(&state_->dy.tail(model_->q_size())(0),
                model_->q_size(),
                &current_state_publisher_.msg().velocity.at(0));
        current_state_publisher_.unlockAndPublish();
    }
    if(desired_state_publisher_.trylock()) {
        desired_state_publisher_.msg().header.stamp = now;
        std::copy_n(&state_d_.y.tail(model_->q_size())(0),
                model_->q_size(),
                &desired_state_publisher_.msg().position.at(0));
        std::copy_n(&state_d_.dy.tail(model_->q_size())(0),
                model_->q_size(),
                &desired_state_publisher_.msg().velocity.at(0));
        desired_state_publisher_.unlockAndPublish();
    }

    JointImpedanceController::update(timestamp);
    // publish command
    if(desired_torque_publisher_.trylock()) {
        desired_torque_publisher_.msg().header.stamp = now;
        std::copy_n(&tau_d_(0),
                model_->q_size(),
                &desired_torque_publisher_.msg().effort.at(0));
        desired_torque_publisher_.unlockAndPublish();
    }
}

} // namespace pbwbc
