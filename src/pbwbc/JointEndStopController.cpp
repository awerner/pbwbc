#include <pbwbc/JointEndStopController.hpp>

namespace pbwbc {

JointEndStopController::JointEndStopController(
            std::shared_ptr<RobotState> const & state,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config
            ) :
    Controller([&](){
            std::string name;
            config->param("name",name,"JointEndStopController");
            return name;}(),model->dq_size()),
    state_(state),
    model_(model),
    config_(config),
    position_min_(model_->stripBasePosition(model_->position_min())),
    position_max_(model_->stripBasePosition(model_->position_max())),
    maximum_torque_(model_->stripBaseVelocity(model_->torque_max())),
    damping_ratio_(model_->dq_size())
    {

    //Get ids of impedance controlled joints
    std::vector<std::string> impedance_controlled_joint_names;
    config->getParam("impedance_controlled_joint_names", impedance_controlled_joint_names);
    std::vector<std::string> jointNames = model_->jointNames();
    for (std::string joint_name : impedance_controlled_joint_names) {
      auto it = find (jointNames.begin(), jointNames.end(), joint_name);
      if (it != jointNames.end()) {
        impedance_controlled_joints_ids_.push_back(std::distance(jointNames.begin(), it)); // Get index of element from iterator
      } else {
        throw std::runtime_error("Joint not found");
      }
    }
    task_jacobian_.resize(impedance_controlled_joints_ids_.size(),model_->dy_size());
    task_jacobian_.setZero();
    for(std::size_t idx=0; idx<impedance_controlled_joints_ids_.size();idx++) {
        task_jacobian_(idx,6+impedance_controlled_joints_ids_[idx]) = 1.;
    }

    // Get settings from configuration
    // JointEndStopControllerROS the add dynamic reconfigure functionality
    double damping_ratio;
    config_->param("damping_ratio",damping_ratio,1.);
    damping_ratio_.setConstant(damping_ratio);
    PBWBC_DEBUG("damping_ratio: " << damping_ratio);
  
    config_->param("endstop_offset",endstop_offset_,0.);
    config_->param("endstop_distance",endstop_distance_,0.1); 

    // acquired list of joints and settings from config
    for(int idx=0;idx<model_->dq_size();idx++) {
        std::string result;
        std::string const & joint_name = model_->jointNames().at(idx);
        PBWBC_DEBUG("Getting settings for joint " << joint_name);
        config_->param("joints/"+joint_name+"/position_min",
                position_min_(idx),
                position_min_(idx));
        config_->param("joints/"+joint_name+"/position_max",
                position_max_(idx),
                position_max_(idx));
        config_->param("joints/"+joint_name+"/damping_ratio",
                damping_ratio_(idx),
                damping_ratio_(idx));
    }
}

JointEndStopController::~JointEndStopController() {}

void JointEndStopController::start(timestamp_t const &) {
    counter_ = 0;
}

void JointEndStopController::stop(timestamp_t const &) {}

namespace {

double square_nonlinearity(double x) {
   if( x < -2.0 ) {
	return -1;
   } else if( x < 0. ) {
	return (x + (x*x)/4.);
   } else if( x < 2. ) {
	return (x - (x*x)/4.);
   } else {
	return 1;
   }
}

double fade_in(double x) {
   return (square_nonlinearity(x*4.-2.)+1.)/2.;
}

}

double JointEndStopController::potential_d1(double d, int idx) {
    return (maximum_torque_(idx) / std::pow(endstop_distance_,2.) )
            *  std::pow(endstop_distance_ - d,2.);
}

double JointEndStopController::potential_d2(double d, int idx ) {
    return ( 2. * maximum_torque_(idx) / std::pow(endstop_distance_,2.) )
            *  (endstop_distance_ - d);
}

void JointEndStopController::update(timestamp_t const &) {
    tau_d_.setZero();
    const int q_offset = model_->y_size() - model_->q_size();
    const int dq_offset = model_->dy_size() - model_->dq_size();

    double fade_in_val = fade_in(counter_/1000.); // 1s fade in
    if(counter_ < std::numeric_limits<int>::max()) {
        counter_++;
    }
    // Compute stiffness and torque 
    for (int idx : impedance_controlled_joints_ids_) {
        double p_min = position_min_(idx) + endstop_offset_;
        double p_max = position_max_(idx) - endstop_offset_;
        double q = state_->y(q_offset+idx);
        double dq = state_->dy(dq_offset+idx);
        double local_k_endstop;
        if(q - p_min - endstop_distance_ < 0.) {
            double d_stop  = q - p_min;
            tau_d_(idx) += potential_d1(d_stop,idx) * fade_in_val;
            local_k_endstop = potential_d2(d_stop,idx) * fade_in_val;
        }else if(- q + p_max - endstop_distance_ < 0.) {
            double d_stop = - q + p_max;
            tau_d_(idx) += - potential_d1(d_stop,idx) * fade_in_val;
            local_k_endstop = potential_d2(d_stop,idx) * fade_in_val;
        } else {
            local_k_endstop = 0.;
        }
        local_k_endstop = std::max(local_k_endstop,1e-4);
        const double mass = model_->MassMatrix()(dq_offset+idx,dq_offset+idx);
        tau_d_(idx) -= 2. * damping_ratio_(idx) 
            * std::sqrt(local_k_endstop * mass) * dq;

        tau_d_(idx) = std::min( maximum_torque_(idx),tau_d_(idx));
        tau_d_(idx) = std::max(-maximum_torque_(idx),tau_d_(idx));
    }
}
    
bool JointEndStopController::hasTaskJacobian() const {
    return true;
}

Eigen::MatrixXd const & JointEndStopController::getTaskJacobian() const {
    return task_jacobian_;
}


} // namespace pbwbc
