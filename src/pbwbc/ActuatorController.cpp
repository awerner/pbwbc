#include <pbwbc/ActuatorController.hpp>

namespace pbwbc {

ActuatorController::ActuatorController() {}

ActuatorController::~ActuatorController() {}

void ActuatorController::start(timestamp_t const &) {}
void ActuatorController::stop(timestamp_t const &) {}

JointNullController::JointNullController() {}

JointNullController::~JointNullController() {}

void JointNullController::update(timestamp_t const &, double tau_d) {}



ActuatorAdmittanceController::ActuatorAdmittanceController(
		std::string const & joint_name,
        std::shared_ptr<RobotState> const & state,
        std::shared_ptr<Model> const & model,
        std::shared_ptr<ConfigMap> const & config,
        std::shared_ptr<SensorState> const & sensor_state,
        std::shared_ptr<ActuatorController> const & underlying_controller
        ) : 
joint_name_(joint_name),
state_(state),
model_(model),
config_(config),
sensor_state_(sensor_state),
underlying_controller_(underlying_controller),
joint_axis_(0),
joint_direction_(1) {

	if(!config_->getParam("proportional_gain", K_desired_)) {
            throw std::runtime_error("ActuatorAdmittanceController: parameter 'proportional_gain' not found");
    }
    K_ = 0.;
	if(!config_->getParam("integral_gain", I_)) {
            throw std::runtime_error("ActuatorAdmittanceController: parameter 'integral_gain' not found");
    }
	if(!config_->getParam("integral_windup", integral_windup_)) {
            throw std::runtime_error("ActuatorAdmittanceController: parameter 'integral_windup' not found");
    }
	config_->param("feedforward_gain", feedforward_gain_,1.);

    std::string ft_sensor_name;
    if(config_->getParam("ft_sensor",ft_sensor_name)) {
        auto itr = std::find_if(sensor_state_->ftsWrenches.begin(),
            sensor_state_->ftsWrenches.end(),
            [&](std::tuple<int,std::string,wrench_t> const &p){
                return std::get<1>(p)==ft_sensor_name;});
        if(itr==sensor_state_->ftsWrenches.end()) {
            throw std::runtime_error("Did not find force torque sensor");
        }
        fts_id_ = std::distance(sensor_state_->ftsWrenches.begin(),itr);
    } else {
        throw std::runtime_error("ActuatorAdmittanceController: parameter 'ft_sensor' not found");
    }

    if(!config_->getParam("ft_filter_gain", ft_filter_gain_)) {
            throw std::runtime_error("ActuatorAdmittanceController: parameter 'ft_filter_gain' not found");
    }

    fts_frame_id_ = std::get<0>(sensor_state_->ftsWrenches.at(fts_id_));
    joint_frame_id_ = model_->getFrameId(joint_name_);

    //Find the axis around which the joint rotates
    //and the rotation direction
    Eigen::MatrixXd jacobian(6,model_->dy_size());
    jacobian.setZero();
    model_->getJacobian(joint_frame_id_, jacobian);
    auto res = std::find(model_->jointNames().begin(),model_->jointNames().end(),joint_name_);
    if(res==model_->jointNames().end()) {
    	throw std::runtime_error("Could not find joint");
   	}
   	const int dq_offset = model_->dy_size() - model_->dq_size();
    joint_idx_ = std::distance(model_->jointNames().begin(),res);
    for (int idx = 0; idx < jacobian.rows(); idx++){
    	if (abs(jacobian.middleCols(dq_offset+joint_idx_,1)(idx)) 
                > abs(jacobian.middleCols(dq_offset+joint_idx_,1)(joint_axis_))) {
    		joint_axis_ = idx;
    		joint_direction_ = jacobian.middleCols(dq_offset+joint_idx_,1)(idx) 
                                / abs(jacobian.middleCols(dq_offset+joint_idx_,1)(idx));
    	}
    }
    config_->getParam("joint_direction",joint_direction_);
    if (joint_axis_ < 3) {
    	std::string error_message = "ActuatorAdmittanceController: Jacobian for joint " + joint_name_ + " does not appear to have a rotation component";
    	throw std::runtime_error(error_message);
    }

    config_->param("dq_throttle_enabled",dq_throttle_enabled_, false);
    dq_max_ = model_->stripBaseVelocity(model_->velocity_max())(joint_idx_);
    config_->getParam("dq_max",dq_max_);
    config_->param("dq_throttle",dq_throttle_,0.8*dq_max_);

    config_->param("command_filter_gain", command_filter_gain_, 0.);
}

ActuatorAdmittanceController::~ActuatorAdmittanceController() {}

void ActuatorAdmittanceController::start(timestamp_t const & timestamp) {
	underlying_controller_->start(timestamp);

	filtered_wrench_ = std::get<2>(sensor_state_->ftsWrenches.at(fts_id_));
    filtered_wrench_.head(3).setZero();
    last_update_ = timestamp;
    integral_store_ = 0.;
    command_filter_state_ = 0.;
}

void ActuatorAdmittanceController::stop(timestamp_t const & timestamp) {
	underlying_controller_->stop(timestamp);	
}

void ActuatorAdmittanceController::update(timestamp_t const & timestamp, double tau_d) {
    double dt = std::chrono::duration<double>(timestamp - last_update_).count();
    last_update_ = timestamp;
    //PBWBC_DEBUG("joint " << joint_name_ << " has joint_axis_ = " << joint_axis_ << " and joint_direction_ = " << joint_direction_);
    //PBWBC_DEBUG("tau_d = " << tau_d);
	
	//tau_d is the desired joint torque (from high level controller)
	//the joint is missing a torque sensor, so we can't have a closed-loop torque controller on this joint
	//we project FTS measurement onto the joint, to simulate a joint torque sensor instead

    if(command_filter_gain_==0.) {
        command_filter_state_ = tau_d;
    } else {   
        command_filter_state_ += command_filter_gain_ * (tau_d - command_filter_state_);
    }

	//Get measured FTS wrench
	measured_wrench_ = std::get<2>(sensor_state_->ftsWrenches.at(fts_id_));
    measured_wrench_.head(3).setZero();
	//PBWBC_DEBUG("measured_wrench_ " << measured_wrench_.transpose());

	//Filter FTS wrench
	// TODO: second order filter here
	filtered_wrench_ += ft_filter_gain_ * (measured_wrench_ - filtered_wrench_);
	//PBWBC_DEBUG("filtered_wrench_ " << filtered_wrench_.transpose());
	
	//Get pose of FTS sensor
	world_H_fts_.setIdentity();
	world_H_fts_.translation() =  model_->bodyTranslation(fts_frame_id_);
	world_H_fts_.linear() = model_->bodyOrientation(fts_frame_id_).toRotationMatrix();

	//Get pose of joint
    world_H_joint_.setIdentity();
    world_H_joint_.translation() = model_->bodyTranslation(joint_frame_id_);
    world_H_joint_.linear() = model_->bodyOrientation(joint_frame_id_).toRotationMatrix();

    // Project FTS forces onto joint
    projected_wrench_ = adjoint(world_H_joint_.inverse() * world_H_fts_).transpose() * filtered_wrench_;
    //PBWBC_DEBUG("projected_wrench = " << projected_wrench_.transpose());

	//need to check which axis of the projected measured wrench is relevant
	//need to check if the direction is inverted

	//select correct direction
	//assume joint rotates around positive z axis
	tau_projected_ = joint_direction_ * projected_wrench_(joint_axis_);
	//PBWBC_DEBUG("tau_projected_ = " << tau_projected_);
    
    // Update measured joint torque (assume joint has no torque sensor
    sensor_state_->jointTorques(joint_idx_) = tau_projected_;

    // interpolate stiffness
    K_ += std::min(std::max(K_desired_ - K_,-0.01),0.01);

	//Compute the joint torque to be applied
	double e = command_filter_state_ - tau_projected_;
    integral_store_ += e * dt;
    integral_store_ = std::max(std::min(integral_store_,integral_windup_),-integral_windup_);
    if(I_==0.) {
        integral_store_ = 0.;
    }

	tau_actuator_d_ = feedforward_gain_ * command_filter_state_ + K_ * e + I_ * integral_store_;
	//PBWBC_DEBUG("tau_actuator_d_ = " << tau_actuator_d_);

    // reduce torque gradually after dq_throttle
   	const int dq_offset = model_->dy_size() - model_->dq_size();
    if(state_->dy(dq_offset+joint_idx_) > dq_throttle_ && tau_actuator_d_ > 0.) {
        double f = (state_->dy(dq_offset+joint_idx_) - dq_throttle_) / (dq_max_ - dq_throttle_);
        tau_actuator_d_ *= f;
    } else if(state_->dy(dq_offset+joint_idx_) < -dq_throttle_ && tau_actuator_d_ < 0.) {
        double f = - (state_->dy(dq_offset+joint_idx_) + dq_throttle_) / (dq_max_ - dq_throttle_);
        tau_actuator_d_ *= f;
    }

	//Send the torque to the underlying controller (the actual controller that sends the torque as control input to the robot)
	underlying_controller_->update(timestamp, tau_actuator_d_);
}


} // namespace pbwbc

