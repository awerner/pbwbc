#include <pbwbc/MotionOrchestratorROS.hpp>

namespace pbwbc {

MotionOrchestratorROS::MotionOrchestratorROS(
    std::shared_ptr<Model> const & model,
    std::shared_ptr<ConfigMap> const & config,
    std::vector<std::shared_ptr<Controller>> const & controllers,
    ros::NodeHandle const & nh) 
: MotionOrchestrator(model,config,controllers),
  nh_(nh) {

    // TODO: implement this
    // add service call to add actions, lock map with mutex
    //
    // service args:
    // - action name
    // - controller
    // - entity
    // - goal
    // - global
    addAction_service_ = nh_.advertiseService("addAction", 
        &MotionOrchestratorROS::callback_AddAction, this);
}

MotionOrchestratorROS::~MotionOrchestratorROS() {}

bool MotionOrchestratorROS::callback_AddAction (
        pbwbc::AddActionRequest & request,
        pbwbc::AddActionResponse & response) {
    
    Action new_action;
    new_action.name = request.name.data;
    new_action.controllerName = request.controller.data;
    new_action.entityName = request.entity.data;
    new_action.destination.resize(request.destination.size());
    std::copy(request.destination.begin(),request.destination.end(),&new_action.destination[0]);
    new_action.absolute = request.absolute;
    new_action.startTime = request.start_time;
    new_action.duration = request.duration;

    // check if the controller / entity combination exists
    if(entity_map_.find(std::make_tuple(new_action.controllerName,new_action.entityName))==entity_map_.end()) {
        std::stringstream str;
        str << "Could not find a controller " << new_action.controllerName << " with entity " 
            << new_action.entityName;
        PBWBC_DEBUG(str.str());
        response.status = -1;
        response.msg.data = str.str();
        return true;
    }
    auto controller = entity_map_.at(std::make_tuple(new_action.controllerName,new_action.entityName));
    Eigen::VectorXd state = controller->getMotionState(new_action.entityName);
    if(state.size() != new_action.destination.size()) {
        std::stringstream str;
        str << "Action destination has wrong size: " << new_action.destination.size() << " but needs "
                << state.size();
        PBWBC_DEBUG(str.str());
        response.status = -2;
        response.msg.data = str.str();
        return true;
    }

    PBWBC_DEBUG("Adding action \"" << new_action.name 
            << "\" for controller \"" << new_action.controllerName
            << "\" for entity \"" << new_action.entityName
            << "\", destination = " << new_action.destination.transpose()
            << ", absolute = " << new_action.absolute
            << ", startTime(relative) = " << new_action.startTime
            << ", duration = " << new_action.duration);
    
    std::unique_lock<std::mutex> lock(action_mutex_);
    new_action.startTime += current_time_;
    actions_.push_back(std::make_shared<Action>(new_action));
    
    response.status = 0;
    return true;
}

}


