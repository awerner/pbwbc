#pragma once
#include <pbwbc/SelfCollisionAvoidanceController.hpp>
#include "pinocchio/multibody/model.hpp"
#include "pinocchio/multibody/data.hpp"
#include "pinocchio/multibody/geometry.hpp"

namespace pbwbc {


class SelfCollisionAvoidanceController::impl {
public:
    std::shared_ptr<pinocchio::GeometryModel> geomModel;
    std::shared_ptr<pinocchio::GeometryData> geomData;

    std::shared_ptr<pinocchio::Model> model;
    std::shared_ptr<pinocchio::Data> data;
};

}


