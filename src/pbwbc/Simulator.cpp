#include <pbwbc/Simulator.hpp>
#include <pbwbc/Algebra.hpp>
#include <pinocchio/algorithm/aba.hpp>

#include <boost/numeric/odeint/stepper/controlled_runge_kutta.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta_dopri5.hpp>
#include <boost/numeric/odeint/stepper/bulirsch_stoer.hpp>
#include <boost/numeric/odeint/stepper/rosenbrock4.hpp>
#include <boost/numeric/odeint/stepper/rosenbrock4_controller.hpp>

#include <boost/numeric/odeint/external/eigen/eigen_algebra.hpp>
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/algorithm/energy.hpp"
#include <pbwbc/ModelPinocchio.hpp>
#include <pbwbc/PinocchioData.hpp>

namespace {
    class reset_request_exception {
        public:
            reset_request_exception(){};
    };
}

namespace pbwbc {

class Simulator::SimulatorDetails {
public:
    typedef Eigen::VectorXd sim_state_t;
    typedef boost::numeric::odeint::runge_kutta_dopri5<
        sim_state_t,double,sim_state_t,double,boost::numeric::odeint::vector_space_algebra> dopri5_stepper_t;
    typedef boost::numeric::odeint::controlled_runge_kutta<dopri5_stepper_t> dopri5_error_stepper_t;
    dopri5_error_stepper_t dopri5_stepper;

    // alternative steppers: (1) bulirsch_stoer
    typedef boost::numeric::odeint::bulirsch_stoer<
        sim_state_t,double,sim_state_t,double,boost::numeric::odeint::vector_space_algebra> bulirsch_stoer_error_stepper_t;
    bulirsch_stoer_error_stepper_t bulirsch_stoer_stepper;

    typedef boost::numeric::odeint::rosenbrock4<double> rosenbrock4_stepper_t;
    typedef boost::numeric::odeint::rosenbrock4_controller<rosenbrock4_stepper_t> rosenbrock4_error_stepper_t;
    rosenbrock4_error_stepper_t rosenbrock4_stepper;

    typedef boost::numeric::odeint::default_error_checker<
        double,boost::numeric::odeint::vector_space_algebra,
        boost::numeric::odeint::default_operations> error_checker_t;
    // see https://www.boost.org/doc/libs/1_63_0/libs/numeric/odeint/doc/html/boost_numeric_odeint/tutorial/harmonic_oscillator.html
    // for parameter description
    SimulatorDetails(double abs_err, double rel_err, double a_x, double a_dxdt) 
      : dopri5_stepper(error_checker_t( abs_err , rel_err , a_x , a_dxdt )),
        rosenbrock4_stepper(abs_err,rel_err) {}

    enum mode {
        dopri5 = 0,
        bulirsch_stoer = 1,
        rosenbrock4 = 2
    };

    void reset() {
        dopri5_stepper.reset();
        bulirsch_stoer_stepper.reset();
    }

    boost::numeric::odeint::controlled_step_result try_step(
            std::function<void(sim_state_t const &,sim_state_t &, double)> const &,
            sim_state_t & sim_state,
            double & t,
            double & micro_step,
            Simulator & sim,
            enum mode m = dopri5 ) {
        switch(m) {
            case dopri5:
                return dopri5_stepper.try_step(
                        [&](sim_state_t const &x,sim_state_t &dx, double t){
                            sim.function(x,dx,t);
                        },
                        sim_state, t, micro_step);
            case bulirsch_stoer:
                return bulirsch_stoer_stepper.try_step(
                        [&](sim_state_t const &x,sim_state_t &dx, double t){
                            sim.function(x,dx,t);
                        },
                        sim_state, t, micro_step);
            case rosenbrock4:
                {
                //TODO: This needs the jacobian of the differential system, see
                // https://www.boost.org/doc/libs/1_71_0/libs/numeric/odeint/doc/html/boost_numeric_odeint/concepts/implicit_system.html
                // use pinocchio autodiff support or numeric differentiation (Simulink ode15s does that)
                copy(sim_state,ublas_sim_state);
                boost::numeric::odeint::controlled_step_result result =
                   rosenbrock4_stepper.try_step(
                        std::make_pair([&](ublas_vector_t const & x,ublas_vector_t &dx, double t){
                            copy(x,eigen_x);
                            sim.function(eigen_x,eigen_dx,t);
                            copy(eigen_dx,dx);
                          },
                          [&](ublas_vector_t const & x, ublas_matrix_t & J, double t, ublas_vector_t & dx ){
                            copy(x,eigen_x);
                            sim.function(eigen_x,eigen_dx,t);
                            sim_state_t k = eigen_dx;
                            copy(eigen_dx,dx);
                            for(std::size_t col=0;col<x.size();col++) {
                                copy(x,eigen_x);
                                const double delta = 1e-8;
                                // perturb
                                eigen_x(col) += delta;
                                // compute
                                sim.function(eigen_x,eigen_dx,t);

                                sim_state_t diff = (eigen_dx - k)/delta;
                                // assign into J
                                copy(diff,ublas_dx);
                                boost::numeric::ublas::matrix_column<ublas_matrix_t>(J,col) = ublas_dx;
                            }
                          }),
                        ublas_sim_state, t, micro_step);
                copy(ublas_sim_state,sim_state);
                return result;
                }
            default:
                throw std::runtime_error("not implemented");
        }
    }
private:
    typedef boost::numeric::ublas::vector< double > ublas_vector_t;
    typedef boost::numeric::ublas::matrix< double > ublas_matrix_t;
    ublas_vector_t ublas_dx;
    ublas_vector_t ublas_sim_state;
    sim_state_t eigen_x;
    sim_state_t eigen_dx;

    void copy(sim_state_t const & in, ublas_vector_t & out) {
        out.resize(in.size());
        for(int idx=0;idx<in.size();idx++) {
            out(idx) = in(idx);
        }
    }
    void copy(ublas_vector_t const & in, sim_state_t & out) {
        out.resize(in.size());
        for(std::size_t idx=0;idx<in.size();idx++) {
            out(idx) = in(idx);
        }
    }
};



Simulator::Simulator(std::shared_ptr<Model> const & model,
        double time_step, double abs_tol, double rel_tol, int solver_type) :
    model_(model),
    time_step_(time_step),
    state_(new RobotState(model->y_size(),model->dy_size())),
    t_(0.),
    micro_step_(time_step),
    solver_type_(solver_type),
    state_filter_constant_(1./time_step),
    damping_(0.),
    tau_(vector_t::Zero(model->dq_size())),
    full_tau_(model_->dy_size()),
    mu_static_(0.5),
    mu_kinetic_(0.5){
    translation_stiffness_.setConstant(1e7);
    translation_damping_.setConstant(1e6);
    minimum_micro_step_ = 1e-8;
    base_wrench_.setZero();
    tau_.setZero();
    sim_ = std::shared_ptr<SimulatorDetails>(new SimulatorDetails(abs_tol,rel_tol,1.0,1.0));
    int idx = 0;
    sim_state_map_[entry_y_state] = std::make_pair(idx,model_->y_size()); idx+=model_->y_size();
    sim_state_map_[entry_dy_state] = std::make_pair(idx,model_->dy_size()); idx+=model_->dy_size();
    sim_state_map_[entry_y_filter_state] = std::make_pair(idx,model_->y_size()); idx+=model_->y_size();
    sim_state_map_[entry_dy_filter_state] = std::make_pair(idx,model_->dy_size()); idx+=model_->dy_size();
}

Simulator::~Simulator() {}

int Simulator::sim_state_size() {
    int idx=0;
    for(auto const & entry : sim_state_map_) {
        idx += entry.second.second;
    }
    return idx;
}

void Simulator::addPointContact(
        std::string const & frame,
        translation_t const & offset) {
    int frame_id = model_->getFrameId(frame);
    //PBWBC_DEBUG("Added contact on frame_id " << frame_id);
    contacts_.push_back(std::make_tuple(frame_id,offset));
    contact_wrenches_.resize(contacts_.size(),wrench_t::Zero());
    contact_binary_state_.push_back(false);
    contact_distance_.push_back(0.);
    sim_state_map_.erase(entry_contact_state);
    sim_state_map_[entry_contact_state] = std::make_pair(
            sim_state_size(),contacts_.size()*2);
}

void Simulator::reset() {
    t_ = 0.;
    step_count_ = 0;
    tried_step_count_ = 0;
    micro_step_ = time_step_;
    sim_->reset();
    reset_request_ = 0;
    for(auto & c : contact_wrenches_) c.setZero();
    for(auto & c : contact_binary_state_) c = false;
    for(auto & c : contact_distance_) c = 0.;
    //std::cout << "sim_state setup" << std::endl;
    //for(auto const & e : sim_state_map_) {
    //    std::cout << static_cast<int>(e.first) << " " << e.second.first << " - " << e.second.first + e.second.second << std::endl;
    //}
    sim_state_.resize(sim_state_size());
    sim_state_.segment(sim_state_map_.at(entry_y_state).first, sim_state_map_.at(entry_y_state).second) = state_->y;
    sim_state_.segment(sim_state_map_.at(entry_dy_state).first, sim_state_map_.at(entry_dy_state).second) = state_->dy;
    sim_state_.segment(sim_state_map_.at(entry_y_filter_state).first, sim_state_map_.at(entry_y_filter_state).second) = state_->y;
    sim_state_.segment(sim_state_map_.at(entry_dy_filter_state).first, sim_state_map_.at(entry_dy_filter_state).second) = state_->dy;
    if(sim_state_map_.count(entry_contact_state)) {
        sim_state_.segment(sim_state_map_.at(entry_contact_state).first, sim_state_map_.at(entry_contact_state).second).setZero();
    }
}

void Simulator::update(timestamp_t const &) {
    // TODO: use timestamp here to compute step
    double t_1 = t_ + time_step_;
    micro_step_ = std::min(micro_step_,time_step_);
    const static Eigen::IOFormat CSVFormat(Eigen::FullPrecision, Eigen::DontAlignCols, ",", "\n");
    while(true) {
        //std::cout << "trying a step of: " << micro_step_ << std::endl;
        double current_t = t_;
        double current_micro_step = micro_step_;
        //std::cout << sim_state << std::endl;
        boost::numeric::odeint::controlled_step_result result;
        try {
            tried_step_count_++;
            result = sim_->try_step(
                    [&](sim_state_t const &x,sim_state_t &dx, double t){
                        this->function(x,dx,t);
                    },
                    sim_state_,
                    t_,
                    micro_step_,
                    *this,
                    static_cast<enum SimulatorDetails::mode>(solver_type_));
        } catch ( reset_request_exception const & e ) {
            // reset step size and solver state
            micro_step_ = 1e-8;
            sim_->reset();
            continue;
        }
        if(result==boost::numeric::odeint::success) {
            step_count_++;
            reset_request_ = 0;
            for(std::size_t contact_idx=0;contact_idx<contact_binary_state_.size();contact_idx++) {
                contact_binary_state_.at(contact_idx) = contact_distance_.at(contact_idx)>0.;
            }
            if(logfile_) {
                *logfile_ << 
                    current_t << "," << current_micro_step << "," << 
                    sim_state_.segment(sim_state_map_.at(entry_y_state).first, sim_state_map_.at(entry_y_state).second).transpose().format(CSVFormat)
                    << "," <<
                    sim_state_.segment(sim_state_map_.at(entry_dy_state).first, sim_state_map_.at(entry_dy_state).second).transpose().format(CSVFormat);
                for(std::size_t contact_idx=0;contact_idx<contacts_.size();contact_idx++) {
                    // TODO: report single contact frames only once, add contact_wrenches
                    *logfile_ << "," << contact_wrenches_.at(contact_idx).head(3).transpose().format(CSVFormat);
                }
                *logfile_ << std::endl;
            }
            Eigen::Quaterniond q(sim_state_(6),sim_state_(3),sim_state_(4),sim_state_(5));
            if(abs(q.norm()-1.) > 1e-5) { // TODO: what threshold?
                std::cout << "quaternion norm before normalization: "<< q.norm() << std::endl;
                sim_state_.segment(3,4) = q.normalized().coeffs();
            }

            if(t_ >= (t_1 - std::numeric_limits<double>::epsilon())) {
                break;
            }
            if(t_ + micro_step_ >= t_1) {
                micro_step_ = t_1 - t_; // stop right at next time step
                //std::cout << "satu" << std::endl;
            }
        }
        //micro_step_ = std::max(micro_step_,minimum_micro_step_);
    }
    state_->y = sim_state_.segment(sim_state_map_.at(entry_y_filter_state).first, sim_state_map_.at(entry_y_filter_state).second);
    state_->dy = sim_state_.segment(sim_state_map_.at(entry_dy_filter_state).first, sim_state_map_.at(entry_dy_filter_state).second);
    //std::cout << "done" << std::endl;
}

std::shared_ptr<RobotState> const & Simulator::state() {
    return state_;
}

void Simulator::setTau(vector_t const & tau) {
    assert(tau_.size()==tau.size());
    tau_ = tau;
}



void Simulator::contact_model(
        wrench_t & wrench, vector2_t & dcontact_state,
        double normal_position, double normal_velocity,
        Eigen::Ref<const vector2_t> const & tangential_velocity,
        Eigen::Ref<const vector2_t> const & contact_state) const {
    wrench.segment(3,3).setZero();
    double sigma = std::sqrt(std::max(0.,-normal_position));
    wrench(2) =
        - translation_stiffness_(2) * sigma * normal_position
        - translation_damping_(2) * sigma * normal_velocity;
    // no positive contact force => no contact
    if (sigma<=0. || wrench(2)<=0.) {
        wrench.setZero();
        dcontact_state.array() = 
            - translation_stiffness_.head(2).array() 
            / translation_damping_.head(2).array()
            * contact_state.array();
        return;
    }

    // in contact
    dcontact_state = tangential_velocity;
   
    // compute tangential force for static contact
    wrench.segment(0,2).array() = 
        - translation_stiffness_.head(2).array() * sigma * contact_state.array()
        - translation_damping_.head(2).array() * sigma * dcontact_state.array();

    if(wrench.segment(0,2).norm() > mu_static_ * wrench(2) ) {
        // slipping contact
        wrench.segment(0,2) *= 
            mu_kinetic_ * wrench(2) / wrench.segment(0,2).norm();
        dcontact_state.array() =
            - wrench.segment(0,2).array() / sigma
            - translation_stiffness_.head(2).array() * contact_state.array();
        dcontact_state.array() /= translation_damping_.head(2).array();
    }
}


void Simulator::function(sim_state_t const & x, sim_state_t & dxdt, double t) {
    const int q_offset = model_->y_size() - model_->q_size();
    const int dq_offset = model_->dy_size() - model_->dq_size();
    
    auto & y = x.segment(sim_state_map_.at(entry_y_state).first, sim_state_map_.at(entry_y_state).second);
    auto & dy = x.segment(sim_state_map_.at(entry_dy_state).first, sim_state_map_.at(entry_dy_state).second);
    auto & y_filter = x.segment(sim_state_map_.at(entry_y_filter_state).first, sim_state_map_.at(entry_y_filter_state).second);
    auto & dy_filter = x.segment(sim_state_map_.at(entry_dy_filter_state).first, sim_state_map_.at(entry_dy_filter_state).second);
    
    dxdt.resize(x.size());
    
    full_tau_.segment(0,dq_offset) = base_wrench_;
    full_tau_.segment(dq_offset,model_->dq_size()) = tau_;
   
    // add contact forces here
    // update kinematics & jacobians
    std::shared_ptr<ModelPinocchio> pmodel = std::dynamic_pointer_cast<ModelPinocchio>(model_);
    auto & pinocchio_model = *pmodel->pinocchio_data()->model_;
    auto & pinocchio_data = *pmodel->pinocchio_data()->data_;
    pinocchio::forwardKinematics(pinocchio_model,pinocchio_data,y); // TODO: Maybe redundant
    pinocchio::computeJointJacobians(pinocchio_model,pinocchio_data,y);
    pinocchio::framesForwardKinematics(pinocchio_model,pinocchio_data,y);
    pinocchio::updateFramePlacements(pinocchio_model,pinocchio_data);

    if(sim_state_map_.count(entry_contact_state)) {
        Eigen::MatrixXd contact_frame_jacobian(6,model_->dy_size()); // TODO: preallocate
        Eigen::MatrixXd contact_point_jacobian(3,model_->dy_size());
        int contact_idx = 0;
        int contact_state_idx = sim_state_map_.at(entry_contact_state).first;
        std::vector<int> contact_binary_state(contact_binary_state_.size());
        for(auto const & contact : contacts_) {
            double normal_position = 
                pinocchio_data.oMf.at(std::get<0>(contact)).translation()(2)
                + (pinocchio_data.oMf.at(std::get<0>(contact)).rotation() * std::get<1>(contact))(2);
            wrench_t wrench;
            wrench.setZero();
            vector2_t dcontact_state;
            dcontact_state.setZero();

            // compute contact point quantities
            contact_frame_jacobian.setZero();
            pinocchio::getFrameJacobian(
                pinocchio_model,
                pinocchio_data,
                std::get<0>(contact),
                pinocchio::WORLD,
                contact_frame_jacobian);
            //std::cout << " =========== contact " << contact_idx << " ==============" << std::endl;
            Eigen::Transform<double,3,Eigen::Isometry> trafo;
            trafo.setIdentity();
            trafo.translation() = 
                pinocchio_data.oMf.at(std::get<0>(contact)).translation()
                + (pinocchio_data.oMf.at(std::get<0>(contact)).rotation() * std::get<1>(contact));
            trafo.linear() = 
                pinocchio_data.oMf.at(std::get<0>(contact)).rotation();
            //std::cout << "contact frame trafo\n" << trafo.matrix() << std::endl;
            //std::cout << "contact_frame_jacobian\n" << (contact_frame_jacobian) << std::endl;
            Eigen::Transform<double,3,Eigen::Isometry> offset_trafo;
            offset_trafo.setIdentity();
            offset_trafo.translation() = trafo.linear() * std::get<1>(contact);
            auto offset_adjoint = adjoint(offset_trafo.inverse()); // this adjoint accounts for the contact point offset
            contact_point_jacobian = (offset_adjoint * contact_frame_jacobian).topRows<3>();
            //std::cout << "contact_point_jacobian\n" << contact_point_jacobian << std::endl;

            double normal_velocity = contact_point_jacobian.middleRows<1>(2) * dy;
            vector2_t tangential_velocity = contact_point_jacobian.middleRows<2>(0) * dy;
            // compute contact model
            contact_model(wrench,
                    dcontact_state,
                    normal_position, normal_velocity,
                    tangential_velocity, x.segment(contact_state_idx,2));
            // project forces into configuration space
            full_tau_ += contact_point_jacobian.transpose() * wrench.head(3);
            //std::cout << "wrench\n" << wrench.head(3).transpose() << std::endl;

            dxdt.segment(contact_state_idx,2) = dcontact_state;
            //if(contact_wrenches_.at(contact_idx)(2)==0. && wrench(2)!=0.) {
            //    std::cout << "contact_activated: " << contact_idx << std::endl;
            //}
            //if(contact_wrenches_.at(contact_idx)(2)!=0. && wrench(2)==0.) {
            //    std::cout << "contact deactivated: " << contact_idx << std::endl;
            //}
            contact_binary_state.at(contact_idx) = normal_position > 0.;
            contact_distance_.at(contact_idx) = normal_position;
            contact_wrenches_.at(contact_idx) = wrench;
            contact_idx++;
            contact_state_idx += 2;
        }
        bool reset_request_local = false;
        for(std::size_t contact_idx=0;contact_idx<contact_binary_state_.size();contact_idx++) {
            if(contact_binary_state_.at(contact_idx)==0 && contact_binary_state.at(contact_idx)==1) {
                reset_request_local = true;
            }
        }
        if(reset_request_==0 && reset_request_local) {
            reset_request_=1;
            std::cout << "r" << std::flush;
        }
    }

    pinocchio::aba(*pmodel->pinocchio_data()->model_,
                   *pmodel->pinocchio_data()->data_,
                   y,
                   dy,
                   full_tau_);
   
    typedef Eigen::Ref<Eigen::VectorXd> ref_t;
    ref_t y_dt = dxdt.segment(sim_state_map_.at(entry_y_state).first, sim_state_map_.at(entry_y_state).second);
    ref_t dy_dt = dxdt.segment(sim_state_map_.at(entry_dy_state).first, sim_state_map_.at(entry_dy_state).second);
    ref_t y_filter_dt = dxdt.segment(sim_state_map_.at(entry_y_filter_state).first, sim_state_map_.at(entry_y_filter_state).second);
    ref_t dy_filter_dt = dxdt.segment(sim_state_map_.at(entry_dy_filter_state).first, sim_state_map_.at(entry_dy_filter_state).second);
    Eigen::Quaterniond q(y(6),y(3),y(4),y(5));
    // base position translation: velocity given in local frame, position state is in inertial frame
    y_dt.segment(0,3) = q.toRotationMatrix() * dy.segment(0,3); // base pos trans
    
    // base position orientation
    Eigen::Quaterniond v(0.,dy(3),dy(4),dy(5)); // TODO: check which frame
    y_dt.segment(3,4) = 0.5 * (q * v).coeffs();

    // joint positions
    y_dt.segment(q_offset,model_->dq_size()) = dy.segment(dq_offset,model_->dq_size());

    // base + joint velocities
    // TODO: use pinocchio functions for this
    // base velocity translation
    dy_dt.segment(0,3) = /*q.toRotationMatrix() */
        pmodel->pinocchio_data()->data_->ddq.segment(0,3);
    // base angular velocities
    dy_dt.segment(3,3) = 
        pmodel->pinocchio_data()->data_->ddq.segment(3,3);
    // joint velocities
    dy_dt.segment(dq_offset,model_->dq_size()) = 
        pmodel->pinocchio_data()->data_->ddq.segment(dq_offset,model_->dq_size());

    // position filter states
    // TODO: fix base orientation
    y_filter_dt = state_filter_constant_ * (y - y_filter);

    dy_filter_dt = state_filter_constant_ * (dy - dy_filter);

    // damping    
    dy_dt -= damping_ * dy; // TODO: scaling damping with inertia
    //for(int idx=0;idx<dxdt.size();idx++) {
    //    std::cerr << idx << std::endl;
    //    std::cerr << dxdt(idx) << std::endl;
    //}
    //std::cout << "E = " << pinocchio::kineticEnergy(pinocchio_model,pinocchio_data,y,dy) + 
    //    pinocchio::potentialEnergy(pinocchio_model,pinocchio_data,y) << std::endl;
    
    if(reset_request_==1) {
        reset_request_=2;
        //throw reset_request_exception();
    }
}

double Simulator::time() {
    return t_;
}

void Simulator::set_contact_stiffness(translation_t const & x) {
    translation_stiffness_ = x;
}

void Simulator::set_contact_damping(translation_t const & x) {
    translation_damping_ = x;
}

void Simulator::setBaseWrench(wrench_t const & wrench) {
    base_wrench_ = wrench;
}
    
void Simulator::enable_log(std::string const & filename) {
    logfile_ = std::shared_ptr<std::ofstream>(new std::ofstream(filename));
    logfile_->setf( std::ios::fixed, std:: ios::floatfield );
    logfile_->precision(14);
    *logfile_ << "time,step,base_x,base_y,base_z,base_q0,base_q1,base_q2,base_qw";
    for(auto const & n : model_->jointNames()) {
        if(n == "universe" || n == "root_joint") continue;
        *logfile_ << "," << n;
    }
    *logfile_ << ",base_dx,base_dy,base_dz,base_ax,base_ay,base_az";
    for(auto const & n : model_->jointNames()) {
        if(n == "universe" || n == "root_joint") continue;
        *logfile_ << ",d" << n;
    }
    std::shared_ptr<ModelPinocchio> pmodel = std::dynamic_pointer_cast<ModelPinocchio>(model_);
    auto & pinocchio_model = *pmodel->pinocchio_data()->model_;
    std::map<std::string,int> frame_count;
    for(std::size_t idx=0;idx<contacts_.size();idx++) {
        auto frame_name = pinocchio_model.frames.at(std::get<0>(contacts_.at(idx))).name;
        if(frame_count.count(frame_name)==0) {
            frame_count[frame_name] = 0;
        } else {
            frame_count[frame_name]++;
        }
        for( auto s : {"_Fx","_Fy","_Fz"}) {
            std::stringstream str;
            str << frame_name << "_" << frame_count[frame_name] << s;
            *logfile_ << "," << str.str();
        }
    }
    *logfile_ << std::endl;
}

Simulator::wrench_t Simulator::contact_wrench(int contact_idx) {
    return contact_wrenches_.at(contact_idx);
}

void Simulator::set_state_filter_constant(double x) { 
    state_filter_constant_ = x;
}

void Simulator::set_damping(double x) { 
    damping_ = x;
}

int Simulator::step_count() { 
    return step_count_;
}

int Simulator::tried_step_count() { 
    return tried_step_count_;
}

} // namespace
