#include <pbwbc/CartesianImpedanceControllerROS.hpp>

namespace pbwbc {

CartesianImpedanceControllerROS::CartesianImpedanceControllerROS(
        std::shared_ptr<RobotState> const & state,
        std::shared_ptr<Model> const & model,
        std::shared_ptr<ConfigMap> const & config,
        ros::NodeHandle const & nh
        ) :
    CartesianImpedanceController(state,model,config),
        nh_(nh) {
        init();
}


void CartesianImpedanceControllerROS::init() {
        desired_state_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::StateStamped>(nh_,"desired_state",1000,10);
        current_state_publisher_ = RealTimePublisherBackEnd::advertise<pbwbc::StateStamped>(nh_,"current_state",1000,10);
        wrench_publisher_ = RealTimePublisherBackEnd::advertise<geometry_msgs::WrenchStamped>(nh_,"wrench",1000,10);
        wrench_publisher_.msg().header.frame_id = name();
        wrench_publisher_.fillBuffer();

        ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure(nh_));
        for(int idx = 0; idx < K_.diagonal().size() ; idx++) {
            std::stringstream str_stiffness; str_stiffness << "stiffness_" << idx;
            ddr_->RegisterVariable(&K_desired_.diagonal()(idx), str_stiffness.str(), 0., 1000.);

            if(damping_mode_==damping_mode_manual) {
                std::stringstream str_damping; str_damping << "damping_" << idx;
                ddr_->RegisterVariable(&D_.diagonal()(idx), str_damping.str(), 0., 300.);
            } else if(damping_mode_==damping_mode_double_diagonalization) {
                std::stringstream str_damping_ratio; str_damping_ratio << "damping_ratio_" << idx;
                ddr_->RegisterVariable(&xi_.diagonal()(idx), str_damping_ratio.str(), 0., 1.);
            }            
        }
        for(int idx = 0; idx < 3; idx++) {
                std::stringstream str_position; str_position << "pose_" << idx;
                ddr_->RegisterVariable(&state_translation_offset_d_(idx), str_position.str(), -3., 3.);
        }
        for(int idx = 0; idx < 3; idx++) {
                std::stringstream str_orientation; str_orientation << "pose_" << idx+3;
                ddr_->RegisterVariable(&state_orientation_offset_euler_d_(idx), str_orientation.str(), -3.14, 3.14);
        }
        if(stiffness_mode_==stiffness_saturated) {
            ddr_->RegisterVariable(&maximum_force_, "maximum_force", 0., 200.);
            ddr_->RegisterVariable(&maximum_torque_, "maximum_torque", 0., 50.);
        }
        ddr_->RegisterVariable(&update_pose_, "update_pose", 0., 1.);
        ddr_->RegisterVariable(&stiffness_step_max_, "stiffness_step_max", 0., 20.);
        ddr_->RegisterVariable(&feedforward_gain_, "feedforward_gain", 0., 1.);

        ddr_->PublishServicesTopics();
}

CartesianImpedanceControllerROS::CartesianImpedanceControllerROS(
        CartesianImpedanceController const & x,
        ros::NodeHandle const & nh
        ) :
    CartesianImpedanceController(x),
    nh_(nh) {
        init();
}

CartesianImpedanceControllerROS::~CartesianImpedanceControllerROS() {}

void CartesianImpedanceControllerROS::update(timestamp_t const & timestamp) {
    ros::Time now(timestamp.time_since_epoch().count());
    CartesianImpedanceController::update(timestamp);
    // publish states
    if(desired_state_publisher_.trylock()) {
        desired_state_publisher_.msg().header.stamp = now;
        assign(desired_state_publisher_.msg().transform,state_d_.position.translation,state_d_.position.orientation);
        assign(desired_state_publisher_.msg().twist,state_d_.velocity.translation,state_d_.velocity.orientation);
        desired_state_publisher_.unlockAndPublish();
    }
    if(current_state_publisher_.trylock()) {
        current_state_publisher_.msg().header.stamp = now;
        assign(current_state_publisher_.msg().transform,cartesian_state_.position.translation,cartesian_state_.position.orientation);
        assign(current_state_publisher_.msg().twist,cartesian_state_.velocity.translation,cartesian_state_.velocity.orientation);
        current_state_publisher_.unlockAndPublish();
    }
    if(wrench_publisher_.trylock()) {
        wrench_publisher_.msg().header.stamp = now;
        assign(wrench_publisher_.msg().wrench,wrench_);
        wrench_publisher_.unlockAndPublish();
    }
}

} // namespace pbwbc
