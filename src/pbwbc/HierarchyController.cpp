#include <pbwbc/HierarchyController.hpp>


namespace pbwbc { 

HierarchyController::HierarchyController(
    std::shared_ptr<Model> const & model,
    std::vector<std::shared_ptr<Controller>> const & controllers
        ) :
    Controller("HierarchyController",model->dq_size()),
    model_(model),
    controllers_(controllers),
    lu_(model->dy_size())
{
    AWIAT_.resize(model_->dy_size(),model_->dy_size());
    nullspace_projector_.resize(model_->dy_size(),model_->dy_size());
    new_nullspace_projector_.resize(model_->dy_size(),model_->dy_size());
    weighted_pseudoinverse_.resize(model_->dy_size(),model_->dy_size());
    minv_.resize(model_->dy_size(),model_->dy_size());
    for(auto const & controller : controllers_) {
        if(!controller->hasTaskJacobian()) {
            throw std::runtime_error("Controller has no task jacobian");
        }
    }
}

HierarchyController::~HierarchyController(){};

void HierarchyController::start(timestamp_t const & timestamp) {
    for(auto const & controller : controllers_)controller->start(timestamp);
}

void HierarchyController::stop(timestamp_t const & timestamp) {
    for(auto const & controller : controllers_)controller->stop(timestamp);
}

void HierarchyController::update(timestamp_t const & timestamp) {
    nullspace_projector_.setIdentity();
    tau_d_.setZero();
    // TODO: computation time for this is too long (0.9ms)
    //       if the nullspace projecters are saved, this can
    //       be cut down
    // probably updating the projectors at a lower rate would by fine
    // would cache the nullspace projectors with double buffering
    //
    // Reference on NullSpace projections
    // https://journals.sagepub.com/doi/pdf/10.1177/0278364914566516
    // TODO: this is probably not valid for floating base systems
    minv_.setIdentity();
    for(std::size_t controller_idx=0;controller_idx<controllers_.size();controller_idx++) {
        auto const & controller = controllers_.at(controller_idx);
        controller->update(timestamp);
        //PBWBC_DEBUG("torque from controller\n" << controller->get_tau().transpose());
        //PBWBC_DEBUG("projected torque\n" << (nullspace_projector_.bottomRightCorner(model_->dq_size(),model_->dq_size()) * controller->get_tau()).transpose());

        tau_d_ += nullspace_projector_.bottomRightCorner(model_->dq_size(),model_->dq_size())
                         * controller->get_tau();

        // don't compute the nullspace projector for the last controller
        if(controller_idx+1==controllers_.size()) break;
        // update nullspace projector
        // A^W+ = W^-1 * A^T * ( A * W^-1 * A^T )^-1
        //PBWBC_DEBUG("task jacobian\n" << controller->getTaskJacobian());
        AWIAT_ = controller->getTaskJacobian() * minv_ * controller->getTaskJacobian().transpose();
        lu_.compute(AWIAT_);
        weighted_pseudoinverse_ = minv_ * controller->getTaskJacobian().transpose() * lu_.inverse();
        new_nullspace_projector_ = Eigen::MatrixXd::Identity(model_->dy_size(),model_->dy_size())
            - controller->getTaskJacobian().transpose() * weighted_pseudoinverse_.transpose();
        nullspace_projector_ = nullspace_projector_ * new_nullspace_projector_;
        //PBWBC_DEBUG("new_nullspace_projector\n" << new_nullspace_projector_);
    }
}

} // namespace

