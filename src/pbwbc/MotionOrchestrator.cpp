#include <pbwbc/MotionOrchestrator.hpp>

namespace pbwbc {

MotionOrchestrator::MotionOrchestrator(
        std::shared_ptr<Model> const & model,
        std::shared_ptr<ConfigMap> const & config,
        std::vector<std::shared_ptr<Controller>> const & controllers)
    : model_(model),
      config_(config) {
    for(auto const & controller : controllers) {
        std::string const & controller_name = controller->name();
        for(auto const & entity_name : controller->motionEntities()) {
            entity_map_.insert(
                    {std::make_tuple(controller_name,entity_name),controller});
            PBWBC_DEBUG("Adding controllable entity in controller " 
                    << controller_name << " with name " << entity_name);
        }
        auto controller_config = config_->submap("Controllers")->submap(controller->name());
        //Read actions
        std::vector<std::string> action_names;
        controller_config->list_items("Actions", action_names);
        for(auto const & action_name : action_names) {
            PBWBC_DEBUG("Adding action " << action_name);
            auto action_config = controller_config->submap("Actions")->submap(action_name);
            Action new_action;
            new_action.controllerName = controller_name;
            //Get action parameters
            if(!action_config->getParam("entityName", new_action.entityName)) {
                throw std::runtime_error("MultiContactForceDistribution: Actions: parameter 'entityName' not found");
            }
            if(!action_config->getParam("destination", new_action.destination)) {
                throw std::runtime_error("MultiContactForceDistribution: Actions: parameter 'destination' not found");
            }
            if(!action_config->getParam("start_time", new_action.startTime)) {
                throw std::runtime_error("MultiContactForceDistribution: Actions: parameter 'start_time' not found");
            }
            if(!action_config->getParam("duration", new_action.duration)) {
                throw std::runtime_error("MultiContactForceDistribution: Actions: parameter 'duration' not found");
            }
            new_action.name = action_name;
            action_config->param("absolute", new_action.absolute, false);
            PBWBC_DEBUG("Adding action \"" << new_action.name 
            << "\" for controller \"" << new_action.controllerName
            << "\" for entity \"" << new_action.entityName
            << "\", destination = " << new_action.destination.transpose()
            << ", absolute = " << new_action.absolute
            << ", startTime = " << new_action.startTime
            << ", duration = " << new_action.duration);
            actions_.push_back(std::make_shared<Action>(new_action));
        }
    }
}

MotionOrchestrator::~MotionOrchestrator() {}


void MotionOrchestrator::start(timestamp_t const & timestamp) {
    start_time_ = timestamp;
    current_time_ = std::chrono::duration<double>(timestamp - start_time_).count();
    actions_.clear();
}

void MotionOrchestrator::stop(timestamp_t const & timestamp) {}


void MotionOrchestrator::update(timestamp_t const & timestamp) {
    // do not block on mutex in RT thread, rather skip this update
    if(action_mutex_.try_lock()) {
        current_time_ = std::chrono::duration<double>(timestamp - start_time_).count();
        for (auto itr = actions_.begin();itr!=actions_.end();) {
            auto action = *itr;
            //Check if an action needs to be started
            if ((current_time_ >= action->startTime)){
                try {
                    auto controller = entity_map_.at(std::make_tuple(action->controllerName,action->entityName));
                    PBWBC_DEBUG("Action " << action->name << " is being executed now");
                    // TODO: malloc
                    Eigen::VectorXd state = controller->getMotionState(action->entityName);
                    
                    if(state.size() != action->destination.size()) {
                        PBWBC_DEBUG("Action destination has wrong size: " << action->destination.size() << " but needs "
                                << state.size());
                    }
                    controller->startMotion(timestamp, action->entityName, action->destination,
                            action->duration, action->absolute);
                    itr = actions_.erase(itr);
                } catch (std::exception const & ex) {
                    PBWBC_DEBUG("Exception in MotionOrchestrator::update: " << ex.what() << " on running action " << action->name);
                    itr = actions_.erase(itr);
                }
                
            } else {
                itr++;
            }
        }
        action_mutex_.unlock();
    }
}

} // namespace
