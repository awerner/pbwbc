#include <pbwbc/JointImpedanceController.hpp>

namespace pbwbc {

JointImpedanceController::JointImpedanceController(
		std::shared_ptr<RobotState> const & state,
		std::shared_ptr<Model> const & model,
		std::shared_ptr<ConfigMap> const & config
		) :
Controller([&](){
	std::string name;
	config->param("name",name,"JointImpedanceController");
	return name;}(),model->dq_size()),
state_(state),
model_(model),
config_(config),
position_min_(model->stripBasePosition(model_->position_min())),
position_max_(model->stripBasePosition(model_->position_max())),
maximum_torque_(model->stripBaseVelocity(model_->torque_max())),
gravity_compensation_gain_(1.),
nominal_stiffness_desired_(model_->dq_size()),
nominal_stiffness_(model_->dq_size()),
damping_value_(model_->dq_size()),
damping_ratio_(model_->dq_size()),
integral_gain_(model_->dq_size()),
integral_windup_(model_->dq_size()),
local_stiffness_(model_->dq_size()),
integrator_state_(model_->dq_size()),
state_d_(model->y_size(),model->dy_size()),
goal_state_(model->y_size(),model->dy_size()),
update_pose_manually_(false)
{

	// Get settings from configuration
	// JointImpedanceControllerROS the add dynamic reconfigure functionality
	//
	// get global default setting
	// get stiffness
	double nominal_stiffness;
	config_->param("nominal_stiffness",nominal_stiffness,0.);
	PBWBC_DEBUG("nominal_stiffness: " << nominal_stiffness);
	nominal_stiffness_desired_.setConstant(nominal_stiffness);
	nominal_stiffness_.setConstant(nominal_stiffness);
	// get stiffness mode
	std::string stiffness_mode;
	config_->param("stiffness_mode",stiffness_mode,"saturated");
	if(stiffness_mode=="linear"){
		stiffness_mode_ = stiffness_linear;
	}else if(stiffness_mode=="saturated") {
		stiffness_mode_ = stiffness_saturated;
	} else {
		throw std::runtime_error("Stiffness mode not supported");
	}
	PBWBC_DEBUG("stiffness_mode: " << stiffness_mode);
	config_->param("stiffness_step_max",stiffness_step_max_,1.);
	PBWBC_DEBUG("stiffness_step_max: " << stiffness_step_max_);
	// get maximum effort
	double maximum_torque;
	config_->param("maximum_torque",maximum_torque,15.);
	maximum_torque_.setConstant(maximum_torque);
	PBWBC_DEBUG("maximum_torque: " << maximum_torque);

	double integral_gain;
	config_->param("integral_gain",integral_gain,0.);
	integral_gain_.setConstant(integral_gain);
	PBWBC_DEBUG("integral_gain: " << integral_gain);

	config_->param("gravity_compensation_gain", gravity_compensation_gain_, 0.);

	config_->param("feedforward_gain", feedforward_gain_, 1.);
	// get damping mode
	std::string damping_mode;
	config_->param("damping_mode",damping_mode,"ratio");
	if(damping_mode=="value"){
		damping_mode_ = damping_value;
	}else if(damping_mode=="ratio") {
		damping_mode_ = damping_ratio;
	}else if(damping_mode=="double_diagonalization") {
		damping_mode_ = damping_double_diagonalization;
	} else {
		throw std::runtime_error("Damping mode not supported");
	}
	PBWBC_DEBUG("damping_mode: " << damping_mode);
	// get damping value
	double damping_value;
	config_->param("damping_value",damping_value,1.);
	damping_value_.setConstant(damping_value);
	PBWBC_DEBUG("damping_value: " << damping_value);
	// get damping ratio
	double damping_ratio;
	config_->param("damping_ratio",damping_ratio,1.);
	damping_ratio_.setConstant(damping_ratio);
	PBWBC_DEBUG("damping_ratio: " << damping_ratio);

	//
	// get stiffness adaptation rate
	// gravity compensation on?
	//
	//
	//

	//Get ids of impedance controlled joints
	if(!config->getParam("impedance_controlled_joint_names", impedance_controlled_joint_names_)) {
		throw std::runtime_error("impedance_controlled_joint_names tag not found");
	}
	std::vector<std::string> jointNames(model_->jointNames());
	for (std::string joint_name : impedance_controlled_joint_names_)
	{
		//PBWBC_DEBUG("Looking for joint: " << joint_name);
		std::vector<std::string>::iterator it;
		it = find (jointNames.begin(), jointNames.end(), joint_name);
		if (it != jointNames.end()) {
			int idx = std::distance(jointNames.begin(), it);
			impedance_controlled_joints_ids_.push_back(idx); // Get index of element from iterator
			//PBWBC_DEBUG("Added joint: " << joint_name);

			jointID_map_.insert({joint_name, idx});

			PBWBC_DEBUG("Getting settings for joint " << joint_name);
			config_->param("joints/"+joint_name+"/nominal_stiffness",
							nominal_stiffness_desired_(idx),
							nominal_stiffness_desired_(idx));
			PBWBC_DEBUG("nominal_stiffness: " << nominal_stiffness_desired_(idx));
	        nominal_stiffness_(idx) = nominal_stiffness_desired_(idx);
			config_->param("joints/"+joint_name+"/maximum_torque",
							maximum_torque_(idx),
							maximum_torque_(idx));
			config_->param("joints/"+joint_name+"/damping_ratio",
							damping_ratio_(idx),
							damping_ratio_(idx));
		}
	}
	impedance_controlled_joint_names_.push_back("all");

	task_jacobian_.resize(impedance_controlled_joints_ids_.size(),model_->dy_size());
	task_jacobian_.setZero();
	for(std::size_t idx=0; idx<impedance_controlled_joints_ids_.size();idx++) {
		task_jacobian_(idx,6+impedance_controlled_joints_ids_[idx]) = 1.;
	}

	integral_windup_.setZero();

	auto trajectory_generator_config = config_->submap("TrajectoryGenerator");
	trajectory_generator_.reset( new JointTrajectoryGenerator(
													model_,
													trajectory_generator_config) );

	q_offset_ = model_->y_size() - model_->q_size();
	dq_offset_ = model_->dy_size() - model_->dq_size();
}

JointImpedanceController::~JointImpedanceController() {}

void JointImpedanceController::start(timestamp_t const & timestamp) {
	state_d_ = *state_;
	state_d_.dy.setZero();
	state_d_.ddy.setZero();
	integrator_state_.setZero();

	goal_state_.y = state_d_.y;
	goal_state_.dy.setZero();
	goal_state_.ddy.setZero();

	trajectory_generator_->start(timestamp);
	trajectory_generator_->setInitialState(state_d_);
}

void JointImpedanceController::stop(timestamp_t const & timestamp) {
	trajectory_generator_->stop(timestamp);
}
		

void JointImpedanceController::update(timestamp_t const & timestamp) {
	tau_d_.setZero();

	if (update_pose_manually_){
		//this is make sure that the controller does not jump when setting manual update off:
        trajectory_generator_->startMotion(timestamp, state_d_, goal_state_, 0.1); 
        //this is to actually move the joint to its manually specified location:
        state_d_ = goal_state_;
    } else {
    	trajectory_generator_->update(timestamp);
    	state_d_ = trajectory_generator_->getState();
    }

    // interpolate stiffness
    nominal_stiffness_.array() += (nominal_stiffness_desired_ - nominal_stiffness_).array().
        max(-stiffness_step_max_).min(stiffness_step_max_);

	// Compute stiffness and torque 
	for (int idx : impedance_controlled_joints_ids_) {
		const double e = state_d_.y(q_offset_+idx) - state_->y(q_offset_+idx);

		if ( stiffness_mode_ == stiffness_linear ) {
			local_stiffness_(idx) = nominal_stiffness_(idx);
			tau_d_(idx) += nominal_stiffness_(idx) * e;
		} else if ( stiffness_mode_ == stiffness_saturated ) {
			const double p1 = e * nominal_stiffness_(idx) / maximum_torque_(idx);
			local_stiffness_(idx) = std::pow( 1. / std::max(0.1, cosh(p1)),2.) * nominal_stiffness_(idx); // TODO: fix constants
			local_stiffness_(idx) = std::max(1e-4/*std::numeric_limits<double>::epsilon()*/,local_stiffness_(idx));
			tau_d_(idx) += tanh(p1) * maximum_torque_(idx);
			// ensure K is strictly positive
		}
		// integral gain
		integrator_state_(idx) += 1e-3 * e; // TODO: fix timestep
		// apply anti windup
		integrator_state_(idx) = std::min(integrator_state_(idx),integral_windup_(idx));
		integrator_state_(idx) = std::max(integrator_state_(idx),-integral_windup_(idx));
		tau_d_(idx) += integral_gain_(idx) * integrator_state_(idx);
		
		// Saturate commands here so that gravity compensation can override it.
		// Introduces nonlinearity. User should choose maximum stiffness related torque so
		// that damping and gravity compensation torque can still be realized to avoid
		// this nonlinearity.
		tau_d_(idx) = std::min( maximum_torque_(idx),tau_d_(idx));
		tau_d_(idx) = std::max(-maximum_torque_(idx),tau_d_(idx));
		// gravity compensation
		tau_d_(idx) += gravity_compensation_gain_ * model_->gravity()(dq_offset_+idx);
	}
	
	// compute damping
	if (damping_mode_ == damping_value ) {
		throw std::runtime_error("Damping mode not implemented");
	} else if (damping_mode_ == damping_ratio ) {
		for (int idx : impedance_controlled_joints_ids_) {
			const double de = state_d_.dy(dq_offset_+idx) - state_->dy(dq_offset_+idx);
			tau_d_(idx) += 
				sqrt(local_stiffness_(idx) * model_->MassMatrix().diagonal()(dq_offset_+idx)) 
					* 2. * damping_ratio_(idx) * de;
		}
	} else if ( damping_mode_ == damping_double_diagonalization ) {
		 // TODO: reduce massmatrix to joints controlled by this instance
		 // the next line takes ~0.2ms computation time for 32 joints
		 eigensolver_.compute(local_stiffness_.asDiagonal(),
						model_->MassMatrix().bottomRightCorner(
								model_->dq_size(),model_->dq_size()),
						Eigen::ComputeEigenvectors);
		 // TODO: preallocate all temporaries
		 thread_local Eigen::MatrixXd MV(model_->dq_size(),model_->dq_size());
		 MV = model_->MassMatrix().bottomRightCorner(
					model_->dq_size(),model_->dq_size()) *
					eigensolver_.eigenvectors();
		 thread_local Eigen::MatrixXd z(model_->dq_size(),model_->dq_size()); //TODO: diagonalmatrix here
		 z.setZero();
		 z.diagonal() = 2. * eigensolver_.eigenvalues().array().sqrt()
							* damping_ratio_.array();
		 thread_local Eigen::MatrixXd D(model_->dq_size(),model_->dq_size());
		 D = MV * z * MV.transpose();

		 for (int idx : impedance_controlled_joints_ids_) {
			const double de = state_d_.dy(dq_offset_+idx) - state_->dy(dq_offset_+idx);
			tau_d_.array() += D.middleCols(idx,1).array() * de;
		 }
	} else {
		 throw std::runtime_error("Damping mode not implemented");
	}

    if(feedforward_gain_!=0.) {
        // TODO: only use joints which are controlled
        tau_d_.noalias() += feedforward_gain_ * 
                model_->MassMatrix().bottomRightCorner(model_->dq_size(),model_->dq_size()) * 
                state_d_.ddy.tail(model_->dq_size());
    }
}
		
bool JointImpedanceController::hasTaskJacobian() const {
	return true;
}

Eigen::MatrixXd const & JointImpedanceController::getTaskJacobian() const {
	return task_jacobian_;
}


std::vector<std::string> JointImpedanceController::motionEntities() {
	return impedance_controlled_joint_names_;
}

void JointImpedanceController::startMotion(timestamp_t const & timestamp, std::string const & entity, 
		Eigen::Ref<Eigen::VectorXd> const & state_offset_d, double const duration, bool absolute) {
		
	if (state_offset_d.size() != model_->dq_size() && state_offset_d.size() != 1 ) {
        throw std::runtime_error("JointImpedanceController::startMotion goal only takes single joint or all joint goals for now");
    }
    if(entity=="all") {
		if (absolute) {
	        goal_state_.y.tail(model_->q_size()) = state_offset_d;
	    } else {
	        goal_state_.y.tail(model_->q_size()) = state_d_.y.tail(model_->q_size()) + state_offset_d;
	    }
	} else {
		if (absolute) {
			goal_state_.y(q_offset_ + jointID_map_.at(entity)) = state_offset_d(0);
		} else {
			goal_state_.y(q_offset_ + jointID_map_.at(entity)) = state_d_.y(q_offset_ + jointID_map_.at(entity)) + state_offset_d(0);
		}

	}
    trajectory_generator_->startMotion(timestamp, state_d_, goal_state_, duration); 
}

bool JointImpedanceController::isMotionRunning(timestamp_t const & timestamp, std::string const &) {
	return trajectory_generator_->isRunning(timestamp);
}

void JointImpedanceController::stopMotion(timestamp_t const & timestamp, std::string const &) {
	trajectory_generator_->stopMotion(timestamp);
}

Eigen::VectorXd const & JointImpedanceController::getMotionState(std::string const & entity) {
	static Eigen::VectorXd buffer;
	if (entity == "all") {
		buffer.resize(model_->q_size());
		buffer = goal_state_.y.tail(model_->q_size());
	} else {
		//entity = jointName
		buffer.resize(1);
		try {
			buffer(0) = goal_state_.y(q_offset_ + jointID_map_.at(entity));			
		} catch (std::exception const & ex) {
			PBWBC_DEBUG("Exception in JointImpedanceController::getMotionState: " << ex.what() << ". Entity" << entity << " could not be found in joint map");
		}
	}

	return buffer;
}

} // namespace pbwbc
