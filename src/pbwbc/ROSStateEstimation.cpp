#include <pbwbc/ROSStateEstimation.hpp>
#include <pbwbc/ROSTools.hpp>

namespace pbwbc {

ROSStateEstimation::ROSStateEstimation(
        std::shared_ptr<ConfigMap> const & config,
        std::shared_ptr<Model> const & model,
        ros::NodeHandle const & nh
        ) : nh_(nh),
        model_(model),
        state_(new RobotState(model->y_size(),model->dy_size()))
        {
            initialized_ = false;
            subscriber_ = nh_.subscribe("/floating_base_pose_simulated", 1, 
                    &ROSStateEstimation::callback, this);
            tf_broadcaster_.reset(new RealTimeTransformBroadcaster(nh_, "world","base_link",model_));
        }

ROSStateEstimation::~ROSStateEstimation() {}

void ROSStateEstimation::start(timestamp_t const &) {
    if(!initialized_) {
        throw std::runtime_error("ROSStateEstimation has no data yet (in start)");
    }
}

void ROSStateEstimation::stop(timestamp_t const &) {}

void ROSStateEstimation::update(timestamp_t const & timestamp) {
    if(mutex_.try_lock()) {
        model_->setBasePosition(base_state_.position.translation,
                base_state_.position.orientation,*state_);

        Eigen::Matrix3d R_B = base_state_.position.orientation.toRotationMatrix();
        model_->setBaseVelocity(
                R_B.transpose() * base_state_.velocity.translation,
                R_B.transpose() * base_state_.velocity.orientation,
                *state_);
        mutex_.unlock();
    }
    tf_broadcaster_->setBasePosition(base_state_.position.translation,base_state_.position.orientation);
    tf_broadcaster_->setJointPositions(*state_);
    tf_broadcaster_->publish(timestamp);
}


void ROSStateEstimation::callback(const nav_msgs::Odometry::ConstPtr & msg) {
    if(!mutex_.try_lock())return;
    // assign to buffer
    initialized_ = true;
    assign(base_state_.position, msg->pose.pose);
    assign(base_state_.velocity, msg->twist.twist);
    mutex_.unlock();
}

std::shared_ptr<RobotState> const & ROSStateEstimation::state() {
    return state_;
}

} // namespace


