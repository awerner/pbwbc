#include <pinocchio/fwd.hpp>
#include <pbwbc/SelfCollisionAvoidanceControllerROS.hpp>
#include <pbwbc/SelfCollisionAvoidanceControllerImpl.hpp>

namespace pbwbc {

SelfCollisionAvoidanceControllerROS::SelfCollisionAvoidanceControllerROS(
            std::shared_ptr<RobotState> const & state,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config,
            std::string const & urdf_content,
            ros::NodeHandle const & nh
            ) :
    SelfCollisionAvoidanceController(state,model,config,urdf_content),
    nh_(nh)
    {
        // TODO: publish distances in JointState message
        desired_torque_publisher_ = RealTimePublisherBackEnd::advertise<sensor_msgs::JointState>(nh_,"desired_torque",1000,10);
        desired_torque_publisher_.msg().name = model_->jointNames();
        desired_torque_publisher_.msg().effort.resize(model_->jointNames().size());
        desired_torque_publisher_.fillBuffer();

        ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure(nh_));
        ddr_->RegisterVariable(&max_force_, "max_force", 0., 2000.);
        ddr_->RegisterVariable(&damping_, "damping", 0., 1.);
        auto & geomModel = *impl_->geomModel;
        for ( std::size_t idx=0; idx<geomModel.collisionPairs.size(); idx++ ) {
            auto const & pair = geomModel.collisionPairs.at(idx);
            std::stringstream str; str << "sd_"
                << geomModel.geometryObjects.at(pair.first).name 
                << "_"
                << geomModel.geometryObjects.at(pair.second).name;
            ddr_->RegisterVariable(&start_distance_.at(idx), str.str(), 0., 1.);
        }
        ddr_->PublishServicesTopics();
    }

SelfCollisionAvoidanceControllerROS::~SelfCollisionAvoidanceControllerROS() {}

    
void SelfCollisionAvoidanceControllerROS::update(timestamp_t const & timestamp) {
  ros::Time now(timestamp.time_since_epoch().count());

  SelfCollisionAvoidanceController::update(timestamp);

  if(desired_torque_publisher_.trylock()) {
    desired_torque_publisher_.msg().header.stamp = now;
    std::copy_n(&tau_d_(0), tau_d_.size(), &desired_torque_publisher_.msg().effort[0]);
    desired_torque_publisher_.unlockAndPublish();
  }
}

} // namespace pbwbc
