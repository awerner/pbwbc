#include <pbwbc/RealSenseStateEstimationROS.hpp>

namespace pbwbc {


RealSenseStateEstimationROS::RealSenseStateEstimationROS(
            std::shared_ptr<ConfigMap> const & config,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<SensorState> const & sensor_state,
            ros::NodeHandle const & nh
            ) :
        RealSenseStateEstimation(config,model,sensor_state),
        nh_(nh) {
        state_pose_pub_ = RealTimePublisherBackEnd::advertise<geometry_msgs::PoseStamped>(nh_,"state/pose",1000,10);
        state_twist_pub_ = RealTimePublisherBackEnd::advertise<geometry_msgs::TwistStamped>(nh_,"state/twist",1000,10);

        // set up sensor publishers
        for(auto const & s : camera_buffers_) {
            camera_pose_pub_.push_back(
                    RealTimePublisherBackEnd::advertise<geometry_msgs::PoseStamped>(nh_,s.name + "/pose",1000,10)
                    );
            camera_twist_pub_.push_back(
                    RealTimePublisherBackEnd::advertise<geometry_msgs::TwistStamped>(nh_,s.name + "/twist",1000,10)
                    );
            camera_accel_pub_.push_back(
                    RealTimePublisherBackEnd::advertise<geometry_msgs::TwistStamped>(nh_,s.name + "/accel",1000,10)
                    );
            camera_confidence_pub_.push_back(
                    RealTimePublisherBackEnd::advertise<std_msgs::Float32>(nh_,s.name + "/confidence",1000,10)
                    );
            camera_active_pub_.push_back(
                    RealTimePublisherBackEnd::advertise<std_msgs::Float32>(nh_,s.name + "/active",1000,10)
                    );
            camera_pose_error_pub_.push_back(
                    RealTimePublisherBackEnd::advertise<geometry_msgs::PoseStamped>(nh_,s.name + "/pose_error",1000,10)
                    );
        }

        imu_pose_pub_ = RealTimePublisherBackEnd::advertise<geometry_msgs::PoseStamped>(nh_,"imu/pose",1000,10);
        imu_velocity_pub_ = RealTimePublisherBackEnd::advertise<geometry_msgs::TwistStamped>(nh_,"imu/twist",1000,10);
        imu_acceleration_pub_ = RealTimePublisherBackEnd::advertise<geometry_msgs::TwistStamped>(nh_,"imu/accel",1000,10);

        config->param("publisher_subsampling", publisher_subsampling_, 10);
        int frame;
        config->param("publisher_frame", frame, 0);
        frame_ = static_cast<enum frame_enum>(frame);
        if(frame_==world) {
            PBWBC_DEBUG("Publishing in world frame");
        } else if(frame_==sensor) {
            PBWBC_DEBUG("Publishing in sensor frame");
        } else if(frame_==world_position_body_velocity) {
            PBWBC_DEBUG("Publishing in positions w.r.t world and velocities in body frame");
        }
        ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure( ros::NodeHandle(nh_, "RSSE")));

        ddr_->RegisterVariable(&position_translation_gain_,"position_translation_gain");
        ddr_->RegisterVariable(&velocity_translation_gain_,"velocity_translation_gain");
        ddr_->RegisterVariable(&orientation_gain_,"orientation_gain");
        ddr_->RegisterVariable(&world_frame_gain_,"world_frame_gain");
        ddr_->RegisterVariable(&W_T_H_gain_,"W_T_H_gain");

        ddr_->RegisterVariable(&translation_rejection_threshold_,"translation_rejection_threshold");
        ddr_->RegisterVariable(&orientation_rejection_threshold_,"orientation_rejection_threshold");

        for(auto & s: camera_buffers_ ) {
            ddr_->RegisterVariable(&s.B_T_C_p(0),s.name+"/B_T_C_x");
            ddr_->RegisterVariable(&s.B_T_C_p(1),s.name+"/B_T_C_y");
            ddr_->RegisterVariable(&s.B_T_C_p(2),s.name+"/B_T_C_z");
            
            ddr_->RegisterVariable(&s.B_T_C_q.coeffs()(0),s.name+"/B_T_C_qx");
            ddr_->RegisterVariable(&s.B_T_C_q.coeffs()(1),s.name+"/B_T_C_qy");
            ddr_->RegisterVariable(&s.B_T_C_q.coeffs()(2),s.name+"/B_T_C_qz");
            ddr_->RegisterVariable(&s.B_T_C_q.coeffs()(3),s.name+"/B_T_C_qw");
            
            ddr_->RegisterVariable(&s.initialized,s.name+"/initialized");
            ddr_->RegisterVariable(&s.enabled,s.name+"/enabled");
        }
        
        ddr_->RegisterVariable(&frame_, "publisher_frame",0,2);
        ddr_->RegisterVariable(&publisher_subsampling_,"publisher_subsampling",1,100);
        ddr_->RegisterVariable(&reset_state_, "reset_state");

        ddr_->RegisterVariable(&start_state_.position.translation(0),"start_state/x");
        ddr_->RegisterVariable(&start_state_.position.translation(1),"start_state/y");
        ddr_->RegisterVariable(&start_state_.position.translation(2),"start_state/z");
        
        ddr_->RegisterVariable(&start_state_.position.orientation.coeffs()(0),"start_state/qx");
        ddr_->RegisterVariable(&start_state_.position.orientation.coeffs()(1),"start_state/qy");
        ddr_->RegisterVariable(&start_state_.position.orientation.coeffs()(2),"start_state/qz");
        ddr_->RegisterVariable(&start_state_.position.orientation.coeffs()(3),"start_state/qw");

        ddr_->PublishServicesTopics();

        tf_broadcaster_.reset(new RealTimeTransformBroadcaster(nh_, "world","base_link", model));
};

RealSenseStateEstimationROS::~RealSenseStateEstimationROS() {}

void RealSenseStateEstimationROS::start(timestamp_t const & timestamp) {
    RealSenseStateEstimation::start(timestamp);
    publisher_subsampling_counter_ = 0;
}

void RealSenseStateEstimationROS::update(timestamp_t const & timestamp) {
    RealSenseStateEstimation::update(timestamp);
        
    if( ((publisher_subsampling_counter_++) % publisher_subsampling_) !=0 )return;
    
    ros::Time now(timestamp.time_since_epoch().count());
    // publish data if available
    auto stampedpose_msg_fun = [&](std::string const & name, Eigen::Vector3d const & p, Eigen::Quaterniond const & q) {
        geometry_msgs::PoseStamped msg;
        msg.header.seq = 0;
        msg.header.stamp = now;
        msg.header.frame_id = name;
        msg.pose.position.x = p(0);
        msg.pose.position.y = p(1);
        msg.pose.position.z = p(2);
        msg.pose.orientation.x = q.x();
        msg.pose.orientation.y = q.y();
        msg.pose.orientation.z = q.z();
        msg.pose.orientation.w = q.w();
        return msg;
    };
    auto stampedtwist_msg_fun = [&](std::string const & name, Eigen::Matrix<double,6,1> const & v) {
        geometry_msgs::TwistStamped msg;
        msg.header.seq = 0;
        msg.header.stamp = now;
        msg.header.frame_id = name;
        msg.twist.linear.x = v(0);
        msg.twist.linear.y = v(1);
        msg.twist.linear.z = v(2);
        msg.twist.angular.x = v(3);
        msg.twist.angular.y = v(4);
        msg.twist.angular.z = v(5);
        return msg;
    };
    auto confidence_msg_fun = [&](double confidence) {
        std_msgs::Float32 msg;
        msg.data = confidence;
        return msg;
    };

    state_pose_pub_.publish(stampedpose_msg_fun("state",state_.position.translation,state_.position.orientation));
    Vector6d W_V_B;
    W_V_B << state_.velocity.translation, state_.velocity.orientation; // translation_vel in world, angular_vel in body
    if(frame_==world || frame_==sensor) {
        Vector6d W_V_B_w;
        W_V_B_w.head(3) = /*state_.orientation.toRotationMatrix().transpose() */ W_V_B.head(3);
        W_V_B_w.tail(3) = state_.position.orientation.toRotationMatrix() * W_V_B.tail(3);
        state_twist_pub_.publish(stampedtwist_msg_fun("state",W_V_B));
    } else if(frame_==world_position_body_velocity) {
        Vector6d W_V_B_b;
        // PAL convention: translation in world, angular in moving frame -> have that in filter state
        W_V_B_b.head(3) = /*state_.orientation.toRotationMatrix().transpose() */ W_V_B.head(3);
        W_V_B_b.tail(3) = /*state_.orientation.toRotationMatrix().transpose() */ W_V_B.tail(3);
        state_twist_pub_.publish(stampedtwist_msg_fun("state",W_V_B_b));
    }
    for(std::size_t cam_idx=0;cam_idx<camera_buffers_.size();cam_idx++) {
        auto const & cam = camera_buffers_.at(cam_idx);
        if(cam.timestamp!=0) {
            // Publish what the camera thinks is happening to the base frame expressed in the world frame
            Eigen::Isometry3d W_T_H = TfromVQ(cam.W_T_H_p,cam.W_T_H_q);
            Eigen::Isometry3d H_T_C = TfromVQ(cam.state.position.translation,cam.state.position.orientation);
            Eigen::Isometry3d B_T_C = TfromVQ(cam.B_T_C_p,cam.B_T_C_q);
            Eigen::Isometry3d W_T_B = W_T_H * H_T_C * B_T_C.inverse();
            Eigen::Isometry3d W_T_B_s = TfromVQ(state_.position.translation,state_.position.orientation);
            Eigen::Isometry3d error = W_T_B_s * W_T_B.inverse();
            
            if(frame_==world || frame_==world_position_body_velocity) {
                camera_pose_pub_.at(cam_idx).publish(stampedpose_msg_fun(cam.name,
                            W_T_B.translation(),Eigen::Quaterniond(W_T_B.linear())));
            } else if(frame_==sensor) {
                camera_pose_pub_.at(cam_idx).publish(stampedpose_msg_fun(cam.name,
                            H_T_C.translation(),Eigen::Quaterniond(H_T_C.linear())));
            }
            camera_pose_error_pub_.at(cam_idx).publish(stampedpose_msg_fun(cam.name,
                        error.translation(),Eigen::Quaterniond(error.linear())));

            if(frame_==world) {
                // TODO: this is duplicate code from the correction in RealSenseStateEstimation::update
                Vector6d H_V_B;
                H_V_B.head(3) = cam.state.velocity.translation - skew(H_T_C.linear() * B_T_C.inverse().translation()) * cam.state.velocity.orientation;
                H_V_B.tail(3) = cam.state.velocity.orientation;
                Vector6d W_V_B;
                W_V_B.head(3) = W_T_H.linear() * H_V_B.head(3);
                W_V_B.tail(3) = W_T_H.linear() * H_V_B.tail(3);
                camera_twist_pub_.at(cam_idx).publish(stampedtwist_msg_fun("state",W_V_B));
                Vector6d H_A_B;
                // TODO: acceleration is missing some terms
                H_A_B.head(3) = cam.sensor_acceleration;
                H_A_B.tail(3).setZero();
                Vector6d W_A_B;
                W_A_B.head(3) = W_T_H.linear() * H_A_B.head(3);
                W_A_B.tail(3) = W_T_H.linear() * H_A_B.tail(3);
                camera_accel_pub_.at(cam_idx).publish(stampedtwist_msg_fun("state",W_A_B));
            } else if(frame_==sensor) {
                Vector6d H_V_C;
                H_V_C.head(3) = cam.state.velocity.translation;
                H_V_C.tail(3) = cam.state.velocity.orientation;
                camera_twist_pub_.at(cam_idx).publish(stampedtwist_msg_fun("state",H_V_C));

                Vector6d H_A_C;
                H_A_C.head(3) = cam.sensor_acceleration;
                H_A_C.tail(3).setZero();
                camera_accel_pub_.at(cam_idx).publish(stampedtwist_msg_fun("state",H_A_C));
            } else if(frame_==world_position_body_velocity) {
                Vector6d H_V_B;
                H_V_B.head(3) = cam.state.velocity.translation - skew(H_T_C.linear() * B_T_C.inverse().translation()) * cam.state.velocity.orientation;
                H_V_B.tail(3) = cam.state.velocity.orientation;
                Vector6d W_V_B;
                Eigen::Isometry3d B_T_H = W_T_B_s.inverse() * W_T_H;
                W_V_B.head(3) = W_T_H.linear() * H_V_B.head(3); // To compare with PAL BSE: linear velocities in world frame
                W_V_B.tail(3) = B_T_H.linear() * H_V_B.tail(3); // angular velocities in moving frame
                camera_twist_pub_.at(cam_idx).publish(stampedtwist_msg_fun("state",W_V_B));
                Vector6d H_A_B;
                // TODO: acceleration is missing some terms
                H_A_B.head(3) = cam.sensor_acceleration;
                H_A_B.tail(3).setZero();
                Vector6d W_A_B;
                W_A_B.head(3) = W_T_H.linear() * H_A_B.head(3);
                W_A_B.tail(3) = B_T_H.linear() * H_A_B.tail(3);
                camera_accel_pub_.at(cam_idx).publish(stampedtwist_msg_fun("state",W_A_B));
            }

            camera_confidence_pub_.at(cam_idx).publish(confidence_msg_fun(cam.tracker_confidence));
            camera_active_pub_.at(cam_idx).publish(
                    confidence_msg_fun(cam.enabled && cam.counter_until_valid==0));

        }
    }

    auto const & imu_data = std::get<2>(sensor_state_->imuStates.at(imu_idx_));
    imu_pose_pub_.publish(stampedpose_msg_fun("imu",
                Eigen::Vector3d::Zero(),
                imu_data.orientation));
    Vector6d twist = Vector6d::Zero();
    if(frame_==sensor || frame_==world_position_body_velocity) {
        twist.tail(3) = imu_data.angularVelocity;
    } else if(frame_==world) {
        twist.tail(3) = state_.position.orientation.toRotationMatrix() * imu_data.angularVelocity;
    }
    imu_velocity_pub_.publish(stampedtwist_msg_fun("imu",twist));
    Vector6d accel = Vector6d::Zero();
    if(frame_==sensor || frame_==world_position_body_velocity) {
        accel.head(3) = imu_data.linearAcceleration;
    } else if(frame_==world) {
        accel.head(3) = state_.position.orientation.toRotationMatrix() * imu_data.linearAcceleration;
    }
    imu_acceleration_pub_.publish(stampedtwist_msg_fun("imu",accel));

    tf_broadcaster_->setBasePosition(state_.position.translation,state_.position.orientation);
    tf_broadcaster_->setJointPositions(*robotstate_);
    tf_broadcaster_->publish(timestamp);
}

} // namespace
