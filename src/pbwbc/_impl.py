#!/usr/bin/env python3


class CartesianImpedanceController(object):
    def __init__(self,namespace,mcfd):
        self.namespace = namespace
        self.name = self.namespace.split('/')[-2]
        self.mcfd = mcfd
        import dynamic_reconfigure.client
        self.client = dynamic_reconfigure.client.Client(namespace,timeout=1)
        from msg import StateStamped
        import rospy
        self._state = None
        self._current_state_subscriber = rospy.Subscriber(self.namespace+"/current_state",StateStamped,
                lambda msg: self._state_callback(msg))
        

    def reset(self):
        import rospy
        from msg import StateStamped
        msg = rospy.wait_for_message(self.namespace+"/current_state",StateStamped,timeout=1)
        from std_msgs.msg import String
        destination = [
            msg.transform.translation.x,
            msg.transform.translation.y,
            msg.transform.translation.z,
            msg.transform.rotation.x,
            msg.transform.rotation.y,
            msg.transform.rotation.z,
            msg.transform.rotation.w,
            ]
        res = self.mcfd.action_service(String(self.name+"_reset"),
                String(self.mcfd.name.split('/')[-1]),
                String(self.name),
                destination,
                True,0.,1e-3)

    def _state_callback(self,msg):
        self._state = msg

    def state(self):
        return self._state

    def set(self,pmap):
        return self.client.update_configuration(pmap)

class COM(object):
    def __init__(self):
        import rospy
        from msg import StateStamped
        self._current_pose = None
        self._current_velocity = None
        self._current_state_subscriber_ = rospy.Subscriber("/pbwbc_controller/MultiContactForceDistribution/com_current_state", StateStamped, 
        lambda msg: self.current_state_callback(msg))

    def current_state_callback(self,msg): #gets com pose in world
        self._current_pose = msg.transform
        self._current_velocity = msg.twist

    def pose(self):
        if self._current_pose is None:
            import rospy
            rospy.sleep(0.0001)
            self.pose()
            #raise Exception("COM status not received yet")
        pose = [self._current_pose.translation.x, self._current_pose.translation.y, self._current_pose.translation.z, 
                self._current_pose.rotation.x, self._current_pose.rotation.y, self._current_pose.rotation.z, self._current_pose.rotation.w ]
        return pose

    def velocity(self):
        if self._current_velocity is None:
            import rospy
            rospy.sleep(0.0001)
            self.velocity()
        velocity = [self._current_velocity.linear.x, self._current_velocity.linear.y, self._current_velocity.linear.z, 
                    self._current_velocity.angular.x, self._current_velocity.angular.y, self._current_velocity.angular.z ]
        return velocity



class Contact(object):
    def __init__(self,name,mcfd):
        self.name = name
        self.mcfd = mcfd
        import dynamic_reconfigure.client
        self.client = dynamic_reconfigure.client.Client("/pbwbc_controller/MultiContactForceDistribution/Contacts/"+name,timeout=1)
        self.cartimp = CartesianImpedanceController(
                "/pbwbc_controller/MultiContactForceDistribution/Contacts/"+name+"/CartesianImpedanceController",
                self.mcfd)
        import rospy
        from geometry_msgs.msg import WrenchStamped
        self._measured_wrench = None
        self._measured_wrench_subscriber = rospy.Subscriber("/pbwbc_controller/MultiContactForceDistribution/Contacts/"+name+"/measured_wrench",
                WrenchStamped, lambda msg: self.measured_wrench_callback(msg))

        from msg import Int32Stamped
        self._contact_state = None
        self._contact_state_subscriber = rospy.Subscriber("/pbwbc_controller/MultiContactForceDistribution/Contacts/"+name+"/contact_state", Int32Stamped, 
        lambda msg: self.contact_state_callback(msg))

        from msg import StateStamped
        self._current_pose = None
        self._current_velocity = None
        self._current_state_subscriber_ = rospy.Subscriber("/pbwbc_controller/MultiContactForceDistribution/Contacts/"+name+"/CartesianImpedanceController/current_state", StateStamped, 
        lambda msg: self.current_state_callback(msg))

    def contact_state_callback(self,msg): #gets if state is on (1) or off (0) or in between (10X)
        self._contact_state = msg.data.data 

    def current_state_callback(self,msg): #gets foot pose in world
        self._current_pose = msg.transform
        self._current_velocity = msg.twist

    def contact_state(self):
        return self._contact_state

    def pose(self):
        if self._current_pose is None:
            import rospy
            rospy.sleep(0.0001)
            self.pose()
        pose = [self._current_pose.translation.x, self._current_pose.translation.y, self._current_pose.translation.z, 
                self._current_pose.rotation.x, self._current_pose.rotation.y, self._current_pose.rotation.z, self._current_pose.rotation.w ]
        return pose

    def velocity(self):
        if self._current_velocity is None:
            import rospy
            rospy.sleep(0.0001)
            self.velocity()
        velocity = [self._current_velocity.linear.x, self._current_velocity.linear.y, self._current_velocity.linear.z, 
                    self._current_velocity.angular.x, self._current_velocity.angular.y, self._current_velocity.angular.z ]
        return velocity

    def reset(self):
        self.cartimp.reset()
    
    def set(self,pmap):
        return self.client.update_configuration(pmap)

    def measured_wrench(self):
        return self._measured_wrench
    
    def measured_wrench_callback(self,msg):
        self._measured_wrench = msg

class MultiContactForceDistribution(object):
    def __init__(self,name="pbwbc_controller/MultiContactForceDistribution"):
        self.name = name
        import dynamic_reconfigure.client
        self.client = dynamic_reconfigure.client.Client(name,timeout=1)

        # discover contacts
        import rospy
        topic_info = rospy.get_published_topics(namespace=name+'/Contacts') 
        self.contact_names = []
        for info in topic_info:
            if info[0].find("transform")!=-1:
                # /pbwbc_controller/MultiContactForceDistribution/Contacts/ContactRightFoot/contact_state
                contact_name = info[0].split('/')[-2]
                self.contact_names.append(contact_name)
        self.contacts = [Contact(name,self) for name in self.contact_names]
        
        from srv import AddAction
        self.action_service = rospy.ServiceProxy(self.name.split('/')[0]+'/addAction', AddAction)

        self._current_state = None
        from msg import StateStamped
        self._current_state_subscriber = rospy.Subscriber(self.name+"/com_current_state",
                StateStamped,
                lambda msg: self._current_state_callback(msg))

    def _current_state_callback(self,msg):
        self._current_state = msg
        
    def current_state(self):
        return self._current_state

    def set(self,pmap):
        return self.client.update_configuration(pmap)

    def reset(self):
        # get current state
        import rospy
        from msg import StateStamped
        msg = rospy.wait_for_message(self.name+"/com_current_state",StateStamped,timeout=1)
        destination = [
            msg.transform.translation.x,
            msg.transform.translation.y,
            msg.transform.translation.z,
            msg.transform.rotation.x,
            msg.transform.rotation.y,
            msg.transform.rotation.z,
            msg.transform.rotation.w,
            ]
        # create action to move com to this pose in short time
        from std_msgs.msg import String
        res = self.action_service(String("com_reset"),
                String(self.name.split('/')[-1]),
                String("com"),
                destination,
                True,0.,1e-3)
        # TODO: reset base rotation
        # reset desired to current state
        for contact in self.contacts: contact.reset()
       

class JointImpedanceController(object):
    def __init__(self,name="JointImpedanceController"):
        import dynamic_reconfigure.client
        self.client = dynamic_reconfigure.client.Client("/pbwbc_controller/"+name,timeout=1)

    def set(self,pmap):
        return self.client.update_configuration(pmap)


class PBWBCController(object):
    def __init__(self,namespace='/pbwbc_controller'):
        import rospy
        from std_msgs.msg import Bool
        self._controller_active_subscriber = rospy.Subscriber("/pbwbc_controller/controller_active", Bool, 
                lambda x: self.controller_active_callback(x))
        self._controller_active_status = None
        from sensor_msgs.msg import JointState
        self._current_state_subscriber = rospy.Subscriber("/pbwbc_controller/current_state", JointState,
                lambda x: self.controller_state_callback(x))
        from pbwbc.srv import controllerSwitch
        self._controller_switch = rospy.ServiceProxy("/pbwbc_controller/controllerSwitch",controllerSwitch)
        import time
        from srv import triggerRosbag
        self.triggerRosbag_service = rospy.ServiceProxy('/pbwbc_controller/triggerRosbagDump', triggerRosbag)
        from std_msgs.msg import Bool
        #rospy.wait_for_message('/pbwbc_controller/controller_active',Bool,timeout=1.)


    def controller_active_callback(self,msg):
        import rospy
        self._controller_active_timestamp = rospy.get_time()
        self._controller_active_status = msg.data

    def active(self):
        import rospy
        if self._controller_active_status is None:
            rospy.sleep(0.00001)
            self.active()
            #raise Exception("Controller status not received yet")
        #print ("{} vs {} ".format(self._controller_active_timestamp + 1,rospy.get_time()))
        if self._controller_active_timestamp + 1 < rospy.get_time():
            print("Controller did not publish active status for 1s")
            return False
        return self._controller_active_status

    def switchController(self,value):
        self._controller_switch(value)

    def controller_state_callback(self,msg):
        self._controller_state = msg

    def get_state(self):
        return self._controller_state

    
    def download_topic_data(self, topic_filter):
        """ 
        Download rosbag data from the controller and return the Bag object,
        param topic_filter to select topics
        """
        from srv import triggerRosbagRequest
        request = triggerRosbagRequest()
        request.filename.data = "/tmp/dump.bag"
        request.topic_filter.data = topic_filter
        request.download_data.data = True
        resp = self.triggerRosbag_service(request)
        f = open('/tmp/dump2.bag','wb') # TODO: temp file name
        import struct
        f.write(struct.pack('b'*len(resp.rosbag_data.data),*resp.rosbag_data.data))
        f.close()
        import rosbag
        bag = rosbag.Bag('/tmp/dump2.bag')
        # TODO: delete file
        return bag

    

class ActionInterface(object):
    def __init__(self,name,wait=False):
        self.name = name
        import rospy
        from srv import AddAction
        rospy.wait_for_service('/pbwbc_controller/addAction')
        self.action_service = rospy.ServiceProxy('/pbwbc_controller/addAction', AddAction)

        # create contact object for all existing contacts in MultiContactForceDistribution
        import rospy
        from msg import Int32Stamped
        topic_info = rospy.get_published_topics(namespace='/pbwbc_controller/MultiContactForceDistribution/Contacts') 
        self.contacts = {}
        for info in topic_info:
            if info[0].find("transform")!=-1:
                # /pbwbc_controller/MultiContactForceDistribution/Contacts/ContactRightFoot/contact_state
                contact_name = info[0].split('/')[-2]
                self.contacts[contact_name] = Contact(contact_name,self)

        # create COM object
        self.COM = COM()

    def action(self,name,controller,entity,destination,starttime,duration,absolute=False):
        from std_msgs.msg import String
        res = self.action_service(String(name),String(controller),String(entity),destination,absolute,starttime,duration)
        if res.status != 0:
            raise Exception(res.msg)

    def contactState(self, contact_name):
        if "Contact"+contact_name in self.contacts:
            return self.contacts["Contact"+contact_name].contact_state()
        else:
            raise Exception("Contact"+contact_name+" does not exist") 

    def switchContact(self,endeffector_name, new_state, starttime):
        name="switch contact on "+endeffector_name
        controller="MultiContactForceDistribution"
        entity="contact:Contact"+endeffector_name

        if new_state == "on" or new_state == 1:
            destination = [1.]
        elif new_state == "off" or new_state == 0:
            destination = [0.]

        self.action(name,controller,entity,destination,starttime,1.0,absolute=True)

    def EEpose(self, endeffector_name):
        if "Contact"+endeffector_name in self.contacts:
            return self.contacts["Contact"+endeffector_name].pose()
        else:
            raise Exception("Contact"+contact_name+" does not seem to exist in MultiContactForceDistribution, cannot give you the associated end effector pose") 

    def EEvelocity(self, endeffector_name):
        if "Contact"+endeffector_name in self.contacts:
            return self.contacts["Contact"+endeffector_name].velocity()
        else:
            raise Exception("Contact"+contact_name+" does not seem to exist in MultiContactForceDistribution, cannot give you the associated end effector velocity") 

    def COMpose(self):
        return self.COM.pose()

    def COMvelocity(self):
        return self.COM.velocity()

    def moveEE(self,endeffector_name, destination, starttime, duration, absolute=False):
        """
        destination can be either 6 or 7 values. For six values in contains:
        - position x w.r.t to the world frame in meter
        - position y
        - position z
        - roll angle in radians
        - pitch
        - yaw
        For the seven value variant:
        - position x w.r.t to the world frame in meter
        - position y
        - position z
        - quaternion x
        - quaternion y
        - quaternion z
        - quaternion w
        """
        name="move "+endeffector_name
        controller="MultiContactForceDistribution"
        entity="Contact"+endeffector_name
        if len(destination) == 6:
            destination = destination[0:3] + rpy2quat(destination[3], destination[4], destination[5]).coeffs().T.tolist()
        elif len(destination) == 3:
            if absolute:
                 raise Exception("This is not supported")
            destination = destination[0:3] + rpy2quat(0., 0., 0.).coeffs().T.tolist()
        self.action(name,controller,entity,destination,starttime,duration, absolute)

    def moveCOM(self, destination, starttime, duration, absolute=False):
        name="move com"
        controller="MultiContactForceDistribution"
        if len(destination) == 6:
            destination = destination[0:3] + rpy2quat(destination[3], destination[4], destination[5]).coeffs().T.tolist()
        elif len(destination) == 3:
            destination = destination[0:3] + rpy2quat(0., 0., 0.).coeffs().T.tolist()
        self.action(name, controller, "com", destination, starttime, duration, absolute)

    def moveJoint(self, jointName, destination, starttime, duration, absolute=False):
        name="move_"+jointName
        controller="JointImpedanceController"
        self.action(name, controller, jointName, destination, starttime, duration, absolute)

    def moveJoints(self, jointNames, destinations, starttime, duration, absolute=False):
        for jointName, destination in zip(jointNames,destinations):
            self.moveJoint(jointName, [destination], starttime, duration, absolute)

    def movePosture(self, destination, starttime, duration, absolute=False):
        #destination is an array of size (# of joint impedance controlled joints)
        #containing the list of joint positions
        #in the same order as defined in the <config >.yaml file
        #under parameter .../Controllers/JointImpedanceController/impedance_controlled_joint_names
        name="move_all_joints"
        controller="JointImpedanceController"
        self.action(name,controller,"all", destination, starttime, duration, absolute) 

class Model(object):
    """
    A wrapper around pinocchio with helper functions to compute kinematics using
    ROS messages received from the controller
    """
    def __init__(self,namespace):
        import pinocchio
        import rospy
        robot_description = rospy.get_param('/robot_description')
        robot_description_filename = "/tmp/robot_description.urdf"
        open(robot_description_filename,'w').write(robot_description)
        model = pinocchio.buildModelFromUrdf(robot_description_filename,pinocchio.JointModelFreeFlyer())
        import os
        os.remove(robot_description_filename)
        locked_joint_ids = [model.getJointId(name) for name in rospy.get_param('/pbwbc_controller/locked_joint_names')] 
        self.model = pinocchio.buildReducedModel(model,locked_joint_ids,pinocchio.utils.zero(model.nq))
        self.data = model.createData()
    
    def get_state_from_msg(self,msg):
        """
        Extract pinocchio state from a message on the current_state topic
        """
        import numpy
        stamp = msg.header.stamp.to_sec()
        y = numpy.array(msg.position)
        dy = numpy.array(msg.velocity[0:6]+msg.velocity[7:])
        tau = numpy.array(msg.effort[7:])
        return {'stamp': stamp, 'y': y, 'dy': dy, 'tau': tau}

    def update(self,state,dynamics=True):
        import pinocchio
        pinocchio.forwardKinematics(self.model,self.data,state['y'],state['dy'])
        pinocchio.updateFramePlacements(self.model,self.data)
        # TODO: update jacobians
        if dynamics:
            pass # TODO: add mass matrix computation here

    def bodyTranslation(self, frame_name):
        if not model.existFrameName(frame_name):
            raise Exception("Body does not exist")
        frame_id = model.getFrameId(frame_name)
        return self.model.oMf[frame_id].translation
    
    def bodyOrientation(self, frame_name):
        if not model.existFrameName(frame_name):
            raise Exception("Body does not exist")
        frame_id = model.getFrameId(frame_name)
        return self.model.oMf[frame_id].orientation

    def rnea(self,state):
        import pinocchio
        pinocchio.rnea(self.model,self.data,state['y'],state['dy'],state['ddy'])
        return self.data.tau

class TextToSpeech(object):
    def __init__(self):
        import actionlib
        from pal_interaction_msgs.msg import TtsAction, TtsActionGoal
        self.client = actionlib.SimpleActionClient("/tts",TtsAction)
        import time
        time.sleep(0.6)

    def say(self, text, blocking=True):
        from pal_interaction_msgs.msg import TtsAction, TtsActionGoal
        ag = TtsActionGoal()
        ag.goal.rawtext.text = text
        ag.goal.rawtext.lang_id = 'en_GB'
        self.client.send_goal(ag.goal)
        if blocking:
            self.client.wait_for_result()

class WaitForEvent(object):
    def __init__(self,button=None,keyboard=None,speak=False):
        self.button = button
        self.keyboard = keyboard
        self.msg = None
        if button is not None:
            from sensor_msgs.msg import Joy
            import rospy
            self._joy_subscriber = rospy.Subscriber("/joy", Joy, 
                    lambda x: self.callback(x))
        if speak:
            self.tts = TextToSpeech()
        else:
            self.tts = None

    def callback(self,msg):
        self.msg = msg

    def wait(self,text=""):
        print(text)
        if text and self.tts is not None:
            self.tts.say(text)
        button_edge = None
        while True:
            import time
            time.sleep(0.02)
            if self.msg is not None:
                if self.button is not None:
                    if button_edge is None:
                        button_edge = (self.msg.buttons[self.button]==1)
                    else:
                        if self.msg.buttons[self.button]==0:
                            button_edge = False
                        elif button_edge==False and self.msg.buttons[self.button]==1:
                            print("Confirmation received")
                            break
            if self.keyboard is not None:
                raw_input("Hit enter to confirm")
                print("Confirmation received")
                break
                #import select
                #import sys
                #print "enter select"
                #if select.select([sys.stdin],[],[],0.01):
                #    print "got a key",sys.stdin.next()
                #    break
                #print "select timeout"



def float32multiarray2numpy(msg):
    import numpy
    m = numpy.zeros(shape=(msg.data.layout.dim[1].size,msg.data.layout.dim[0].size))
    for c in range(msg.data.layout.dim[0].size):
        for r in range(msg.data.layout.dim[1].size):
            m[r,c] = msg.data.data[c*msg.data.layout.dim[0].stride + r]
    return m






def rpy2quat(r,p,y):
    import eigenpy
    eigenpy.switchToNumpyArray()
    import numpy
    return eigenpy.Quaternion(eigenpy.fromEulerAngles(
        numpy.array([r,p,y]),0,1,2))



if __name__ == "__main__":
    import rospy
    rospy.init_node("test_client")
    mylittletalos = ActionInterface("talos")
    import time
    time.sleep(0.4)
    print("controller_active: {}".format(mylittletalos.active()))
    mylittletalos.action("foo","MultiContactForceDistribution","com",[0.,0.,0.8, 0.,0.,0.,1.],2.,1.0,absolute=True)
    #mylittletalos.action("foo","MultiContactForceDistribution","contact:ContactLeftFoot",[0.],5.,1.0,absolute=True)




