#include <pbwbc/CartesianTrajectoryGenerator.hpp>

namespace pbwbc {

/////////////////////////////////////
/* CartesianTrajectoryGenerator */
/////////////////////////////////////
CartesianTrajectoryGenerator::CartesianTrajectoryGenerator(
        std::shared_ptr<Model> const & model,
        std::shared_ptr<ConfigMap> const & config
        ):
    model_(model),
    config_(config),
    trajectory_time_ratio_(0),
    trajectory_duration_(0)
    {}

CartesianTrajectoryGenerator::~CartesianTrajectoryGenerator(){}


void CartesianTrajectoryGenerator::start(timestamp_t const & timestamp){
    start_time_ = timestamp;

    start_state_.velocity.translation.setZero();
    start_state_.velocity.orientation.setZero();
    start_state_.acceleration.translation.setZero();
    start_state_.acceleration.orientation.setZero();

    final_state_.velocity.translation.setZero();
    final_state_.velocity.orientation.setZero();
    final_state_.acceleration.translation.setZero();
    final_state_.acceleration.orientation.setZero();

    run_ = false;
}

void CartesianTrajectoryGenerator::stop(timestamp_t const & timestamp){ }

        
void CartesianTrajectoryGenerator::setInitialState(CartesianState & state) {

    state_ = state;

    state_.velocity.translation.setZero();
    state_.velocity.orientation.setZero();
    state_.acceleration.translation.setZero();
    state_.acceleration.orientation.setZero();

    state_.velocity.translation.setZero();
    state_.velocity.orientation.setZero();
    state_.acceleration.translation.setZero();
    state_.acceleration.orientation.setZero();

}


CartesianState const & CartesianTrajectoryGenerator::getState() const {
    return state_;
}



void CartesianTrajectoryGenerator::start6DMotion(timestamp_t const & timestamp, const CartesianState start_point, const CartesianState final_point, double duration) {
    std::lock_guard<std::mutex> lock(state_lock_);
    start_time_ = timestamp;
    //set initial point as current point
    start_state_ = start_point;
    //set final point
    final_state_ = final_point;
    //set trajectory duration
    trajectory_duration_ = duration;
    run_ = true;
}


void CartesianTrajectoryGenerator::startTranslation(timestamp_t const & timestamp, const Eigen::Ref<const Eigen::VectorXd> & start_point, const Eigen::Ref<const Eigen::VectorXd> & final_point , double duration){
    
    std::lock_guard<std::mutex> lock(state_lock_);
    start_time_ = timestamp;
    //set initial point as current point
    start_state_.position.translation = start_point;
    //set final point
    final_state_.position.translation = final_point;
    //set trajectory duration
    trajectory_duration_ = duration;
    run_ = true;
}

void CartesianTrajectoryGenerator::startOrientation(timestamp_t const & timestamp, const Eigen::Quaterniond & start_point, const Eigen::Quaterniond & final_point , double duration){
    std::lock_guard<std::mutex> lock(state_lock_);
    start_time_ = timestamp;
    //set initial point as current point
    start_state_.position.orientation = start_point;
    //set final point
    final_state_.position.orientation = final_point;
    //set trajectory duration
    trajectory_duration_ = duration;
    run_ = true;
}


void CartesianTrajectoryGenerator::stopMotion(timestamp_t const & timestamp){
    std::lock_guard<std::mutex> lock(state_lock_);
    start_time_ = timestamp;
    trajectory_duration_ = 0.;
    run_ = false;
}


void CartesianTrajectoryGenerator::update(timestamp_t const & timestamp){

    //This minimum jerk trajectory computation assumes the following (in translation):
    //        initial position = current position, final position = desired values
    //        initial velocity = final velocity = 0
    //        initial acceleration = final acceleration = 0

    //trajectory_time_ = timestamp - start_time_;
    std::lock_guard<std::mutex> lock(state_lock_);
    trajectory_time_ratio_ = std::chrono::duration<double>(timestamp - start_time_).count() / trajectory_duration_;

    // ensure that state is updated once even if update() is called too late on a short trajectory (1ms long trajectory)
    if(trajectory_time_ratio_ > 1. && run_) {
        trajectory_time_ratio_ = 1.;
        run_ = false;
    }


    if (trajectory_duration_!=0. && trajectory_time_ratio_ <= 1) {

        trajectory_time_ratio2_ = trajectory_time_ratio_  * trajectory_time_ratio_;
        trajectory_time_ratio3_ = trajectory_time_ratio2_ * trajectory_time_ratio_;
        trajectory_time_ratio4_ = trajectory_time_ratio3_ * trajectory_time_ratio_;
        trajectory_time_ratio5_ = trajectory_time_ratio4_ * trajectory_time_ratio_;
    
    
        //for translation:
        state_.position.translation = start_state_.position.translation 
            + (final_state_.position.translation - start_state_.position.translation) 
               * ( 10 * trajectory_time_ratio3_ - 15 * trajectory_time_ratio4_ +  6 * trajectory_time_ratio5_);
    
        //derivative of the above
        state_.velocity.translation = 
            (final_state_.position.translation - start_state_.position.translation) 
                / trajectory_duration_
                * ( 30 * trajectory_time_ratio2_ - 60 * trajectory_time_ratio3_ + 30 * trajectory_time_ratio4_);
        //derivative of the above
        state_.acceleration.translation = 
            (final_state_.position.translation - start_state_.position.translation) 
                / (trajectory_duration_ * trajectory_duration_)
                * (  60 * trajectory_time_ratio_ - 180 * trajectory_time_ratio2_ + 120 * trajectory_time_ratio3_);


        //for orientation:
        // from https://en.wikipedia.org/wiki/Smoothstep
        double x = trajectory_time_ratio_;
        double rotation_ratio = x * x * x * (x * (x * 6 - 15) + 10);
        state_.position.orientation = start_state_.position.orientation.slerp(rotation_ratio, final_state_.position.orientation);
        double rotation_dratio = 30*x*x*(x*x - 2*x + 1); // d rotation_ratio / d x
        Eigen::Quaterniond delta_q = final_state_.position.orientation * start_state_.position.orientation.inverse();
        Eigen::AngleAxisd delta_aa(delta_q);
        state_.velocity.orientation = delta_aa.axis() * rotation_dratio / trajectory_duration_;
        double rotation_ddratio = 60*x*(2*x*x - 3*x + 1); // d rotation_ratio / d2 x
        state_.acceleration.orientation = delta_aa.axis() * rotation_ddratio / trajectory_duration_ / trajectory_duration_;

    }
}
        
bool CartesianTrajectoryGenerator::isRunning(timestamp_t const & timestamp) const {
    return ( timestamp < ( start_time_ + std::chrono::duration<double>(trajectory_duration_) )) ;
}


} // namespace
