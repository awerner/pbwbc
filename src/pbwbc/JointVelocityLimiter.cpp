#include <pbwbc/JointVelocityLimiter.hpp>

namespace pbwbc {


JointVelocityLimiter::JointVelocityLimiter(
		std::shared_ptr<RobotState> const & state,
		std::shared_ptr<Model> const & model,
		std::shared_ptr<ConfigMap> const & config
		) :
Controller([&](){
	std::string name;
	config->param("name",name,"JointVelocityLimiter");
	return name;}(),model->dq_size()),
state_(state),
model_(model),
config_(config),
maximum_torque_(model->stripBaseVelocity(model_->torque_max())),
velocity_max_(model->stripBaseVelocity(model_->velocity_max())),
torque_scale_(model_->dq_size()),
offset_(model_->dq_size())
{
	// Set DEFAULT settings from default values or from configuration
	double offset;
	config_->param("offset", offset, 0.);
	offset_.setConstant(offset);

	double torque_scale;
	config_->param("torque_scale", torque_scale, 0.2);
	torque_scale_.setConstant(torque_scale);

	//Get ids of velocity limited joints
	config->getParam("velocity_limited_joint_names", velocity_limited_joint_names_);
	std::vector<std::string> jointNames(model_->jointNames());
	for (std::string joint_name : velocity_limited_joint_names_)
	{
		//PBWBC_DEBUG("Looking for joint: " << joint_name);
		std::vector<std::string>::iterator it;
		it = find (jointNames.begin(), jointNames.end(), joint_name);
		if (it != jointNames.end()) {
			int idx = std::distance(jointNames.begin(), it);
			velocity_limited_joints_ids_.push_back(idx); // Get index of element from iterator
			//PBWBC_DEBUG("Added joint: " << joint_name);

			jointID_map_.insert({joint_name, idx});

			//Get settings, for each joint that is defined, from configuration
			PBWBC_DEBUG("Getting settings for joint " << joint_name);
	        config_->param("joints/"+joint_name+"/offset",
							offset_(idx),
							offset_(idx));
	        config_->param("joints/"+joint_name+"/torque_scale",
							torque_scale_(idx),
							torque_scale_(idx));
		}
	}
	dq_offset_ = model_->dy_size() - model_->dq_size();
}


JointVelocityLimiter::~JointVelocityLimiter() { }

void JointVelocityLimiter::start(timestamp_t const &) { }

void JointVelocityLimiter::stop(timestamp_t const &) { }

void JointVelocityLimiter::update(timestamp_t const &) {

	tau_d_.setZero();

	for (int idx : velocity_limited_joints_ids_) {
		if( 0 < offset_(idx) ) {
			vel_min_ = - velocity_max_(idx) + offset_(idx);
			vel_max_ =   velocity_max_(idx) - offset_(idx);
			vel_ = state_->dy(dq_offset_+idx);
			max_effort_ = torque_scale_(idx) * maximum_torque_(idx);
			if ( vel_max_ < vel_ ) {
				tau_d_(idx) +=  - max_effort_ * std::pow((vel_ - vel_max_)/offset_(idx),2.);
			} else if ( vel_ < vel_min_ ) {
				tau_d_(idx) +=    max_effort_ * std::pow((vel_min_ - vel_)/offset_(idx),2.);
			}
		}
	}
}



} // namespace pbwbc
