#include <pbwbc/ModelInComCoordinates.hpp>

namespace pbwbc {

ModelInComCoordinates::ModelInComCoordinates(
	        std::shared_ptr<Model> const & model,
	        std::shared_ptr<RobotState> const & state
        	): 
	model_(model),
    state_(state),
    Q_(model_->dy_size(),model_->dy_size()),
    lu_(model_->dy_size()),
    transformation_(model_->dy_size(),model_->dy_size()),
    jacobian_(6, model_->dy_size()),
    com_jacobian_(6, model_->dy_size()),
    massmatrix_(model_->dy_size(), model_->dy_size()) {
    }


void ModelInComCoordinates::update(timestamp_t const &) {
        PBWBC_TRACEF("MODCOM start");
        //model_->update(timestamp_t const &); // TODO: this need to hit a cache ---> Right now, assumes that model is up to date
	    Q_.setZero();
        Q_.middleRows(0,3) = model_->comJacobian();
        // The next line depends on the setup of the configuration vector
        // for pinocchio the Identity mapping should be correct here as
        // the velocities before and after the coordinate transformation
        // are velocities in the local frame.
        // TODO: Check this
        Q_.middleRows(3,3).middleCols(3,3).setIdentity();
        Q_.block(6,6,model_->dq_size(),model_->dq_size()).setIdentity();
        lu_.compute(Q_);
        //A = PLU
        // U.inverse() * L.inverse() * P.inverse()
        //    .triangularView<StrictlyLower>()
        //lu_.matrixLU().triangularView<StrictlyLower>();
#       ifdef EIGEN_RUNTIME_NO_MALLOC
        bool was_malloc_allowed = Eigen::internal::is_malloc_allowed();
        Eigen::internal::set_is_malloc_allowed(true);
#       endif 
        transformation_ = lu_.inverse(); // TODO: could make this sparse
#       ifdef EIGEN_RUNTIME_NO_MALLOC
        Eigen::internal::set_is_malloc_allowed(was_malloc_allowed);
#       endif
       // TODO: fix this memory allocation 
        PBWBC_TRACEF("MODCOM end");
}

int ModelInComCoordinates::y_size()  { return model_->y_size(); }
int ModelInComCoordinates::dy_size()  { return model_->dy_size(); }
int ModelInComCoordinates::q_size()  { return model_->q_size(); }
int ModelInComCoordinates::dq_size()  { return model_->dq_size(); }
int ModelInComCoordinates::contact_size()  { return model_->contact_size(); }

ModelInComCoordinates::translation_t ModelInComCoordinates::com() { return model_->com(); }
ModelInComCoordinates::translation_t ModelInComCoordinates::dcom() { return model_->dcom(); }
ModelInComCoordinates::translation_t ModelInComCoordinates::bodyTranslation(int body_id)  { return model_->bodyTranslation(body_id); }
ModelInComCoordinates::orientation_t ModelInComCoordinates::bodyOrientation(int body_id)  { return model_->bodyOrientation(body_id); }

double ModelInComCoordinates::mass() { return model_->mass(); }

ModelInComCoordinates::matrix_t const & ModelInComCoordinates::MassMatrix() {
    massmatrix_.noalias() = transformation_.transpose() * model_->MassMatrix() * transformation_;
    return massmatrix_;
}

ModelInComCoordinates::rowmatrix_t const & ModelInComCoordinates::InvMassMatrix() {
    throw std::runtime_error("not implemented");
}

ModelInComCoordinates::matrix_t const & ModelInComCoordinates::Coriolis() {
    //C_out = invQ' * (C_in * invQ + M_in * dinvQ);
    throw std::runtime_error("not implemented");
}

ModelInComCoordinates::vector_t const & ModelInComCoordinates::gravity() {
    //g_out = invQ' * g_in;
    throw std::runtime_error("not implemented");
}

ModelInComCoordinates::matrix_t const & ModelInComCoordinates::comJacobian() {
     com_jacobian_.noalias() = model_->comJacobian() * transformation_; //TODO: check this
     return com_jacobian_;
}

void ModelInComCoordinates::getJacobian(int frame_id, Eigen::Ref<Eigen::MatrixXd> jac ) {
    jacobian_.setZero();
    model_->getJacobian(frame_id, jacobian_);
    jac.noalias() = jacobian_ * transformation_;
}

void ModelInComCoordinates::getJacobianWorld(int frame_id, Eigen::Ref<Eigen::MatrixXd> jac ) {
    jacobian_.setZero();
    model_->getJacobianWorld(frame_id, jacobian_);
    jac.noalias() = jacobian_ * transformation_;
}

int ModelInComCoordinates::getFrameId(std::string const & frame_name) { return model_->getFrameId(frame_name); }
std::string const & ModelInComCoordinates::getFrameName(int frameId) { return model_->getFrameName(frameId); }
std::vector<int> ModelInComCoordinates::getJointFrames() { return model_->getJointFrames(); }
std::vector<int> ModelInComCoordinates::getLinkFrames() { return model_->getLinkFrames(); }

std::vector<std::string> const & ModelInComCoordinates::jointNames() { return model_->jointNames(); }
ModelInComCoordinates::vector_t const & ModelInComCoordinates::position_max() { return model_->position_max(); }
ModelInComCoordinates::vector_t const & ModelInComCoordinates::position_min() { return model_->position_min(); }
ModelInComCoordinates::vector_t const & ModelInComCoordinates::velocity_max() { return model_->velocity_max(); }
ModelInComCoordinates::vector_t const & ModelInComCoordinates::torque_max() { return model_->torque_max(); }

void ModelInComCoordinates::setState(std::shared_ptr<RobotState> const & state) { model_->setState(state); }

void ModelInComCoordinates::setBasePosition(
    translation_t const & translation,
    orientation_t const & orientation,
    RobotState & state) { model_->setBasePosition(translation, orientation, state); }

void ModelInComCoordinates::setBaseVelocity(
    translation_t const & translation,
    translation_t const & orientation,
    RobotState & state) { model_->setBaseVelocity(translation,orientation, state); }

Eigen::Ref<const Eigen::VectorXd> const ModelInComCoordinates::stripBasePosition(Eigen::Ref<const Eigen::VectorXd> const & unfiltered) {
        return model_->stripBasePosition(unfiltered);
}

Eigen::Ref<const Eigen::VectorXd> const ModelInComCoordinates::stripBaseVelocity(Eigen::Ref<const Eigen::VectorXd> const & unfiltered) {
        return model_->stripBaseVelocity(unfiltered);
}

} // namespace
