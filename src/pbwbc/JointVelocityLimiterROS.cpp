#include <pbwbc/JointVelocityLimiterROS.hpp>

namespace pbwbc {


JointVelocityLimiterROS::JointVelocityLimiterROS(
            std::shared_ptr<RobotState> const & state,
            std::shared_ptr<Model> const & model,
            std::shared_ptr<ConfigMap> const & config,
            ros::NodeHandle const & nh
            ) :
    JointVelocityLimiter(state,model,config),
    nh_(nh)
    {
        desired_torque_publisher_ = RealTimePublisherBackEnd::advertise<sensor_msgs::JointState>(nh_,"desired_torque",1000,10);
        desired_torque_publisher_.msg().name = model_->jointNames();
        desired_torque_publisher_.msg().effort.resize(model_->q_size());
        desired_torque_publisher_.fillBuffer();

        ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure(nh_));

        for (std::string joint_name : velocity_limited_joint_names_) {

            int idx = jointID_map_.at(joint_name);
            std::stringstream str_offset; str_offset << "offset_" << joint_name;
            std::stringstream str_torque_scale; str_torque_scale << "torque_scale_" << joint_name;

            ddr_->RegisterVariable(&offset_(idx), str_offset.str(), 0., velocity_max_(idx));
            ddr_->RegisterVariable(&torque_scale_(idx), str_torque_scale.str(), 0., 1.);
        }

        ddr_->PublishServicesTopics();
    }

JointVelocityLimiterROS::~JointVelocityLimiterROS() {}

    
void JointVelocityLimiterROS::update(timestamp_t const & timestamp) {
    ros::Time now(timestamp.time_since_epoch().count());

    JointVelocityLimiter::update(timestamp);
    // publish command
    if(desired_torque_publisher_.trylock()) {
        desired_torque_publisher_.msg().header.stamp = now;
        std::copy_n(&tau_d_(0),
                model_->q_size(),
                &desired_torque_publisher_.msg().effort.at(0));
        desired_torque_publisher_.unlockAndPublish();
    }
}

} // namespace pbwbc
