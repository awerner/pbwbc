#include <pbwbc/JointTrajectoryGenerator.hpp>

namespace pbwbc {

JointTrajectoryGenerator::JointTrajectoryGenerator(
        std::shared_ptr<Model> const & model,
        std::shared_ptr<ConfigMap> const & config):
	model_(model),
	config_(config),
	trajectory_time_ratio_(0),
    trajectory_duration_(0),
    start_state_(model->y_size(),model->dy_size()),
    final_state_(model->y_size(),model->dy_size()),
    state_(model->y_size(),model->dy_size())
    {}

JointTrajectoryGenerator::~JointTrajectoryGenerator(){}

void JointTrajectoryGenerator::start(timestamp_t const & timestamp) {
	start_time_ = timestamp;

	start_state_.dy.setZero();
	start_state_.ddy.setZero();

	final_state_.dy.setZero();
	final_state_.ddy.setZero();
}

void JointTrajectoryGenerator::stop(timestamp_t const & timestamp) {}

void JointTrajectoryGenerator::update(timestamp_t const & timestamp) {
	//This minimum jerk trajectory computation assumes the following (in translation):
    //        initial position = current position, final position = desired values
    //        initial velocity = final velocity = 0
    //        initial acceleration = final acceleration = 0

    //trajectory_time_ = timestamp - start_time_;
    trajectory_time_ratio_ = std::chrono::duration<double>(timestamp - start_time_).count() / trajectory_duration_;


    if (trajectory_time_ratio_ <= 1) {

        trajectory_time_ratio2_ = trajectory_time_ratio_  * trajectory_time_ratio_;
        trajectory_time_ratio3_ = trajectory_time_ratio2_ * trajectory_time_ratio_;
        trajectory_time_ratio4_ = trajectory_time_ratio3_ * trajectory_time_ratio_;
        trajectory_time_ratio5_ = trajectory_time_ratio4_ * trajectory_time_ratio_;
    
        //TODO: only act on tail(model_->q_size())

        //for translation:
        state_.y = start_state_.y + (final_state_.y - start_state_.y) * ( 10 * trajectory_time_ratio3_
		                                                                - 15 * trajectory_time_ratio4_ 
		                                                                +  6 * trajectory_time_ratio5_);
    
        //derivative of the above
        state_.dy.tail(model_->dq_size()) = (final_state_.y.tail(model_->dq_size()) 
                                          - start_state_.y.tail(model_->dq_size())) / trajectory_duration_  * ( 30 * trajectory_time_ratio2_
                            	                                                                              - 60 * trajectory_time_ratio3_
                            	                                                                              + 30 * trajectory_time_ratio4_);
        //derivative of the above
        state_.ddy.tail(model_->dq_size()) = (final_state_.y.tail(model_->dq_size()) 
                                            - start_state_.y.tail(model_->dq_size())) / (trajectory_duration_ * trajectory_duration_)  * ( 60 * trajectory_time_ratio_ 
                                		                                                                                                 - 180 * trajectory_time_ratio2_
                                		                                                                                                 + 120 * trajectory_time_ratio3_);
    }
}

void JointTrajectoryGenerator::setInitialState(RobotState & state) {
	state_ = state;

	state_.dy.setZero();
	state_.ddy.setZero();
}

void JointTrajectoryGenerator::startMotion(timestamp_t const & timestamp, const RobotState start_point, const RobotState final_point, double duration) {
	start_time_ = timestamp;
	//set initial point as current point
    start_state_ = start_point;
    //set final point
    final_state_ = final_point;
    //set trajectory duration
    trajectory_duration_ = duration;
}

void JointTrajectoryGenerator::stopMotion(timestamp_t const & timestamp) {
	start_time_ = timestamp;
	trajectory_duration_ = 0.;
}

RobotState const & JointTrajectoryGenerator::getState() const {
	return state_;
}

bool JointTrajectoryGenerator::isRunning(timestamp_t const & timestamp) const {
	return ( timestamp > ( start_time_ + std::chrono::duration<double>(trajectory_duration_) )) ;
}


} //namespace