#include <pbwbc/RealSenseStateEstimation.hpp>
#include <pbwbc/Controller.hpp>
#ifdef HAVE_LIBREALSENSE
#include <librealsense2/rs.hpp>     // Include RealSense Cross Platform API
#endif // HAVE_LIBREALSENSE

namespace pbwbc {

#ifdef HAVE_LIBREALSENSE
class rs2_camera_details {
public:
        std::shared_ptr<rs2::config> cfg;
        std::shared_ptr<rs2::pipeline> pipeline;
};

class rs2_details {
public:
    std::shared_ptr<rs2::context> ctx_;
};

class rs2_frame : public rs2::frame {
public:
    rs2_frame(rs2::frame const & f) : rs2::frame(f) {}

};
#endif // HAVE_LIBREALSENSE

RealSenseStateEstimation::RealSenseStateEstimation(
        std::shared_ptr<ConfigMap> const & config,
        std::shared_ptr<Model> const & model,
        std::shared_ptr<SensorState> const & sensor_state
        ) :
    model_(model),
    robotstate_(new RobotState(model->y_size(),model->dy_size())),
    sensor_state_(sensor_state)
{
#   ifndef HAVE_LIBREALSENSE
    throw std::runtime_error("librealsense2 features not compiled in");
#   else
    std::vector<std::string> camera_names;
    config->list_items("Cameras", camera_names);
    for(auto const & camera_name : camera_names) {
        PBWBC_DEBUG("Creating camera " << camera_name);
        auto camera_config = config->submap("Cameras")->submap(camera_name);
        std::string serial_no;
        if(!camera_config->getParam("serial_no",serial_no))
            throw std::runtime_error("serial_no missing for camera");
        CameraBuffer b;
        PBWBC_DEBUG("Creating entry for camera with serial_no: " << serial_no);
        b.name = camera_name;
        b.serial_no = serial_no;

        camera_config->param("B_T_C_translation",
                b.B_T_C_p,Eigen::VectorXd::Zero(3));
        PBWBC_DEBUG("B_T_C_translation: " << b.B_T_C_p.transpose());
        camera_config->param("B_T_C_orientation",
                b.B_T_C_q.coeffs(),
                Eigen::Quaterniond::Identity().coeffs());
        PBWBC_DEBUG("B_T_C_orientation: " << b.B_T_C_q.coeffs().transpose());

        b.timestamp = 0;
        b.counter_until_valid = 0;
        b.found = false;
        b.tracker_max_confidence = 0;
        b.initialized = false;
        int enabled;
        camera_config->param("enabled",enabled,1);
        b.enabled = enabled;
        camera_config->param("mapping_enabled",enabled,1);
        b.mapping_enabled = enabled;

        camera_buffers_.push_back(std::move(b));
    }

    config->param("start_state_translation",
            start_state_.position.translation,Eigen::VectorXd::Zero(3));
    config->param("start_state_orientation",
            start_state_.position.orientation.coeffs(),
            Eigen::Quaterniond::Identity().coeffs());
    start_state_.velocity.translation.setZero();
    start_state_.velocity.orientation.setZero();
    PBWBC_DEBUG("state_state_translation " << start_state_.position.translation.transpose());
    PBWBC_DEBUG("state_state_orientation " << start_state_.position.orientation.coeffs().transpose());
    state_ = start_state_;
    reset_state_ = false;
    
    config->param("position_translation_gain", position_translation_gain_, 1);
    config->param("velocity_translation_gain", velocity_translation_gain_, 1.);
    config->param("orientation_gain", orientation_gain_, 1.);
    config->param("translation_rejection_threshold", translation_rejection_threshold_, 0.5); //0.1
    config->param("orientation_rejection_threshold", orientation_rejection_threshold_, 2.); //0.3
    config->param("W_T_H_gain", W_T_H_gain_, 1e-2); // must be significantly lower that state gains

    // TODO: implement identification mode where linear acceleration vectors and angular rates of all imus are
    // recorded and aligned, this should give good orientation alignment
    // TODO: implement identification mode where translation offsets of cameras are properly identified

    std::string imu_name;
    if(!config->getParam("imu",imu_name)) {
        throw std::runtime_error("imu not specified");
    }
    auto imu_itr = std::find_if(
            sensor_state_->imuStates.begin(),
            sensor_state_->imuStates.end(),
            [&](std::tuple<int,std::string,ImuState> const &x) {
                return std::get<1>(x)==imu_name;
            });
    if(imu_itr==sensor_state_->imuStates.end()) {
        throw std::runtime_error("specified imu not found");
    }
    imu_idx_ = std::distance(
            sensor_state_->imuStates.begin(),
            imu_itr);
    std::string base_link_name;
    if(!config->getParam("base_link",base_link_name)) {
        throw std::runtime_error("base_link not specified");
    }
    base_link_idx_ = model_->getFrameId(base_link_name);

    // Start a streaming pipe per each connected device
    //std::this_thread::sleep_for(std::chrono::milliseconds(100));
    rs2_ = std::shared_ptr<rs2_details>(new rs2_details);
    
    rs2_->ctx_ = std::shared_ptr<rs2::context>(new rs2::context);
    for ( auto && dev : rs2_->ctx_->query_devices() ) {
        //PBWBC_DEBUG("Got camera with serial no: " << dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
        for(auto & buffer : camera_buffers_) {
            if(buffer.serial_no == dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER)) {
                PBWBC_DEBUG("Found camera " << buffer.name);
                std::shared_ptr<rs2::pipeline> pipe(new rs2::pipeline(*rs2_->ctx_));
                buffer.d.reset(new rs2_camera_details);
                buffer.d->cfg = std::shared_ptr<rs2::config>(new rs2::config);
                buffer.d->cfg->enable_device(dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
                buffer.d->cfg->enable_stream(RS2_STREAM_POSE, RS2_FORMAT_6DOF);
                auto sensor = dev.first<rs2::pose_sensor>();
                sensor.set_option(RS2_OPTION_ENABLE_MAPPING, buffer.mapping_enabled==1?true:false);
                if(buffer.mapping_enabled) {
                    sensor.set_option(RS2_OPTION_ENABLE_POSE_JUMPING, false);
                    sensor.set_option(RS2_OPTION_ENABLE_RELOCALIZATION, false); // TODO: makes only sense with a loaded map
                }
                //std::vector<uint8_t> localization_map(1,0); // TODO: load,save map from file
                //sensor.import_localization_map(localization_map); // TODO: need minimal empty map
                //auto empty_map = sensor.export_localization_map();
                //for(auto const & e : empty_map)std::cout<< static_cast<int>(e) <<std::endl;
                buffer.d->pipeline = pipe;
                buffer.found = true;
                break;
            }
        }
    }
    for(auto itr=camera_buffers_.begin();itr<camera_buffers_.end();) {
        if(!itr->found) {
            PBWBC_DEBUG("Camera " << itr->name << " with serial_no " << itr->serial_no << " not found, removing it");
            itr = camera_buffers_.erase(itr);
        } else {
            itr++;
        }
    }
    if(camera_buffers_.size()==0) {
        PBWBC_DEBUG("No cameras found");
        throw std::runtime_error("No cameras found");
    }
    for(auto & buffer : camera_buffers_) {
        if(!buffer.enabled) {    
            PBWBC_DEBUG("Camera " << buffer.name << " disabled");
        }
        PBWBC_DEBUG("Starting pipeline for camera " << buffer.name);
        buffer.d->pipeline->start(*buffer.d->cfg,
                [&,this](rs2::frame const & f){this->camera_callback(buffer,f);});
        PBWBC_DEBUG("Started pipeline for camera " << buffer.name);
    }
    // block until first sample received
    std::vector<bool> locked_on(camera_buffers_.size(),false);
    PBWBC_DEBUG("Waiting for cameras to provide data with required confidence level");
    for(int idx=0;idx<4000;idx++) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1)); // TODO: condition var
        std::lock_guard<std::mutex> lock(lock_);
        std::size_t cam_idx = 0;
        for(auto & buffer : camera_buffers_) {
            if(buffer.timestamp!=0 && buffer.tracker_confidence >= 0x02 && locked_on.at(cam_idx)==false) {
                PBWBC_DEBUG("Camera locked on: " << buffer.name);
                locked_on.at(cam_idx) = true;
            }
            cam_idx++;
        }
        if(std::all_of(locked_on.begin(),locked_on.end(),[](bool u){return u;})) {
            PBWBC_DEBUG("All cameras locked on.");
            break;
        }
    }
    for(auto const & buffer : camera_buffers_) {
        if(buffer.timestamp==0 || buffer.tracker_confidence < 0x02) {
            PBWBC_DEBUG("Camera " << buffer.name << " did not provide data with the required confidence level during initialization");
        }
    }
#   endif // HAVE_LIBREALSENSE
}

RealSenseStateEstimation::~RealSenseStateEstimation(){
#   ifdef HAVE_LIBREALSENSE
     PBWBC_DEBUG("Stopping camera streams");
     for(auto & buffer : camera_buffers_) {
         if(buffer.d && buffer.d->pipeline) {
            buffer.d->pipeline->stop();
         }
     }
#   endif
}
    
CartesianState const & RealSenseStateEstimation::getState() const {
    return state_;
}

CartesianState & RealSenseStateEstimation::getStartState() {
    return start_state_;
}

void RealSenseStateEstimation::start(timestamp_t const & timestamp) {
    // update state_ 
    state_ = start_state_;

    // set world orientation correctly from base imu acceleration
    auto const & imu_data = std::get<2>(sensor_state_->imuStates.at(imu_idx_));
    const int imu_link_idx = std::get<0>(sensor_state_->imuStates.at(imu_idx_));
    // acceleration in base from:
    Eigen::Vector3d B_A_G = 
        model_->bodyOrientation(base_link_idx_).toRotationMatrix().transpose() * (
                (model_->bodyOrientation(imu_link_idx).toRotationMatrix() * imu_data.linearAcceleration));
       // note: any base orientation cancels itself out here
    if(B_A_G.norm() < (9.81-0.5) || B_A_G.norm() > (9.81+0.5)) {
        throw std::runtime_error("Robot not at rest or acceleration vector incorrect");
    }
    B_A_G /= 9.81;
    B_A_G.normalize();

    state_.position.orientation = Eigen::Quaterniond::FromTwoVectors(Eigen::Vector3d::UnitZ(),B_A_G).inverse();
    //PBWBC_DEBUG("state_.position.orientation " << state_.position.orientation.coeffs().transpose());

    for( auto & b : camera_buffers_ ) {
        b.initialized = false;
        b.timestamp = 0;
    }

    robotstate_->y.setZero(); // TODO: assign start state so this is always valid
    robotstate_->dy.setZero();
    last_update_ = timestamp;
    last_correction_ = timestamp;
}

void RealSenseStateEstimation::stop(timestamp_t const &) {
}

void RealSenseStateEstimation::update(timestamp_t const & timestamp) {
#   ifdef HAVE_LIBREALSENSE
    //PBWBC_DEBUG("##### UPDATE ####");
    const static Eigen::IOFormat CSVFormat(Eigen::FullPrecision, Eigen::DontAlignCols, ",", "\n");
    const static Eigen::IOFormat iof(3);
    // FILTER PREDICTION
    // compute delta_t since last update (detect missed iterations)
    const double delta_t = std::chrono::duration<double>(
            timestamp - last_update_).count();
    last_update_ = timestamp;
    std::lock_guard<std::mutex> lock(lock_); // TODO: this should use double buffering
    if(reset_state_) {
        PBWBC_DEBUG("Reset state");
        state_ = start_state_;
        PBWBC_DEBUG("state_.position.translation = " << state_.position.translation.transpose());
        PBWBC_DEBUG("state_.position.orientation = " << state_.position.orientation.coeffs().transpose());
        reset_state_ = false;

        for( auto & s: camera_buffers_ ) {
            s.initialized = false;
            s.timestamp = 0;
        }
    }

    // integrate filter state
    state_.position.translation += state_.velocity.translation * delta_t;
    state_.position.orientation = integrate_quaternion(state_.velocity.orientation,state_.position.orientation,delta_t);
    if(std::abs(state_.position.orientation.norm()-1.) > 1e-5) {
        state_.position.orientation.normalize();
        PBWBC_DEBUG("Quaternion normalize");
    }
    
    // FOR TESTING ONLY: use first camera as acceleration source
    //if(camera_buffers_.at(0).timestamp!=0) {
    //    state_.velocity_translation += delta_t * 
    //        camera_buffers_.at(0).W_T_H_q.toRotationMatrix() *
    //        camera_buffers_.at(0).sensor_acceleration; // TODO: effects of angular_velocity
    //    state_.angular_velocity =
    //        camera_buffers_.at(0).W_T_H_q.toRotationMatrix() *
    //        camera_buffers_.at(0).state.angular_velocity; // TODO: orientation error is still large when sensors are turned quickly
    //}
    // NORMAL
    auto const & imu_data = std::get<2>(sensor_state_->imuStates.at(imu_idx_));
    Eigen::Vector3d gravity;
    gravity << 0., 0., 9.81;
    const int imu_link_idx = std::get<0>(sensor_state_->imuStates.at(imu_idx_));
    // TODO: remove gravity inside fusion + bias observer
    Eigen::Vector3d acceleration = 
        model_->bodyOrientation(base_link_idx_).toRotationMatrix().transpose() * (
                (model_->bodyOrientation(imu_link_idx).toRotationMatrix() * imu_data.linearAcceleration) - gravity);
    state_.velocity.translation += state_.position.orientation.toRotationMatrix() * acceleration * delta_t;
    // TODO: terms missing of acceleration transform
    //state_.orientation = imu_orientation_getter_(); // if desired
    //angular velocity from imu
    state_.velocity.orientation = model_->bodyOrientation(base_link_idx_).toRotationMatrix().transpose() *
        model_->bodyOrientation(imu_link_idx) * imu_data.angularVelocity;
    // TODO: missing hip joint velocities

    if(logfile_) {
        std::chrono::duration<double> ts = timestamp.time_since_epoch();
        *logfile_ << 
            ts.count() << "," << delta_t << "," << 
            state_.position.translation.transpose().format(CSVFormat) << "," <<
            state_.position.orientation.toRotationMatrix().eulerAngles(2,1,0).transpose().format(CSVFormat) << "," <<
            state_.velocity.translation.transpose().format(CSVFormat) << "," <<
            state_.velocity.orientation.transpose().format(CSVFormat);
        for( auto const & s : camera_buffers_ ) {
            auto R = s.W_T_H_q.toRotationMatrix();
            *logfile_ << "," <<
                (R * s.state.position.translation).transpose().format(CSVFormat) << "," <<
                /*TODO*/s.state.position.orientation.toRotationMatrix().eulerAngles(2,1,0).transpose().format(CSVFormat) << "," <<
                (R * s.state.velocity.translation).transpose().format(CSVFormat) << "," <<
                (R * s.state.velocity.orientation).transpose().format(CSVFormat);
        }
        *logfile_ << std::endl;
    }
    
    // FOR TESTING only: orientation from camera
    //{
    //    auto const & s = camera_buffers_.at(0);
    //    Eigen::Isometry3d W_T_H = TfromVQ(s.W_T_H_p,s.W_T_H_q);
    //    Eigen::Isometry3d B_T_C = TfromVQ(s.B_T_C_p,s.B_T_C_q);
    //    Eigen::Isometry3d H_T_C = TfromVQ(s.state.position_translation,s.state.orientation);
    //    Eigen::Isometry3d W_T_B = W_T_H * H_T_C * B_T_C.inverse();
    //    state_.orientation = Eigen::Quaterniond(W_T_B.linear());
    //    state_.angular_velocity = W_T_H.linear() * camera_buffers_.at(0).state.angular_velocity;
    //}
        


    // FILTER CORRECTION
    for ( auto & s : camera_buffers_ ) {
        if(!s.enabled) {
            continue;
        }
        if(s.timestamp==0) {
            // TODO: check for old timestamp and reject
            PBWBC_DEBUG("Rejecting camera " << s.name << " no data available");
            continue;
        }
        bool accept = true;
        // if tracker certainty is not good, status=reject
        if (s.tracker_confidence < s.tracker_max_confidence) {
            if(s.counter_until_valid<100) {
                PBWBC_DEBUG("Rejecting camera " << s.name << " confidence below threshold");
            }
            accept = false;
        }
        // compute base pose in the world using sensor data
        Eigen::Isometry3d W_T_H = TfromVQ(s.W_T_H_p,s.W_T_H_q);
        Eigen::Isometry3d B_T_C = TfromVQ(s.B_T_C_p,s.B_T_C_q);
        Eigen::Isometry3d H_T_C = TfromVQ(s.state.position.translation,s.state.position.orientation);
        Eigen::Isometry3d W_T_B = W_T_H * H_T_C * B_T_C.inverse();

        //PBWBC_DEBUG("W_T_H:\n" << W_T_H.matrix().format(iof));
        //PBWBC_DEBUG("B_T_C:\n" << B_T_C.matrix().format(iof));
        //PBWBC_DEBUG("H_T_C:\n" << H_T_C.matrix().format(iof));
        //PBWBC_DEBUG("W_T_B:\n" << W_T_B.matrix().format(iof));

        // compute base velocity in the world using sensor data
        Vector6d H_V_B;
        H_V_B.head(3) = s.state.velocity.translation - skew(H_T_C.linear() * B_T_C.inverse().translation()) * s.state.velocity.orientation;
        H_V_B.tail(3) = s.state.velocity.orientation;
        // Project tracker data into correct frame
        Vector6d W_V_B;
        W_V_B.head(3) = W_T_H.linear() * H_V_B.head(3);
        W_V_B.tail(3) = W_T_H.linear() * H_V_B.tail(3);

        // compare the estimated state to this sensors if too far away => reject
        if ( (state_.position.translation - W_T_B.translation()).norm() > translation_rejection_threshold_ ||
                state_.position.orientation.angularDistance(
                    Eigen::Quaterniond(W_T_B.linear())) > orientation_rejection_threshold_ ) {
            if(s.counter_until_valid<100) {
                PBWBC_DEBUG("Camera " << s.name << ": correction rejected due to high delta: translation: " <<
                        (state_.position.translation - W_T_B.translation()).transpose().norm()
                        << " angular: " << state_.position.orientation.angularDistance(
                            Eigen::Quaterniond(W_T_B.linear())));
                PBWBC_DEBUG("   state_.position.translation " << state_.position.translation.transpose());
                PBWBC_DEBUG("   W_T_B.translation " << W_T_B.translation().transpose());
            }
            accept = false;
        }

        if(s.counter_until_valid==0 && accept) { // TODO: log if camera is used in logfile
            //PBWBC_DEBUG("Running Update");
            // perform correction with fixed gain
            state_.position.translation += position_translation_gain_ * delta_t 
                            * (W_T_B.translation() - state_.position.translation);
            state_.velocity.translation += velocity_translation_gain_ * delta_t
                            * (W_V_B.head(3) - state_.velocity.translation); 

            state_.position.orientation = state_.position.orientation.slerp( // TODO: correct slerp direction?
                    std::min(orientation_gain_ * delta_t,1.) ,Eigen::Quaterniond(W_T_B.linear()));
            //state.velocity_orientation += velocity_orientation_gain * (source_state.velocity_orientation - source_state.velocity_orientation); 
        }

        if(s.counter_until_valid==0 && !accept) {
            PBWBC_DEBUG("Camera " << s.name << " dropped out, starting recovery");
            // and reject for the comming 200 samples
            // to give the tracking camera state estimation time to recover
            // and the world offset to be correctly estimated
            s.counter_until_valid = 200;
        }
        if(s.counter_until_valid>0) {
            if(s.counter_until_valid==100) {
                PBWBC_DEBUG("Camera " << s.name << " W_T_H reset (recovery)");
                // update W_T_H so that camera information matches state
                auto W_T_B_s = TfromVQ(state_.position.translation,state_.position.orientation);
                Eigen::Isometry3d W_T_H = W_T_B_s * B_T_C * H_T_C.inverse();

                s.W_T_H_p = W_T_H.translation();
                s.W_T_H_q = Eigen::Quaterniond(W_T_H.linear());
            }
            if(s.counter_until_valid<100 && !accept) {
                PBWBC_DEBUG("Camera " << s.name << " bad reading during test period, cntr: " << s.counter_until_valid
                        << " (recovery)");
                // monitor error for next 100 frames to verify that camera has locked on again
                // if tracker_confidence or translation error wait another 200
                s.counter_until_valid = 200;
            }

            s.counter_until_valid--;
            if(s.counter_until_valid==0) {
                PBWBC_DEBUG("Camera " << s.name << " locked on again (recovery)");
            }
        } else {
            Eigen::Isometry3d W_T_B_s = TfromVQ(state_.position.translation,state_.position.orientation);
            // update world offsets slowly to reduce error
            // to avoid numerical creep this is not done on the first camera
            if(&s!=&camera_buffers_.at(0) && 
                    camera_buffers_.at(0).timestamp!=0 && 
                    camera_buffers_.at(0).tracker_confidence==0x03) {
                Eigen::Isometry3d W_T_H = W_T_B_s * B_T_C * H_T_C.inverse();
                s.W_T_H_p += W_T_H_gain_ * delta_t * (W_T_H.translation() - s.W_T_H_p);
                s.W_T_H_q = s.W_T_H_q.slerp(
                        std::min(W_T_H_gain_ * delta_t,1.), Eigen::Quaterniond(W_T_H.linear()));
            }
            
            // compute camera offset w.r.t. base using current state
            // compute base pose in the world using sensor data
            // TODO: reset this when tracking is lost
            // Better do this offline
            //Eigen::Isometry3d B_T_C_o = W_T_B_s.inverse() * W_T_H * H_T_C;
            //if(&s==&camera_buffers_.at(0)) {
            //    thread_local Eigen::Vector3d b = Eigen::Vector3d::Zero();
            //    thread_local std::size_t cntr = 0;
            //    b += 1e-3 * ((B_T_C_o.translation() - B_T_C.translation()) - b);
            //    if(cntr % 1000 == 0) {
            //        PBWBC_DEBUG("offset: " << b.transpose().format(iof));
            //    }
            //    cntr++;
            //}
        }
    }
    if(std::all_of(camera_buffers_.begin(), camera_buffers_.end(), 
                [](CameraBuffer const &b ){ return b.timestamp==0 || !b.enabled || b.counter_until_valid>0;})) {
        PBWBC_DEBUG("No tracking source active");
    } else {
        last_correction_ = timestamp;
    }
    if( std::chrono::duration<double>(timestamp - last_correction_).count() > 0.02 ) {
        // triggers when four samples lost (200Hz -> 5ms)
        throw ControllerException("Lost tracking on all cameras");
    }

    // TODO: check frames
    model_->setBasePosition(state_.position.translation,
                            state_.position.orientation,
                            *robotstate_);
    model_->setBaseVelocity(state_.velocity.translation,
                            state_.velocity.orientation,
                            *robotstate_);
#   endif // HAVE_LIBREALSENSE
}

std::shared_ptr<RobotState> const & RealSenseStateEstimation::state() {
    return robotstate_;
}

void RealSenseStateEstimation::enable_log(std::string const & filename) {
    logfile_ = std::shared_ptr<std::ofstream>(new std::ofstream(filename));
    logfile_->setf( std::ios::fixed, std:: ios::floatfield );
    logfile_->precision(14);
    *logfile_ << "time,delta_t,"
        << "state_position.x,state_position.y,state_position.z,"
        << "state_orientation.roll,state_orientation.pitch,state_orientation.yaw,"
        << "state_velocity.x,state_velocity.y,state_velocity.z,"
        << "state_angvel.x,state_angvel.y,state_angvel.z";
    for( auto const & s : camera_buffers_ ) {
        *logfile_ << "," <<
            "cam_" << s.name << "_position.x," <<
            "cam_" << s.name << "_position.y," <<
            "cam_" << s.name << "_position.z," <<
            "cam_" << s.name << "_orientation.roll," <<
            "cam_" << s.name << "_orientation.pitch," <<
            "cam_" << s.name << "_orientation.yaw," <<
            "cam_" << s.name << "_velocity.x," <<
            "cam_" << s.name << "_velocity.y," <<
            "cam_" << s.name << "_velocity.z," <<
            "cam_" << s.name << "_angvel.x," <<
            "cam_" << s.name << "_angvel.y," <<
            "cam_" << s.name << "_angvel.z";
    }
    *logfile_ << std::endl;
}

void RealSenseStateEstimation::camera_callback(CameraBuffer & buffer,rs2_frame const & frame) {
#   ifdef HAVE_LIBREALSENSE
    try {
        // Cast the frame to pose_frame and get its data
        //auto p = f.as<rs2::pose_frame>().get_pose_data();
        rs2_pose p;
        double timestamp = 0.;
        bool data_good = false;
        if (rs2::frameset fs = frame.as<rs2::frameset>()) {
            // With callbacks, all synchronized stream will arrive in a single frameset
            for (rs2::frame const & f : fs) {
                if(rs2::pose_frame pf = f.as<rs2::pose_frame>()) {
                    p = pf.get_pose_data();
                    timestamp = pf.get_timestamp();
                    data_good = true;
                }
            }
        } else if (rs2::pose_frame pf = frame.as<rs2::pose_frame>()) {
            p = pf.get_pose_data();
            timestamp = pf.get_timestamp();
            data_good = true;
        }
        if(!data_good) {
            return;
        }
        std::lock_guard<std::mutex> lock(lock_);
        if(std::isnan(p.translation.x) || std::isnan(p.rotation.w)) {
            PBWBC_DEBUG("Camera " << buffer.name << " reports NaN");
            buffer.initialized = false;
            buffer.timestamp = 0;
            buffer.counter_until_valid = 200;
            // TODO: trigger pipeline stop/start if this happens to recover (not clear if works)
            return;
        }
        buffer.state.position.translation << p.translation.x, p.translation.y, p.translation.z;
        buffer.state.position.orientation = Eigen::Quaterniond(p.rotation.w,p.rotation.x,p.rotation.y,p.rotation.z);
        buffer.state.velocity.translation << p.velocity.x, p.velocity.y, p.velocity.z;
        // velocity_translation is in the world frame
        buffer.state.velocity.orientation << p.angular_velocity.x, p.angular_velocity.y, p.angular_velocity.z;
        // angular_velocity is in the world frame
        buffer.sensor_acceleration << p.acceleration.x, p.acceleration.y, p.acceleration.z;
        buffer.tracker_confidence = p.tracker_confidence;
        buffer.mapper_confidence = p.mapper_confidence;
        if( buffer.tracker_max_confidence < buffer.tracker_confidence ) {
            buffer.tracker_max_confidence = buffer.tracker_confidence;
        }

        if(!buffer.initialized) { // TODO: this requires that all cameras start more or less at the same time
            PBWBC_DEBUG("Camera " << buffer.name << " resetting W_T_H");
            auto W_T_B = TfromVQ(state_.position.translation,state_.position.orientation);
            PBWBC_DEBUG(" ... with W_T_B\n" << W_T_B.matrix());
            auto B_T_C = TfromVQ(buffer.B_T_C_p,buffer.B_T_C_q);
            PBWBC_DEBUG(" ... with B_T_C\n" << B_T_C.matrix());
            auto H_T_C = TfromVQ(buffer.state.position.translation,buffer.state.position.orientation);
            PBWBC_DEBUG(" ... with H_T_C\n" << B_T_C.matrix());

            Eigen::Isometry3d W_T_H = W_T_B * B_T_C * H_T_C.inverse();

            buffer.W_T_H_p = W_T_H.translation();
            buffer.W_T_H_q = Eigen::Quaterniond(W_T_H.linear());
            PBWBC_DEBUG(" ... yields W_T_H\n" << W_T_H.matrix());
            buffer.initialized = true;
        }
        if(buffer.timestamp==0) {
            PBWBC_DEBUG("camera got first sample, setting timestamp");
        }
        buffer.timestamp = timestamp; // TODO: use synced sensor and sensor timestamp to trace delay > 1ms
    } catch ( std::exception const & ex ) {
        std::cout << "Caught exception while aquiring camera data " << ex.what() << std::endl;
    }
#   endif // HAVE_LIBREALSENSE
}
    
Eigen::Isometry3d RealSenseStateEstimation::TfromVQ(Eigen::Vector3d const & v, Eigen::Quaterniond const & q) {
    Eigen::Isometry3d r = Eigen::Isometry3d::Identity();
    r.linear() = q.toRotationMatrix();
    r.translation() = v;
    return r;
}


} // namespace

