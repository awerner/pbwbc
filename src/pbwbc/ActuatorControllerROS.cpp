#include <pbwbc/ActuatorControllerROS.hpp>

namespace pbwbc {

ActuatorAdmittanceControllerROS::ActuatorAdmittanceControllerROS(
        std::string const & joint_name,
        std::shared_ptr<RobotState> const & state,
        std::shared_ptr<Model> const & model,
        std::shared_ptr<ConfigMap> const & config,
        std::shared_ptr<SensorState> const & sensor_state,
        std::shared_ptr<ActuatorController> const & underlying_controller,
        ros::NodeHandle const & nh
        ) : 
    ActuatorAdmittanceController(joint_name, state, model, config, sensor_state, underlying_controller),
    nh_(nh)
    {
        tau_actuator_d_pub_ = RealTimePublisherBackEnd::advertise<pbwbc::Float32Stamped>(nh_, "ActuatorAdmittanceController/" + joint_name + "/tau_actuator_d",1000,10);
        tau_projected_pub_ = RealTimePublisherBackEnd::advertise<pbwbc::Float32Stamped>(nh_, "ActuatorAdmittanceController/" + joint_name + "/tau_projected",1000,10);
        wrench_projected_pub_ = RealTimePublisherBackEnd::advertise<geometry_msgs::WrenchStamped>(nh_, "ActuatorAdmittanceController/" + joint_name + "/wrench_projected",1000,10);
        wrench_measured_pub_ = RealTimePublisherBackEnd::advertise<geometry_msgs::WrenchStamped>(nh_, "ActuatorAdmittanceController/" + joint_name + "/wrench_measured",1000,10);

        ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure(nh_));
        ddr_->RegisterVariable(&K_desired_, "proportional_gain", 0.0, 100.);
        ddr_->RegisterVariable(&I_, "integral_gain", 0.0, 100.);
        ddr_->RegisterVariable(&integral_windup_, "integral_windup", 0.0, 100.);
        ddr_->RegisterVariable(&feedforward_gain_, "feedforward_gain", 0., 1.);

        ddr_->RegisterVariable(&ft_filter_gain_, "ft_filter_gain", 0.0, 1.);
        if(dq_throttle_enabled_) {
            ddr_->RegisterVariable(&dq_max_, "dq_throttle_max", 0., dq_max_);
            ddr_->RegisterVariable(&dq_throttle_, "dq_throttle", 0., dq_max_);
        }
        ddr_->RegisterVariable(&command_filter_gain_, "command_filter_gain", 0., 1.);

        ddr_->PublishServicesTopics();
    }

ActuatorAdmittanceControllerROS::~ActuatorAdmittanceControllerROS() {}

    
void ActuatorAdmittanceControllerROS::update(timestamp_t const & timestamp, double tau_d) {
    ros::Time now(timestamp.time_since_epoch().count());

    ActuatorAdmittanceController::update(timestamp, tau_d);

    pbwbc::Float32Stamped msg;
    msg.header.stamp = now;
    msg.data.data = tau_actuator_d_;
    tau_actuator_d_pub_.publish(msg);

    msg.header.stamp = now;
    msg.data.data = tau_projected_;
    tau_projected_pub_.publish(msg);
   
    geometry_msgs::WrenchStamped wmsg;
    wmsg.header.stamp = now;
    assign(wmsg.wrench,projected_wrench_);
    wrench_projected_pub_.publish(wmsg);
    
    assign(wmsg.wrench,measured_wrench_);
    wrench_measured_pub_.publish(wmsg);
}

} //namespace pbwbc
