#include <pbwbc/ROSTools.hpp>

namespace pbwbc {

RealTimePublisherBackEnd::RealTimePublisherBackEnd() : 
    keep_running_(true),
    run_(false),
    file_run_(false),
    thread_([this](){
                this->thread_function();
                }),
    file_thread_([this](){
                this->file_thread_function();
                }) {
      pthread_setname_np(thread_.native_handle(),"ros_publisher");
      pthread_setname_np(file_thread_.native_handle(),"rosbag_write");
      // TODO: check if this gets rid of the FIFO priority class
      struct sched_param param;
      param.sched_priority = 0;
      pthread_setschedparam(thread_.native_handle(),SCHED_OTHER,&param);
      pthread_setschedparam(file_thread_.native_handle(),SCHED_OTHER,&param);
}

RealTimePublisherBackEnd::~RealTimePublisherBackEnd() {
    {
        std::unique_lock<std::mutex> lock(this->mutex_);
        this->keep_running_ = false;
        this->run_ = true;
        this->cv_.notify_all();
    }
    for(int idx=0;idx<100;idx++) {
        std::unique_lock<std::mutex> lock(mutex_);
        if(!run_)break;
        cv_.notify_one();
        std::this_thread::sleep_for(std::chrono::microseconds(10)); 
    }
    thread_.join();
    {
        std::unique_lock<std::mutex> lock(this->mutex_);
        this->keep_running_ = false;
        this->file_run_ = true;
        this->file_cv_.notify_all();
    }
    for(int idx=0;idx<100;idx++) {
        std::unique_lock<std::mutex> lock(mutex_);
        if(!file_run_)break;
        file_cv_.notify_one();
        std::this_thread::sleep_for(std::chrono::microseconds(10)); 
    }
    file_thread_.join();

    // remove callbacks from static storage
    storage_send_.clear();
    storage_file_.clear();
}
    
bool RealTimePublisherBackEnd::trigger_send() {
    if(!mutex_.try_lock()) return false;
    run_ = true;
    cv_.notify_all();
    mutex_.unlock();
    return true;
}
    
bool RealTimePublisherBackEnd::trigger_write_to_file(
        std::string const & filename,
        std::string const & topic_filter) {
    if(!mutex_.try_lock()) {
        PBWBC_DEBUG("trigger_write_to_file called, but mutex already locked");
    }
    file_run_ = true;
    rosbag_filename_ = filename;
    rosbag_topic_filter_ = topic_filter;
    file_cv_.notify_all();
    mutex_.unlock();
    return true;
}

bool RealTimePublisherBackEnd::write_to_file(
        std::string const & rosbag_filename,
        std::string const & rosbag_topic_filter) {
    std::unique_lock<std::mutex> lock(mutex_);
    rosbag::Bag bag(rosbag_filename, rosbag::bagmode::Write);
    bag.setCompression(rosbag::compression::LZ4);
    std::regex rosbag_topic_filter_regex(rosbag_topic_filter);
    int messages = 0;
    for(auto & callback : storage_file_) {
        messages += callback(bag, rosbag_topic_filter_regex);
    }
    bag.close();
    PBWBC_DEBUG("Dumped " << messages << " to rosbag");
    return true;
}

void RealTimePublisherBackEnd::thread_function() {
    while (true) {
        std::unique_lock<std::mutex> lock(mutex_);
        cv_.wait(lock);
        if(!keep_running_) {
            run_ = false;
            break;
        }
        if(!run_) continue;

        for(auto & callback : storage_send_) {
            callback();
        }
        run_ = false;
    }
}

void RealTimePublisherBackEnd::file_thread_function() {
    while (true) {
        std::unique_lock<std::mutex> lock(mutex_);
        file_cv_.wait(lock);
        if(!keep_running_) {
            run_ = false;
            break;
        }
        if(!file_run_) continue;
        PBWBC_DEBUG("Starting dump");
        rosbag::Bag bag(rosbag_filename_, rosbag::bagmode::Write);
        bag.setCompression(rosbag::compression::LZ4);
        std::regex rosbag_topic_filter_regex(rosbag_topic_filter_);
        int messages = 0;
        for(auto & callback : storage_file_) {
            messages += callback(bag, rosbag_topic_filter_regex);
        }
        bag.close();
        PBWBC_DEBUG("Dumped " << messages << " to rosbag");
        file_run_ = false;
    }
}


RealTimeTransformBroadcaster::RealTimeTransformBroadcaster(ros::NodeHandle const & nh,
        std::string const & origin_name,
        std::string const & base_frame_name,
        std::shared_ptr<Model> const & model) 
    : nh_(nh),
      origin_name_(origin_name),
      base_frame_name_(base_frame_name),
      model_(model),
      keep_running_(true),
      run_(false) {
      thread_ = std::move(std::thread([this](){
            this->thread_function();
         }));
      pthread_setname_np(thread_.native_handle(),"tf_brdctr");
      struct sched_param param;
      param.sched_priority = 0;
      pthread_setschedparam(thread_.native_handle(),SCHED_OTHER,&param);

      logging_publisher_ = RealTimePublisherBackEnd::advertise<tf2_msgs::TFMessage>(
             nh_,"/tf",1000,0);
      for(auto const & frameId : model_->getLinkFrames()) {
          tf_map_.insert(std::make_pair(frameId,
                      std::make_tuple(model_->getFrameName(frameId),
                          Eigen::Vector3d::Zero(),
                          Eigen::Quaterniond::Identity())));
      }
}

RealTimeTransformBroadcaster::~RealTimeTransformBroadcaster() {
    {
        std::unique_lock<std::mutex> lock(this->mutex_);
        this->keep_running_ = false;
        this->run_ = true;
        this->cv_.notify_all();
    }
    for(int idx=0;idx<100;idx++) {
        std::unique_lock<std::mutex> lock(mutex_);
        if(!run_)break;
        cv_.notify_one();
        std::this_thread::sleep_for(std::chrono::microseconds(10)); 
    }
    thread_.join();
}

void RealTimeTransformBroadcaster::setBasePosition(
        Eigen::Ref<const Eigen::VectorXd> const & translation,
        Eigen::Quaterniond const & orientation) {
    std::unique_lock<std::mutex> lock(this->mutex_);
    translation_ = translation;
    orientation_ = orientation;
}

void RealTimeTransformBroadcaster::setJointPositions(
        RobotState const & state) {
    std::unique_lock<std::mutex> lock(this->mutex_);
    for(auto & info : tf_map_) {
        auto & frameId = info.first;
        std::get<1>(info.second) = model_->bodyTranslation(frameId);
        std::get<2>(info.second) = model_->bodyOrientation(frameId);
    }
}

void RealTimeTransformBroadcaster::publish(
        timestamp_t const & timestamp) {
    std::unique_lock<std::mutex> lock(this->mutex_);
    timestamp_ = timestamp;
    run_ = true;
    cv_.notify_all();
}


void RealTimeTransformBroadcaster::thread_function() {
    while (true) {
        std::unique_lock<std::mutex> lock(mutex_);
        cv_.wait(lock);
        if(!keep_running_) {
            run_ = false;
            break;
        }
        if(!run_) continue;
        
        geometry_msgs::TransformStamped transform;
        ros::Time ros_timestamp(timestamp_.time_since_epoch().count());
        transform.header.stamp = ros_timestamp;
        transform.header.frame_id = origin_name_;
        transform.child_frame_id = base_frame_name_;
        transform.transform.translation.x = translation_(0);
        transform.transform.translation.y = translation_(1);
        transform.transform.translation.z = translation_(2);
        transform.transform.rotation.x = orientation_.x();
        transform.transform.rotation.y = orientation_.y();
        transform.transform.rotation.z = orientation_.z();
        transform.transform.rotation.w = orientation_.w();
        tf_broadcaster_.sendTransform(transform);
        // TODO: we do not want to publish the transforms below when running, but only the one above
        
        tf2_msgs::TFMessage tfmsg;
        tfmsg.transforms.push_back(transform);
        for(auto const & info : tf_map_) {
            auto const & translation = std::get<1>(info.second);
            auto const & orientation = std::get<2>(info.second);
            // TODO: copy much, malloc much
            geometry_msgs::TransformStamped transform;
            transform.header.stamp = ros_timestamp;
            transform.header.frame_id = origin_name_;
            transform.child_frame_id = std::get<0>(info.second);
            transform.transform.translation.x = translation(0);
            transform.transform.translation.y = translation(1);
            transform.transform.translation.z = translation(2);
            transform.transform.rotation.x = orientation.x();
            transform.transform.rotation.y = orientation.y();
            transform.transform.rotation.z = orientation.z();
            transform.transform.rotation.w = orientation.w();
            tfmsg.transforms.push_back(transform);
        }

        logging_publisher_.publish(tfmsg);
        run_ = false;
    }
}

std::vector<std::function<void()>> pbwbc::RealTimePublisherBackEnd::storage_send_;
std::vector<std::function<int(rosbag::Bag&,std::regex const &)>> pbwbc::RealTimePublisherBackEnd::storage_file_;

void assign(std_msgs::Float32MultiArray & msg,
        Eigen::Ref<const Eigen::MatrixXd> const m) {
    msg.layout.dim.resize(2);
    msg.layout.dim[0].label = "cols";
    msg.layout.dim[0].size = m.cols();
    msg.layout.dim[0].stride = m.rows();
    msg.layout.dim[1].label = "rows";
    msg.layout.dim[1].size = m.rows();
    msg.layout.dim[1].stride = 1;
    msg.layout.data_offset = 0;
    msg.data.resize(m.rows()*m.cols());
    for(std::size_t idx=0;idx<msg.data.size();idx++) {
        msg.data[idx] = (&m.coeffRef(0,0))[idx];
    }
}

void assign(std_msgs::Float64MultiArray & msg,
        Eigen::Ref<const Eigen::MatrixXd> const m) {
    msg.layout.dim.resize(2);
    msg.layout.dim[0].label = "cols";
    msg.layout.dim[0].size = m.cols();
    msg.layout.dim[0].stride = m.rows();
    msg.layout.dim[1].label = "rows";
    msg.layout.dim[1].size = m.rows();
    msg.layout.dim[1].stride = 1;
    msg.layout.data_offset = 0;
    msg.data.resize(m.rows()*m.cols());
    for(std::size_t idx=0;idx<msg.data.size();idx++) {
        msg.data[idx] = (&m.coeffRef(0,0))[idx];
    }
}

void assign(Eigen::MatrixXf & m,
        std_msgs::Float32MultiArray const & msg) {
    if(msg.layout.dim.size()==1) {
        m.resize(msg.layout.dim.at(0).size,1);
        m = Eigen::Map<const Eigen::MatrixXf>(
                &msg.data.at(0)+msg.layout.data_offset,
                msg.layout.dim.at(0).size,
                1);
    } else if(msg.layout.dim.size()==2) {
        m.resize(msg.layout.dim.at(0).size,
                 msg.layout.dim.at(1).size);
        typedef Eigen::Stride<Eigen::Dynamic,2> stride_t;
        m = Eigen::Map<const Eigen::MatrixXf,0,stride_t>(
                &msg.data.at(0)+msg.layout.data_offset,
                msg.layout.dim.at(0).size,
                msg.layout.dim.at(1).size,
                stride_t(
                    msg.layout.dim.at(0).stride,
                    msg.layout.dim.at(1).stride
                    ));
    } else {
        throw std::runtime_error("Dimensionality not implemented");
    }
}

void assign(Eigen::MatrixXd & m,
        std_msgs::Float64MultiArray const & msg) {
    if(msg.layout.dim.size()==1) {
        m.resize(msg.layout.dim.at(0).size,1);
        m = Eigen::Map<const Eigen::MatrixXd>(
                &msg.data.at(0)+msg.layout.data_offset,
                msg.layout.dim.at(0).size,
                1);
    } else if(msg.layout.dim.size()==2) {
        m.resize(msg.layout.dim.at(0).size,
                 msg.layout.dim.at(1).size);
        typedef Eigen::Stride<Eigen::Dynamic,2> stride_t;
        m = Eigen::Map<const Eigen::MatrixXd,0,stride_t>(
                &msg.data.at(0)+msg.layout.data_offset,
                msg.layout.dim.at(0).size,
                msg.layout.dim.at(1).size,
                stride_t(
                    msg.layout.dim.at(0).stride,
                    msg.layout.dim.at(1).stride
                    ));
    } else {
        throw std::runtime_error("Dimensionality not implemented");
    }
}

void assign(Eigen::VectorXf & m,
        std_msgs::Float32MultiArray const & msg) {
    if(msg.layout.dim.size()==1) {
        m.resize(msg.layout.dim.at(0).size,1);
        m = Eigen::Map<const Eigen::MatrixXf>(
                &msg.data.at(0)+msg.layout.data_offset,
                msg.layout.dim.at(0).size,
                1);
    } else {
        throw std::runtime_error("Dimensionality too large");
    }
}

void assign(Eigen::VectorXd & m,
        std_msgs::Float64MultiArray const & msg) {
    if(msg.layout.dim.size()==1) {
        m.resize(msg.layout.dim.at(0).size,1);
        m = Eigen::Map<const Eigen::MatrixXd>(
                &msg.data.at(0)+msg.layout.data_offset,
                msg.layout.dim.at(0).size,
                1);
    } else {
        throw std::runtime_error("Dimensionality too large");
    }
}

void assign(geometry_msgs::Transform & msg, 
        const Eigen::Ref<const Eigen::VectorXd> & translation,
        Eigen::Quaterniond orientation) {
       msg.translation.x = translation(0);
       msg.translation.y = translation(1);
       msg.translation.z = translation(2);
       msg.rotation.x = orientation.x();
       msg.rotation.y = orientation.y();
       msg.rotation.z = orientation.z();
       msg.rotation.w = orientation.w();
};

void assign(geometry_msgs::Twist & msg, 
        const Eigen::Ref<const Eigen::VectorXd> translation,
        const Eigen::Ref<const Eigen::VectorXd> orientation) {
    msg.linear.x = translation(0);
    msg.linear.y = translation(1);
    msg.linear.z = translation(2);
    msg.angular.x = orientation(0);
    msg.angular.y = orientation(1);
    msg.angular.z = orientation(2);
}

void assign(
        CartesianState::position_state & state,
        geometry_msgs::Transform const & msg) {
       state.translation(0) = msg.translation.x;
       state.translation(1) = msg.translation.x;
       state.translation(2) = msg.translation.x;
       state.orientation.x() = msg.rotation.x;
       state.orientation.y() = msg.rotation.y;
       state.orientation.z() = msg.rotation.z;
       state.orientation.w() = msg.rotation.w;
};


void assign(geometry_msgs::Wrench & msg, 
        const Eigen::Ref<const Eigen::VectorXd> & wrench) {
       msg.force.x = wrench(0);
       msg.force.y = wrench(1);
       msg.force.z = wrench(2);
       msg.torque.x = wrench(3);
       msg.torque.y = wrench(4);
       msg.torque.z = wrench(5);
};

void assign(
        CartesianState::position_state & state,
        geometry_msgs::Pose const & msg) {
    state.translation(0) = msg.position.x;
    state.translation(1) = msg.position.y;
    state.translation(2) = msg.position.z;
    state.orientation.x() = msg.orientation.x;
    state.orientation.y() = msg.orientation.y;
    state.orientation.z() = msg.orientation.z;
    state.orientation.w() = msg.orientation.w;
};

void assign(
        CartesianState::velocity_state & state,
        geometry_msgs::Twist const & msg) {
    state.translation(0) = msg.linear.x;
    state.translation(1) = msg.linear.y;
    state.translation(2) = msg.linear.z;
    state.orientation(0) = msg.angular.x;
    state.orientation(1) = msg.angular.y;
    state.orientation(2) = msg.angular.z;
};

void assign(
        Eigen::Ref<Eigen::VectorXd> wrench,
        geometry_msgs::Wrench const & msg) {
    wrench(0) = msg.force.x;
    wrench(1) = msg.force.y;
    wrench(2) = msg.force.z;
    wrench(3) = msg.torque.x;
    wrench(4) = msg.torque.y;
    wrench(5) = msg.torque.z;
};

}
