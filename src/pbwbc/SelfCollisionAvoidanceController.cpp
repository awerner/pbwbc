#include <pbwbc/SelfCollisionAvoidanceController.hpp>
#include <pbwbc/PinocchioData.hpp>
#include <pbwbc/ModelPinocchio.hpp>
#include <pbwbc/Algebra.hpp>
#include "pinocchio/parsers/urdf.hpp"
#include "pinocchio/multibody/data.hpp"
#include "pinocchio/multibody/geometry.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/algorithm/geometry.hpp"
#include "pinocchio/algorithm/crba.hpp"
#include <pbwbc/SelfCollisionAvoidanceControllerImpl.hpp>
#ifdef PINOCCHIO_WITH_HPP_FCL
#include "hpp/fcl/collision_object.h"
#endif

namespace pbwbc {

SelfCollisionAvoidanceController::SelfCollisionAvoidanceController(
        std::shared_ptr<RobotState> const &state,
        std::shared_ptr<Model> const &model,
        std::shared_ptr<ConfigMap> const & config,
        std::string const & urdf_content)
    : Controller([&](){
        std::string name;
        config->param("name",name,"SelfCollisionAvoidanceController");
        return name;}(),model->dq_size()),
      state_(state),
      model_(model),
      config_(config),
      impl_(new impl),
      compute_thread_state_(*state_),
      last_update_(std::chrono::system_clock::now()),
      jacobian_A_(1,model->dy_size()),
      jacobian_B_(1,model->dy_size()),
      frame_jacobian_(6,model_->dy_size()),
      jacobian_temp_(1,model_->dy_size()),
      mass_lu_(model_->dy_size()),
      mass_inv_(model_->dy_size(),model_->dy_size()),
      mass_inv_initialized_(false) {
#   ifndef PINOCCHIO_WITH_HPP_FCL
    throw std::runtime_error("SelfCollisionAvoidance not available because HPP_FCL was not available at compile time");
#   else
    // get pinocchio::model
    auto pmodel = std::dynamic_pointer_cast<ModelPinocchio>(model_);
    if(!pmodel) {
        throw std::runtime_error("Need pinocchio model");
    }
    impl_->model = pmodel->pinocchio_data()->model_;
    impl_->data.reset(new pinocchio::Data(*impl_->model));
    // build pinocchio::geometryModel
    impl_->geomModel.reset(new pinocchio::GeometryModel);
    std::stringstream urdf_stream;
    urdf_stream << urdf_content;
    pinocchio::urdf::buildGeom(*impl_->model,
            urdf_stream, pinocchio::COLLISION, *impl_->geomModel);
    auto & geomModel = *impl_->geomModel;
    // ATT: model could have been processed with reduceModel
    // get urdf again

    // pinocchio::buildGeom() to build the GeometryModel,
    // then build GeometryData
    // disable all collisions
    
    config->param("max_force",max_force_,100);
    PBWBC_TRACE("max_force " << max_force_);
    config->param("damping",damping_,0.7);
    PBWBC_TRACE("damping " << damping_);
    double default_start_distance;
    config->param("default_start_distance",default_start_distance,0.05);
    PBWBC_TRACE("default_start_distance " << default_start_distance);
  
    std::vector<std::string> additional_objects;
    config_->list_items("additional_objects",additional_objects); 
    for( auto const & object_name: additional_objects ) {
        auto oconfig = config_->submap("additional_objects")->submap(object_name);
        std::string object_type_string;
        if(!oconfig->getParam("type",object_type_string)) {
            throw std::runtime_error("Missing object type");
        }
        std::string parent_frame_name;
        if(!oconfig->getParam("parent_frame",parent_frame_name)) {
            throw std::runtime_error("Missing parent_frame");        
        }
        int parent_frame_Id = model_->getFrameId(parent_frame_name);
        if(object_type_string=="capsule") {
            Eigen::Vector3d a,b;
            double radius;
            if(!oconfig->getParam("a",a) || !oconfig->getParam("b",b)
                    || !oconfig->getParam("radius",radius)) {
                throw std::runtime_error("Capsule information not complete");
            }
            Eigen::Vector3d direction = (b-a).normalized();
            double lz = (a-b).norm();
            Eigen::Vector3d middle_point = (a.array() + b.array()) / 2.;
            Eigen::Quaterniond quat = 
                Eigen::Quaterniond::FromTwoVectors(direction,Eigen::Vector3d::UnitZ()).inverse(); //TODO: inverse() added following capsule visualization, but missing proof that it is correct
            typedef hpp::fcl::Capsule capsule_t;
            boost::shared_ptr<hpp::fcl::CollisionGeometry> cg(new capsule_t(radius,lz));
            pinocchio::SE3 placement(quat,middle_point);
            int parent_joint_Id = impl_->model->frames[parent_frame_Id].parent;
            PBWBC_TRACE("Adding a capsule with a: " << a.transpose() << " b: " << b.transpose()
                    << " r: " << radius << " to frame " << parent_frame_Id << " with offset " <<
                    middle_point.transpose() << " and rotation " << quat.coeffs().transpose());
            geomModel.addGeometryObject(
                    pinocchio::GeometryObject(object_name,parent_frame_Id,parent_joint_Id,
                        cg,placement),*impl_->model);
        } else {
            throw std::runtime_error("Object type not supported");
        }
    }

    int list_objects;
    config->param("list_objects",list_objects,false);
    if(list_objects) {
        PBWBC_TRACE("--- Geometry objects (start) ---");
        for( auto const & object : geomModel.geometryObjects) {
            PBWBC_TRACE(object.name);
        }
        PBWBC_TRACE("--- Geometry objects (end) ---");
    }
    // add collision at user request
    std::vector<std::string> collision_names;
    config_->list_items("collisions",collision_names);
    for( auto const & collision_name : collision_names ) {
        auto myconfig = config_->submap("collisions")->submap(collision_name);
        std::string collision_object_a_name;
        if(!myconfig->getParam("a",collision_object_a_name)) {
            throw std::runtime_error("No collision partner a specified");
        }
        std::string collision_object_b_name;
        if(!myconfig->getParam("b",collision_object_b_name)) {
            throw std::runtime_error("No collision partner b specified");
        }
        PBWBC_TRACE("Adding collision pair " << collision_object_a_name << " and " <<
                collision_object_b_name);
        // resolve to which links the collision pairs are associated
        if(!geomModel.existGeometryName(collision_object_a_name) ||
            !geomModel.existGeometryName(collision_object_b_name) ) {
            throw std::runtime_error("Collision object not found");
        }
        pinocchio::GeomIndex id_A = geomModel.getGeometryId(collision_object_a_name);
        pinocchio::GeomIndex id_B = geomModel.getGeometryId(collision_object_b_name);
        PBWBC_TRACE("Collision pair ids " << id_A << " and " << id_B);
        geomModel.addCollisionPair(pinocchio::CollisionPair(id_A,id_B));
        double start_distance;
        myconfig->param("start_distance",start_distance,default_start_distance);
        start_distance_.push_back(start_distance);
        // useful stuff:
        //  setGeometryMeshScales
        //
    }
    PBWBC_TRACE("Configured collision pairs: " << geomModel.collisionPairs.size());
    impl_->geomData.reset(new pinocchio::GeometryData(geomModel));
    auto & geomData = *impl_->geomData;
    std::fill(geomData.activeCollisionPairs.begin(),geomData.activeCollisionPairs.end(),true);

    // start thread
    config_->param("compute_downsample",compute_downsample_,10);
    request_compute_ = false;
    keep_running_ = true;
    compute_thread_ = std::thread([this](){
          this->collision_compute_thread();
          });
    // assign thread name
    pthread_setname_np(compute_thread_.native_handle(),"sca_compute");
    
    // this constructor is called from a non-RT thread on the robot,
    // the priority is set in ::start

    compute_thread_state_.assignAll(*state_);
    collision_tau_.assignAll(Eigen::VectorXd(model_->dq_size()));
        
    if(auto pmodel = std::dynamic_pointer_cast<ModelPinocchio>(model_)) {
        model_compute_task_id_ = pmodel->register_compute_task([this](){
                this->model_compute_task();
                });
    } else {
        throw std::runtime_error("Need pinocchio model");
    }
#   endif
}

SelfCollisionAvoidanceController::~SelfCollisionAvoidanceController() {
    // delete the model task, it is bound to this instance
    if(model_compute_task_id_!=-1) {
        auto pmodel = std::dynamic_pointer_cast<ModelPinocchio>(model_);
        pmodel->deregister_compute_task(model_compute_task_id_);
    }

    {
        std::unique_lock<std::mutex> lock(mutex_);
        keep_running_ = false;
        request_compute_ = true;
    }

    for(int idx=0;idx<100;idx++) {
        std::unique_lock<std::mutex> lock(mutex_);
        if(!request_compute_)break;
        cv_.notify_one();
        std::this_thread::sleep_for(std::chrono::microseconds(10)); 
    }
    compute_thread_.join();
    
    if(update_too_late_!=0) {
        PBWBC_WARN("SelfCollisionAvoidance updates too late: "
                << update_too_late_);
    }
}

void SelfCollisionAvoidanceController::start(timestamp_t const & timestamp) {
    counter_ = 0;
    dynamic_gain_ = 0.;
    // initialize buffers so that first write fills all slots
    compute_thread_state_.reset();
    collision_tau_.reset();
    update_too_late_ = 0;
    
    // decreate priority of compute_thread by two
    struct sched_param param;
    int policy;
    errno = pthread_getschedparam(pthread_self(),&policy,&param);
    if(errno) {
		perror("ERROR: pthread_getschedparam");
		throw std::runtime_error("errno!=0");
	}
    param.sched_priority = std::max(static_cast<int>(param.sched_priority) - 2,0);
    PBWBC_DEBUG("Setting SelfCollisionAvoidance compute_thread to priority: " 
            << param.sched_priority);
    errno = pthread_setschedparam(compute_thread_.native_handle(),policy,&param);
    if(errno) {
		perror("ERROR: pthread_setschedparam");
		throw std::runtime_error("errno!=0");
	}
}

void SelfCollisionAvoidanceController::stop(timestamp_t const &) {}

void SelfCollisionAvoidanceController::update(timestamp_t const &) {
    // trigger compute thread through condition variable at desired rate
    if (counter_==0) {
        compute_thread_state_.getWriteBuffer() = *state_;
        compute_thread_state_.commitWriteBuffer();
        PBWBC_TRACEF("SCA trigger");
        if(mutex_.try_lock()) {
            request_compute_ = true;
            cv_.notify_one();
            mutex_.unlock();
        }
    }
    counter_++;
    if(counter_>=compute_downsample_)counter_=0;
    
    // TODO: compute damping torque here (with damping matrix computed in other thread)
    // to have nice feel. Alternatively damping torque should decay quicker.
    if(collision_tau_.ready()) {
        // decay output if update frequency is too low,
        // ramp it up slowly if it comes back and at startup
        // TODO: delta is based on wall time. Should this be sim time?
        auto delta = std::chrono::duration<double>(
                std::chrono::system_clock::now()-last_update_.load());
        //PBWBC_DEBUG("delta " << delta.count());
        if(delta < std::chrono::milliseconds(50)) {
            dynamic_gain_ += 1e-3;
            dynamic_gain_ = std::min(dynamic_gain_,1.);
        } else {
            dynamic_gain_ -= 1e-3;
            dynamic_gain_ = std::max(dynamic_gain_,0.);
            update_too_late_++;
            PBWBC_WARN("[SCA] update came too late");
            PBWBC_TRACEF("SCA too late");
        }
        // TODO: last_update_ is not connected to TripleBuffer output
        tau_d_ = - dynamic_gain_ * collision_tau_.getReadBuffer();
        collision_tau_.freeReadBuffer();
    } else {
        tau_d_.setZero();
    }
}

void SelfCollisionAvoidanceController::model_compute_task() {
    if(!model_compute_mutex_.try_lock())return;
    mass_inv_ = model_->InvMassMatrix(); // could use TripleBuffer here instead
    mass_inv_initialized_ = true;
    model_compute_mutex_.unlock();
}

void SelfCollisionAvoidanceController::collision_compute_thread() {
#   ifdef PINOCCHIO_WITH_HPP_FCL
    // TODO: if this thread is not connected to the model update,
    // it should compute its own model (jacobians)
    while(true) {
        {
            // wait for condition variable
            std::unique_lock<std::mutex> lock(mutex_);
            cv_.wait(lock);
            if(!request_compute_)continue; // spurious wakeup
            if(!keep_running_) {
                request_compute_ = false;
                break;
            }
        }
        if(!compute_thread_state_.ready())continue;
        PBWBC_TRACEF("SCA start");
        auto start = std::chrono::system_clock::now();
        auto const & state = compute_thread_state_.getReadBuffer();
        auto & model = *impl_->model;
        auto & data = *impl_->data;
        auto & geomModel = *impl_->geomModel;
        auto & geomData = *impl_->geomData;
        // what needs to be updated every iteration?
        // kinematics + distances (just call the computeDistances() version which
        // takes the state
        // the jacobians and mass matrix can be supplied from a model compute task
        //
        // write a model compute tasks which puts the requested jacobians into a buffer
        // all kinematics should be computed.
        // TODO: how long do the jacobians take? which line dominates the computation time?
        // 
        //
        // next 2 lines: 25us
        pinocchio::forwardKinematics(model,data,state.y,state.dy);
        pinocchio::computeJointJacobians(model,data,state.y);
        // next line: 39us
        pinocchio::framesForwardKinematics(model,data,state.y); // also compute forwardKinematics
        //auto end3 = std::chrono::system_clock::now();
        // next line: 12ms with 6 pairs
        // maybe switch to capsules here?
        pinocchio::computeDistances(model,data,geomModel,geomData);
        //auto end4 = std::chrono::system_clock::now();
        //std::chrono::duration<double> elapsed_seconds4 = end4-end3;
        //PBWBC_DEBUG("end4 " << elapsed_seconds4.count());
            // TODO: which things are computed twice
        
         // TODO:
         // use model compute task to copy
         // mass matrix (maybe already inverted)here
        //pinocchio::crba(model,data,state.y);
        //data.M.triangularView<Eigen::StrictlyLower>() = 
        //   data.M.transpose().triangularView<Eigen::StrictlyLower>();
        //mass_lu_.compute(data.M);
        //mass_inv_ = mass_lu_.inverse();
        //mass_inv_ = pinocchio::computeMinverse(model,data,state.y);
        // damping computation still be too expensive for main thread
        // output into collision_tau_ (TripleBuffer adds delay)
        
        // loop below: 30us avg with 6 collisions (not all active)
        auto & tau_d = collision_tau_.getWriteBuffer();
        tau_d.setZero();
        for ( std::size_t idx = 0; idx < geomModel.collisionPairs.size(); idx++ ) {
            if(!geomData.activeCollisionPairs.at(idx))continue;
            // skip 
            if(start_distance_.at(idx) < 0.) continue;
            std::size_t geomIdx1 = geomModel.collisionPairs[idx].first;
            std::size_t geomIdx2 = geomModel.collisionPairs[idx].second;
            auto const & obj1 = geomModel.geometryObjects.at(geomIdx1);
            auto const & obj2 = geomModel.geometryObjects.at(geomIdx2);
            pinocchio::FrameIndex frameId_A = obj1.parentFrame;
            pinocchio::FrameIndex frameId_B = obj2.parentFrame;

            // for details in the distanceResult see
            // https://flexible-collision-library.github.io/d3/d37/structfcl_1_1DistanceResult.html
            auto const & distanceResult = geomData.distanceResults.at(idx);
            
            // for capsules, fcl correctly reports negative distances if the objects
            // collide. Untested for meshes.
            //if( distanceResult.min_distance < 1e-3 ) {
            //    std::stringstream str;
            //    str << "Objects are colliding " << distanceResult.min_distance;
            //    throw ControllerException(str.str());
            //}

            //PBWBC_DEBUG("distance: " << distanceResult.min_distance); 
            double pot_d1 = potential_d1(distanceResult.min_distance,idx);
            //PBWBC_DEBUG("pot_d1: " << pot_d1); 

            if (pot_d1==0.0){
                continue;
            }

            //distanceResult.nearest_points[1]; <- seems to be in the world frame
            //PBWBC_DEBUG("p[0]: " << distanceResult.nearest_points[0].transpose());
            //PBWBC_DEBUG("p[1]: " << distanceResult.nearest_points[1].transpose());
            vector3_t direction;
            if(distanceResult.min_distance>0.) {
                direction = distanceResult.nearest_points[0] - distanceResult.nearest_points[1];
            } else {
                direction = distanceResult.nearest_points[1] - distanceResult.nearest_points[0];
            }
            
            //PBWBC_DEBUG("direction: " << direction.transpose()); 
            //if(direction.norm() < 1e-3) {
            //    // TODO: check fcl behavior for penetration
            //    PBWBC_DEBUG("p[0]: " << distanceResult.nearest_points[0].transpose());
            //    PBWBC_DEBUG("p[1]: " << distanceResult.nearest_points[1].transpose());
            //    throw ControllerException("Small direction");
            //}
            direction.normalize();
            
                                          
            // fill in the Jacobian at collision points in each body
            jacobian_at_collision(jacobian_A_,distanceResult.nearest_points[0],
                                 direction,frameId_A);
            //PBWBC_DEBUG("jacobian_A\n" << jacobian_A_);

            jacobian_at_collision(jacobian_B_,distanceResult.nearest_points[1],
                                 -direction,frameId_B);
            //PBWBC_DEBUG("jacobian_B\n" << jacobian_B_);

            // relative velocity of the collision points
            double vel_A = (jacobian_A_ * state.dy)(0);
            double vel_B = (jacobian_B_ * state.dy)(0);

            // fill in the damping matrix
            matrix_2x2_t D = matrix_2x2_t::Zero();
            double_diag_damping(D, distanceResult.min_distance, idx);

            tau_d += jacobian_A_.rightCols(model_->dq_size()).transpose()
                * (- pot_d1 - D(0,0) * vel_A );
            tau_d += jacobian_B_.rightCols(model_->dq_size()).transpose()
                * (- pot_d1 - D(1,1) * vel_B );
            //PBWBC_DEBUG("tau\n" << tau_d.transpose());
        }
        collision_tau_.commitWriteBuffer();
        compute_thread_state_.freeReadBuffer();
        last_update_.store(std::chrono::system_clock::now());
        {
            std::unique_lock<std::mutex> lock(mutex_);
            request_compute_ = false;
        }
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        thread_local double max_runtime = 0., accumulated_runtime = 0.;
        thread_local int reduction = 0;
        const int reduction_rate = 1000;
        if(elapsed_seconds.count()>max_runtime)max_runtime = elapsed_seconds.count();
        accumulated_runtime += elapsed_seconds.count();
        
        if(reduction++==(reduction_rate*compute_downsample_)) {
            PBWBC_DEBUG("[SCA] maximum compute time (wall): " << max_runtime);
            PBWBC_DEBUG("[SCA] average compute time (wall): " << accumulated_runtime/reduction_rate);
            reduction = 0;
            max_runtime = 0.;
            accumulated_runtime = 0.;
        }
        PBWBC_TRACEF("SCA stop");
    }
#   endif // PINOCCHIO_WITH_HPP_FCL
}

void SelfCollisionAvoidanceController::jacobian_at_collision(
        Eigen::MatrixXd & jacobian_i,
    const vector3_t & point_i, const vector3_t & direction_i,
    const std::size_t frameId) {
    vector3_t c_i = 
        point_i - impl_->data->oMf.at(frameId).translation();

    Eigen::Isometry3d H_i = Eigen::Isometry3d::Identity();
    H_i.linear() = impl_->data->oMf.at(frameId).rotation().transpose();
    H_i.translation() = H_i.linear() * c_i;

    Eigen::Matrix<double,6,6> Adj_i = adjoint(H_i.inverse());

    frame_jacobian_.setZero(); 
    pinocchio::getFrameJacobian(*impl_->model,*impl_->data,
            frameId,pinocchio::LOCAL,frame_jacobian_);
    jacobian_i.noalias() = direction_i.transpose()
            * Adj_i.topRows<3>() * frame_jacobian_;
}



void SelfCollisionAvoidanceController::double_diag_damping(matrix_2x2_t& D,
    double distance, std::size_t pair_idx) {
    // Project the mass matrix into the distance space
    // and handle cases where joint is immovable
    std::unique_lock<std::mutex> lock(model_compute_mutex_);
    double mass_A;
    bool compute_damping_A = true;
    if(!mass_inv_initialized_) {
        D.setZero();
        return;
    }
    if( !(jacobian_A_.array().abs() < 1e-9).all() ) {
        jacobian_temp_.transpose().noalias() =
                mass_inv_ * jacobian_A_.transpose();
        mass_A = 1./
                (jacobian_A_ * jacobian_temp_.transpose())(0);
    } else {
        compute_damping_A = false;
        mass_A = 0.;
    }

    double mass_B;
    bool compute_damping_B = true;
    if( !(jacobian_B_.array().abs() < 1e-9).all() ) {
        jacobian_temp_.transpose().noalias() =
                mass_inv_ * jacobian_B_.transpose();
        mass_B = 1./
                (jacobian_B_ * jacobian_temp_.transpose())(0);
    } else {
        compute_damping_B = false;
        mass_B = 0.;
    }

    // double diagonalization damping design
    double pot_d2 = potential_d2(distance,pair_idx);
    if(compute_damping_A && compute_damping_B) {
        matrix_2x2_t M;
        M.setZero();
        M(0,0) = mass_A;
        M(1,1) = mass_B;
        matrix_2x2_t K;
        K.setZero();
        K.diagonal().setConstant(pot_d2);
        Eigen::GeneralizedSelfAdjointEigenSolver<matrix_2x2_t>
                eigensolver(K,M,Eigen::ComputeEigenvectors);
        matrix_2x2_t MV = M * eigensolver.eigenvectors();
        matrix_2x2_t z;
        z.setZero();
        z.diagonal() = 2 * eigensolver.eigenvalues().array().sqrt() * damping_;
        D = MV * z * MV.transpose();
    }
    else if(compute_damping_A) {
        D(0,0) = 2. * damping_ * std::sqrt(pot_d2 * mass_A);
    }
    else if(compute_damping_B) {
        D(1,1) = 2. * damping_ * std::sqrt(pot_d2 * mass_B);
    }
}

double SelfCollisionAvoidanceController::potential_d0(double distance, std::size_t pair_idx )
    const {
    if ( distance > start_distance_[pair_idx] ||
            start_distance_[pair_idx]==0. ) return 0.;
    return (max_force_ / 3. / std::pow(start_distance_[pair_idx],2.) )
            *  std::pow(start_distance_[pair_idx] - distance,3.);
}

double SelfCollisionAvoidanceController::potential_d1(double distance, std::size_t pair_idx )
    const {
    if ( distance > start_distance_[pair_idx] ||
            start_distance_[pair_idx]==0. ) return 0.;
    return (max_force_ / std::pow(start_distance_[pair_idx],2.) )
            *  std::pow(start_distance_[pair_idx] - distance,2.);
}

double SelfCollisionAvoidanceController::potential_d2(double distance, std::size_t pair_idx )
    const {
    if ( distance > start_distance_[pair_idx] ||
            start_distance_[pair_idx]==0. ) return 0.;
    return ( 2. * max_force_ / std::pow(start_distance_[pair_idx],2.) )
            *  (start_distance_[pair_idx] - distance);
}


} // namespace

