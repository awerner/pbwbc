#include <pluginlib/class_loader.h>
#include <controller_interface/controller_base.h>

int main(int argc, char** argv) {
    pluginlib::ClassLoader<controller_interface::ControllerBase> loader("controller_interface", "controller_interface::ControllerBase");

    try {
        boost::shared_ptr<controller_interface::ControllerBase> instance 
            = loader.createInstance("pbwbc::PBWBCControllerROSControl");
    } catch(pluginlib::PluginlibException const & ex) {
        ROS_ERROR("The plugin failed to load for some reason. Error: %s", ex.what());
    }

    return 0;
}
