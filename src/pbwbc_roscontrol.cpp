#include <pinocchio/fwd.hpp>
#include <pbwbc/pbwbc_roscontrol.hpp>
#include <pbwbc/pbwbc_roscontrol.hxx>

template
class PBWBC_EXPORT pbwbc::PBWBCController<controller_interface::ControllerBase>;

PLUGINLIB_EXPORT_CLASS(pbwbc::PBWBCController<controller_interface::ControllerBase>, 
		 controller_interface::ControllerBase) 
