#include <pbwbc/MultiContactForceDistribution.hpp>
#include <pbwbc/StandaloneConfig.hpp>
#include <pbwbc/Simulator.hpp>
#include <pbwbc/ModelPinocchio.hpp>
#include <ros/ros.h>
#include <pbwbc/ROSConfig.hpp>

int main(int argc, char **argv) {
    ros::init(argc, argv, "my_node_name");

    ros::NodeHandle robot_nh;
    std::string robot_description_str;
    robot_nh.getParam("/robot_description",robot_description_str);
    auto model = pbwbc::ModelPinocchio::create_from_urdf_data(robot_description_str);

    //std::shared_ptr<pbwbc::Model> model = pbwbc::ModelPinocchio::create_from_urdf_file("robot.urdf");
    std::shared_ptr<pbwbc::RobotState> state(
            new pbwbc::RobotState(model->y_size(),
                                  model->dy_size()));
    state->y.setZero();
    state->dy.setZero();
    model->setState(state);
    std::dynamic_pointer_cast<pbwbc::ModelPinocchio>(model)->blocking_update(std::chrono::system_clock::now());

    std::shared_ptr<pbwbc::ConfigMap> config(new pbwbc::ROSConfigMap(robot_nh));
    config = config->submap("pbwbc_controller");

    std::shared_ptr<pbwbc::SensorState> sensor_state(new pbwbc::SensorState);
    pbwbc::MultiContactForceDistribution ji_1(
                  state,
                  sensor_state,
                  std::shared_ptr<pbwbc::ModelInComCoordinates>(new pbwbc::ModelInComCoordinates(model, state)),
                  config->submap("MultiContactForceDistribution")
              );
    ji_1.start(std::chrono::system_clock::now());
    ji_1.update(std::chrono::system_clock::now());
    ji_1.stop(std::chrono::system_clock::now());
    
    // simulator test
    return 0;
    pbwbc::Simulator simulator(model,1e-3);
    model->setState(simulator.state());
    simulator.state()->y.setZero();
    simulator.state()->dy.setZero();
    Eigen::VectorXd tau(model->dq_size());
    for(int idx=0;idx<1000;idx++) {
        tau = ji_1.get_tau();
        simulator.setTau(tau);
        simulator.update(std::chrono::system_clock::now());
        std::cout << simulator.state()->y.head(3).transpose() << std::endl;
    }
}

