#include <pbwbc/Simulator.hpp>
#include <pbwbc/StandaloneConfig.hpp>
#include <pbwbc/ModelPinocchio.hpp>
#include <pbwbc/PBWBC.hpp>
#include <pbwbc/JointImpedanceController.hpp>
#include <fstream>
#include <iomanip>
#include <fenv.h>



int main(int, char **) {
    std::shared_ptr<pbwbc::Model> model = pbwbc::ModelPinocchio::create_from_urdf_file("robot.urdf");
    feenableexcept(FE_DIVBYZERO|FE_INVALID|FE_OVERFLOW);

    pbwbc::Simulator simulator(model,1e-3);
    model->setState(simulator.state());
    simulator.state()->y.setZero();
    simulator.state()->y(2) = 1.1;
    simulator.state()->y(6) = 1.;
    simulator.state()->y.tail(model->q_size()).setConstant(0.1);

    simulator.state()->dy.setZero();
    std::dynamic_pointer_cast<pbwbc::ModelPinocchio>(model)->blocking_update(std::chrono::system_clock::now());
   
    std::vector<pbwbc::Simulator::translation_t> offsets;
    pbwbc::Simulator::translation_t offset;
    offset <<  0.1, 0.1,0.; offsets.push_back(offset);
    offset <<  0.1,-0.1,0.; offsets.push_back(offset);
    offset << -0.1,-0.1,0.; offsets.push_back(offset);
    offset << -0.1, 0.1,0.; offsets.push_back(offset);
    for(auto const & link_name : {"right_sole_link", "left_sole_link"}) {
        for( auto const & my_offset : offsets ) {
            simulator.addPointContact(link_name,my_offset);
        }
    }

    simulator.enable_log("simulator.csv");

    // setup Actuator Controllers
    Eigen::VectorXd tau(model->dq_size());
    std::vector<std::shared_ptr<pbwbc::ActuatorController>> actuatorcontrollers;
    for(int idx=0;idx<model->dq_size();idx++) {
        actuatorcontrollers.push_back(std::shared_ptr<pbwbc::ActuatorController>(
                    new pbwbc::FakeActuator(tau(idx)) ));
    }


    // setup PBWBC
    std::shared_ptr<pbwbc::StandaloneConfigMap> config(new pbwbc::StandaloneConfigMap);
    config->add("nominal_stiffess",10.);
    config->add("torso_1_joint/stiffness",10.);
    pbwbc::PBWBC pbwbc(
            simulator.state(),
            std::shared_ptr<pbwbc::SensorState>(new pbwbc::SensorState),
            model,
            config,
            actuatorcontrollers);

    // config based creation not supported, manually adding controllers
    //
    // pick state either from simulation or from state estimation (not here)
    pbwbc.addController(std::shared_ptr<pbwbc::Controller>(
                new pbwbc::JointImpedanceController{simulator.state(),model,config}));
    
    // always reset before start, initialized state size
    simulator.reset();
    pbwbc.start(std::chrono::system_clock::now());
    //const int dq_offset = model->dy_size() - model->dq_size();
    for(int idx=0;idx<3000;idx++) {
        // stateestimation.update(std::chrono::system_clock::now()); // updates shared state
        pbwbc.update(std::chrono::system_clock::now());
        simulator.setTau(pbwbc.getTau());
        simulator.update(std::chrono::system_clock::now()); // advances time_step
        std::cout << "." << std::flush; // progress bar
    }
    pbwbc.stop(std::chrono::system_clock::now());
    std::cout << std::endl;
}
