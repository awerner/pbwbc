#include <pbwbc/CartesianImpedanceController.hpp>
#include <pbwbc/StandaloneConfig.hpp>
#include <pbwbc/Simulator.hpp>
#include <pbwbc/ModelPinocchio.hpp>

int main(int, char **) {
    std::shared_ptr<pbwbc::Model> model = pbwbc::ModelPinocchio::create_from_urdf_file("robot.urdf");
    std::shared_ptr<pbwbc::RobotState> state(
            new pbwbc::RobotState(model->y_size(),
                                  model->dy_size()));
    state->y.setZero();
    state->dy.setZero();
    model->setState(state);
    std::dynamic_pointer_cast<pbwbc::ModelPinocchio>(model)->blocking_update(std::chrono::system_clock::now());
    std::shared_ptr<pbwbc::StandaloneConfigMap> config(new pbwbc::StandaloneConfigMap);
    config->add("nominal_stiffess",10.);
    config->add("torso_1_joint/stiffness",10.);
    pbwbc::CartesianImpedanceController ji_1(state,model,config);
    ji_1.start(std::chrono::system_clock::now());
    ji_1.update(std::chrono::system_clock::now());
    ji_1.stop(std::chrono::system_clock::now());
    
    // simulator test 
    pbwbc::Simulator simulator(model,1e-3);
    model->setState(simulator.state());
    simulator.state()->y.setZero();
    simulator.state()->dy.setZero();
    Eigen::VectorXd tau(model->dq_size());
    for(int idx=0;idx<1000;idx++) {
        tau = ji_1.get_tau();
        simulator.setTau(tau);
        simulator.update(std::chrono::system_clock::now());
        std::cout << simulator.state()->y.head(3).transpose() << std::endl;
    }
}

