#include <pbwbc/TripleBuffer.hpp>
#include <thread>
#include <iostream>
#include <Eigen/Dense>

// This test has errors in valgrind/helgrind, but apparently these are false
// positives. See https://stackoverflow.com/questions/39037964/how-to-avoid-false-positives-with-helgrind

int main() {
    pbwbc::TripleBuffer<Eigen::Matrix<double,1,1>> buffer(Eigen::Matrix<double,1,1>::Zero());
    static const int max_iterations = 200;

    std::thread t1( [&]() {
                for(int idx=0;idx<max_iterations;idx++) {
                    auto & b = buffer.getWriteBuffer();
                    b(0) = idx;
                    buffer.commitWriteBuffer();
                    std::this_thread::sleep_for(std::chrono::milliseconds(10));
                }});
    std::thread t2( [&]() {
                for(int idx=0;idx<max_iterations;idx++) {
                    if(!buffer.ready())continue;
                    auto const & b = buffer.getReadBuffer();
                    std::cout << b(0) << std::endl;
                    buffer.freeReadBuffer();
                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                }});

    t1.join();
    t2.join();
    return 0;
}
