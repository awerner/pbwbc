#include <pbwbc/Simulator.hpp>
#include <pbwbc/ModelPinocchio.hpp>
#include <pbwbc/StandaloneConfig.hpp>
#include <fstream>
#include <iomanip>
#include <fenv.h>

int main(int, char **) {
    std::string model_filename("robot.urdf");
    if(getenv("PBWBC_MODEL")) {
        model_filename = std::string(getenv("PBWBC_MODEL"));
    }
    std::shared_ptr<pbwbc::Model> model = pbwbc::ModelPinocchio::create_from_urdf_file(model_filename);
    //feenableexcept(FE_DIVBYZERO|FE_INVALID|FE_OVERFLOW);

    pbwbc::Simulator simulator(model,1e-3,1e-6,1e-6,0);
    model->setState(simulator.state());
    simulator.state()->y.setZero();
    simulator.state()->y(2) = 1.1;
    // test case 1:
    // base joint only, contact point offset 0,0,-0.01 => works
    // .. add 
    Eigen::Quaterniond q(Eigen::AngleAxisd(0., Eigen::Vector3d::UnitZ())
                       * Eigen::AngleAxisd(1e-6, Eigen::Vector3d::UnitX())
                       * Eigen::AngleAxisd(0., Eigen::Vector3d::UnitY()));
    simulator.state()->y.segment(3,4) = q.coeffs();
    simulator.state()->y.tail(model->q_size()).setConstant(0.1);

    
    simulator.state()->dy.setZero();
    simulator.state()->dy(3) = 0.;
    simulator.state()->ddy.setZero();
    Eigen::VectorXd tau(model->dq_size());
    pbwbc::Simulator::wrench_t wrench;
    wrench.setZero();
    tau.setZero();
    simulator.setTau(tau);
    pbwbc::Simulator::translation_t offset;
    offset << 0.,0.,0.;
    //simulator.addPointContact("base_link",offset);
    //simulator.addPointContact("torso_2_link",pbwbc::Simulator::translation_t::Zero());
    offset << 0.1, 0.1, -0.2;
    simulator.addPointContact("base_link",offset);
    //offset << 0.1, -0.1, -0.2;
    //simulator.addPointContact("base_link",offset);
    //offset << -0.1, 0.1, -0.2;
    //simulator.addPointContact("base_link",offset);
    offset << -0.1, -0.1, -0.2;
    simulator.addPointContact("base_link",offset);
    
    //pbwbc::Simulator::translation_t o1; o1 << 0., 1.1, 0.;
    //simulator.addPointContact("base_link",o1);
    //pbwbc::Simulator::translation_t o2; o2 << 0., -1.1, 0.;
    //simulator.addPointContact("base_link",o2);
    
    //std::vector<pbwbc::Simulator::translation_t> offsets;
    //pbwbc::Simulator::translation_t offset;
    //offset <<  0.1, 0.1,0.; offsets.push_back(offset);
    //offset <<  0.1,-0.1,0.; offsets.push_back(offset);
    //offset << -0.1,-0.1,0.; offsets.push_back(offset);
    //offset << -0.1, 0.1,0.; offsets.push_back(offset);
    //for(auto const & link_name : {"right_sole_link", "left_sole_link"}) {
    //    for( auto const & my_offset : offsets ) {
    //        simulator.addPointContact(link_name,my_offset);
    //    }
    //}
    
    pbwbc::Simulator::translation_t translation_stiffness;
    translation_stiffness << 1e5, 1e5, 1e7;
    simulator.set_contact_stiffness(translation_stiffness);
    pbwbc::Simulator::translation_t translation_damping;
    translation_damping << 1e3, 1e3, 1e6;
    simulator.set_contact_damping(translation_damping);

    // two log files are created:
    // controller.csv (1ms step)
    // simulator.csv (variable step)
    std::ofstream logfile("controller.csv");
    logfile << "time,base_x,base_y,base_z,base_q0,base_q1";
    for(auto const & n : model->jointNames()) logfile << "," << n;
    logfile << std::endl;
    logfile.setf( std::ios::fixed, std:: ios::floatfield );
    const static Eigen::IOFormat CSVFormat(Eigen::FullPrecision, Eigen::DontAlignCols, ",", "\n");

    simulator.enable_log("simulator.csv");
    //const int dq_offset = model->dy_size() - model->dq_size();
    
    // always reset before start, initialized state size
    simulator.reset();
    simulator.set_state_filter_constant(1e3);
    simulator.set_damping(0.);
    for(int idx=0;idx<3000;idx++) {
        simulator.update(std::chrono::system_clock::now()); // advances time_step

        model->update(std::chrono::system_clock::now()); // compute model
        //if(idx==500) {
        //    tau = 1e2 * model->MassMatrix().diagonal().segment(dq_offset,model->dq_size());
        //} else {
        //    double goal = 0.1;
        //    if(idx> 1500)goal = 0.2;
        //    tau.setZero();
        //    const double gain = 1e2;
        //    tau.array() = gain * (goal -simulator.state()->y.segment(7,model->q_size()).array());
        //    tau.array() -= 
        //      sqrt(gain * model->MassMatrix().diagonal().segment(dq_offset,model->dq_size()).array()) 
        //        * 2. * 1. * simulator.state()->dy.segment(dq_offset,model->dq_size()).array();
        //    tau += model->gravity().segment(dq_offset,model->dq_size());
        //}

        // apply tau and wrench on base DOF
        simulator.setTau(tau);
        simulator.setBaseWrench(wrench);

        // controller log output
        logfile << simulator.time() << ",0.001," << simulator.state()->y.transpose().format(CSVFormat) << std::endl;
        std::cout << "." << std::flush; // progress bar
    }
    std::cout << std::endl;
    std::cout << "Step count: " << simulator.step_count() << std::endl;
    std::cout << "Tried Step count: " << simulator.tried_step_count() << std::endl;
}


