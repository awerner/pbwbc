#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <chrono>
#include <iostream>


int main (int argc, char **argv) {
    ros::init(argc, argv, "test_sub");
    ros::NodeHandle nh("test_sub");

    int received_messages;
    std::chrono::system_clock::time_point start;

    received_messages = 0;
   
    boost::function<void(const boost::shared_ptr<sensor_msgs::JointState const>&) > callback;
    callback = [&](const boost::shared_ptr<sensor_msgs::JointState const> & msg) -> void
    {
        if(received_messages==0) start = std::chrono::system_clock::now();
        received_messages++;
        if(received_messages==500) {
            auto end = std::chrono::system_clock::now();
            std::chrono::duration<double> elapsed_seconds = end-start;
            std::cout << "Got 500 messages in " << elapsed_seconds.count() << std::endl;
            received_messages = 0;
        }
    };

    ros::SubscribeOptions ops;
    ops.initByFullCallbackType("/my_topic", 1, callback);
    ops.transport_hints = ros::TransportHints();
    ops.transport_hints.udp();
    ros::Subscriber sub = nh.subscribe(ops);

    ops.initByFullCallbackType("/my_topic1", 1, callback);
    ops.transport_hints = ros::TransportHints();
    ops.transport_hints.udp();
    ros::Subscriber sub1 = nh.subscribe(ops);

    ros::spin();
    
    return 0;
}

