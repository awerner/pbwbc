#include <pbwbc/RealSenseStateEstimation.hpp>
#include <pbwbc/StandaloneConfig.hpp>
#include <fstream>
#include <iomanip>
#include <fenv.h>
#include <chrono>
#include <pbwbc/ModelPinocchio.hpp>


int main(int, char **) {
    std::shared_ptr<pbwbc::Model> model = pbwbc::ModelPinocchio::create_from_urdf_file("robot.urdf");
    feenableexcept(FE_DIVBYZERO|FE_INVALID|FE_OVERFLOW);

    std::shared_ptr<pbwbc::StandaloneConfigMap> config(new pbwbc::StandaloneConfigMap);
    config->add("camera_0/serial_no","905312111396");
    config->add("camera_0/name","front");
    config->add("camera_0/B_T_C_translation","0., 0., 0.");
    config->add("camera_0/B_T_C_orientation","-0.5., -0.5, 0.5, 0.5");
    config->add("camera_1/serial_no","905312111042");
    config->add("camera_1/name","rear");
    config->add("camera_1/B_T_C_translation","0., 0., 0.");
    config->add("camera_1/B_T_C_orientation","-0.5., 0.5, -0.5, 0.5");
    std::shared_ptr<pbwbc::SensorState> sensor_state(new pbwbc::SensorState);
    pbwbc::RealSenseStateEstimation se(config,model,sensor_state);

    se.enable_log("se.csv");

    se.start(std::chrono::system_clock::now());
    for(int idx=0;idx<1000;idx++) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        se.update(std::chrono::system_clock::now());
        //std::cout << ".";
    }
    se.stop(std::chrono::system_clock::now());
}
