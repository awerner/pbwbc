#include <pbwbc/ROSTools.hpp>
#include <sensor_msgs/JointState.h>


int main (int argc, char **argv) {
    ros::init(argc, argv, "test_pub");
    ros::NodeHandle nh("test");

    pbwbc::RealTimePublisherBackEnd pb; // start the background publisher thread
    typedef sensor_msgs::JointState msg_t;
    pbwbc::RealTimePublisher<msg_t> pub = pb.advertise<msg_t>(nh,  "/my_topic",1000,10);
    pbwbc::RealTimePublisher<msg_t> pub1 = pb.advertise<msg_t>(nh,"/my_topic1",1000,10);
    bool keep_running = true;
    std::thread pub_thread = std::thread([&](){
        // wait for registration to propagate
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        msg_t cartesian_impedance_msg;
        cartesian_impedance_msg.name.push_back("joint");
        msg_t cartesian_impedance_msg1;
        cartesian_impedance_msg1.name.push_back("joint1");
        auto start_time = std::chrono::system_clock::now();
        for(int idx=0;idx<30000;idx++) {
            pub.publish(cartesian_impedance_msg);
            pub1.publish(cartesian_impedance_msg1);
            pb.trigger_send();
            //std::this_thread::sleep_for(std::chrono::milliseconds(1));
            std::this_thread::sleep_until(start_time
                    + std::chrono::milliseconds(idx));
            if(!keep_running)return;
        }
        ros::shutdown();
            });
    std::thread write_thread = std::thread([&](){
            std::this_thread::sleep_for(std::chrono::seconds(6));
            std::cout << "Trigger writing" << std::endl;
            pb.trigger_write_to_file("myfile.rosbag");
            });
    ros::spin();
    keep_running = false;
    pub_thread.join();
    write_thread.join();
    return 0;
}

