#include <pbwbc/RealSenseStateEstimationROS.hpp>
#include <pbwbc/StandaloneConfig.hpp>
#include <fstream>
#include <iomanip>
#include <fenv.h>
#include <chrono>
#include <pbwbc/ModelPinocchio.hpp>


int main(int argc, char **argv) {
    std::shared_ptr<pbwbc::Model> model = pbwbc::ModelPinocchio::create_from_urdf_file("robot.urdf");
    feenableexcept(FE_DIVBYZERO|FE_INVALID|FE_OVERFLOW);

    std::shared_ptr<pbwbc::StandaloneConfigMap> config(new pbwbc::StandaloneConfigMap);
    config->add("camera_0/serial_no","905312111396");
    config->add("camera_0/name","front");
    config->add("camera_0/mapping_enabled",1);
    config->add("camera_0/B_T_C_translation", "0.04886514, 0.00155419, -0.15270918");
    config->add("camera_0/B_T_C_orientation","0.166024,  -0.15702, -0.712806,   0.66309");
    config->add("camera_1/serial_no","905312111042");
    config->add("camera_1/name","rear");
    config->add("camera_1/enabled",0);
    config->add("camera_1/mapping_enabled",1);
    config->add("camera_1/B_T_C_translation","-0.17191768,  0.01183944, -0.16414398");
    config->add("camera_1/B_T_C_orientation","0.189763, 0.152956, 0.700659, 0.670575");
    config->add("position_translation_gain",10.);
    config->add("velocity_translation_gain",10.);
    config->add("orientation_gain",10.);
    config->add("publisher_frame",0);
    config->add("start_state_translation","0., 0., 1.05");

    ros::init(argc, argv, "se");
    ros::NodeHandle nh;

    std::shared_ptr<pbwbc::SensorState> sensor_state(new pbwbc::SensorState);
    pbwbc::RealSenseStateEstimation se(config,model,sensor_state);

    //se.enable_log("se.csv");

    se.start(std::chrono::system_clock::now());
    bool keep_running = true;
    auto t1 = std::thread([&](){
            std::cout << "Hit enter to stop" << std::endl;
            std::cin.ignore();
            std::cout << "Terminating" << std::endl;
            keep_running = false;
            });
    auto t2 = std::thread([&](){
            ros::Rate loop_rate(1000);
            while(keep_running && ros::ok()) {
                ros::spinOnce();
                loop_rate.sleep();
            }
            keep_running = false;
            });

    ros::Rate rate(1e3);
    while(keep_running) {
        rate.sleep();
        try {
            se.update(std::chrono::system_clock::now());
        } catch ( std::exception const & e ) {
            PBWBC_DEBUG(e.what());
            keep_running = false;
            std::cout << "Hit enter to clean up" << std::endl;
        }
    }
    se.stop(std::chrono::system_clock::now());
    t1.join();
    t2.join();
}
