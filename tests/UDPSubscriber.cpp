#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <chrono>

std::chrono::time_point<std::chrono::system_clock> last_update_;
bool initialized = false;
std::size_t counter = 0;
double delta_sum = 0;

void chatterCallback(const geometry_msgs::PoseStamped::ConstPtr& msg) {
  if(initialized) {
      const double delta_t = std::chrono::duration<double>(
            std::chrono::high_resolution_clock::now() - last_update_).count();
      delta_sum += delta_t;
  }
  last_update_ = std::chrono::high_resolution_clock::now();
  counter++;
  initialized = true;
  if(counter%1000==0) {
    std::cout << delta_sum/counter << std::endl;
  }
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "UDPSubscriber");
    ros::NodeHandle nh;
    ros::TransportHints hints;
    hints.udp();
    ros::Subscriber sub = nh.subscribe("/state/pose", 1, chatterCallback, hints);
    ros::spin();
    return 0;
}
