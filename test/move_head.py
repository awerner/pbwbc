#!/usr/bin/env python

#Use this script after launching the pbwbc controller with head contact, e.g.
#roslaunch pbwbc pbwbc_gazebo.launch config:=pbwbc_with_head_contact

import rospy
rospy.init_node("test_client")

from pbwbc import ActionInterface, rpy2quat

talos = ActionInterface('talos')

#Make sure the robot is in its initial pose
talos.movePosture([0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],0.,1.)
talos.moveEE("Head", [0., 0., 0., 0., 0., 0.], 0., 1.)

#Move head forward and down
talos.moveEE("Head", [0.1,0.,-0.3, 0.,2.0,0.], 2., 2.)

#Move head backward and up
talos.moveEE("Head", [-0.1, 0., 0., 0., -1., 0.], 4., 2.)

#Bring head back to initial pose
talos.moveEE("Head", [0., 0., 0., 0., 0., 0.], 6., 1.)

#Move head to the right
talos.moveEE("Head", [0., -0.1, -0.05, 1., 0., -0.2], 7., 2.)

#Bring the robot back to initial posture
talos.moveEE("Head", [0., 0., 0., 0., 0., 0.], 11., 1.)
talos.movePosture([0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],11.,1.)

#Move head to the left
talos.moveEE("Head", [0., 0.1, -0.05, -1., 0., 0.2], 12., 2.)

#Bring the robot back to initial posture
talos.moveEE("Head", [0., 0., 0., 0., 0., 0.], 16., 1.)
talos.movePosture([0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],16.,1.)
