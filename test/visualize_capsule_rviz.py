import rospy
import numpy
import eigenpy
from interactive_markers.interactive_marker_server import *
#from visualization_msgs.msg import *
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
import time


lifetime = 45.
marker_id = 0

def add_sphere(origin, radius, parent_frame, name):
    global marker_id
    global lifetime

    box_marker = Marker()
    box_marker.ns = name
    box_marker.id = marker_id
    box_marker.header.frame_id = parent_frame
    box_marker.action = Marker.ADD
    box_marker.type = Marker.SPHERE
    box_marker.pose.position.x = origin[0]
    box_marker.pose.position.y = origin[1]
    box_marker.pose.position.z = origin[2]
    box_marker.pose.orientation.w = 1.
    box_marker.scale.x = radius
    box_marker.scale.y = radius
    box_marker.scale.z = radius
    box_marker.color.r = 0.0
    box_marker.color.g = 1.0
    box_marker.color.b = 0.5
    box_marker.color.a = 1.0
    box_marker.lifetime.secs = lifetime
    marker_id += 1
    return box_marker

def add_cylinder(origin, radius, length, quaternion, parent_frame, name):
    global marker_id
    global lifetime
    box_marker = Marker()
    box_marker.ns = name
    box_marker.header.frame_id = parent_frame
    box_marker.action = Marker.ADD
    box_marker.type = Marker.CYLINDER
    box_marker.pose.position.x = origin[0]
    box_marker.pose.position.y = origin[1]
    box_marker.pose.position.z = origin[2]
    box_marker.pose.orientation.x = quaternion.x
    box_marker.pose.orientation.y = quaternion.y
    box_marker.pose.orientation.z = quaternion.z
    box_marker.pose.orientation.w = quaternion.w
    box_marker.scale.x = radius
    box_marker.scale.y = radius
    box_marker.scale.z = length
    box_marker.color.r = 0.0
    box_marker.color.g = 1.0
    box_marker.color.b = 0.5
    box_marker.color.a = 1.0
    box_marker.lifetime.secs = lifetime
    marker_id += 1
    return box_marker

def add_capsule(a, b, radius, parent_frame, name):
    direction = numpy.subtract(b,a) / numpy.linalg.norm(numpy.subtract(b, a))
    length = numpy.linalg.norm(numpy.subtract(b, a))
    origin = numpy.add(a, b)/2.
    q = eigenpy.Quaternion()
    q.setFromTwoVectors(numpy.array(direction),numpy.array([0.,0.,1.]))

    sphere_1 = add_sphere(a, radius, parent_frame, name+'sphere_1')
    sphere_2 = add_sphere(b, radius, parent_frame, name+'sphere_2')
    cylinder = add_cylinder(origin, radius, length, q.inverse(), parent_frame, name+'cylinder')

    msg = MarkerArray()
    msg.markers = [sphere_1,sphere_2,cylinder]
    mypub.publish(msg)

def show_capsules_from_rosparam():
    #get capsules from params
    capsules = rospy.get_param('/pbwbc_controller/Controllers/SelfCollisionAvoidance/additional_objects/') 
    for capsule_name, data in capsules.items():
        print 'adding capsule '+capsule_name
        #a = raw_input("Press Enter to continue...")
        #if a == 'q':
        #    break
        add_capsule(data['a'], data['b'], data['radius'], data['parent_frame'], capsule_name)


if __name__=="__main__":
    rospy.init_node("simple_marker")
    mypub = rospy.Publisher("/visualization_marker_array", MarkerArray, queue_size=1)
    time.sleep(2.)

    show_capsules_from_rosparam()

    #a = [0.,0.,0.02]
    #b = [0.,0.,0.1]
    #radius = 0.08
    #parent_frame="head_1_link"
    #add_capsule(a, b, radius, parent_frame, 'manual_capsule1')

    rospy.spin()



