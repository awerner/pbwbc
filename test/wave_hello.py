#!/usr/bin/env python
import rospy
rospy.init_node("test_client")

from pbwbc import ActionInterface, rpy2quat

talos = ActionInterface('talos')

talos.moveCOM([0.,0.,0.90], 0., 3., True)

talos.moveCOM([0.,0.14,0.,0.,0.,0.], 3., 1.)

talos.switchContact("RightFoot", "off", 4.)
talos.moveEE("RightFoot", [0.,0.,0.2, 0.,0.,0.], 4., 1.)

talos.moveEE("RightFoot", [0.,0.,0., 0.,0.,0.], 5., 1.)
talos.switchContact("RightFoot", "on", 6)
talos.moveCOM([0.,0.,0.90], 6.1, 1., True)

talos.moveEE("RightHand", [0., -0.1, 1.0, 1.5, 1.3, 0.], 6., 3.)
talos.moveEE("RightHand", [0.,0.,0.,0.,0.,0.], 9., 4.)

talos.moveJoint("head_1_joint", [0.5], 6.,1., True)
talos.moveJoint("head_2_joint", [0.5], 6.,1., True)

talos.moveJoints(["head_1_joint", "head_2_joint"], [0.5, -0.5], 11., 1.)
talos.moveJoints(["head_1_joint", "head_2_joint"], [0., 0.], 11., 1.)
#talos.movePosture([0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],12.,1.)
#talos.movePosture([0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],13.,1.,True)
