#!/usr/bin/env python

import rospy
rospy.init_node("test_client")

from pbwbc.pbwbc import Talos
from pbwbc.gazebo import Gazebo

talos = Talos('talos',wait=True)
import time
time.sleep(0.4)
print("controller_active: {}".format(talos.active()))
gazebo = Gazebo()

print talos.active()
if not talos.active():
    raise Exception("Controller not active at start")

gazebo.apply_body_wrench("base_link",[0.,70.,0.],1.)

import time
time.sleep(10.)
# need to wait for simTime

if not talos.active():
    raise Exception("The robot has fallen")


